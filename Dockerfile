FROM openjdk:8-jre
VOLUME /tmp
ADD target/secudium.jar secudium.jar
EXPOSE 8085
RUN sh -c 'touch /secudium.jar'
ENV JAVA_OPTS=""
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Dspring.profiles.active=local -jar /secudium.jar"]