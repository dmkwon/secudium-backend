/**
 * COPYRIGHT(c) SK Infosec co.,Ltd 2019
 * This software is the proprietary information of SK Infosec.
 */
package com.secudium.aop;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.secudium.api.systemoper.program.service.ProgramService;
import com.secudium.error.GlobalExceptionHandler;
import com.secudium.mybatis.utils.Ognl;
import com.secudium.security.UserPrincipal;

/**
 * <p>
 * 업무명 : Authorized Request Aspect
 * <p>
 * 업무구분 : Aspect
 * <p>
 * 설명 : Controller에 도착하기전에 해당 요청이 인증된 요청인지 확인하는 Aspect
 *
 * date: 2019-04-15
 * 
 * @author Intellicode
 * @version 0.1
 */
@Component
@Aspect		// 여기를 막으면 동작 자체가 되지 않음.
@Order(2)
public class AuthorizedRequestAspect extends GlobalExceptionHandler {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ProgramService programService;
	
	@Autowired 
	private Environment env;
	
	public AuthorizedRequestAspect() {		
	}

	/**
	 *
	 * @param point
	 * @throws Exception
	 */
	@Before("execution(public * com.secudium..*Controller.*(..))")
	public void AuthorizedRequestCheck(JoinPoint point) throws Exception {

		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String uri = Ognl.isBlank(request.getRequestURI()) ? "" : request.getRequestURI().trim();
		log.debug("AuthorizedRequestCheck uri: {}", uri);
		String contextPath = env.getProperty("server.servlet.context-path");

		List<String> excludeUri = new ArrayList<>();
		excludeUri.add((contextPath.equals("/") ? "" : contextPath)+ "/api/auth/signin");
		excludeUri.add((contextPath.equals("/") ? "" : contextPath)+ "/api/auth/me");
		excludeUri.add((contextPath.equals("/") ? "" : contextPath)+ "/api/auth/menus");
		excludeUri.add((contextPath.equals("/") ? "" : contextPath)+ "/error");
		excludeUri.add((contextPath.equals("/") ? "" : contextPath)+ "/api/change/language");
		
		boolean isNeedCheckUrl = true;
		for (String exuri : excludeUri) {
			if (Pattern.matches(exuri, uri)) {
				isNeedCheckUrl = false;
				break;
			}
		}

		if (isNeedCheckUrl) {
			UserPrincipal userPrincipal = getSignInUserInfo();
				String usrId = userPrincipal.getUsername();

				if (Ognl.isNotEmpty(usrId)) {
					// session에 사용자 role 적합하지 않으면 예외 발생
				
					if(!contextPath.equals("/")) {
						uri = uri.replace(contextPath, "");
					}
				
					if (!isRoleFunction(uri, request.getMethod(), userPrincipal)) {
						log.debug("권한 없음!: {}", uri);
						throw new AccessDeniedException(uri);
					}
				} else {
					log.info("-- 로그인 필요! --");
					String query = request.getQueryString();
					String returnUri = uri + (Ognl.isNotBlank(query) && !"/".equals(query) ? "?" + query : "");

					log.debug("return url: {}", returnUri);

					try {
						returnUri = URLEncoder.encode(returnUri, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new UnsupportedEncodingException();
					}

					request.setAttribute("return", returnUri);
				throw new Exception();
			}
		}
	}

	/**
	 * 역할별로 URL, METHOD에 매핑된 기능갯수
	 * 
	 * @param uri
	 * @param method
	 * @return
	 */
	private boolean isRoleFunction(String uri, String method, UserPrincipal userPrincipal) {
		int count = 0;
		if ("/".equals(uri)) {
			return true;
		}

		// 기능 개수 가져오는 구문
		List<String> roles = getUserRoles(userPrincipal);
		count = programService.getProgramCountByRoleId(roles, uri, method.toUpperCase());
		return count > 0;
	}

	/**
	 * 현재 로그인된 사용자 정보를 받아옴
	 * 
	 * @return
	 */
	private UserPrincipal getSignInUserInfo() {
			UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			log.debug("login id: {}", userPrincipal.getUsername());
			return userPrincipal;
	}

	/**
	 * 해당 사용자의 roles를 String[]으로 받아옴
	 * 
	 * @param userPrincipal
	 * @return
	 */
	private List<String> getUserRoles(UserPrincipal userPrincipal) {
		List<String> listAuthorities = new ArrayList<>();
		for (GrantedAuthority ga : userPrincipal.getAuthorities()) {
			listAuthorities.add(ga.getAuthority());
		}
		return listAuthorities;
	}

}
