package com.secudium.aop;

import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


@Component // 1
@Aspect // 2
public class RequestLoggingAspect {

	private static final Logger logger = LoggerFactory.getLogger(RequestLoggingAspect.class);

	private String paramMapToString(Map<String, String[]> paramMap) {
		return paramMap.entrySet().stream()
				.map(entry -> String.format("%s -> (%s)",
						entry.getKey(), String.join(",", entry.getValue())))
				.collect(Collectors.joining(", "));
	}

	@Pointcut("within(@org.springframework.web.bind.annotation.RestController *)") // 3
	public void onRequest() {
		throw new UnsupportedOperationException();
	}

	@Around("com.secudium.aop.RequestLoggingAspect.onRequest()") // 4
	public Object doLogging(ProceedingJoinPoint pjp) throws Throwable {
		HttpServletRequest request = // 5
				((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		
		Map<String, String[]> paramMap = request.getParameterMap();
		String params = "";
		if (!paramMap.isEmpty()) {
			params = " [" + paramMapToString(paramMap) + "]";
		}

		long start = System.currentTimeMillis();
		try {
			return pjp.proceed(pjp.getArgs()); // 6
		} finally {
			long end = System.currentTimeMillis();
			logger.debug("[AUDIT]Request: {} {}{} < {} ({}ms)", request.getMethod(), request.getRequestURI(),
					params, request.getRemoteHost(), end - start);
		}
	}
}