package com.secudium.api.auth.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import com.secudium.api.systemoper.menu.entity.MenuInfo;
import com.secudium.api.systemoper.menu.service.MenuService;
import com.secudium.api.systemoper.user.service.UserService;
import com.secudium.common.dto.LoginRequest;
import com.secudium.common.dto.OkResponse;
import com.secudium.common.dto.UserSummary;
import com.secudium.error.ErrorCode;
import com.secudium.exception.BusinessException;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;
import com.secudium.util.ApplicationUtils;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

	private final AuthenticationManager authenticationManager;
	private final MenuService menuService;
	private final UserService userService;
	private final LocaleResolver localeResolver;

	@PostMapping("/signin")
	public ResponseEntity<OkResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest,
			HttpServletRequest request) {

		Authentication authentication = null;
		// 로그인 잠김상태 확인
		if ( userService.isAccLocked(loginRequest.getUsername()) ) {
			try {
				authentication = authenticationManager.authenticate(
						new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

			} catch(Exception e) {
				// 관리자계정은 계정잠김 기능 미적용
				if(userService.getUsersByUsername(loginRequest.getUsername()).getAccTypeCode().equals("ATCD02")) {
					throw new BusinessException(ErrorCode.INVALID_LOGIN_VALUE);
				} else {
					if(userService.getLoginFailCount(loginRequest.getUsername()) >= 5) {
						// 로그인 5회이상 실패시 계정잠김 메세지 출력
						throw new BusinessException(ErrorCode.ACCOUNT_LOCK);
					} else {
						// 로그인 실패 메세지 출력
						userService.loginFail(loginRequest.getUsername());
						throw new BusinessException(ErrorCode.INVALID_LOGIN_VALUE);
					}
				}
			}

			SecurityContextHolder.getContext().setAuthentication(authentication);

			HttpSession session = request.getSession();
			UserPrincipal p = (UserPrincipal) authentication.getPrincipal();
			p.setAcsTk(session.getId());
			// IP 추가
			String ip = ApplicationUtils.getClientIp(request);
			if ("0:0:0:0:0:0:0:1".equals(ip) || "127.0.0.1".equals(ip)) {
				try {
					ip = InetAddress.getLocalHost().getHostAddress();
				} catch (UnknownHostException e) {
					ip = "Unknown IP";
				}
			}
			p.setAcsIp(ip);
			// HttpServletRequest 추가
			p.setRequest(request);
			// 로그인 성공시 로그인 실패횟수 0으로 초기화
			userService.updateLoginCntReset(loginRequest.getUsername());
			return ResponseEntity.ok(OkResponse.of());
		} else {
			if(userService.isDuplicate(loginRequest.getUsername())) {
				throw new BusinessException(ErrorCode.ACCOUNT_LOCK);
			} else {
				// 로그인 실패 메세지 출력
				throw new BusinessException(ErrorCode.NO_ACCOUNT_INFO);
			}
		}
	}

	@GetMapping("/me")
	public ResponseEntity<UserSummary> me(
			@CurrentUser UserPrincipal currentUser, 
			Locale locale) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = format.format(new Date());

		UserSummary summary = new UserSummary(
				currentUser.getUsername(), 
				currentUser.getUsrNm(),
				currentUser.getUsrDeptNm(), 
				currentUser.getUsrEmail(), 
				date, 
				currentUser.getAuthorities(),
				locale.getLanguage());
		return ResponseEntity.ok(summary);
	}

	@GetMapping("/menus")
	public ResponseEntity<List<MenuInfo>> getMenus(
			@CurrentUser UserPrincipal currentUser, 
			Locale locale) {
		return ResponseEntity.ok(menuService.getMenuOfUser(currentUser, locale.getLanguage()));
	}

}