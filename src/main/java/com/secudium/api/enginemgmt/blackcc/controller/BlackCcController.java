package com.secudium.api.enginemgmt.blackcc.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.secudium.api.enginemgmt.blackcc.dto.BlackCcIpInsertDto;
import com.secudium.api.enginemgmt.blackcc.dto.BlackCcIpUpdateDto;
import com.secudium.api.enginemgmt.blackcc.dto.BlackCcUrlInsertDto;
import com.secudium.api.enginemgmt.blackcc.dto.BlackCcUrlUpdateDto;
import com.secudium.api.enginemgmt.blackcc.entity.BlackCcIp;
import com.secudium.api.enginemgmt.blackcc.entity.BlackCcUrl;
import com.secudium.api.enginemgmt.blackcc.service.BlackCcService;
import com.secudium.common.dto.OkResponse;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/enginemgmt")
public class BlackCcController {

	private final BlackCcService blackCcService;

	/*
	 * black & cc ip
	 */
	@GetMapping("/blackcc/ips")
	public ResponseEntity<PagedResponse<BlackCcIp>> getBlackCcIps(
			HttpServletRequest request 
			,Pageable pageable
			,@CurrentUser UserPrincipal currentUser, 
			Locale locale) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		return ResponseEntity.ok(blackCcService.getBlackCcIps(pageable, searchMap, locale.getLanguage()));
	}

	@GetMapping("/blackcc/ips/isDuplicated")
	public List<BlackCcIp> getBlackCcIpsDupl( @RequestParam long stIp, @RequestParam long edIp ) {
		return blackCcService.getBlackCcIpsDupl( stIp , edIp );
	}

	@GetMapping("/blackcc/ips/upDuplicated")
	public List<BlackCcIp> getBlackCcIpsDuplUpdate( HttpServletRequest req ) {

		Map <String, Long> map = new HashMap<String, Long>();
		map.put( "stIp", Long.parseLong( req.getParameter("stIp") ) );  
		map.put( "edIp", Long.parseLong(req.getParameter("edIp") ) );
		map.put( "ipListSeq", Long.parseLong(req.getParameter("ipListSeq") ));

		return blackCcService.getBlackCcIpsDuplUpdate( map );
	}

	@GetMapping("/blackcc/ip/isDuplicated")
	public BlackCcIp getBlackCcIpDupl( @RequestParam long stIp  ) {
		return blackCcService.getBlackCcIpDupl( stIp  );
	}

	@GetMapping("/blackcc/ip/upDuplicated")
	public BlackCcIp getBlackCcIpDuplUpdate( HttpServletRequest req ) {
		Map <String, Long> map = new HashMap<String, Long>();
		map.put( "stIp", Long.parseLong( req.getParameter("stIp") ) );  
		map.put( "ipListSeq", Long.parseLong(req.getParameter("ipListSeq") ));
		return blackCcService.getBlackCcIpDuplUpdate( map );
	}

	@PostMapping("/blackcc/ip")
	public ResponseEntity<BlackCcIp> getBlackCcIpsById(
			@RequestBody @Valid BlackCcIp dto) {
		return ResponseEntity.ok(blackCcService.getBlackCcIpsById(dto.getIpListSeq()));
	}

	@PostMapping("/blackcc/ips")
	public ResponseEntity<OkResponse> createBlackCcIp(
			@RequestBody @Valid final BlackCcIpInsertDto dto
			, @CurrentUser UserPrincipal currentUser) {
		blackCcService.createBlackCcIp(dto, currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/blackcc/ips").build().toUri();

		return ResponseEntity.created(location).body(OkResponse.of());
	}

	@PutMapping("/blackcc/ip")
	public ResponseEntity<OkResponse> updateBlackCcIp(
			@RequestBody @Valid final BlackCcIpUpdateDto dto
			, @CurrentUser UserPrincipal currentUser) {
		blackCcService.updateBlackCcIp(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/blackcc/ips")
	public ResponseEntity<OkResponse> updateBlackCcIps(
			@RequestBody @Valid final List<BlackCcIpUpdateDto> dto
			, @CurrentUser UserPrincipal currentUser) {
		blackCcService.updateBlackCcIps(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	// black & cc Url 
	@GetMapping("/blackcc/urls")
	public ResponseEntity<PagedResponse<BlackCcUrl>> getBlackCcUrls(
			HttpServletRequest request, 
			Pageable pageable, 
			Locale locale) {
		SearchMap searchMap = SearchMap.buildFrom(request);		
		return ResponseEntity.ok(blackCcService.getBlackCcUrls(pageable, searchMap, locale.getLanguage()));		
	}

	@GetMapping("/blackcc/url")
	public ResponseEntity<BlackCcUrl> selectblackccUrl(
			@RequestParam long seq) {
		return ResponseEntity.ok(blackCcService.getBlackCcUrlsById(seq));
	}

	@PostMapping("/blackcc/urls")
	public ResponseEntity<OkResponse> createBlackCcUrl(
			@RequestBody @Valid final BlackCcUrlInsertDto dto
			, @CurrentUser UserPrincipal currentUser) {
		blackCcService.createBlackCcUrl(dto, currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/blackcc/urls").build().toUri();
		return ResponseEntity.created(location).body(OkResponse.of());
	}

	@PutMapping("/blackcc/url")
	public ResponseEntity<OkResponse> updateblackccUrl(
			@RequestBody @Valid final BlackCcUrlUpdateDto dto
			, @CurrentUser UserPrincipal currentUser) {
		blackCcService.updateBlackCcUrl(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/blackcc/urls")
	public ResponseEntity<OkResponse> updateBlackCcUrls(
			@RequestBody @Valid final List<BlackCcUrlUpdateDto> dto
			, @CurrentUser UserPrincipal currentUser) {
		blackCcService.updateBlackCcUrls(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/blackcc/deployAll")
	public ResponseEntity<OkResponse> deployAll(
			@CurrentUser UserPrincipal currentUser, 
			Locale locale) {
		blackCcService.deployAll(currentUser, locale.getLanguage());
		return ResponseEntity.ok(OkResponse.of());
	}
}