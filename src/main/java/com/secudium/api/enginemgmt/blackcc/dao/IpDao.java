package com.secudium.api.enginemgmt.blackcc.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.enginemgmt.blackcc.entity.BlackCcIp;
import com.secudium.common.entity.SearchMap;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class IpDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<BlackCcIp> selectBlackCcIps(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search); 		
		params.put("lang", locale);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<BlackCcIp> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public List<BlackCcIp> selectBlackCcIpsByDstnct(String dstnct) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), dstnct);
	}

	public List<BlackCcIp> selectBlackCcIpsDupl(  Map<String, Long> map ) {
		List<BlackCcIp> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), map);
		return list;
	}

	public List<BlackCcIp> selectBlackCcIpsDuplUpdate(  Map<String, Long> map ) {
		List<BlackCcIp> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), map);
		return list;
	}

	public BlackCcIp selectBlackCcIpDupl( Long stIp  ) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), stIp);
	}
	
	public BlackCcIp selectBlackCcIpDuplUpdate(  Map<String, Long> map ) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	public BlackCcIp selectIpById(Long id) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), id);
	}

	public BlackCcIp selectBlackCcIpsById(Long id) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), id);
	}

	public int insertBlackCcIp(BlackCcIp blackCcIp) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), blackCcIp);
	}

	public int updateBlackCcIp(BlackCcIp blackCcIp) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), blackCcIp);
	}

	public int updateBlackCcIps(Map<String, Object> map) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

}
