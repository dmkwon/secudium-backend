package com.secudium.api.enginemgmt.blackcc.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.enginemgmt.blackcc.entity.BlackCcUrl;
import com.secudium.common.entity.SearchMap;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class UrlDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<BlackCcUrl> selectBlackCcUrls(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search); 
		params.put("lang", locale);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<BlackCcUrl> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public List<BlackCcUrl> selectBlackCcUrlsByDstnct(String dstnct, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("dstnct", dstnct); 
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public BlackCcUrl selectUrlById(Long id) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), id);
	}

	public int insertBlackCcUrl(BlackCcUrl blackCcUrl) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), blackCcUrl);
	}

	public int updateBlackCcUrl(BlackCcUrl blackCcUrl) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), blackCcUrl);
	}

	public int updateBlackCcUrls(Map<String, Object> map) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

}
