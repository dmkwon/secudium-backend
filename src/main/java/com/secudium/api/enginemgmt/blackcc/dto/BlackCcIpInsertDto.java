package com.secudium.api.enginemgmt.blackcc.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.enginemgmt.blackcc.entity.BlackCcIp;
import com.secudium.security.UserPrincipal;

import lombok.Getter;

@Getter
@JsonInclude(Include.NON_NULL)
public class BlackCcIpInsertDto {

	private Long stIp;
	private Long edIp;
	
	private String country ;
	private String detectDate;
	private String expDate;
	private String attkType;
	private String ipDesc;
	private String useYn;
	private String ipDstnct;
	private Long companyId;
	private Long deviceIp;
	
	public BlackCcIp toBlackCcIpEntity(UserPrincipal currentUser) {
		return BlackCcIp.builder()
				.ipDstnct(ipDstnct)
				.stIp(stIp)
				.edIp(edIp)
				.country(country)
				.detectDate(detectDate)
				.expDate(expDate)
				.attkType(attkType)
				.ipDesc(ipDesc)
				.companyId(companyId)
				.deviceIp(deviceIp)
				.useYn(useYn)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}

}