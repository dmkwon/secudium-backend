package com.secudium.api.enginemgmt.blackcc.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.enginemgmt.blackcc.entity.BlackCcIp;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BlackCcIpUpdateDto {

	private Long ipListSeq;

	private String ipDstnct;
	private Long stIp;
	private Long edIp;
	private String country;
	private String detectDate;
	private String expDate;
	private String attkType;
	private String ipDesc;
	private String useYn;
	private Long companyId;
	private Long deviceIp;
	
	public BlackCcIp toBlackCcIpEntity(UserPrincipal currentUser) {
		return BlackCcIp.builder()
				.ipListSeq(ipListSeq)
				.ipDstnct(ipDstnct)
				.stIp(stIp)
				.edIp(edIp)
				.country(country)
				.detectDate(detectDate)
				.expDate(expDate)
				.attkType(attkType)
				.ipDesc(ipDesc)
				.companyId(companyId)
				.deviceIp(deviceIp)
				.useYn(useYn)
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}
}