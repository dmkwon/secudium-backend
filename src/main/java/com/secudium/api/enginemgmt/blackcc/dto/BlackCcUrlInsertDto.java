package com.secudium.api.enginemgmt.blackcc.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.enginemgmt.blackcc.entity.BlackCcUrl;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BlackCcUrlInsertDto {
	
	private String url;
	private String urlDstnct;
	private String detectDate;
	private String expDate;
	private String attkType;
	private String urlDesc;
	private String useYn;

	
	public BlackCcUrl toBlackCcUrlEntity(UserPrincipal currentUser) {
		return BlackCcUrl.builder()
				.urlDstnct(urlDstnct)
				.url(url)
				.detectDate(detectDate)
				.expDate(expDate)
				.attkType(attkType)
				.urlDesc(urlDesc)
				.useYn(useYn)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}