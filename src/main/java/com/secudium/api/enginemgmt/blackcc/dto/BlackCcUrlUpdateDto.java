package com.secudium.api.enginemgmt.blackcc.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.enginemgmt.blackcc.entity.BlackCcUrl;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BlackCcUrlUpdateDto {

	private Long urlListSeq;
	private String urlDstnct;
	private String url;
	private String detectDate;
	private String expDate;
	private String attkType;
	private String useYn;
	private String urlDesc;

	public BlackCcUrl toBlackCcUrlEntity(UserPrincipal currentUser) {
		return BlackCcUrl.builder()
				.urlListSeq(urlListSeq)
				.urlDstnct(urlDstnct)
				.url(url)
				.detectDate(detectDate)
				.expDate(expDate)
				.attkType(attkType)
				.urlDesc(urlDesc)
				.useYn(useYn)
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}

}