package com.secudium.api.enginemgmt.blackcc.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class BlackCcIp extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -5676512744350590500L;

	private Long ipListSeq;
	private String ipDstnct;

	private Long stIp;
	private Long edIp;
	private String country;
	private String detectDate;
	private String expDate;
	private String attkType;
	private String attkTypeString;
	private String ipDesc;
	private String useYn;
	
	private String checkedSeq;
	
	private Long companyId;
	private Long deviceIp;

}