package com.secudium.api.enginemgmt.blackcc.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class BlackCcUrl extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -2639078451675049418L;

	private Long urlListSeq;
	private String urlDstnct;
	private String url;
	private String detectDate;
	private String expDate;
	private String attkType;
	private String attkTypeString;
	private String urlDesc;
	private String useYn;		
}