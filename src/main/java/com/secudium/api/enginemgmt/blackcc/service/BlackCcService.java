package com.secudium.api.enginemgmt.blackcc.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.enginemgmt.blackcc.dao.IpDao;
import com.secudium.api.enginemgmt.blackcc.dao.UrlDao;
import com.secudium.api.enginemgmt.blackcc.dto.BlackCcIpInsertDto;
import com.secudium.api.enginemgmt.blackcc.dto.BlackCcIpUpdateDto;
import com.secudium.api.enginemgmt.blackcc.dto.BlackCcUrlInsertDto;
import com.secudium.api.enginemgmt.blackcc.dto.BlackCcUrlUpdateDto;
import com.secudium.api.enginemgmt.blackcc.entity.BlackCcIp;
import com.secudium.api.enginemgmt.blackcc.entity.BlackCcUrl;
import com.secudium.api.enginemgmt.engineenv.dao.EngineCommandDao;
import com.secudium.api.enginemgmt.engineenv.entity.EngineCommand;
import com.secudium.api.enginemgmt.engineenv.type.EngineMenuType;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.exception.BusinessException;
import com.secudium.security.UserPrincipal;
import com.secudium.storage.zk.ZKService;
import com.secudium.util.ZkUtils;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class BlackCcService {

	private final IpDao ipDao;
	private final UrlDao urlDao;
	private final ZKService zkService;
	private final EngineCommandDao engineCommandDao;

	public PagedResponse<BlackCcIp> getBlackCcIps(Pageable pageable, SearchMap searchMap, String locale) {
		Page<BlackCcIp> page = ipDao.selectBlackCcIps(pageable, searchMap, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());	
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public List<BlackCcIp> getBlackCcIpsDupl( Long stIp, Long  edIp ) {
		Map <String, Long> map = new HashMap<String, Long>();
		map.put( "stIp", stIp);
		map.put( "edIp", edIp);
		List<BlackCcIp> list = ipDao.selectBlackCcIpsDupl( map );
		return list;
	}

	public List<BlackCcIp> getBlackCcIpsDuplUpdate( Map<String, Long> map ) {
		return ipDao.selectBlackCcIpsDuplUpdate( map );
	}

	public BlackCcIp getBlackCcIpDupl( Long stIp ) {
		return ipDao.selectBlackCcIpDupl( stIp );
	}

	public BlackCcIp getBlackCcIpDuplUpdate( Map<String, Long> map ) {
		return ipDao.selectBlackCcIpDuplUpdate( map );
	}

	public BlackCcIp getBlackCcIpsById(Long id) {
		return ipDao.selectIpById(id);
	}

	@Transactional(rollbackFor=BusinessException.class)
	public int createBlackCcIp(BlackCcIpInsertDto dto, UserPrincipal currentUser) {
		int count = 0;
		BlackCcIp blackCcIp = dto.toBlackCcIpEntity(currentUser);
		count = ipDao.insertBlackCcIp(blackCcIp);
		//insertZkData("IP", blackCcIp.getIpDstnct(), currentUser);
		return count;
	}

	public void updateBlackCcIp(BlackCcIpUpdateDto dto, UserPrincipal currentUser) {
		BlackCcIp blackCcIp = dto.toBlackCcIpEntity(currentUser);
		ipDao.updateBlackCcIp(blackCcIp);
		//insertZkData("IP", blackCcIp.getIpDstnct(), currentUser);
	}

	public void updateBlackCcIps(List<BlackCcIpUpdateDto> dto, UserPrincipal currentUser) {
		List<BlackCcIp> list = new ArrayList<BlackCcIp>();
		for(BlackCcIpUpdateDto vo : dto) {
			list.add(vo.toBlackCcIpEntity(currentUser));
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("modUsrNo", currentUser.getUsrNo());
		ipDao.updateBlackCcIps(map);

		//		Map<String, List<BlackCcIp>> dstnctGrouped =
		//				list.stream().collect(Collectors.groupingBy(w -> w.getIpDstnct()));
		//		for(String dstnct : dstnctGrouped.keySet()) {
		//			insertZkData("IP", dstnct, currentUser);
		//		}
	}


	public PagedResponse<BlackCcUrl> getBlackCcUrls(Pageable pageable, SearchMap searchMap, String locale) {
		Page<BlackCcUrl> page = urlDao.selectBlackCcUrls(pageable, searchMap, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());	
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public BlackCcUrl getBlackCcUrlsById(Long id) {
		return urlDao.selectUrlById(id);
	}

	public int createBlackCcUrl(BlackCcUrlInsertDto dto, UserPrincipal currentUser) {
		int count = 0;
		BlackCcUrl blackCcUrl = dto.toBlackCcUrlEntity(currentUser);
		count = urlDao.insertBlackCcUrl(blackCcUrl);
		//insertZkData("URL", blackCcUrl.getUrlDstnct(), currentUser);
		return count;
	}

	public void updateBlackCcUrl(BlackCcUrlUpdateDto dto, UserPrincipal currentUser) {
		BlackCcUrl blackCcUrl = dto.toBlackCcUrlEntity(currentUser);
		urlDao.updateBlackCcUrl(blackCcUrl);
		//insertZkData("URL", blackCcUrl.getUrlDstnct(), currentUser);
	}

	public void updateBlackCcUrls(List<BlackCcUrlUpdateDto> dto, UserPrincipal currentUser) {
		List<BlackCcUrl> list = new ArrayList<BlackCcUrl>();
		for(BlackCcUrlUpdateDto vo : dto) {
			list.add(vo.toBlackCcUrlEntity(currentUser));
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("modUsrNo", currentUser.getUsrNo());
		urlDao.updateBlackCcUrls(map);

		//		Map<String, List<BlackCcUrl>> dstnctGrouped =
		//				list.stream().collect(Collectors.groupingBy(w -> w.getUrlDstnct()));
		//		for(String dstnct : dstnctGrouped.keySet()) {
		//			insertZkData("URL", dstnct, currentUser);
		//		}
	}

	private void insertZkData(String dataType, String dstnct, UserPrincipal currentUser, String locale) {
		// 주키퍼 처리
		EngineMenuType menuType = EngineMenuType.UPD_DEFAULT;
		StringBuilder sb = new StringBuilder();
		List<Integer> lengthList = new ArrayList<>();

		if(dataType.equals("IP")) {
			if(dstnct.toUpperCase().equals("BLACK")) {
				menuType = EngineMenuType.UPD_BLACK_IP;
			} else if(dstnct.toUpperCase().equals("CC")) {
				menuType = EngineMenuType.UPD_CC_IP;
			} else if(dstnct.toUpperCase().equals("WHITE")) {
				menuType = EngineMenuType.UPD_WHITE_IP;
			}

			EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());
			// 주키퍼 명령어 정보 조회
			String separator = ZkUtils.TAB;
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				separator = ZkUtils.getHexToText(ec.getDataSprtn());
			}

			// BlackCcIp 정보 조회
			List<BlackCcIp> ipList = ipDao.selectBlackCcIpsByDstnct(dstnct);

			// 헤더정보가 존재하는 경우 첫 라인에 추가
			if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
				String hdr = ec.getHdrDtl();
				if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
					hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
				}
				sb.append(hdr);
				if (ipList != null && ipList.size() > 0) {
					sb.append(ZkUtils.NL);
				}
			}

			if (ipList != null && ipList.size() > 0) {
				for(int i = 0; i < ipList.size(); i++) {
					if(i < ipList.size()-1) {
						String data = ZkUtils.convertZkDataFormat(ipList.get(i), separator, true);
						lengthList.add(data.length());
						sb.append(data);
					} else {
						String data = ZkUtils.convertZkDataFormat(ipList.get(i), separator, false);
						lengthList.add(data.length());
						sb.append(data);
					}
				}
			}

		} else if(dataType.equals("URL")) {
			if(dstnct.toUpperCase().equals("BLACK")) {
				menuType = EngineMenuType.UPD_BLACK_URL;
			} else if(dstnct.toUpperCase().equals("CC")) {
				menuType = EngineMenuType.UPD_CC_URL;
			}

			EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());

			// 주키퍼 명령어 정보 조회
			String separator = ZkUtils.TAB;
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				separator = ZkUtils.getHexToText(ec.getDataSprtn());
			}

			// BlackCcUrl 정보 조회
			List<BlackCcUrl> urlList = urlDao.selectBlackCcUrlsByDstnct(dstnct, locale);

			// 헤더정보가 존재하는 경우 첫 라인에 추가
			if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
				String hdr = ec.getHdrDtl();
				if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
					hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
				}
				sb.append(hdr);
				if (urlList != null && urlList.size() > 0) {
					sb.append(ZkUtils.NL);
				}
			}

			if (urlList != null && urlList.size() > 0) {
				for(int i = 0; i < urlList.size(); i++) {
					if(i < urlList.size()-1) {
						String data = ZkUtils.convertZkDataFormat(urlList.get(i), separator, true);
						lengthList.add(data.length());
						sb.append(data);
					} else {
						String data = ZkUtils.convertZkDataFormat(urlList.get(i), separator, false);
						lengthList.add(data.length());
						sb.append(data);
					}
				}
			}
		}		
		zkService.setDataForPathWithWatchMsg(
				lengthList, sb.toString(), currentUser.getUsername(), menuType);
	}

	public void deployAll(UserPrincipal currentUser, String locale) {		
		insertZkData("IP", "BLACK", currentUser, locale);
		insertZkData("IP", "CC", currentUser, locale);
		insertZkData("IP", "WHITE", currentUser, locale);
		insertZkData("URL", "BLACK", currentUser, locale);
		insertZkData("URL", "CC", currentUser, locale);
	}
}