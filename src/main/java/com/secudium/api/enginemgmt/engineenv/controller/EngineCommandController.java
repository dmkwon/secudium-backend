package com.secudium.api.enginemgmt.engineenv.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.secudium.api.enginemgmt.engineenv.dto.EngineCommandDto;
import com.secudium.api.enginemgmt.engineenv.entity.EngineCommand;
import com.secudium.api.enginemgmt.engineenv.service.EngineCommandService;
import com.secudium.common.dto.OkResponse;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * 
 * git commit test
 * @author intellicode
 *
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/enginemgmt")
public class EngineCommandController {

	private final EngineCommandService engineCommandService;

	@GetMapping("/engineenv/enginecommands") 
	public ResponseEntity<List<EngineCommand>> getEngineCommands() {
		return ResponseEntity.ok(engineCommandService.getEngineCommands());
	}

	@PostMapping("/engineenv/enginecommandbycomseq") 
	public ResponseEntity<EngineCommand> getEngineCommand(
			@RequestBody EngineCommand dto
			) {
		return ResponseEntity.ok(engineCommandService.getEngineCommand(dto.getEngineCmdSeq()));		
	}

	@PostMapping("/engineenv/enginecommand") 
	public ResponseEntity<OkResponse> createEngineCommands(
			@RequestBody @Valid final EngineCommandDto dto
			, @CurrentUser UserPrincipal currentUser) {
		engineCommandService.createEngineCommand(dto, currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/api/enginemgmt/engineenv/enginecommands").build().toUri();
		return ResponseEntity.created(location).body(OkResponse.of());
	}

	@PutMapping("/engineenv/enginecommand") 
	public ResponseEntity<OkResponse> updateEngineCommands(
			@RequestBody @Valid final EngineCommandDto dto
			, @CurrentUser UserPrincipal currentUser) {
		engineCommandService.updateEngineCommand(dto, currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/api/enginemgmt/engineenv/enginecommands").build().toUri();
		return ResponseEntity.created(location).body(OkResponse.of());
	}

	@PostMapping("/engineenv/enginecommand/delete") 
	public ResponseEntity<OkResponse> removeEngineCommands(
			@RequestBody EngineCommand dto
			, @CurrentUser UserPrincipal currentUser) {
		engineCommandService.removeEngineCommand(dto.getEngineCmdSeq(), currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/api/enginemgmt/engineenv/enginecommands").build().toUri();
		return ResponseEntity.created(location).body(OkResponse.of());
	}

}