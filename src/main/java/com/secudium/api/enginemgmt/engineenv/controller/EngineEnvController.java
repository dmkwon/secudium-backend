package com.secudium.api.enginemgmt.engineenv.controller;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.enginemgmt.engineenv.dto.EngineEnvDto;
import com.secudium.api.enginemgmt.engineenv.entity.EngineEnv;
import com.secudium.api.enginemgmt.engineenv.service.EngineEnvService;
import com.secudium.common.dto.OkResponse;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * 
 * git commit test
 * @author intellicode
 *
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/enginemgmt")
public class EngineEnvController {

	private final EngineEnvService engineEnvService;

	@GetMapping("/engineenv")
	public ResponseEntity<EngineEnv> getEngineEnvInfo() {
		return ResponseEntity.ok(engineEnvService.getEngineEnvInfo());		
	}

	@PutMapping("/engineenv")
	public ResponseEntity<OkResponse> updateEngineEnvInfo(
			@RequestBody @Valid final EngineEnvDto dto
			, @CurrentUser UserPrincipal currentUser) {
		engineEnvService.updateEngineEnvInfo(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

}