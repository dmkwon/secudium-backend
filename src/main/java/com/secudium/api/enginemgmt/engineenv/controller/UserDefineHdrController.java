package com.secudium.api.enginemgmt.engineenv.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.secudium.api.enginemgmt.engineenv.dto.UserDefineHdrDto;
import com.secudium.api.enginemgmt.engineenv.entity.UserDefineHdr;
import com.secudium.api.enginemgmt.engineenv.service.UserDefineHdrService;
import com.secudium.common.dto.OkResponse;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * 
 * git commit test
 * @author intellicode
 *
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/enginemgmt")
public class UserDefineHdrController {

	private final UserDefineHdrService userDefineHdrService;

	@GetMapping("/engineenv/userdefinehdr") 
	public ResponseEntity<List<UserDefineHdr>> getUserDefineHdrs() {
		return ResponseEntity.ok(userDefineHdrService.getUserDefineHdrs());		
	}

	@PostMapping("/engineenv/userdefinehdr") 
	public ResponseEntity<OkResponse> createUserDefineHdr(
			@RequestBody @Valid final UserDefineHdrDto dto
			, @CurrentUser UserPrincipal currentUser) {
		userDefineHdrService.createUserDefineHdr(dto, currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/engines").build().toUri();
		return ResponseEntity.created(location).body(OkResponse.of());
	}
	
	@PutMapping("/engineenv/userdefinehdr") 
	public ResponseEntity<OkResponse> updateUserDefineHdr(
			@RequestBody @Valid final UserDefineHdrDto dto
			, @CurrentUser UserPrincipal currentUser) {
		userDefineHdrService.updateUserDefineHdr(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/engineenv/userdefinehdr/delete") 
	public ResponseEntity<OkResponse> removeUserDefineHdr(
			@RequestBody @Valid final UserDefineHdrDto dto
			, @CurrentUser UserPrincipal currentUser) {
		userDefineHdrService.removeUserDefineHdr(dto.getUserDefineHdrSeq(), currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

}