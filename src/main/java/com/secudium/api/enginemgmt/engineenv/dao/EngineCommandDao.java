package com.secudium.api.enginemgmt.engineenv.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.enginemgmt.engineenv.entity.EngineCommand;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class EngineCommandDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public List<EngineCommand> selectEngineCommands() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}
	
	public EngineCommand selectEngineCommandByCmd(String zkCmd) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), zkCmd);
	}

	public EngineCommand selectEngineCommandBySeq(Long engineCmdSeq) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), engineCmdSeq);
	}

	public void insertEngineCommand(EngineCommand engineCommand) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), engineCommand);
	}

	public void updateEngineCommand(EngineCommand engineCommand) {
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), engineCommand);
	}

	public void deleteEngineCommand(Map<String, Object> params) {
		sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}
}
