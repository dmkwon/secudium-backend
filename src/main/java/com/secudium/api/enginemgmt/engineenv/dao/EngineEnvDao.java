package com.secudium.api.enginemgmt.engineenv.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.enginemgmt.engineenv.entity.EngineEnv;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class EngineEnvDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public EngineEnv selectEngineEnvInfo() {
		return this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public void updateEngineEnvInfo(EngineEnv engineEnv) {
		this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), engineEnv);
	}
}
