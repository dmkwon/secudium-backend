package com.secudium.api.enginemgmt.engineenv.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.enginemgmt.engineenv.entity.UserDefineHdr;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class UserDefineHdrDao extends RepositorySupport {

	private final SqlSession sqlSession;
	
	public List<UserDefineHdr> selectUserDefineHdrs() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}
	
	public UserDefineHdr selectUserDefineHdrsByNodePath(String nodePath) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), nodePath);
	}

	public void insertUserDefineHdr(UserDefineHdr userDefineHdr) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), userDefineHdr);
	}

	public void updateUserDefineHdr(UserDefineHdr userDefineHdr) {
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), userDefineHdr);
	}

	public void deleteUserDefineHdr(Map<String, Object> params) {
		sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}
}
