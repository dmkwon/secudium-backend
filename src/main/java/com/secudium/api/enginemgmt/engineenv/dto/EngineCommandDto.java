package com.secudium.api.enginemgmt.engineenv.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.enginemgmt.engineenv.entity.EngineCommand;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EngineCommandDto {
	
	private Long engineCmdSeq;
	private String zkCmd;
	private String zkDataPath;
	private String zkWatchPath;
	private String zkWatchMsg;
	private String dataSprtn;
	private String hdrDtl;
	private String useYn;

	public EngineCommand toEntityWithCurrentUser(UserPrincipal currentUser) {
		return EngineCommand.builder()
			.engineCmdSeq(engineCmdSeq)
			.zkCmd(zkCmd)
			.zkDataPath(zkDataPath)
			.zkWatchPath(zkWatchPath)
			.zkWatchMsg(zkWatchMsg)
			.dataSprtn(dataSprtn)
			.hdrDtl(hdrDtl)
			.useYn(useYn)
			.regUsrNo(currentUser.getUsrNo())
			.modUsrNo(currentUser.getUsrNo())
			.build();
	}
}
