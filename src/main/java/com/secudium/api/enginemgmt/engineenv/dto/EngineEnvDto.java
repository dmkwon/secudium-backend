package com.secudium.api.enginemgmt.engineenv.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.enginemgmt.engineenv.entity.EngineEnv;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EngineEnvDto {

	private Long engineEnvSeq;

	/** 접속 정보 */
	private String zkConnInfo;

	/** root 경로 */
	private String rootPath;

	/** backup 경로 */
	private String bakPath;

	/** writing limit size */
	private int nodeSize;

	/** command 경로 in zookeeper */
	private String cmdPath;

	public EngineEnv toEntityWithCurrentUser(UserPrincipal currentUser) {
		return EngineEnv.builder()
				.engineEnvSeq(engineEnvSeq)
				.zkConnInfo(zkConnInfo)
				.rootPath(rootPath)
				.bakPath(bakPath)
				.nodeSize(nodeSize)
				.cmdPath(cmdPath)
				.regUsrNo(currentUser.getUsrNo())
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}
}
