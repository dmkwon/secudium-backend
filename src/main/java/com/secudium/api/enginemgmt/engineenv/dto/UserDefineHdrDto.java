package com.secudium.api.enginemgmt.engineenv.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.enginemgmt.engineenv.entity.UserDefineHdr;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class UserDefineHdrDto {

	private Long userDefineHdrSeq;
	private String nodePath;
	private String hdr;

	public UserDefineHdr toEntityWithCurrentUser(UserPrincipal currentUser) {
		return UserDefineHdr.builder()
				.userDefineHdrSeq(userDefineHdrSeq)
				.nodePath(nodePath)
				.hdr(hdr)
				.regUsrNo(currentUser.getUsrNo())
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}
}
