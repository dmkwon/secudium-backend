package com.secudium.api.enginemgmt.engineenv.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EngineCommand extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 2366243007274207187L;

	private Long engineCmdSeq;
	private String zkCmd;
	private String zkDataPath;
	private String zkWatchPath;
	private String zkWatchMsg;
	private String dataSprtn;
	private String hdrDtl;
	private String useYn;

}