package com.secudium.api.enginemgmt.engineenv.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EngineEnv extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -4235874762899714175L;

	private Long engineEnvSeq;

	/** 접속 정보 */
	private String zkConnInfo;

	/** root 경로 */
	private String rootPath;

	/** backup 경로 */
	private String bakPath;

	/** writing limit size */
	private int nodeSize;

	/** command 경로 in zookeeper */
	private String cmdPath;	
}
