package com.secudium.api.enginemgmt.engineenv.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class UserDefineHdr extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 7188345463839096360L;
	
	private Long userDefineHdrSeq;
	private String nodePath;
	private String hdr;

}
