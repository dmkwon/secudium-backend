package com.secudium.api.enginemgmt.engineenv.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.enginemgmt.engineenv.dao.EngineCommandDao;
import com.secudium.api.enginemgmt.engineenv.dto.EngineCommandDto;
import com.secudium.api.enginemgmt.engineenv.entity.EngineCommand;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class EngineCommandService {

	private final EngineCommandDao engineCommandDao;

	public List<EngineCommand> getEngineCommands() {
		return engineCommandDao.selectEngineCommands();
	}
	
	public EngineCommand getEngineCommand(Long engineCmdSeq) {
		return engineCommandDao.selectEngineCommandBySeq(engineCmdSeq);
	}
	
	public void createEngineCommand(EngineCommandDto dto, UserPrincipal currentUser) { 
		engineCommandDao.insertEngineCommand(dto.toEntityWithCurrentUser(currentUser));
	}

	public void updateEngineCommand(EngineCommandDto dto, UserPrincipal currentUser) { 
		engineCommandDao.updateEngineCommand(dto.toEntityWithCurrentUser(currentUser));
	}

	public void removeEngineCommand(Long engineCmdSeq, UserPrincipal currentUser) {
		Map<String, Object> params = new HashMap<>();
		params.put("engineCmdSeq", engineCmdSeq);
		params.put("modUsrNo", currentUser.getUsrNo());
		engineCommandDao.deleteEngineCommand(params);
	}

}
