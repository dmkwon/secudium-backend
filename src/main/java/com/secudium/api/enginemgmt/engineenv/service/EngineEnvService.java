package com.secudium.api.enginemgmt.engineenv.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.enginemgmt.engineenv.dao.EngineEnvDao;
import com.secudium.api.enginemgmt.engineenv.dto.EngineEnvDto;
import com.secudium.api.enginemgmt.engineenv.entity.EngineEnv;
import com.secudium.component.zk.CuratorFrameworkFactoryBean;
import com.secudium.error.ErrorCode;
import com.secudium.exception.BusinessException;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class EngineEnvService {

	private final EngineEnvDao engineEnvDao;
	private final CuratorFrameworkFactoryBean factoryBean;

	public EngineEnv getEngineEnvInfo() {
		EngineEnv engineEnv = engineEnvDao.selectEngineEnvInfo();
		return EngineEnv.builder()
				.zkConnInfo(engineEnv.getZkConnInfo())
				.rootPath(engineEnv.getRootPath())
				.bakPath(engineEnv.getBakPath())
				.nodeSize(engineEnv.getNodeSize())
				.cmdPath(engineEnv.getCmdPath())
				.build();
	}

	public void updateEngineEnvInfo(EngineEnvDto dto, UserPrincipal currentUser) {
		engineEnvDao.updateEngineEnvInfo(dto.toEntityWithCurrentUser(currentUser));
		try {
			factoryBean.destroy();
			factoryBean.setConnectString(dto.getZkConnInfo());
			factoryBean.afterPropertiesSet();
		} catch (Exception e) {	
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}
}
