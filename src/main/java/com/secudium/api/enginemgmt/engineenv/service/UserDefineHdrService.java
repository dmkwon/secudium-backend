package com.secudium.api.enginemgmt.engineenv.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.enginemgmt.engineenv.dao.UserDefineHdrDao;
import com.secudium.api.enginemgmt.engineenv.dto.UserDefineHdrDto;
import com.secudium.api.enginemgmt.engineenv.entity.UserDefineHdr;
import com.secudium.component.db.UserDefineHdrBean;
import com.secudium.error.ErrorCode;
import com.secudium.exception.BusinessException;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class UserDefineHdrService {

	private final UserDefineHdrDao userDefineHdrDao;
	private final UserDefineHdrBean uerDefineHdrBean;

	public List<UserDefineHdr> getUserDefineHdrs() { 
		return userDefineHdrDao.selectUserDefineHdrs();
	}

	public void createUserDefineHdr(UserDefineHdrDto dto, UserPrincipal currentUser) { 
		userDefineHdrDao.insertUserDefineHdr(dto.toEntityWithCurrentUser(currentUser));
		
		try {
			uerDefineHdrBean.destroy();
			uerDefineHdrBean.afterPropertiesSet();	
		} catch(Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}
	
	public void updateUserDefineHdr(UserDefineHdrDto dto, UserPrincipal currentUser) { 
		userDefineHdrDao.updateUserDefineHdr(dto.toEntityWithCurrentUser(currentUser));
		try {
			uerDefineHdrBean.destroy();
			uerDefineHdrBean.afterPropertiesSet();	
		} catch(Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	public void removeUserDefineHdr(Long userDefineHdrSeq, UserPrincipal currentUser) {
		Map<String, Object> params = new HashMap<>();
		params.put("userDefineHdrSeq", userDefineHdrSeq);
		params.put("modUsrNo", currentUser.getUsrNo());
		userDefineHdrDao.deleteUserDefineHdr(params);
		try {
			uerDefineHdrBean.destroy();
			uerDefineHdrBean.afterPropertiesSet();	
		} catch(Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

}
