package com.secudium.api.enginemgmt.field.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.enginemgmt.field.entity.Field;
import com.secudium.api.enginemgmt.field.service.FieldService;

import lombok.RequiredArgsConstructor;
/*
 * git commit test
 * @author intellicode
 *
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/enginemgmt")
public class FieldController {

	private final FieldService fieldService;

	@GetMapping("/field/all")
	public ResponseEntity<List<Field>> getFields(
			HttpServletRequest request, 
			Locale locale) {
		return ResponseEntity.ok(fieldService.getFields(locale.getLanguage()));
	}

	@PostMapping("/field")
	public ResponseEntity<List<Field>> getFieldsByHistSeq(
			HttpServletRequest request,
			@RequestBody @Valid Field dto, 
			Locale locale
			) {
		return ResponseEntity.ok(fieldService.getFieldsByHistSeq(dto.getFieldHistSeq(), locale.getLanguage()));
	}

	@GetMapping("/field/group")
	public ResponseEntity<List<Field>> getFieldsByFieldGroup(
			HttpServletRequest request,
			@RequestParam Long fieldGroupCd,
			Locale locale
			){
		return ResponseEntity.ok(fieldService.getFieldByFieldGroup(fieldGroupCd, locale.getLanguage()));
	}

	@GetMapping("/field/isduplicate")
	public ResponseEntity<Boolean> isDuplicate(
			@RequestParam String value) {
		return ResponseEntity.ok(fieldService.isDuplicate(value));
	}

	@GetMapping("/field/isDuplicateFieldId")
	public ResponseEntity<Boolean> isDuplicateFieldId(
			@RequestParam(value="value") int value,
			@RequestParam(value="fieldSeq", required=false) Long fieldSeq) {
		return ResponseEntity.ok(fieldService.isDuplicateFieldId(value, fieldSeq));
	}
}
