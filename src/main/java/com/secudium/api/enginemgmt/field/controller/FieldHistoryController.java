package com.secudium.api.enginemgmt.field.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.enginemgmt.field.dto.FieldHistoryInsertDto;
import com.secudium.api.enginemgmt.field.entity.FieldHistory;
import com.secudium.api.enginemgmt.field.service.FieldHistoryService;
import com.secudium.common.dto.OkResponse;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/enginemgmt")
public class FieldHistoryController {

	private final FieldHistoryService fieldHistoryService;

	@GetMapping("/field/history")
	public ResponseEntity<List<FieldHistory>> getFieldHists() {
		return ResponseEntity.ok(fieldHistoryService.getFieldHist());
	}

	@PostMapping("/field/history/rollback")
	public ResponseEntity<OkResponse> rollbackHistory(
			HttpServletRequest request, 
			@RequestBody FieldHistoryInsertDto histDto,
			@CurrentUser UserPrincipal currentUser,
			Locale locale) {
		fieldHistoryService.rollbackHistory(histDto, currentUser, locale.getLanguage());
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/field/history/insert")
	public  ResponseEntity<OkResponse> createFieldHist(
			@RequestBody FieldHistoryInsertDto histDto, 
			@CurrentUser UserPrincipal currentUser, 
			Locale locale) {
		fieldHistoryService.createFieldHist4insert(histDto, currentUser, locale.getLanguage());
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/field/history/update")
	public  ResponseEntity<OkResponse> updateFieldHist(
			@RequestBody FieldHistoryInsertDto histDto, 
			@CurrentUser UserPrincipal currentUser, 
			Locale locale) {
		fieldHistoryService.createFieldHist4update(histDto, currentUser, locale.getLanguage());
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/field/history/delete")
	public  ResponseEntity<OkResponse> deleteFieldHist(
			@RequestBody FieldHistoryInsertDto histDto, 
			@CurrentUser UserPrincipal currentUser,
			Locale locale) {
		fieldHistoryService.createFieldHist4delete(histDto, currentUser, locale.getLanguage());
		return ResponseEntity.ok(OkResponse.of());
	}
}
