package com.secudium.api.enginemgmt.field.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.enginemgmt.field.entity.Field;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class FieldDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public List<Field> selectFields(Map<String, String> params) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);		
	}

	public List<Field> selectFieldsByHistSeq(Map<String, Object> params) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);		
	}

	public List<Field> selectVarFieldsByHistSeq(Map<String, Object> params) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);		
	}
	
	/**
	 * 신규필드 추가
	 * @param field
	 * @return row count
	 */
	public int insertField(Field field) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), field);
	}

	/**
	 * 기존 이력의 필드들을 신규이력에 복사
	 * @param oldHistSeq 이전이력 일련번호
	 * @param newHistSeq 신규이력 일련번호
	 * @return row count
	 */
	public int copyFieldsForInsert(Map<String, Object> params) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	/**
	 * 기존 이력의 필드들(파라미터 필드 제외)을 신규이력에 복사
	 * @param oldHistSeq 이전이력 일련번호
	 * @param newHistSeq 신규이력 일련번호
	 * @param exclSeq 제외된 필드 일련번호
	 * @return row count
	 */
	public int copyFieldsForMod(Map<String, Object> params) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public Boolean isDuplicate(String value) {
		return (Integer) this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), value) > 0 ?
				true : false;
	}
	public Boolean isDuplicateFieldId(int value, Long fieldSeq) {
		Map<String, Object> params = new HashMap<>();
		params.put("value", value);
		params.put("fieldSeq", fieldSeq);
		return (Integer) this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params) > 0 ?
				true : false;
	}
	public List<Field> getFieldByFieldGroup(Map<String, Object> params) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}
}
