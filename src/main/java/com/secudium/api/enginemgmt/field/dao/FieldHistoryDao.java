package com.secudium.api.enginemgmt.field.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.enginemgmt.field.entity.FieldHistory;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class FieldHistoryDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public List<FieldHistory> getFieldHists() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());		
	}
	
	public void insertFieldHist(FieldHistory fieldHist) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), fieldHist);
	}
	
	public void rollbackHistory(Map<String, Object> params) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public long getRecentHistSeq() {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName());
	}
}
