package com.secudium.api.enginemgmt.field.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.enginemgmt.field.entity.FieldHistory;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class FieldHistoryInsertDto {

    private Long fieldHistSeq;
    private String fieldChgReason;
    private FieldInsertDto field;
    private Long targetHistSeq;

    public FieldHistory toEntityWithCurrentUser(UserPrincipal currentUser) {
        return FieldHistory.builder()
                .fieldChgReason(fieldChgReason)
                .regUsrNo(currentUser.getUsrNo())
                .modUsrNo(currentUser.getUsrNo())
                .build();
    }
}
