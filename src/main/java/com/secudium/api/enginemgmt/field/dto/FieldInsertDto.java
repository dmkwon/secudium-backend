package com.secudium.api.enginemgmt.field.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.enginemgmt.field.entity.Field;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class FieldInsertDto {

    private long fieldSeq;
    private long fieldHistSeq;
    private long fieldId;
    private String fieldType;
    private String field;
    private String fieldGroupCode;
    private String fieldDesc;
    private String keyFieldYn;

    public Field toEntityWithCurrentUser(long fieldHistSeq, UserPrincipal currentUser) {
        return Field.builder()
            .fieldHistSeq(fieldHistSeq)
            .fieldId(fieldId)
            .fieldType(fieldType)
            .field(field)
            .fieldGroupCode(fieldGroupCode)
            .fieldDesc(fieldDesc)
            .keyFieldYn(keyFieldYn)
            .regUsrNo(currentUser.getUsrNo())
            .build();
    }
}
