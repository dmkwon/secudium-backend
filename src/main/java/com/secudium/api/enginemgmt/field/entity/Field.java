package com.secudium.api.enginemgmt.field.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Field extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1749693862311496006L;

    private long fieldSeq;
    private long fieldHistSeq;
    private long fieldId;
    private String fieldType;
    private String fieldTypeString;
    private String field;
    private String fieldGroupCode;
    private String fieldGroupCodeString;
    private String fieldDesc;
    private String keyFieldYn;
}
