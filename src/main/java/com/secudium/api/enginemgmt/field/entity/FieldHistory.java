package com.secudium.api.enginemgmt.field.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class FieldHistory extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -3100935908350615094L;

    private long fieldHistSeq;
    private String fieldChgReason;
    private String isLatest;
}
