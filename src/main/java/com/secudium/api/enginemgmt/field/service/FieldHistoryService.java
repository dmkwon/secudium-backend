package com.secudium.api.enginemgmt.field.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.enginemgmt.engineenv.dao.EngineCommandDao;
import com.secudium.api.enginemgmt.engineenv.entity.EngineCommand;
import com.secudium.api.enginemgmt.engineenv.type.EngineMenuType;
import com.secudium.api.enginemgmt.field.dao.FieldDao;
import com.secudium.api.enginemgmt.field.dao.FieldHistoryDao;
import com.secudium.api.enginemgmt.field.dto.FieldHistoryInsertDto;
import com.secudium.api.enginemgmt.field.entity.Field;
import com.secudium.api.enginemgmt.field.entity.FieldHistory;
import com.secudium.security.UserPrincipal;
import com.secudium.storage.zk.ZKService;
import com.secudium.util.ZkUtils;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class FieldHistoryService {

	private final FieldHistoryDao fieldHistoryDao;
	private final FieldDao fieldDao;
	private final ZKService zkService;
	private final EngineCommandDao engineCommandDao;

	private void insertZkVarFieldData(Long fieldHistSeq, UserPrincipal currentUser, String locale) {
        // 주키퍼 명령어 정보 조회
        EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(EngineMenuType.UPD_VAR_FIELD.name());
        String separator = ZkUtils.TAB;
        if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
            separator = ZkUtils.getHexToText(ec.getDataSprtn());
        }

    	Map<String, Object> params = new HashMap<>();
		params.put("fieldHistSeq", fieldHistSeq);
		params.put("lang", locale);
		
        // 필드 정보 조회
        List<Field> fields = fieldDao.selectVarFieldsByHistSeq(params);
		List<Integer> lengthList = new ArrayList<>();

        // 헤더정보가 존재하는 경우 첫 라인에 추가
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
            String hdr = ec.getHdrDtl();
            if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
                hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
            }
            sb.append(hdr);
            if (fields != null && fields.size() > 0) {
                sb.append(ZkUtils.NL);
            }
        }
        
        if (fields != null && fields.size() > 0) {
        	// 주키퍼에 배포할 데이터 추가
    		for(int i = 0; i < fields.size(); i++) {
    			if(i < fields.size()-1) {
					String data = ZkUtils.convertZkDataFormat(fields.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);
				} else {
					String data = ZkUtils.convertZkDataFormat(fields.get(i), separator, false);
					lengthList.add(data.length());
					sb.append(data);
				}
    		}	
        }
        zkService.setDataForPathWithWatchMsg(
        		lengthList, sb.toString(), currentUser.getUsername(), EngineMenuType.UPD_VAR_FIELD);
	}
	
	private void insertZkFieldData(Long fieldHistSeq, UserPrincipal currentUser, String locale) {
        // 주키퍼 명령어 정보 조회
        EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(EngineMenuType.UPD_FIELD.name());
        String separator = ZkUtils.TAB;
        if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
            separator = ZkUtils.getHexToText(ec.getDataSprtn());
        }

    	Map<String, Object> params = new HashMap<>();
		params.put("fieldHistSeq", fieldHistSeq);
		params.put("lang", locale);
		
        // 필드 정보 조회
        List<Field> fields = fieldDao.selectFieldsByHistSeq(params);
		List<Integer> lengthList = new ArrayList<>();

        // 헤더정보가 존재하는 경우 첫 라인에 추가
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
            String hdr = ec.getHdrDtl();
            if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
                hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
            }
            sb.append(hdr);
            if (fields != null && fields.size() > 0) {
                sb.append(ZkUtils.NL);
            }
        }
        
        if (fields != null && fields.size() > 0) {
        	// 주키퍼에 배포할 데이터 추가
        	for(int i = 0; i < fields.size(); i++) {
    			if(i < fields.size()-1) {
					String data = ZkUtils.convertZkDataFormat(fields.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);
				} else {
					String data = ZkUtils.convertZkDataFormat(fields.get(i), separator, false);
					lengthList.add(data.length());
					sb.append(data);
				}
    		}	
        }
        zkService.setDataForPathWithWatchMsg(
        		lengthList, sb.toString(), currentUser.getUsername(), EngineMenuType.UPD_FIELD);
	}
	
	public List<FieldHistory> getFieldHist() {
		return fieldHistoryDao.getFieldHists();
	}

	public void rollbackHistory(FieldHistoryInsertDto histDto, UserPrincipal currentUser, String locale) {
		long targetHistSeq = histDto.getTargetHistSeq();
		FieldHistory fieldHist = histDto.toEntityWithCurrentUser(currentUser);
		
		Map<String, Object> params = new HashMap<>();
		params.put("targetHistSeq", targetHistSeq);
		params.put("fieldHist", fieldHist);
		
		fieldHistoryDao.rollbackHistory(params);
		fieldDao.copyFieldsForInsert(params);
		
		//주키퍼 처리
		insertZkVarFieldData(fieldHist.getFieldHistSeq(), currentUser, locale);
		insertZkFieldData(fieldHist.getFieldHistSeq(), currentUser, locale);
		
	}

	public void createFieldHist4insert(FieldHistoryInsertDto histDto, UserPrincipal currentUser, String locale) {
		//신규 이력 삽입
		long targetHistSeq = fieldHistoryDao.getRecentHistSeq();
		FieldHistory fieldHist = histDto.toEntityWithCurrentUser(currentUser);
		fieldHistoryDao.insertFieldHist(fieldHist);
		
		long newHistSeq = fieldHist.getFieldHistSeq();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("targetHistSeq", targetHistSeq);
		params.put("fieldHist", fieldHist);
		fieldDao.copyFieldsForInsert(params);
		//신규 필드 삽입
		Field field = histDto.getField().toEntityWithCurrentUser(newHistSeq, currentUser);
		fieldDao.insertField(field);

		//주키퍼 처리
		insertZkVarFieldData(fieldHist.getFieldHistSeq(), currentUser, locale);
		insertZkFieldData(fieldHist.getFieldHistSeq(), currentUser, locale);
	}

	public void createFieldHist4update(FieldHistoryInsertDto histDto, UserPrincipal currentUser, String locale) {
		//신규 이력 삽입
		Long targetHistSeq = fieldHistoryDao.getRecentHistSeq();
		FieldHistory fieldHist = histDto.toEntityWithCurrentUser(currentUser);

		fieldHistoryDao.insertFieldHist(fieldHist);
		Long newHistSeq = fieldHist.getFieldHistSeq();

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("targetHistSeq", targetHistSeq);
		params.put("fieldHist", fieldHist);
		params.put("exclSeq", histDto.getField().getFieldSeq());

		//신규 필드
		Field field = histDto.getField().toEntityWithCurrentUser(newHistSeq, currentUser);
		fieldDao.copyFieldsForMod(params);
		fieldDao.insertField(field);

		insertZkVarFieldData(fieldHist.getFieldHistSeq(), currentUser, locale);
		insertZkFieldData(fieldHist.getFieldHistSeq(), currentUser, locale);
	}

	public void createFieldHist4delete(FieldHistoryInsertDto histDto, UserPrincipal currentUser, String locale) {
		Long targetHistSeq = fieldHistoryDao.getRecentHistSeq();
		FieldHistory fieldHist = histDto.toEntityWithCurrentUser(currentUser);

		fieldHistoryDao.insertFieldHist(fieldHist);

		Map<String, Object> params = new HashMap<>();
		params.put("targetHistSeq", targetHistSeq);
		params.put("fieldHist", fieldHist);
		params.put("exclSeq", histDto.getField().getFieldSeq());

		fieldDao.copyFieldsForMod(params);

		insertZkVarFieldData(fieldHist.getFieldHistSeq(), currentUser, locale);
		insertZkFieldData(fieldHist.getFieldHistSeq(), currentUser, locale);
	}
}
