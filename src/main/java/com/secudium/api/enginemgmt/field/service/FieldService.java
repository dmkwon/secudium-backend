package com.secudium.api.enginemgmt.field.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.enginemgmt.field.dao.FieldDao;
import com.secudium.api.enginemgmt.field.entity.Field;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class FieldService {

	private final FieldDao fieldDao;

	public List<Field> getFields(String locale) {
		Map<String, String> params = new HashMap<>();
		params.put("lang", locale);
		return fieldDao.selectFields(params);
	}

	public List<Field> getFieldsByHistSeq(Long fieldHistSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("fieldHistSeq", fieldHistSeq);
		params.put("lang", locale);
		return fieldDao.selectFieldsByHistSeq(params);
	}

	public Boolean isDuplicate(String value) {
		return fieldDao.isDuplicate(value);
	}

	public Boolean isDuplicateFieldId(int value, Long fieldSeq) {
		return fieldDao.isDuplicateFieldId(value, fieldSeq);
	}

	public List<Field> getFieldByFieldGroup(Long fieldGroupCd, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("fieldGroupCd", fieldGroupCd);
		params.put("lang", locale);
		return fieldDao.getFieldByFieldGroup(params);
	}
}
