package com.secudium.api.enginemgmt.zknode.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.enginemgmt.zknode.entity.ZkInsertDto;
import com.secudium.api.enginemgmt.zknode.entity.ZkNode;
import com.secudium.api.enginemgmt.zknode.service.ZkNodeService;
import com.secudium.common.dto.OkResponse;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/enginemgmt")
public class ZKNodeController {

  private final ZkNodeService zKNodeService;

  @GetMapping("/zkNode/root")
  public ResponseEntity<List<ZkNode>> getZkRootTree(@RequestParam(value = "depth", required = false) Integer depth) {
    if (depth == null) {
      return ResponseEntity.ok(zKNodeService.getRootTree());
    } else {
      return ResponseEntity.ok(zKNodeService.getRootTreeByDepth(depth));
    }
  }

  @GetMapping("/zkNode/childrens")
  public ResponseEntity<List<ZkNode>> watchedGetChildren(@RequestParam("path") String path) {
    return ResponseEntity.ok(zKNodeService.getChildrenNodes(path));
  }

  @GetMapping(value = "/zkNode/data", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<String> getDataByNodePath(@RequestParam("path") String path) {
    return ResponseEntity.ok(zKNodeService.getDataForPath(path));
  }

  @PutMapping("/zkNode/data")
  public ResponseEntity<OkResponse> setData(@RequestBody @Valid ZkInsertDto dto,
      @CurrentUser UserPrincipal currentUser) {
    zKNodeService.setDataForPath(dto, currentUser);
    return ResponseEntity.ok(OkResponse.of());
  }
}
