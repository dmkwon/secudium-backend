package com.secudium.api.enginemgmt.zknode.entity;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ZkInsertDto {

	@NotBlank
	private String nodePath;
	
	private String data;
	
}

