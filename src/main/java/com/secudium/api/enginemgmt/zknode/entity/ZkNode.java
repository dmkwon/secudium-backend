package com.secudium.api.enginemgmt.zknode.entity;

import java.util.List;

import lombok.Data;


@Data
public class ZkNode {

	private String nodePath;
	private boolean hasHeader;
	private String userDefineHdr;
	private boolean hasData;
	private boolean hasSubNodes;
	private List<ZkNode> subNode;
}
