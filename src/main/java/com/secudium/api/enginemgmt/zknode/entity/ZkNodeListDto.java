package com.secudium.api.enginemgmt.zknode.entity;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.Data;


@Data
public class ZkNodeListDto {

	JsonNode zkNodeData;
	
}
