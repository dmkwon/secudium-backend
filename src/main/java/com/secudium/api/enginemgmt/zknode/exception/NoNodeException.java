package com.secudium.api.enginemgmt.zknode.exception;

import org.apache.zookeeper.KeeperException;

public class NoNodeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6325162679443194454L;

	/**
	 * Constructor; wraps the original ZooKeeperException.
	 * 
	 * @param e
	 *            the original ZooKeeperException.
	 */
	public NoNodeException(KeeperException e) {
		super(e);
	}

	/**
	 * Constructor; wraps an explanatory message.
	 * 
	 * @param msg
	 *            the error message.
	 */
	public NoNodeException(String msg) {
		super(msg);
	}

}