package com.secudium.api.enginemgmt.zknode.exception;

import org.apache.zookeeper.KeeperException;

public class NodeExistsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 462649838659302053L;

	/**
	 * Constructor; wraps the original ZooKeeperException.
	 * 
	 * @param e
	 *            the original ZooKeeperException.
	 */
	public NodeExistsException(KeeperException e) {
		super(e);
	}

}