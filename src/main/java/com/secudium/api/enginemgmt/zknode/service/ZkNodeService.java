package com.secudium.api.enginemgmt.zknode.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.secudium.api.enginemgmt.zknode.entity.ZkInsertDto;
import com.secudium.api.enginemgmt.zknode.entity.ZkNode;
import com.secudium.security.UserPrincipal;
import com.secudium.storage.zk.ZKService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ZkNodeService {

	private final ZKService zkService;

	private static final int MAX_DEPTH = 3;

	public List<ZkNode> getRootTree() {
		return getRootTreeByDepth(MAX_DEPTH);
	}

	public List<ZkNode> getRootTreeByDepth(int depth)  {
		return zkService.getZkRootTreeByDepth(depth);
	}

	public List<ZkNode> getChildrenNodes(String path) {
		return zkService.getZkSubTreeNode(path);
	}

	public String getDataForPath(String path) {		
		return zkService.getDataForPath(path);
	}

	public void setDataForPath(ZkInsertDto dto, UserPrincipal currentUser) {	
		List<Integer> lengthList = new ArrayList<>();
		String lines[] =  dto.getData().split("\\r?\\n");
		for(int i = 0; i < lines.length; i++) {
			lengthList.add(lines[i].length());
		}
		zkService.setDataForPath(dto.getNodePath(), lengthList, dto.getData(), currentUser.getUsername());
  }
}
