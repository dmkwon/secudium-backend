package com.secudium.api.enginemgmt.zknode.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.data.Stat;

import com.secudium.api.enginemgmt.zknode.entity.ZkNode;
import com.secudium.api.enginemgmt.zknode.exception.NoNodeException;
import com.secudium.api.enginemgmt.zknode.exception.NodeExistsException;
import com.secudium.util.PatternUtils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class ZkTreeNode {

  protected final CuratorFramework client;
  protected static final String PATH_SEPARATOR = "/";
  protected static final byte[] EMPTY_DATA = new byte[0];
  private static final CreateMode MODE = CreateMode.PERSISTENT;
  protected static final String HDR_PREFIX = "##";
  private static final String EXCEPTIONMSG = "Exception {}";

  private ConcurrentHashMap<String, String> userDefineHdrMap;

  private final String nodePath;
  private boolean hasHeader = false;
  private String userDefineHdr = "";
  private boolean hasData = false;
  private boolean hasSubNodes = false;
  private List<ZkTreeNode> subNode;

  private boolean isTabs;
  private boolean isDisplayData;
  private boolean isToLeaf;

  /**
   * Constructor. Protected so that API users must go through a {@link ZTree}
   * instance to obtain nodes.
   * 
   * @param zooKeeper a valid QuartzZooKeeper reference.
   * @param path      the path of this node.
   */
  protected ZkTreeNode(CuratorFramework client, String nodePath, ConcurrentHashMap<String, String> userDefineHdrMap) {
    this.client = client;
    this.nodePath = nodePath;
    this.userDefineHdrMap = userDefineHdrMap;
  }

  /**
   * @return true if this node exists in the underlying Zookeeper tree.
   */
  public boolean exists() {
    try {
      return client.checkExists().forPath(nodePath) != null;
    } catch (Exception e) {
      log.debug(EXCEPTIONMSG, e);
      return false;
    }
  }

  /**
   * Updates the data associated with this node.
   * 
   * @param data the new data to set for this node.
   * @throws NoNodeException if this node does not exist in the backing ZooKeeper
   *                         tree
   */
  public void setData(String data) {
    try {
      client.setData().forPath(nodePath, data.getBytes());
    } catch (Exception e) {
      log.debug(EXCEPTIONMSG, e);
    }
  }

  /**
   * @return the data associated with this node.
   * @throws NoNodeException if this node does not exist in the backing ZooKeeper
   *                         tree
   */
  public String getData() {
    return getData(nodePath);
  }

  /**
   * @return the name of this node.
   */
  public String getName() {
    if (isRoot(nodePath)) {
      return PATH_SEPARATOR;
    } else {
      return nodePath.substring(nodePath.lastIndexOf(PATH_SEPARATOR) + 1);
    }
  }

  /**
   * Obtains the leaf node of an arbitrarily long branch of nodes, starting at
   * this node. Creates all nodes in the branch as necessary.
   * 
   * @param orderedDescendantNames the ordered names of descendant nodes in the
   *                               branch, starting with the immediate child of
   *                               this node.
   * @return the leaf node in the specified branch.
   */
  public ZkTreeNode getOrCreateBranch(String... orderedDescendantNames) {
    ZkTreeNode descendant = getOrCreateChild(orderedDescendantNames[0]);
    for (int i = 1; i < orderedDescendantNames.length; i++) {
      descendant = descendant.getOrCreateChild(orderedDescendantNames[i]);
    }
    return descendant;
  }

  /**
   * @param childNames the names of children to get or create.
   * @return a hash with the children name by <tt>childNames</tt>, creating them
   *         if they don't exist.
   */
  public Map<String, ZkTreeNode> getOrCreateChildrenIntoHash(String... childNames) {
    Map<String, ZkTreeNode> hash = new HashMap<String, ZkTreeNode>();
    for (String childName : childNames) {
      hash.put(childName, getOrCreateChild(childName));
    }
    return hash;
  }

  /**
   * @return a haspp/watch/Analyze
   */
  public Map<String, ZkTreeNode> getChildrenIntoHash() {
    Map<String, ZkTreeNode> hash = new HashMap<String, ZkTreeNode>();
    for (ZkTreeNode child : getChildren()) {
      hash.put(child.getName(), child);
    }
    return hash;
  }

  /**
   * @return the Children associated with this node.
   */
  public Collection<ZkTreeNode> getChildren() {
    try {
      return getChildrenImpl(nodePath);
    } catch (KeeperException e) {
      return emptyChildrenOnNonodeOrRethrow(e);
    }
  }

  /**
   * Deletes this node's children recursively, but leaves the node in place, with
   * current data.
   */
  public void deleteChildren() {
    deleteChildren(this);
  }

  /**
   * Deletes the specified child and all its children. If the node does not exist,
   * this is a quiet NOOP.
   */
  public void deleteChild(String childName) {
    getChild(childName).delete();
  }

  /**
   * Deletes the Node with the specified path, recursively.
   * <p>
   * If the Node has children, all the children will be deleted as well (and all
   * the children's children, etc.).
   * <p>
   * If the Node already does not exist, this is a quiet NOOP.
   * 
   * @param path the path of the Node to delete.
   */
  public void delete() {
    try {
      client.delete().forPath(nodePath);
    } catch (Exception e) {
      log.debug(EXCEPTIONMSG, e);
    }
  }

  private Collection<ZkTreeNode> getChildrenImpl(String path) throws KeeperException {
    Collection<ZkTreeNode> children = new HashSet<ZkTreeNode>();
    try {
      for (String childPath : client.getChildren().forPath(path)) {
        children.add(new ZkTreeNode(client, pathForChild(path, childPath), this.userDefineHdrMap));
      }
    } catch (Exception e) {
      log.debug(EXCEPTIONMSG, e);
    }
    return children;
  }

  private String pathForChild(String parentPath, String childName) {
    if (isRoot(parentPath)) {
      // Special Case: from root node, avoid "//"
      return parentPath + childName;
    } else {
      return parentPath + PATH_SEPARATOR + childName;
    }
  }

  private Collection<ZkTreeNode> emptyChildrenOnNonodeOrRethrow(KeeperException e) {
    if (isNoNodeException(e)) {
      // a NONODE certainly can't have children
      return Collections.emptySet();
    } else {
      throw new RuntimeException(e);
    }
  }

  /**
   * Obtains the child of this node with the specified name, creating it if it
   * doesn't already exist.
   * 
   * @param childName the name of the desired child node.
   * @return the specified child node.
   */
  public ZkTreeNode getOrCreateChild(String childName) {
    return getOrCreatePath(pathForChild(childName));
  }

  /**
   * Obtains the child node with the specified name, or an empty representation if
   * it doesn't already exist.
   * <p>
   * This method will not create the specified child if it doesn't already exist.
   * The empty representation returned in that case will not support certain
   * operations, such as getting or setting data. Trying to get or set data on an
   * empty representation will result in a NoNodeException.
   * 
   * @param childName the name of the desired child node.
   * @return the node that represents the specified child of this node (by name).
   *         Will be an emtpy representation if the specified child node does not
   *         exist.
   */
  public ZkTreeNode getChild(String childName) {
    return new ZkTreeNode(client, pathForChild(childName), this.userDefineHdrMap);
  }

  /**
   * Ensures that a child node exists with the specified name and data.
   * 
   * @param childName the name of the child node to ensure.
   * @param data      the data to ensure is set for the child node.
   * @return the specified child node.
   */
  public ZkTreeNode createOrUpdateChild(String childName, String data) {
    /**
     * Optimism: we expect the node to not yet exist, more frequently than it
     * already exists.
     */
    try {
      return createChild(childName, data);
    } catch (NodeExistsException e) {
      ZkTreeNode child = getChild(childName);
      child.setData(data);
      return child;
    }
  }

  /**
   * Creates a child of this node, with the specified name and data. There must
   * not already be a child of this node with that name.
   * 
   * @param childName the name of the new child node.
   * @param data      the data of the new child node.
   * @return the newly created child node.
   * @throws NodeExistsException if the specified node (by name) already exists.
   */
  public ZkTreeNode createChild(String childName, String data) {
    return createNode(pathForChild(childName), data);
  }

  /**
   * Creates a node at the specified path, with the specified data. The node must
   * not already exist.
   * 
   * @param path the path to the desired node. "/a/path/to/mynode"
   * @param data the desired data.
   * @return the desired node.
   * @throws NodeExistsException if the specified node (by name) already exists.
   */
  private ZkTreeNode createNode(String path, String data) {
    return createPath(path, data.getBytes());
  }

  private ZkTreeNode createPath(String path, byte[] data) {
    try {
      client.create().creatingParentsIfNeeded().forPath(path, data);
    } catch (Exception e) {
      log.debug(EXCEPTIONMSG, e);
    }

    return new ZkTreeNode(client, path, this.userDefineHdrMap);
  }

  /**
   * Obtains the node at the specified path, creating it if it doesn't already
   * exist.
   * 
   * @param path the path to the desired node. Example: "/a/path/to/mynode"
   * @return the desired node.
   */
  protected ZkTreeNode getOrCreatePath(String path) {
    ZkTreeNode node = null;
    node = new ZkTreeNode(client, path, this.userDefineHdrMap);
    if (!node.exists()) {
      createQuietly(path);
    }
    return node;
  }

  /**
   * Attempts to create the specified path. Quietly swallows a Node Exists
   * Exception.
   */
  private void createQuietly(String path) {

    try {
      client.create().creatingParentsIfNeeded().withMode(MODE).forPath(path, EMPTY_DATA);
    } catch (Exception e) {
      log.debug(EXCEPTIONMSG, e);
    }
  }

  @Override
  public String toString() {
    return nodePath;
  }

  protected String getPath() {
    return nodePath;
  }

  private final String pathForChild(String childName) {
    return pathForChild(nodePath, childName);
  }

  private String getData(String path) {
    Stat nodeStatus;
    try {
      nodeStatus = client.checkExists().forPath(path);
      if (null == nodeStatus) {
        throw new NoNodeException("Node does not exist: " + path);
      } else {
        return new String(client.getData().forPath(path));
      }
    } catch (Exception e) {
      log.debug(EXCEPTIONMSG, e);
    }
    return "";
  }

  private boolean isNoNodeException(KeeperException e) {
    return e.code() == Code.NONODE;
  }

  private void deleteRecursively(ZkTreeNode node) {
    deleteChildren(node);
    node.delete();
  }

  private void deleteChildren(ZkTreeNode node) {
    for (ZkTreeNode child : node.getChildren()) {
      deleteRecursively(child);
    }
  }

  public void injectUserDefineHdr() {

  }

  /**
   * @return true if the specified path is the root of this Zookeeper scheme.
   */
  private static final boolean isRoot(String path) {
    return PATH_SEPARATOR.equals(path);
  }

  public List<ZkNode> getTreeForRootByMaxLevel(int maxLevel) {
    List<ZkNode> out = new ArrayList<>();
    addZkTreeNode(this, 0, maxLevel, out);
    return out;
  }

  private void addZkTreeNode(ZkTreeNode node, int level, int maxLevel, List<ZkNode> out) {
    ZkNode zkNode = new ZkNode();

    node.hasData = dataIfExists(node);
    node.hasHeader = headerIfExists(node);
    node.hasSubNodes = subNodeIfExists(node, false);
    String userDefineHdr = this.userDefineHdrMap.get(node.getPath());
    node.userDefineHdr = userDefineHdr == null ? "" : userDefineHdr;

    zkNode.setNodePath(node.getName());
    zkNode.setHasData(node.hasData);
    zkNode.setHasHeader(node.hasHeader);
    zkNode.setHasSubNodes(node.hasSubNodes);
    zkNode.setUserDefineHdr(node.userDefineHdr);

    if (node.hasSubNodes) {
      zkNode.setSubNode(new ArrayList<>());
    } else {
      zkNode.setSubNode(Collections.emptyList());
    }

    // header 를 가지고 있고, node 명이 숫자로만 되어 있지 않은 경우, tree 구성
    // node.hasHeader && 헤더를 안 가지고 있는 노드가 많아서 일단 로직 제거
    if (!PatternUtils.isNumber(node.getName())) {
      out.add(zkNode);
    }

    if (node.hasSubNodes) {
      for (ZkTreeNode child : node.getChildren()) {
        if (level <= maxLevel) {
          addZkTreeNode(child, level + 1, maxLevel, zkNode.getSubNode());
        }
      }
    }
  }

  private boolean dataIfExists(ZkTreeNode node) {
    String data = node.getData();
    if (data != null && data.length() > 0 && !data.equals("null")) {
      return true;
    } else {
      return false;
    }
  }

  private boolean headerIfExists(ZkTreeNode node) {
    String data = node.getData();
    if (data != null && data.length() > 0 && !data.equals("null")) {
      String hdr = this.userDefineHdrMap.get(node.getPath());
      if (hdr != null && hdr.length() > 0) {
        return true;
      } else {
        if (data.length() > 2) {
          if (data.substring(0, 2).equals(HDR_PREFIX)) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
      }
    } else {
      return false;
    }
  }

  private boolean subNodeIfExists(ZkTreeNode node, boolean isDisplayFolder) {
    if (isDisplayFolder) {
      return node.getChildren().size() == 0 ? false : true;
    } else {
      int count = 0;
      for (ZkTreeNode child : node.getChildren()) {
        if (!PatternUtils.isNumber(child.getName())) {
          count++;
        }
      }
      if (count > 0) {
        return true;
      } else {
        return false;
      }
    }
  }

  /**
   * Convenience method to return a simple textual listing of the tree from this
   * node.
   */
  public String getListing(int maxLevel) {
    StringBuilder listing = new StringBuilder();
    addListing(listing, this, 0, maxLevel);
    return listing.toString();
  }

  private void addListing(StringBuilder listing, ZkTreeNode node, int tabLevel, int maxLevel) {
    if (!PatternUtils.isNumber(node.getName()) || (isToLeaf && PatternUtils.isNumber(node.getName()))) {

      if (isTabs) {
        listing.append(tabs(tabLevel)).append(node.getName());
      } else {
        listing.append("/").append(node.getName());
      }

      if (isDisplayData) {
        appendDataIfExists(listing, node);
        listing.append("\n");
      }
      for (ZkTreeNode child : node.getChildren()) {
        if (tabLevel < maxLevel) {
          addListing(listing, child, tabLevel + 1, maxLevel);
        }
      }
    }
  }

  private void appendDataIfExists(StringBuilder listing, ZkTreeNode node) {
    String data = node.getData();
    if (data != null && data.length() > 0) {
      listing.append(" [").append(node.getData()).append("]");
    }
  }

  private static StringBuilder tabs(int level) {
    StringBuilder sbul = new StringBuilder();
    for (int i = 0; i < level; i++)
      sbul.append("  ");
    return sbul;
  }

}
