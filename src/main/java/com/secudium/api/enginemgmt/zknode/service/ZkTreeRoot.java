package com.secudium.api.enginemgmt.zknode.service;

import java.io.Closeable;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.curator.framework.CuratorFramework;

public class ZkTreeRoot extends ZkTreeNode implements Closeable {

  private volatile boolean isOpen;

  /**
   * Constructor. Ensures that the root node exists.
   * 
   * @param zk the ZooKeeper client.
   */

  public ZkTreeRoot(CuratorFramework client, String rootPath, ConcurrentHashMap<String, String> userDefineHdrMap) {
    super(client, rootPath/* rootPath */, userDefineHdrMap);
    isOpen = true;
    ensureRootNode(rootPath);
  }

  /**
   * Makes sure that the root node exists. This allows clients the convenience of
   * automatically having a root node. If there is already one, this will just
   * back off silently.
   */
  private void ensureRootNode(String rootPath) {
    getOrCreatePath(rootPath);
  }

  /**
   * Closes the connection to Zookeeper. No more zookeeping for you!
   */
  public void close() {
    if (!isOpen) {
      throw new IllegalStateException("ZRoot is already closed.");
    }
    // client.close();
    isOpen = false;
  }

}
