package com.secudium.api.rulemgmt.dstrbt.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.rulemgmt.dstrbt.dto.RuleDistHistoryDto;
import com.secudium.api.rulemgmt.dstrbt.entity.RuleDistDetail;
import com.secudium.api.rulemgmt.dstrbt.entity.RuleDistHistory;
import com.secudium.api.rulemgmt.dstrbt.service.RuleDistHistoryService;
import com.secudium.common.dto.OkResponse;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/*
 * git commit test
 * @author intellicode
 *
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/rulemgmt/rule")
public class RuleDistHistoryController {

	private final RuleDistHistoryService ruleDistHistService;

	@GetMapping("/disthist/all")
	public ResponseEntity<List<RuleDistHistory>> getRuleDistHistories(HttpServletRequest request) {
		return ResponseEntity.ok(ruleDistHistService.getRuleDistHistories());
	}

	@GetMapping("/disthist/detail/latest")
	public ResponseEntity<List<RuleDistDetail>> getLatestRuleDistDetail(
			HttpServletRequest request, 
			Locale locale
			) {
		return ResponseEntity.ok(ruleDistHistService.getLatestRuleDistDetail(locale.getLanguage()));
	}

	@PostMapping("/disthist/detail")
	public ResponseEntity<List<RuleDistDetail>> getRuleDistDetail(
			@RequestBody @Valid RuleDistDetail dto,
			HttpServletRequest request, 
			Locale locale) {
		return ResponseEntity.ok(ruleDistHistService.getRuleDistDetail(dto.getRuleDstrbtHistSeq(), dto.getKeyword(), dto.getDiffYn(), locale.getLanguage()));
	}

	@PostMapping("/disthist/rollback")
	public ResponseEntity<OkResponse> rollbackDistHistory(@RequestBody RuleDistHistoryDto dto,
			@CurrentUser UserPrincipal currentUser, HttpServletRequest request) {
		ruleDistHistService.rollbackDistHistory(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}
}
