package com.secudium.api.rulemgmt.dstrbt.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.dstrbt.entity.RuleDistDetail;
import com.secudium.api.rulemgmt.dstrbt.entity.RuleDistHistory;
import com.secudium.api.rulemgmt.rule.dto.RuleUpdateDto;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class RuleDistHistoryDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public List<RuleDistHistory> getRuleDistHistories() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public List<RuleDistDetail> getRuleDistDetail(long ruleDstrbtHistSeq, String keyword, String diffYn, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("ruleDstrbtHistSeq", ruleDstrbtHistSeq);
		params.put("keyword", keyword);
		params.put("diffYn", diffYn);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<RuleDistDetail> getLatestRuleDistDetail(String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	/**
	 * 룰배포 이력생성
	 * @param vo
	 */
	public void insertRuleDistHistory(RuleDistHistory ruleDist) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), ruleDist);
	}

	/**
	 * 룰배포 이력 상세 생성
	 * @param ruleDist ruleDstrbtHistSeq
	 */
	public void insertRuleDistDetail(RuleDistHistory ruleDist) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), ruleDist);
	}

	public List<RuleUpdateDto> getRuleSeqByDistHistSeq(long ruleDstrbtHistSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), ruleDstrbtHistSeq);
	}

}
