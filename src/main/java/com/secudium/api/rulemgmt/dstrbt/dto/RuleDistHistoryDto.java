package com.secudium.api.rulemgmt.dstrbt.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.dstrbt.entity.RuleDistHistory;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class RuleDistHistoryDto {
	private long ruleDstrbtHistSeq;
	private String dstrbtCont;
	
	public RuleDistHistory toWithCurrentUser(UserPrincipal currentUser) {
		return RuleDistHistory.builder()
				.ruleDstrbtHistSeq(ruleDstrbtHistSeq)
				.dstrbtCont(dstrbtCont)
				.regUsrNo(currentUser.getUsrNo())
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}
}
