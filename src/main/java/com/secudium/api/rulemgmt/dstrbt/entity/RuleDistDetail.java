package com.secudium.api.rulemgmt.dstrbt.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class RuleDistDetail extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 8569305470015772284L;
	
	private long ruleDstrbtHistSeq;
	private long ruleHistSeq;
	private String diffYn;
	private String keyword;
	private long ruleSeq;
	private String ruleNm;
	private String ruleType;
	private String ruleChgReason;
}
