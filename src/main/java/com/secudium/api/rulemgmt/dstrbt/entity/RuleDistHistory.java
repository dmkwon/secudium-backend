package com.secudium.api.rulemgmt.dstrbt.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class RuleDistHistory extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 8569305470015772284L;
	
	private long ruleDstrbtHistSeq;
	private String dstrbtCont;
	private String isLatest;
}
