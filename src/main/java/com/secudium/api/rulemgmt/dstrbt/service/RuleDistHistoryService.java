package com.secudium.api.rulemgmt.dstrbt.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.rulemgmt.dstrbt.dao.RuleDistHistoryDao;
import com.secudium.api.rulemgmt.dstrbt.dto.RuleDistHistoryDto;
import com.secudium.api.rulemgmt.dstrbt.entity.RuleDistDetail;
import com.secudium.api.rulemgmt.dstrbt.entity.RuleDistHistory;
import com.secudium.api.rulemgmt.rule.dto.RuleUpdateDto;
import com.secudium.api.rulemgmt.rule.service.RuleHistoryService;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class RuleDistHistoryService {

	private final RuleDistHistoryDao ruleDistHistDao;
	private final RuleHistoryService ruleHistoryService;

	public List<RuleDistHistory> getRuleDistHistories() {
		return ruleDistHistDao.getRuleDistHistories();
	}

	public void rollbackDistHistory(RuleDistHistoryDto dto, UserPrincipal currentUser) {
		List<RuleUpdateDto> ruleSeqs = ruleDistHistDao.getRuleSeqByDistHistSeq(dto.getRuleDstrbtHistSeq());
		for(RuleUpdateDto vo : ruleSeqs) {
			//ruleHistSeq, ruleSeq, ruleChgReason
			vo.setRuleChgReason(dto.getDstrbtCont());
			ruleHistoryService.rollbackHistoryByRuleHistSeq(vo, currentUser);
		}
	}

	public List<RuleDistDetail> getRuleDistDetail(long ruleDstrbtHistSeq, String keyword, String diffYn, String locale) {
		return ruleDistHistDao.getRuleDistDetail(ruleDstrbtHistSeq, keyword, diffYn, locale);
	}

	public List<RuleDistDetail> getLatestRuleDistDetail(String locale) {
		return ruleDistHistDao.getLatestRuleDistDetail(locale);
	}
	
}
