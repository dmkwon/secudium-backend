/**
* COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
*
* 작 성 자 : ekb(intellicode)
* 작성일자 : 2019.03.05
* 설   명 : "소스 용도 및 기능 설명"
* ----------------------------------------------------------
* 2019.03.05 name "변경사항"
* ----------------------------------------------------------
*
*/

package com.secudium.api.rulemgmt.filtermgmt.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.rulemgmt.filtermgmt.dto.FilterCndtnDto;
import com.secudium.api.rulemgmt.filtermgmt.dto.FilterDto;
import com.secudium.api.rulemgmt.filtermgmt.dto.FilterGroupDto;
import com.secudium.api.rulemgmt.filtermgmt.dto.FilterInfoChgHistDto;
import com.secudium.api.rulemgmt.filtermgmt.dto.FilterInfoDto;
import com.secudium.api.rulemgmt.filtermgmt.service.FilterMgmtService;
import com.secudium.api.rulemgmt.rule.dto.RuleSelectDto;
import com.secudium.common.dto.OkResponse;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * filter 관리 기능을 제공하는 controller 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/filtermgmt")
public class FilterMgmtController {

    @Autowired
    private FilterMgmtService filterMgmtService;

    // filter group ---------------------------------------
    @GetMapping("/group")
    public List<FilterGroupDto> getFilterGroup(
            @RequestParam(value = "searchKeyWord", required = false, defaultValue = "") String keyword) {
        
        log.debug("getFilterGroup keyword= " + keyword);
        List<FilterGroupDto> list = filterMgmtService.selectFilterGroup(keyword);
        //log.debug("getFilterGroup idx=0, " + list.get(0));
        return list;
    }

    // 선택 그룹의 하위 데이터를 가져오는 api
    @GetMapping("/subgroup")
    public Map<String, Object> getFilterGroup(
            @RequestParam(value = "filterGroupSeq", required = true) Long filterGroupSeq) {

        //log.debug("## filterGroupSeq:" + filterGroupSeq);
        Map<String, Object> map = filterMgmtService.selectFilterGroupBySeq(filterGroupSeq);

        return map;
    }
    
    @GetMapping("/filter")
    public ResponseEntity<FilterInfoDto> getFilterByFilterSeq(
            HttpServletRequest request,
            @RequestParam(value = "filterSeq", required = true) long filterSeq) {
        return ResponseEntity.ok(filterMgmtService.selectFilterInfoBySeq(filterSeq));
    }
    
    @PostMapping("/group")
    public ResponseEntity<OkResponse> addFilterGroup(
            @RequestBody @Valid final FilterGroupDto filterGroupDto,
            @CurrentUser UserPrincipal currentUser) {

        //log.debug("## addFilterGroup:" + filterGroupDto);
        filterMgmtService.insertFilterGroup(filterGroupDto, currentUser);

        return ResponseEntity.ok(OkResponse.of());
    }

    @PostMapping("/group/update")
    public ResponseEntity<OkResponse> updateFilterGroup(
            @RequestBody @Valid final FilterGroupDto filterGroupDto,
            @CurrentUser UserPrincipal currentUser) {

        //log.debug("## updateFilterGroup:" + filterGroupDto);
        filterMgmtService.updateFilterGroup(filterGroupDto, currentUser);

        return ResponseEntity.ok(OkResponse.of());
    }

    @PostMapping("/group/del")
    public ResponseEntity<OkResponse> deleteFilterGroup(
            @RequestBody FilterGroupDto filterGroupDto,
            @CurrentUser UserPrincipal currentUser) {

        //log.debug("## updateFilterGroup:" + filterGroupDto);
        filterMgmtService.updateFilterGroup(filterGroupDto, currentUser);
        return ResponseEntity.ok(OkResponse.of());
    }

    // filter info ----------------------------------------
    @GetMapping("/filters")
    public List<FilterInfoDto> getFilters(
            @RequestParam(value = "searchKeyWord", required = false, defaultValue = "") String keyword, 
            @RequestParam(value = "sortKey", required = false, defaultValue = "") String sortKey, 
            @RequestParam(value = "sortType", required = false, defaultValue = "") String sortType,
            @CurrentUser UserPrincipal currentUser) {

        log.debug("getFilterGroup keyword= " + keyword);
        return filterMgmtService.selectFilterInfo(keyword, sortKey, sortType);
    }

    @PostMapping("/filter")
    public Map<String, Object> addFilter(
            @RequestBody @Valid final FilterDto filterWithCndtn,
            @CurrentUser UserPrincipal currentUser) {

        log.debug("addFilter ############" +  filterWithCndtn);

        return filterMgmtService.insertFilterInfo(filterWithCndtn.getFilter(), filterWithCndtn.getCndtn(), currentUser);
    }

    @PostMapping("/filter/update")
    public Map<String, Object> updateFilter(
            @RequestBody @Valid final FilterDto filterWithCndtn,
            @CurrentUser UserPrincipal currentUser) {
    
        log.debug("updateFilter ############" + filterWithCndtn);
        return filterMgmtService.updateFilterInfo(filterWithCndtn.getFilter(), filterWithCndtn.getCndtn(), currentUser);
    }

    // 하나도 여러 개도 하나의 API로 처리 함. 
    @PostMapping("/filterlist/update")
    public ResponseEntity<OkResponse> updateFilterList(
            @RequestBody @Valid final List<FilterInfoDto> filterInfoDtoList,
            @CurrentUser UserPrincipal currentUser) {
                
        filterMgmtService.updateFilterInfoList(filterInfoDtoList, currentUser);
        return ResponseEntity.ok(OkResponse.of());
    }


    // update 사용하고 실제 사용 안됨. 
    // @PostMapping("/filter/del")
    // public ResponseEntity<OkResponse> deleteFilter(
    //         @RequestBody @Valid final FilterInfoDto filterInfoDto,
    //         @CurrentUser UserPrincipal currentUser) {

    //     filterInfoDto.setUseYn('N');
    //     filterMgmtService.updateFilterInfo(filterInfoDto, currentUser);
    //     return ResponseEntity.ok(OkResponse.of());
    // }

    // filter change history ------------------------------
    @GetMapping("/filter/hist")
    public List<FilterInfoChgHistDto> getFilterHist(
            @RequestParam(value = "filterSeq", required = true) Long filterSeq) {
        
        return filterMgmtService.selectFilterInfoChgHistBySeq(filterSeq);
    }

    // filter cndtn change history ------------------------------
    @GetMapping("/filter/cndtn")
    public List<FilterCndtnDto> getFilterCndtn(
            @RequestParam(value = "filterSeq", required = true) Long filterSeq) {

        log.debug("getFilterCndtn: filterSeq=" + filterSeq);
        return filterMgmtService.selectFilterCndtnBySeq(filterSeq);
    }

    // @PostMapping("/cndtn")
    // public ResponseEntity<OkResponse> addFilterCndtnList(
    //         @RequestBody @Valid final List<FilterCndtnDto> filterCndtnList,
    //         @CurrentUser UserPrincipal currentUser) {

    //     log.debug("addFilterCndtnList #####" + filterCndtnList);
    //     filterMgmtService.insertFilterCndtnList(filterCndtnList, currentUser);

    //     return ResponseEntity.ok(OkResponse.of());
    // }

    @PostMapping("/filter/rule")
    public ResponseEntity<List<RuleSelectDto>> getRuleByRuleSeq(
            HttpServletRequest request,
            @RequestBody @Valid final Map<String, Long> seqMap) {
        
        log.debug("getRuleByRuleSeq #####" + seqMap);
        return ResponseEntity.ok(filterMgmtService.getRuleByRuleSeq(seqMap));
    }
}