package com.secudium.api.rulemgmt.filtermgmt.controller;


import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.rulemgmt.filtermgmt.entity.Agent;
import com.secudium.api.rulemgmt.filtermgmt.entity.AgentModel;
import com.secudium.api.rulemgmt.filtermgmt.entity.AgentModelTree;
import com.secudium.api.rulemgmt.filtermgmt.entity.AgentVendor;
import com.secudium.api.rulemgmt.filtermgmt.entity.Company;
import com.secudium.api.rulemgmt.filtermgmt.entity.CompanyTree;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterField;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterFieldTree;
import com.secudium.api.rulemgmt.filtermgmt.service.FilterPopupService;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * agent 자산 팝업 관리 기능을 제공하는 controller 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.04.09)
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/filtermgmt")
public class FilterPopupController {

	private final FilterPopupService filterPopupService;

	@GetMapping("/filterField")
	public ResponseEntity<PagedResponse<FilterField>> getFilterFields(
			HttpServletRequest request, 
			Pageable pageable, 
			Locale locale) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		log.debug("" + searchMap);
		return ResponseEntity.ok(filterPopupService.getFilterFields(pageable, searchMap, locale.getLanguage()));
	}

	@GetMapping("/filterFieldTree")
	public ResponseEntity<PagedResponse<FilterFieldTree>> getFilterFieldTrees(
			HttpServletRequest request,
			Pageable pageable, 
			Locale locale) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		log.debug("" + searchMap);
		return ResponseEntity.ok(filterPopupService.getFilterFieldTrees(pageable, searchMap, locale.getLanguage()));
	}

	@GetMapping("/company")
	public ResponseEntity<PagedResponse<Company>> getCompanys(HttpServletRequest request, Pageable pageable) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		log.debug("" + searchMap);
		return ResponseEntity.ok(filterPopupService.getCompanys(pageable, searchMap));
	}

	@GetMapping("/companyTree")
	public ResponseEntity<PagedResponse<CompanyTree>> getCompanyTrees(HttpServletRequest request, Pageable pageable) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		log.debug("" + searchMap);
		return ResponseEntity.ok(filterPopupService.getCompanyTrees(pageable, searchMap));
	}

	@GetMapping("/agent")
	public ResponseEntity<PagedResponse<Agent>> getAgents(HttpServletRequest request, Pageable pageable) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		log.debug("" + searchMap);
		return ResponseEntity.ok(filterPopupService.getAgents(pageable, searchMap));
	}

	@GetMapping("/agentVendor")
	public ResponseEntity<PagedResponse<AgentVendor>> getAgentVendors(HttpServletRequest request, Pageable pageable) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		log.debug("" + searchMap);
		return ResponseEntity.ok(filterPopupService.getAgentVendors(pageable, searchMap));
	}

	@GetMapping("/agentModel")
	public ResponseEntity<PagedResponse<AgentModel>> getAgentModels(HttpServletRequest request, Pageable pageable) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		log.debug("" + searchMap);
		return ResponseEntity.ok(filterPopupService.getAgentModels(pageable, searchMap));
	}

	@GetMapping("/agentModelTree")
	public ResponseEntity<PagedResponse<AgentModelTree>> getAgentModelTrees(HttpServletRequest request, Pageable pageable) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		log.debug("" + searchMap);
		return ResponseEntity.ok(filterPopupService.getAgentModelTrees(pageable, searchMap));
	}


}
