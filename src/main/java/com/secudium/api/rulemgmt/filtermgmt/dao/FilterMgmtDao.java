/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode)
 * 작성일자 : 2019.03.07
 * 설   명 : 필터 관리에 필요한 DAO(data access object) 구현
 * ----------------------------------------------------------
 * 2019.03.07 ekb initial dev.
 * (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.filtermgmt.entity.FilterCndtn;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterCndtnHist;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterGroup;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterInfo;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterInfoChgHist;
import com.secudium.api.rulemgmt.rule.dto.RuleSelectDto;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 필터 관리에 필요한 DAO(Data Access Object) class
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.07)
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class FilterMgmtDao extends RepositorySupport {

	private final SqlSession sqlSession; 

	// - control: tb_filter_group ---------------------------------------------
	public List<FilterGroup> selectFilterGroup(String keyword) {

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("keyword", keyword);

		List<FilterGroup> list = 
				sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);

		return list;
	}

	public List<FilterGroup> selectFilterGroupBySeq(Long filterGroupSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), filterGroupSeq);
	}

	public void insertFilterGroup(FilterGroup fg) {
		//log.debug("## insertFilterGroup:" + fg);
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), fg);
	}

	public void updateFilterGroup(FilterGroup fg) {
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), fg);
	}

	// - control: tb_filter_info ----------------------------------------------
	public List<FilterInfo> selectFilterInfo(String keyword, 
			String sortKey, String sortType) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("keyword", keyword);
		params.put("sortKey", sortKey);
		params.put("sortType", sortType);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public FilterInfo selectFilterInfoBySeq(Long filterSeq) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), filterSeq);
	}

	public List<FilterInfo> selectFiltersByFG(List<FilterGroup> groupList) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), groupList);
	}

	public void insertFilterInfo(FilterInfo filter) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), filter);
	}

	public void updateFilterInfo(FilterInfo filter) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), filter);
	}

	public void updateFilterInfoList(List<FilterInfo> filterList) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), filterList);
	}

	// - control: tb_filter_info_chg_hist -------------------------------------
	public List<FilterInfoChgHist> selectFilterInfoChgHistBySeq(Long filterSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), filterSeq);
	}

	public void insertFilterInfoChgHist(FilterInfoChgHist filterHist) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), filterHist);
	}

	public void insertFilterChangeHistList(List<FilterInfoChgHist> filterHistList) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), filterHistList);
	}

	public List<FilterCndtn> selectFilterCndtnBySeq(long filterSeq) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("filterSeq", filterSeq);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public void insertFilterCndtnList(List<FilterCndtn> list) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), list);
	}

	public void insertFilterCndtn(FilterCndtn cndtn) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), cndtn);
	}

	public void updateFilterCndtn(FilterCndtn cndtn) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), cndtn);
	}

	public void deleteFilterCndtn(FilterCndtn cndtn) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), cndtn);
	}

	public void insertFilterCntdnHistList(List<FilterCndtnHist> list) {
		sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), list);
	}

	public void insertFilterCntdnHist(FilterCndtnHist cndtnHist) {
		sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), cndtnHist);
	}

	public List<RuleSelectDto> getRuleByFilterSeq(Map<String, Long> seqMap) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), seqMap);
	}

	public List<FilterGroup> selectParentFilterGroupBySeq(long filterGroupSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), filterGroupSeq);
	}
}