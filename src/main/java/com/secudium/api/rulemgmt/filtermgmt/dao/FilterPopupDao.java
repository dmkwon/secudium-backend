/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode) 작성일자 : 2019.03.07 설 명 : 필터 관리에 필요한 DAO(data access object) 구현
 * ---------------------------------------------------------- 2019.03.07 ekb initial dev. (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.filtermgmt.entity.Agent;
import com.secudium.api.rulemgmt.filtermgmt.entity.AgentModel;
import com.secudium.api.rulemgmt.filtermgmt.entity.AgentModelTree;
import com.secudium.api.rulemgmt.filtermgmt.entity.AgentVendor;
import com.secudium.api.rulemgmt.filtermgmt.entity.Company;
import com.secudium.api.rulemgmt.filtermgmt.entity.CompanyTree;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterField;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterFieldTree;
import com.secudium.common.entity.SearchMap;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
// import com.secudium.api.enginemgmt.field.entity.Field;

/**
 * agent 자 관리에 필요한 DAO(Data Access Object) class
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.04.09)
 */
@Slf4j
@Repository
@RequiredArgsConstructor
public class FilterPopupDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<FilterField> selectFilterFields(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		params.put("lang", locale);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<FilterField> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		log.debug("list : " + list);
		return new PageImpl<>(list, pageable, total);
	}

	public Page<FilterFieldTree> selectFilterFieldTrees(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		params.put("lang", locale);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<FilterFieldTree> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		log.debug("list : " + list);
		return new PageImpl<>(list, pageable, total);
	}

	public Page<Agent> selectAgents(Pageable pageable, SearchMap search) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<Agent> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		log.debug("list : " + list);
		return new PageImpl<>(list, pageable, total);
	}

	public Page<Company> selectCompanys(Pageable pageable, SearchMap search) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<Company> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		log.debug("list : " + list);
		return new PageImpl<>(list, pageable, total);
	}

	public Page<CompanyTree> selectCompanyTrees(Pageable pageable, SearchMap search) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<CompanyTree> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		log.debug("list : " + list);
		return new PageImpl<>(list, pageable, total);
	}

	public Page<AgentVendor> selectAgentVendors(Pageable pageable, SearchMap search) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<AgentVendor> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		log.debug("list : " + list);
		return new PageImpl<>(list, pageable, total);
	}

	public Page<AgentModel> selectAgentModels(Pageable pageable, SearchMap search) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<AgentModel> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		log.debug("list : " + list);
		return new PageImpl<>(list, pageable, total);
	}

	public Page<AgentModelTree> selectAgentModelTrees(Pageable pageable, SearchMap search) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<AgentModelTree> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		log.debug("list : " + list);
		return new PageImpl<>(list, pageable, total);
	}
}
