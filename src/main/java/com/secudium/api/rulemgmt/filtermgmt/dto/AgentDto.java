/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode) 작성일자 : 2019.03.06 설 명 : 필터 관리 화면에서 사용할 필터 정보를 가지는 객체 구현
 * ---------------------------------------------------------- 2019.03.06 ekb initial dev. (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.dto;

import com.secudium.api.rulemgmt.filtermgmt.entity.Agent;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 장비 벤더를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.04.09)
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentDto {

    private long agentSeq;
    private long agentModelSeq;
    private long agentId;
    private long agentModelId;
    private String agentIp;
    private String publicIp;
    private String privateIp;
    private String agentHosts;
    private char nmsPublicYn;
    private String agentDesc;
    private char actFlag;
    private long custmId;
    private long zoneId;
    private long custmSeq;
    private char useYn;
    private String agentModelName;

    public Agent toEntity(UserPrincipal currentUser) {
        return Agent.builder().agentSeq(agentSeq).agentModelSeq(agentModelSeq).agentId(agentId).agentModelId(agentModelId).agentIp(agentIp)
                .publicIp(publicIp).privateIp(privateIp).agentHosts(agentHosts).nmsPublicYn(nmsPublicYn).agentDesc(agentDesc).actFlag(actFlag)
                .custmId(custmId).zoneId(zoneId).custmSeq(custmSeq).useYn(useYn).agentModelName(agentModelName)
                .regUsrNo(currentUser.getUsrNo())
                .modUsrNo(currentUser.getUsrNo())
                .build();
    }
}
