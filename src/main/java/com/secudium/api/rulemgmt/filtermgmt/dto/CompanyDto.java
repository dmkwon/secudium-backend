/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode) 작성일자 : 2019.03.06 설 명 : 필터 관리 화면에서 사용할 필터 정보를 가지는 객체 구현
 * ---------------------------------------------------------- 2019.03.06 ekb initial dev. (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.dto;

import com.secudium.api.rulemgmt.filtermgmt.entity.Company;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 장비 벤더를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.04.09)
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDto {

    private long companySeq;
    private long companyGroupSeq;
    private long companyId;
    private long companyGroupId;
    private String companyName;
    private String companyTypeCode;
    private String companyGroupTypeCode;
    private char activeYn;
    private char useYn;

    public Company toEntity(UserPrincipal currentUser) {
        return Company.builder().companySeq(companySeq).companyGroupSeq(companyGroupSeq).companyId(companyId).companyGroupId(companyGroupId)
                .companyName(companyName).companyTypeCode(companyTypeCode).companyGroupTypeCode(companyGroupTypeCode).activeYn(activeYn).useYn(useYn)
                .regUsrNo(currentUser.getUsrNo())
                .modUsrNo(currentUser.getUsrNo())
                .build();
    }
}
