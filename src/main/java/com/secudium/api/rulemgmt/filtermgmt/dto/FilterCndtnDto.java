/**
* COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
*
* 작 성 자 : ekb(intellicode)
* 작성일자 : 2019.03.06
* 설   명 : 필터 관리 화면에서 사용하는 필터 그룹 정보를 가지는 객체 구현
* ----------------------------------------------------------
* 2019.03.06 ekb initial dev.
* (변경시 추가)
* ----------------------------------------------------------
*/

package com.secudium.api.rulemgmt.filtermgmt.dto;

import java.util.List;

import com.secudium.api.rulemgmt.filtermgmt.entity.FilterCndtn;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterCndtnHist;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 필터 그룹 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilterCndtnDto {
    
    private long filterCndtnSeq;
    private long filterSeq;
    private long topFilterCndtnSeq;

    private String groupOprtr;

    private int fieldId;
    private char fieldType;
    private String field;
    private String oprtr;
    private String cndtnValue;

    private char keyFieldYn;

    private int depth;
    private int order;

    // private long regUsrid;
    // private String regDate;

    // DB 테이블과 상관없이, 구현을 위해 추가된 정보
    private List<FilterCndtnDto> sub;
    private long filterHistSeq;

    // new 'n', change 'c', delete 'd', 변경없음 original 'o'
    // 상태에 따라 n - insert, c/d - update, o - no action 
    private char changeState;

    public FilterCndtn toEntity(UserPrincipal currentUser) {
        return FilterCndtn.builder()
            .filterCndtnSeq(filterCndtnSeq)
            .filterSeq(filterSeq)
            .topFilterCndtnSeq(topFilterCndtnSeq)
            .groupOprtr(groupOprtr)
            .fieldId(fieldId)
            .fieldType(fieldType)
            .field(field)
            .oprtr(oprtr)
            .cndtnValue(cndtnValue)
            .keyFieldYn(keyFieldYn)
            .depth(depth)
            .order(order)
            .regUsrId(currentUser.getUsrNo())
            .build();
    }

    public FilterCndtnHist toHistEntity(UserPrincipal currentUser) {
        return FilterCndtnHist.builder()
            .filterCndtnSeq(filterCndtnSeq)
            .filterHistSeq(filterHistSeq)
            .filterSeq(filterSeq)
            .topFilterCndtnSeq(topFilterCndtnSeq)
            .groupOprtr(groupOprtr)
            .fieldId(fieldId)
            .fieldType(fieldType)
            .field(field)
            .oprtr(oprtr)
            .cndtnValue(cndtnValue)
            .keyFieldYn(keyFieldYn)
            .depth(depth)
            .order(order)
            .regUsrId(currentUser.getUsrNo())
            .build();
    }
}