/**
* COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
*
* 작 성 자 : ekb(intellicode)
* 작성일자 : 2019.03.06
* 설   명 : 필터 관리 화면에서 사용하는 필터 그룹 정보를 가지는 객체 구현
* ----------------------------------------------------------
* 2019.03.06 ekb initial dev.
* (변경시 추가)
* ----------------------------------------------------------
*/

package com.secudium.api.rulemgmt.filtermgmt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 필터 그룹 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilterCndtnHistDto {

    private long filterCndtnHistSeq;   
    private long filterHistSeq;

    private long filterCndtnSeq;   
    private long filterSeq;
    private long topFilterCndtnSeq;

    private String groupOprtr;

    private int fieldId;
    private char fieldType;
    private String field;
    private String oprtr;
    private String cndtnValue;

    private char keyFieldYn;

    private int depth;
    private int order;

    private long regUsrid;
    private String regDate;
}