/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode) 작성일자 : 2019.03.06 설 명 : 필터 관리 화면에서 사용할 필터 정보를 가지는 객체 구현
 * ---------------------------------------------------------- 2019.03.06 ekb initial dev. (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.dto;

import com.secudium.api.rulemgmt.filtermgmt.entity.FilterFieldTree;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 장비 벤더를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.04.09)
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilterFieldTreeDto {

    private String fieldGroupCode;
    private String fieldGroupName;

    public FilterFieldTree toEntity(UserPrincipal currentUser) {
        return FilterFieldTree.builder().fieldGroupCode(fieldGroupCode).fieldGroupName(fieldGroupName).regUsrNo(currentUser.getUsrNo())
                .modUsrNo(currentUser.getUsrNo()).build();
    }
}
