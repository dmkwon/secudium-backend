/**
* COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
*
* 작 성 자 : ekb(intellicode)
* 작성일자 : 2019.03.06
* 설   명 : 필터 관리 화면에서 사용하는 필터 그룹 정보를 가지는 객체 구현
* ----------------------------------------------------------
* 2019.03.06 ekb initial dev.
* (변경시 추가)
* ----------------------------------------------------------
*/

package com.secudium.api.rulemgmt.filtermgmt.dto;

import com.secudium.api.rulemgmt.filtermgmt.entity.FilterGroup;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * 필터 그룹 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */
@Getter @Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilterGroupDto {

    private long filterGroupSeq;
    private long topFilterGroupSeq;
    private char status;
    private String name;
    private int depth;
    private int order;
    private long bakFilterGroupSeq;

    private char useYn;
    private long regUsrid;
    private String regDate;
    private long modUsrid;
    private String modDate;

    // 필터관리 목록(grid)에 표시하기 위해 자료 구조 맞춤 
    private String filterNm = "-";
    private char type = 'G';
    private String periodInfo = "-";

    public void setUserYn(char isUse) {
        useYn = isUse;
    }

    public FilterGroup toEntity(UserPrincipal currentUser) {
        return FilterGroup.builder()
            .filterGroupSeq(filterGroupSeq).topFilterGroupSeq(topFilterGroupSeq)
            .status(status).name(name).depth(depth).order(order)
            .bakFilterGroupSeq(bakFilterGroupSeq)
            .useYn(useYn)
            .regUsrId(currentUser.getUsrNo()).modUsrId(currentUser.getUsrNo())
            .build();
    }
}