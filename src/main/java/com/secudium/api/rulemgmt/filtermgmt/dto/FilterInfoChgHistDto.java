/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode)
 * 작성일자 : 2019.03.06
 * 설   명 : 필터 정보 변경 사항에 대한 내용을 가지는 객체 구현
 * ----------------------------------------------------------
 * 2019.03.06 ekb initial dev.
 * (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 필터 변경 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */

@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
 public class FilterInfoChgHistDto {

    private long filterChgHistSeq;

    private long filterGroupSeq;
    private char status;
    private String filterGroupNmPath;

    private long filterSeq;
    private String filterNm;
    private String filterDesc;
    private String filterCndtnJson;

    private char filterCommitPeriodYn;
    private String filterCommitPeriodStrDay;
    private String filterCommitPeriodEndDay;
    private String filterIconImg;

    private String filterChgReason;

    private long no;
    private char useYn;
    private long regUsrId;
    private String regDate;

    private String regUsrName;
 }