/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode)
 * 작성일자 : 2019.03.06
 * 설   명 : 필터 관리 화면에서 사용할 필터 정보를 가지는 객체 구현
 * ----------------------------------------------------------
 * 2019.03.06 ekb initial dev.
 * (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.dto;

import java.util.Base64;

import com.secudium.api.rulemgmt.filtermgmt.entity.FilterInfo;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterInfoChgHist;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 필터 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
 public class FilterInfoDto {
 
     // 그룹정보
     private long filterGroupSeq;
     private char status;
     private String filterGroupNm;
    
    // 필터 정보
    private long filterSeq;
    private String name;
    private String filterDesc;
    private String filterCndtnJson;
    // 필터 정보 - 적용기간
    private char filterCommitPeriodYn;
    private String filterCommitPeriodStrDay;
    private String filterCommitPeriodEndDay;
    // 필터 정보 - 이미지
    private String filterIconImg;
    
    private char useYn;
    //private String regUsrid;
    //private String regDate;
    //private String modUsrid;
    //private String modDate;
    
    private int ruleCnt;
    
    // 필터관리 목록(grid)에 표시하기 위해 자료 구조 맞춤 
    private String filterChgReason; 
    private String filterGroupNmPath;
    private char type = 'F';

    private byte[] getBytesFromBase64() {
        if (filterIconImg == null) {
            return null;
        } else {
            return Base64.getDecoder().decode(filterIconImg);
        }
    }

    public FilterInfo toEntity(UserPrincipal currentUser) {
        return FilterInfo.builder()
            .filterGroupSeq(filterGroupSeq)
            .filterGroupNm(filterGroupNm)
            .status(status)
            .filterSeq(filterSeq)
            .name(name)
            .filterDesc(filterDesc)
            .filterCndtnJson(filterCndtnJson)
            .filterCommitPeriodYn(filterCommitPeriodYn)
            .filterCommitPeriodStrDay(filterCommitPeriodStrDay)
            .filterCommitPeriodEndDay(filterCommitPeriodEndDay)
            .filterIconImg(getBytesFromBase64())
            .useYn(useYn)
            .regUsrId(currentUser.getUsrNo())
            .modUsrId(currentUser.getUsrNo())
            .build();
        }

    public FilterInfoChgHist toChangeHistEntity(UserPrincipal currentUser) {
        return FilterInfoChgHist.builder()
            .filterSeq(filterSeq)
            .filterGroupSeq(filterGroupSeq)
            .filterGroupNmPath(filterGroupNmPath)
            .status(status)           
            .filterNm(name)
            .filterDesc(filterDesc)
            .filterCndtnJson(filterCndtnJson)
            .filterCommitPeriodYn(filterCommitPeriodYn)
            .filterCommitPeriodStrDay(filterCommitPeriodStrDay)
            .filterCommitPeriodEndDay(filterCommitPeriodEndDay)
            .filterIconImg(getBytesFromBase64())
            .filterChgReason(filterChgReason)
            .regUsrId(currentUser.getUsrNo())
            .build();
    }
}