/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode) 작성일자 : 2019.03.06 설 명 : 필터 관리 > 필터 정보
 * ---------------------------------------------------------- 2019.03.06 ekb initial dev. (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;



/**
 * 장비 벤더 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.04.09)
 */

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class CompanyTree extends BaseEntity implements Serializable {

       /**
        * serialVersionUID
        * @see
        */
    private static final long serialVersionUID = 8583937307470221408L;
    private long custmGroupSeq;
    private String custmGroupNm;

}
