/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode)
 * 작성일자 : 2019.03.06
 * 설   명 : 필터 관리 > 필터 정보
 * ----------------------------------------------------------
 * 2019.03.06 ekb initial dev.
 * (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;



/**
 * 필터 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = false)
public class FilterCndtn implements Serializable {

    private static final long serialVersionUID = 3223046116972988977L;
    
    private long filterCndtnSeq;
    private long filterSeq;
    private long topFilterCndtnSeq;

    private String groupOprtr;

    private int fieldId;
    private char fieldType;
    private String field;
    private String oprtr;
    private String cndtnValue;

    private char keyFieldYn;

    private int depth;
    private int order;

    private long regUsrId;
    private String regDate;
}