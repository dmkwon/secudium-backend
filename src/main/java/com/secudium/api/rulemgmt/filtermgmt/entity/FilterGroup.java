/**
* COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
*
* 작 성 자 : ekb(intellicode)
* 작성일자 : 2019.03.06
* 설   명 : 필터 관리 > 필터 그룹 정보
* ----------------------------------------------------------
* 2019.03.06 ekb initial dev.
* (변경시 추가)
* ----------------------------------------------------------
*/

package com.secudium.api.rulemgmt.filtermgmt.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * 필터 그룹 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */
@Getter @Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = false)
public class FilterGroup implements Serializable {

    private static final long serialVersionUID = 5758637753036868217L;
    
    private long filterGroupSeq;
    private long topFilterGroupSeq;
    private char status;
    private String name;
    private int depth;
    private int order;
    private long bakFilterGroupSeq;

    private char useYn;
    private long regUsrId;
	private String regDate;
	private long modUsrId;
	private String modDate;
}