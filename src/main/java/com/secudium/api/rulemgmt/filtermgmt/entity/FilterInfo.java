/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode)
 * 작성일자 : 2019.03.06
 * 설   명 : 필터 관리 > 필터 정보
 * ----------------------------------------------------------
 * 2019.03.06 ekb initial dev.
 * (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;



/**
 * 필터 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
public class FilterInfo implements Serializable {

    private static final long serialVersionUID = 1019677764361916454L;
    // 그룹정보
    private long filterGroupSeq;
    private char status;
    private String filterGroupNm;
    
    // 필터 정보
    private long filterSeq;
    private String name;
    private String filterDesc;
    private String filterCndtnJson;
    // 필터 정보 - 적용기간
    private char filterCommitPeriodYn;
    private String filterCommitPeriodStrDay;
    private String filterCommitPeriodEndDay;
    // 필터 정보 - 이미지
    private byte[] filterIconImg;

    private int ruleCnt;

    // 사용, 저장에 대한 정보
    private char useYn;
    private long regUsrId;
    //private String regDate;
    private long modUsrId;
    //private String modDate;

    // public FilterInfoDto toDto() {
    //     return FilterInfoDto.builder()
    //         .filterGroupSeq(filterGroupSeq).filterGroupNm(filterGroupNm).status(status)
    //         .filterSeq(filterSeq).name(name).filterDesc(filterDesc)
    //         .filterCndtnJson(filterCndtnJson)
    //         .filterCommitPeriodYn(filterCommitPeriodYn)
    //         .filterCommitPeriodStrDay(filterCommitPeriodStrDay)
    //         .filterCommitPeriodEndDay(filterCommitPeriodEndDay)
    //         .filterIconImg(filterIconImg)
    //         .build();
    // }
}