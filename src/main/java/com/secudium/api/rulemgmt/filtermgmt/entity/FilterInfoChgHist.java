/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode)
 * 작성일자 : 2019.03.06
 * 설   명 : 필터 관리 > 필터 정보 변경 이력 
 * ----------------------------------------------------------
 * 2019.03.06 ekb initial dev.
 * (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;


/**
 * 필터 변경 정보를 가지는 클래스
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.06)
 */
@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ToString
public class FilterInfoChgHist implements Serializable {

    private static final long serialVersionUID = 3673466232479119604L;

    private long filterChgHistSeq;

    private long filterGroupSeq;
    private char status;
    private String filterGroupNmPath;

    private long filterSeq;
    private String filterNm;
    private String filterDesc;
    private String filterCndtnJson;

    private char filterCommitPeriodYn;
    private String filterCommitPeriodStrDay;
    private String filterCommitPeriodEndDay;
    private byte[] filterIconImg;

    private String filterChgReason;
    
    private long no;
    private char useYn;
    private long regUsrId;
    private String regDate;

    private String regUsrName;
}