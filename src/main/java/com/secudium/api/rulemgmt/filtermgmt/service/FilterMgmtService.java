/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode)
 * 작성일자 : 2019.03.08
 * 설   명 : filter 관리에 사용할 서비스 객체 구현
 * ----------------------------------------------------------
 * 2019.03.08 ekb initial dev.
 * (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.rulemgmt.filtermgmt.dao.FilterMgmtDao;
import com.secudium.api.rulemgmt.filtermgmt.dto.FilterCndtnDto;
import com.secudium.api.rulemgmt.filtermgmt.dto.FilterGroupDto;
import com.secudium.api.rulemgmt.filtermgmt.dto.FilterInfoChgHistDto;
import com.secudium.api.rulemgmt.filtermgmt.dto.FilterInfoDto;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterCndtn;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterGroup;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterInfo;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterInfoChgHist;
import com.secudium.api.rulemgmt.rule.dto.RuleSelectDto;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 필터 관리에 사용할 서비스 객체
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.03.08)
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class FilterMgmtService {

    private final FilterMgmtDao filterDao;

    // - control: tb_filter_group ---------------------------------------------
    public List<FilterGroupDto> selectFilterGroup(String keyword) {
        List<FilterGroup> _inList = filterDao.selectFilterGroup(keyword);
        List<FilterGroupDto> _outList = new ArrayList<>();

        for(FilterGroup _in : _inList) {
            FilterGroupDto _out = new FilterGroupDto();
            BeanUtils.copyProperties(_in, _out);
            _outList.add(_out);
        }
        return _outList; 
    }

    // 특정 필터그룹 아래 그룹과 필터 정보까지 가져오도록 구현 
    public Map<String, Object>selectFilterGroupBySeq(Long filterGroupSeq) {

        List<FilterGroup> _inFGroupList = filterDao.selectFilterGroupBySeq(filterGroupSeq);
        List<FilterGroupDto> _outList = new ArrayList<>();

        for(FilterGroup _in : _inFGroupList) {
            FilterGroupDto _out = new FilterGroupDto();
            BeanUtils.copyProperties(_in, _out);
            _outList.add(_out);
        }

        List<FilterInfo> _inFilters = filterDao.selectFiltersByFG(_inFGroupList);
        List<FilterInfoDto> _outFList = new ArrayList<>();

        for(FilterInfo _in : _inFilters) {
            FilterInfoDto _out = new FilterInfoDto();
            BeanUtils.copyProperties(_in, _out);
            if(_in.getFilterIconImg() != null) {
                _out.setFilterIconImg(Base64.getEncoder().encodeToString(_in.getFilterIconImg()));
            };
            _outFList.add(_out);
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("filtergroup", _outList);
        map.put("filterList", _outFList);

        return map;
    }

    public void insertFilterGroup(FilterGroupDto dto, UserPrincipal currentUser) {
        filterDao.insertFilterGroup(dto.toEntity(currentUser));
    }

    public void updateFilterGroup(FilterGroupDto dto, UserPrincipal currentUser) {
        filterDao.updateFilterGroup(dto.toEntity(currentUser));
    }

    // - control: tb_filter_info ----------------------------------------------
    public List<FilterInfoDto> selectFilterInfo(String keyword, 
        String sortKey, String sortType) { // TODO_ekb sorting, paging, searching 기능추가 필요
    
        List<FilterInfo> _inList = filterDao.selectFilterInfo(keyword, sortKey, sortType);
        List<FilterInfoDto> _outList = new ArrayList<>();

        for(FilterInfo _in : _inList) {
            FilterInfoDto _out = new FilterInfoDto();
            BeanUtils.copyProperties(_in, _out);
            if(_in.getFilterIconImg() != null) {
                _out.setFilterIconImg(Base64.getEncoder().encodeToString(_in.getFilterIconImg()));
            };
            _outList.add(_out);
        }

        return _outList;
    }
    
    public FilterInfoDto selectFilterInfoBySeq(Long filterSeq) {
        FilterInfo _in = filterDao.selectFilterInfoBySeq(filterSeq);
        /* 필터그룹명경로 추가 */
        List<FilterGroup> filterGroups = filterDao.selectParentFilterGroupBySeq(_in.getFilterGroupSeq());
        StringBuilder str = new StringBuilder();
        for(FilterGroup fg : filterGroups) {
            if(str.length() > 0) {
                str.append(">");
            }
            str.append(fg.getName());
        }
        
        FilterInfoDto _out = new FilterInfoDto();
        _out.setFilterGroupNmPath(str.toString());

        BeanUtils.copyProperties(_in, _out);
        if(_in.getFilterIconImg() != null) {
            _out.setFilterIconImg(Base64.getEncoder().encodeToString(_in.getFilterIconImg()));
        };

        return _out;
    }

    public Map<String, Object> insertFilterInfo(
            FilterInfoDto dto, List<FilterCndtnDto> cndtn, UserPrincipal currentUser) {

        // 필터 insert 
        FilterInfo filterInfo = dto.toEntity(currentUser);
        filterDao.insertFilterInfo(filterInfo);
        
        // insert뒤에 반환된 filterSeq를 사용해서 
        long filterSeq = filterInfo.getFilterSeq();
        dto.setFilterSeq(filterSeq);
        
        // 필터 history 
        FilterInfoChgHist filterHist = dto.toChangeHistEntity(currentUser);
        filterDao.insertFilterInfoChgHist(filterHist);

        long filterHistSeq = filterHist.getFilterChgHistSeq();
        
        // 필터 조건 저장 
        if (cndtn != null) {
            recursiveInsert(cndtn, currentUser, 0, filterSeq, filterHistSeq);
        }

        // return 값을 map에 저장해서 전달
        log.debug("insertFilterInfo #####" + filterSeq, filterHistSeq);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("filterSeq", filterSeq);
        map.put("filterHistSeq", filterHistSeq);

        return map;
    }

    public Map<String, Object> updateFilterInfo(
            FilterInfoDto dto, List<FilterCndtnDto> cndtn, UserPrincipal currentUser) {
        
        // 필터 정보 upate 
        filterDao.updateFilterInfo(dto.toEntity(currentUser));

        // history 생성하고 seq 받아옴
        FilterInfoChgHist filterHist = dto.toChangeHistEntity(currentUser);
        filterDao.insertFilterInfoChgHist(filterHist);
        long filterHistSeq = filterHist.getFilterChgHistSeq();
        
        if (cndtn != null) {
            recursiveInsert(cndtn, currentUser, 0, dto.getFilterSeq(), filterHistSeq);
        }

        log.debug("updateFilterInfo #####" + filterHistSeq + "," + cndtn);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("filterHistSeq", filterHistSeq);

        return map;
    }

    public void updateFilterInfoList(List<FilterInfoDto> dtoList, UserPrincipal currentUser) {
        List<FilterInfo> _ftlist = new ArrayList<>();
        List<FilterInfoChgHist> _ftHistList = new ArrayList<>();
        for(FilterInfoDto _in : dtoList) {
            _ftlist.add(_in.toEntity(currentUser));
            _ftHistList.add(_in.toChangeHistEntity(currentUser));
            //log.debug("updateFilterInfoList #####" + _in);
        }

        filterDao.updateFilterInfoList(_ftlist);
        filterDao.insertFilterChangeHistList(_ftHistList);
    }

    // - control: tb_filter_info_hist -------------------------------------    
    public List<FilterInfoChgHistDto> selectFilterInfoChgHistBySeq(Long filterSeq) {
        List<FilterInfoChgHist> _inList = filterDao.selectFilterInfoChgHistBySeq(filterSeq);
        List<FilterInfoChgHistDto> _outList = new ArrayList<>();

        for(FilterInfoChgHist _in : _inList) {
            FilterInfoChgHistDto _out = new FilterInfoChgHistDto();
            BeanUtils.copyProperties(_in, _out);
            if(_in.getFilterIconImg() != null) {
                _out.setFilterIconImg(Base64.getEncoder().encodeToString(_in.getFilterIconImg()));
            };
            _outList.add(_out);
        }
        return _outList;
    }

    // - control: tb_filter_cndtn ------------------------------------- 
    public List<FilterCndtnDto> selectFilterCndtnBySeq(long filterSeq) { 

        List<FilterCndtn> _inList = filterDao.selectFilterCndtnBySeq(filterSeq);
        List<FilterCndtnDto> _outList = new ArrayList<>();

        for(FilterCndtn _in : _inList) {
            FilterCndtnDto _out = new FilterCndtnDto();
            BeanUtils.copyProperties(_in, _out);
            _outList.add(_out);
        }

        return _outList;
    }

    private void recursiveInsert( List<FilterCndtnDto> filterCndtnList, 
            UserPrincipal currentUser, long tobCndtnSeq, long filterSeq, long filterHistSeq) {

        //log.debug("recursiveInsert ##### IN=" + filterCndtnList + ", " + tobCndtnSeq + ", " + filterSeq + ", "+ filterHistSeq);
        for(FilterCndtnDto _inFtCndtn : filterCndtnList) {
            long newTobCndtnSeq = _inFtCndtn.getFilterCndtnSeq();

            log.debug("recursiveInsert ##### [IN] state=" + _inFtCndtn.getChangeState() + 
                    ", newTobCndtnSeq=" + newTobCndtnSeq + ", cndtnData=" + _inFtCndtn);

            if(tobCndtnSeq != 0)_inFtCndtn.setTopFilterCndtnSeq(tobCndtnSeq);
            if(filterSeq != 0) _inFtCndtn.setFilterSeq(filterSeq);
            if(filterHistSeq != 0) _inFtCndtn.setFilterHistSeq(filterHistSeq);

            // o - original 변경없음 / c - changed 변경 / n - new 생성 / d - delect 삭제
            // cndtn history는 현재 상태를 그대로 반영하고 있음.
            if (_inFtCndtn.getChangeState() == 'o') { 
                // 변경 사항 없음.
                filterDao.insertFilterCntdnHist(_inFtCndtn.toHistEntity(currentUser));
            } else { 
                
                FilterCndtn cndtn = _inFtCndtn.toEntity(currentUser);
                if (_inFtCndtn.getFilterCndtnSeq() == 0 || _inFtCndtn.getChangeState() == 'n') {
                    filterDao.insertFilterCndtn(cndtn); // 신규 등록
                    filterDao.insertFilterCntdnHist(_inFtCndtn.toHistEntity(currentUser));
                } else if (_inFtCndtn.getChangeState() == 'c') {
                    filterDao.updateFilterCndtn(cndtn); // 변경
                    filterDao.insertFilterCntdnHist(_inFtCndtn.toHistEntity(currentUser));
                } else if (_inFtCndtn.getChangeState() == 'd') {
                    filterDao.deleteFilterCndtn(cndtn); // 삭제
                    //filterDao.insertFilterCntdnHist(_inFtCndtn.toHistEntity(currentUser));
                }

                newTobCndtnSeq = cndtn.getFilterCndtnSeq(); // 다음 서브의 tobCndtnSeq가 됨
                log.debug("recursiveInsert ##### [after histSave] OUT=" + newTobCndtnSeq);
            }

            if(_inFtCndtn.getSub() != null) {
                recursiveInsert(_inFtCndtn.getSub(), currentUser, newTobCndtnSeq, filterSeq, filterHistSeq);
            }
        }
    }

    public void insertFilterCndtnList(
            List<FilterCndtnDto> filterCndtnList, UserPrincipal currentUser) {

            recursiveInsert(filterCndtnList, currentUser, 0, 0, 0);
    }

    public List<RuleSelectDto> getRuleByRuleSeq(Map<String, Long> seqMap) {
        return filterDao.getRuleByFilterSeq(seqMap);
    }

}