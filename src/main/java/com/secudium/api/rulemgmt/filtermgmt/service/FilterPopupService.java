/**
 * COPYRIGHT 2019 SK INFOSEC CO., LTD. ALL RIGHTS RESERVED.
 *
 * 작 성 자 : ekb(intellicode) 작성일자 : 2019.03.08 설 명 : filter 관리에 사용할 서비스 객체 구현
 * ---------------------------------------------------------- 2019.03.08 ekb initial dev. (변경시 추가)
 * ----------------------------------------------------------
 *
 */

package com.secudium.api.rulemgmt.filtermgmt.service;

import java.util.Collections;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.rulemgmt.filtermgmt.dao.FilterPopupDao;
import com.secudium.api.rulemgmt.filtermgmt.entity.Agent;
import com.secudium.api.rulemgmt.filtermgmt.entity.AgentModel;
import com.secudium.api.rulemgmt.filtermgmt.entity.AgentModelTree;
import com.secudium.api.rulemgmt.filtermgmt.entity.AgentVendor;
import com.secudium.api.rulemgmt.filtermgmt.entity.Company;
import com.secudium.api.rulemgmt.filtermgmt.entity.CompanyTree;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterField;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterFieldTree;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;

import lombok.RequiredArgsConstructor;

/**
 * 필터 관리에 사용할 서비스 객체
 *
 * @author ekb(intellicode)
 * @version 0.1 (2019.04.09)
 */
@Service
@Transactional
@RequiredArgsConstructor
public class FilterPopupService {

	private final FilterPopupDao filterPopupDao;

	public PagedResponse<FilterField> getFilterFields(Pageable pageable, SearchMap search, String locale) {
		Page<FilterField> page = filterPopupDao.selectFilterFields(pageable, search, locale);
		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		}
	}

	public PagedResponse<FilterFieldTree> getFilterFieldTrees(Pageable pageable, SearchMap search, String locale) {
		Page<FilterFieldTree> page = filterPopupDao.selectFilterFieldTrees(pageable, search, locale);
		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		}
	}

	public PagedResponse<AgentModel> getAgentModels(Pageable pageable, SearchMap search) {
		Page<AgentModel> page = filterPopupDao.selectAgentModels(pageable, search);
		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		}
	}

	public PagedResponse<AgentModelTree> getAgentModelTrees(Pageable pageable, SearchMap search) {
		Page<AgentModelTree> page = filterPopupDao.selectAgentModelTrees(pageable, search);
		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		}
	}

	public PagedResponse<AgentVendor> getAgentVendors(Pageable pageable, SearchMap search) {
		Page<AgentVendor> page = filterPopupDao.selectAgentVendors(pageable, search);
		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		}
	}

	public PagedResponse<Agent> getAgents(Pageable pageable, SearchMap search) {
		Page<Agent> page = filterPopupDao.selectAgents(pageable, search);
		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		}
	}

	public PagedResponse<Company> getCompanys(Pageable pageable, SearchMap search) {
		Page<Company> page = filterPopupDao.selectCompanys(pageable, search);
		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		}
	}

	public PagedResponse<CompanyTree> getCompanyTrees(Pageable pageable, SearchMap search) {
		Page<CompanyTree> page = filterPopupDao.selectCompanyTrees(pageable, search);
		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements(), page.getTotalPages(),
					page.isLast());
		}
	}

}
