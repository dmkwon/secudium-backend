package com.secudium.api.rulemgmt.nmlzrule.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleDeleteDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleHistDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleInsertDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleRollBackDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleUpdateDto;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRule;
import com.secudium.api.rulemgmt.nmlzrule.service.NmlzRuleService;
import com.secudium.common.dto.OkResponse;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/rulemgmt")
public class NmlzRuleController {

	private final NmlzRuleService nmlzRuleService;

	/**
	 * 룰 목록 가져오는 Api (agent, model, sc) 
	 * 룰 수정하는 Api 
	 * 룰 삭제하는 Api 
	 * 룰 검색하는 Api (벤더명, 장비유형, 모델명 like 검색 or)
	 */

	@GetMapping("/nmlzrules")
	public ResponseEntity<PagedResponse<NmlzRule>> getNmlzRules(
			HttpServletRequest request, 
			Pageable pageable, 
			Locale locale) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		return ResponseEntity.ok(nmlzRuleService.getNmlzRules(pageable, searchMap, locale.getLanguage()));
	}

	/**
	 * nmlzRuleSeq 에 의한 rule 정보 가져오는 API 
	 */
	@GetMapping("/nmlzrule")
	public ResponseEntity<NmlzRuleDto> getNmlzRuleBySeq(
			@RequestParam("nmlzRuleSeq") long nmlzRuleSeq,
			Locale locale) {
		return ResponseEntity.ok(nmlzRuleService.getNmlzRuleBySeq(nmlzRuleSeq, locale.getLanguage()));
	}


	/**
	 * rule 생성하는 API 
	 */
	@PostMapping("/nmlzrules")
	public ResponseEntity<OkResponse> createNmlzRule(
			@RequestBody NmlzRuleInsertDto dto,
			@CurrentUser UserPrincipal currentUser,
			Locale locale
			) {
		nmlzRuleService.createNmlzRule(dto, currentUser, locale.getLanguage());
		return ResponseEntity.ok(OkResponse.of());
	}

	/**
	 * rule 수정하는 API 
	 */
	@PutMapping("/nmlzrules")
	public ResponseEntity<OkResponse> updateNmlzRule(
			@RequestBody NmlzRuleUpdateDto dto, 
			@CurrentUser UserPrincipal currentUser, 
			Locale locale) {
		nmlzRuleService.updateNmlzRule(dto, currentUser, locale.getLanguage());
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/nmlzrules/delete")
	public ResponseEntity<OkResponse> removeOneNmlzRule(
			@RequestBody NmlzRuleDeleteDto dto,
			@CurrentUser UserPrincipal currentUser, 
			Locale locale) {
		nmlzRuleService.removeNmlzRule(dto, currentUser, locale.getLanguage());
		return ResponseEntity.ok(OkResponse.of());
	}

	@GetMapping("/nmlzrules/hists")
	public ResponseEntity<PagedResponse<NmlzRuleHistDto>> getNmlzRuleHists(
			@RequestParam("nmlzRuleSeq") String nmlzRuleSeq
			,Pageable pageable) {
		return ResponseEntity.ok(nmlzRuleService.getNmlzRuleHists(Long.parseLong(nmlzRuleSeq), pageable));
	}

	@GetMapping("/nmlzrules/hist")
	public ResponseEntity<NmlzRuleHistDto> getNmlzRuleHistsByNmlzRuleHistSeq(
			@RequestParam("nmlzRuleHistSeq") Long nmlzRuleHistSeq,
			Locale locale
			) {
		return ResponseEntity.ok(nmlzRuleService.getNmlzRuleHistsByNmlzRuleHistSeq(nmlzRuleHistSeq, locale.getLanguage()));
	}

	@PostMapping("/nmlzrules/rollback")
	public ResponseEntity<OkResponse> rollbackNmlzRule(
			@RequestBody NmlzRuleRollBackDto dto,
			@CurrentUser UserPrincipal currentUser, 
			Locale locale) {
		nmlzRuleService.rollbackNmlzRule(dto, currentUser, locale.getLanguage());
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/nmlzrules/deploy")
	public ResponseEntity<OkResponse> deployNmlzRule(
			@CurrentUser UserPrincipal currentUser) {
		nmlzRuleService.deployNmlzRule(currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}


}
