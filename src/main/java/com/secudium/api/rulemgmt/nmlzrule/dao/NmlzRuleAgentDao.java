package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleAgent;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleAgentDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<NmlzRuleAgent> selectNmlzRuleAgents(Long nmlzRuleSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleSeq", nmlzRuleSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleAgent> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleAgent> selectAllNmlzRuleAgents(String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<NmlzRuleAgent> selectNmlzRuleAgentsByNmlzRuleSeq(Long nmlzRuleSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleSeq", nmlzRuleSeq);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	// nmlzRule 기준으로 여러개 들어감
	public int insertNmlzRuleAgent(NmlzRuleAgent nmlzRuleAgent) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleAgent);
	}

	// nmlzRule 이 업데이트되면 기존 데이터 제거, 삭제 시에는 db 데이터 삭제
	public int deleteNmlzRuleAgentByNmlzRuleSeq(Long nmlzRuleSeq) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleSeq);
	}
}
