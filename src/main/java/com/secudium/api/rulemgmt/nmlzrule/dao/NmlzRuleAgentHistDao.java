package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleAgentHist;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleAgentHistDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<NmlzRuleAgentHist> selectNmlzRuleAgentHists(Long nmlzRuleHistSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleHistSeq", nmlzRuleHistSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleAgentHist> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleAgentHist> selectNmlzRuleAgentHistsByNmlzRuleHistSeq(Long nmlzRuleHistSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleHistSeq", nmlzRuleHistSeq);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);		
	}

	public int insertBulkNmlzRuleAgentHist(List<NmlzRuleAgentHist> nmlzRuleAgentHists) {
		Map<String, Object> params = new HashMap<>();
		params.put("list", nmlzRuleAgentHists);
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	// nmlzRuleHist 기준으로 여러개 들어감
	public int insertNmlzRuleAgentHist(NmlzRuleAgentHist nmlzRuleAgentHist) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleAgentHist);
	}
}
