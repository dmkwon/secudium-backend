package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleCont;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkCont;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkTokenize;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleContDao extends RepositorySupport {

	private final SqlSession sqlSession;

	/**
	 * 목록 가져오기 
	 */
	public Page<NmlzRuleCont> selectNmlzRuleConts(Long nmlzRuleSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleSeq", nmlzRuleSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleCont> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleZkCont> selectAllNmlzRuleZkConts() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public List<NmlzRuleZkTokenize> selectAllNmlzRuleZkTokenize() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}


	public List<NmlzRuleCont> selectNmlzRuleContsByNmlzRuleHdrSeq(Long nmlzRuleHdrSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleHdrSeq);
	}

	public List<NmlzRuleCont> selectNmlzRuleContsByNmlzRuleSeq(Long nmlzRuleSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleSeq);
	}

	/**
	 * 추가하기
	 */
	public int insertNmlzRuleCont(NmlzRuleCont nmlzRuleCont) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleCont);
	}

	/**
	 * nmlzRuleSeq 로 삭제하기
	 */
	public int deleteNmlzRuleContByNmlzRuleSeq(Long nmlzRuleSeq) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleSeq);
	}

	/**
	 * nmlzRuleContSeq 로 삭제하기
	 */
	public int deleteNmlzRuleContBySeq(Long nmlzRuleContSeq) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleContSeq);
	}

	/**
	 * 컨텐츠 룰 존재 여부 
	 */
	public boolean isExistNmlzRuleCont(Long nmlzRuleContSeq) {
		return (Integer) this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleContSeq) > 0 ?
				true : false;
	}
}
