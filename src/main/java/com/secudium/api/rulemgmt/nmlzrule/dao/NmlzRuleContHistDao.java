package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleContHist;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleContHistDao extends RepositorySupport {

	private final SqlSession sqlSession;

	/**
	 * nmlzRuleHistSeq 에 의해 nmlzRuleContHist 목록을 가져온다.
	 * @param nmlzRuleHistSeq
	 * @param pageable
	 * @return
	 */
	public Page<NmlzRuleContHist> selectNmlzRuleContHists(Long nmlzRuleHistSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleHistSeq", nmlzRuleHistSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleContHist> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleContHist> selectNmlzRuleContHistsByNmlzRuleHdrHistSeq(Long nmlzRuleHdrHistSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleHdrHistSeq);		
	}

	/**
	 * nmlzRuleCont history 를 남긴다.
	 * @param nmlzRuleContHist
	 * @return
	 */
	public int insertNmlzRuleContHist(NmlzRuleContHist nmlzRuleContHist) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleContHist);
	}
}
