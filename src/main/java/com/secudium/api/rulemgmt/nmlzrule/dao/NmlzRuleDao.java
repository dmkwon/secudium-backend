package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRule;
import com.secudium.common.entity.SearchMap;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<NmlzRule> selectNmlzRules(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		params.put("lang", locale);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRule> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRule> selectAllNmlzRules() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public int selectCountNmlzRuleByName(String nmlzRuleNm, Long nmlzRuleSeq) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleSeq", nmlzRuleSeq);
		params.put("nmlzRuleNm", nmlzRuleNm);
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public NmlzRule selectNmlzRuleBySeq(Long nmlzRuleSeq) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleSeq);
	}

	public int insertNmlzRule(NmlzRule nmlzRule) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRule);
	}

	public int updateNmlzRule(NmlzRule nmlzRule) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRule);
	}

	public int deleteNmlzRuleBySeq(Long nmlzRuleSeq) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleSeq);
	}
}
