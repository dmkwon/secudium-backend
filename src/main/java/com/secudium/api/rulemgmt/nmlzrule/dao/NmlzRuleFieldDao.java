package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleField;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkArrange;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkField;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleFieldDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<NmlzRuleField> selectNmlzRuleFields(Long nmlzRuleContSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleContSeq", nmlzRuleContSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleField> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleZkField> selectAllNmlzRuleZkFields() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public List<NmlzRuleZkArrange> selectAllNmlzRuleZkArrange() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public List<NmlzRuleField> selectNmlzRuleFieldsByNmlzRuleContSeq(Long nmlzRuleContSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleContSeq);
	}

	public int insertNmlzRuleField(NmlzRuleField nmlzRuleField) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleField);
	}

	public int deleteNmlzRuleFieldByNmlzRuleContSeq(Long nmlzRuleContSeq) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleContSeq);
	}
}
