package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldFunc;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkFieldFunc;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleFieldFuncDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<NmlzRuleFieldFunc> selectNmlzRuleFieldFuncs(Long nmlzRuleContSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleContSeq", nmlzRuleContSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleFieldFunc> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleZkFieldFunc> selectAllNmlzRuleZkFieldFuncs(String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<NmlzRuleFieldFunc> selectNmlzRuleFieldFuncsByNmlzRuleContSeq(Long nmlzRuleContSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleContSeq", nmlzRuleContSeq);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public int insertNmlzRuleFieldFunc(NmlzRuleFieldFunc nmlzRuleFieldFunc) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleFieldFunc);
	}

	public int deleteNmlzRuleFieldFuncByNmlzRuleContSeq(Long nmlzRuleContSeq) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleContSeq);
	}
}
