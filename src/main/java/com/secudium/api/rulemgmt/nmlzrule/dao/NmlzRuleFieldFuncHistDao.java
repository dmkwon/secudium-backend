package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldFuncHist;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleFieldFuncHistDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<NmlzRuleFieldFuncHist> selectNmlzRuleFields(Long nmlzRuleContHistSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleContHistSeq", nmlzRuleContHistSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleFieldFuncHist> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleFieldFuncHist> selectNmlzRuleFieldFuncHistsByNmlzRuleContHistSeq(Long nmlzRuleContHistSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleContHistSeq", nmlzRuleContHistSeq);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);		
	}

	public int insertNmlzRuleFieldFuncHist(NmlzRuleFieldFuncHist nmlzRuleFieldFuncHist) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleFieldFuncHist);
	}
}
