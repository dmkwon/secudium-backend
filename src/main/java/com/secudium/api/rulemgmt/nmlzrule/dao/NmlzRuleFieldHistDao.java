package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldHist;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleFieldHistDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<NmlzRuleFieldHist> selectNmlzRuleFieldHists(Long nmlzRuleContHistSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleContHistSeq", nmlzRuleContHistSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleFieldHist> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleFieldHist> selectNmlzRuleFieldHistsByNmlzRuleContHistSeq(Long nmlzRuleContHistSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleContHistSeq);		
	}

	// nmlzRule 기준으로 여러개 들어감
	public int insertNmlzRuleFieldHist(NmlzRuleFieldHist nmlzRuleFieldHist) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleFieldHist);
	}
}
