package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldMerge;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkFieldMerge;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleFieldMergeDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<NmlzRuleFieldMerge> selectNmlzRuleFieldMerges(Long nmlzRuleContSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleContSeq", nmlzRuleContSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleFieldMerge> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleZkFieldMerge> selectAllNmlzRuleZkFieldMerges() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}


	public List<NmlzRuleFieldMerge> selectNmlzRuleFieldMergesByNmlzRuleContSeq(Long nmlzRuleContSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleContSeq);
	}

	// nmlzRule 기준으로 여러개 들어감
	public int insertNmlzRuleFieldMerge(NmlzRuleFieldMerge nmlzRuleFieldMerge) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleFieldMerge);
	}

	// nmlzRule 이 업데이트되면 기존 데이터 제거, 삭제 시에는 db 데이터 삭제
	public int deleteNmlzRuleFieldMergeByNmlzRuleContSeq(Long nmlzRuleContSeq) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleContSeq);
	}
}
