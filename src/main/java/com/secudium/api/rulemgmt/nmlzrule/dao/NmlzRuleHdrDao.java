package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleHdr;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkHdr;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleHdrDao extends RepositorySupport {

	private final SqlSession sqlSession;

	/**
	 * 목록 가져오기 
	 */
	public Page<NmlzRuleHdr> selectNmlzRuleHdrs(Long nmlzRuleSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleSeq", nmlzRuleSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleHdr> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleZkHdr> selectAllNmlzRuleZkHdrs() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public List<NmlzRuleHdr> selectNmlzRuleHdrsByNmlzRuleSeq(Long nmlzRuleSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleSeq);
	}

	/**
	 * 추가하기
	 */
	public int insertNmlzRuleHdr(NmlzRuleHdr nmlzRuleHdr) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleHdr);
	}

	/**
	 * nmlzRuleSeq 로 삭제하기
	 */
	public int deleteNmlzRuleHdrByNmlzRuleSeq(Long nmlzRuleSeq) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleSeq);
	}

	/**
	 * NmlzRuleHdrSeq 로 삭제하기
	 */
	public int deleteNmlzRuleHdrBySeq(Long NmlzRuleHdrSeq) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), NmlzRuleHdrSeq);
	}


	/**
	 * 헤더 룰 존재 여부 
	 */
	public boolean isExistNmlzRuleHdr(Long nmlzRuleHdrSeq) {
		return (Integer) this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleHdrSeq) > 0 ?
				true : false;
	}
}
