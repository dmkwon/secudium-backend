package com.secudium.api.rulemgmt.nmlzrule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleHdrHist;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class NmlzRuleHdrHistDao extends RepositorySupport {

	private final SqlSession sqlSession;

	/**
	 * nmlzRuleHistSeq 에 의해 nmlzRuleHdrHist 목록을 가져온다.
	 * @param nmlzRuleHistSeq
	 * @param pageable
	 * @return
	 */
	public Page<NmlzRuleHdrHist> selectNmlzRuleHdrHists(Long nmlzRuleHistSeq, Pageable pageable) {
		Map<String, Object> params = new HashMap<>();
		params.put("nmlzRuleHistSeq", nmlzRuleHistSeq);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<NmlzRuleHdrHist> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);		
		return new PageImpl<>(list, pageable, total);
	}

	public List<NmlzRuleHdrHist> selectNmlzRuleHdrHistsByNmlzRuleHistSeq(Long nmlzRuleHistSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleHistSeq);		
	}

	/**
	 * nmlzRuleCont history 를 남긴다.
	 * @param nmlzRuleHdrHist
	 * @return
	 */
	public int insertNmlzRuleHdrHist(NmlzRuleHdrHist nmlzRuleHdrHist) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), nmlzRuleHdrHist);
	}
}
