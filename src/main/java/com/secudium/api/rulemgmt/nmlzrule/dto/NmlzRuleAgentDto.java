package com.secudium.api.rulemgmt.nmlzrule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleAgent;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleAgentDto {

	private Long nmlzRuleAgentSeq;
	private Long nmlzRuleSeq;
	private Long agentVendorId;
	private String agentVendorNm;
	private String serviceItemCode;
	private String serviceItemCodeNm;
	private Long agentModelId;
	private String agentModelNm;

	public NmlzRuleAgent toEntityWithCurrentUser(Long nmlzRuleSeq, UserPrincipal currentUser) {
		return NmlzRuleAgent.builder()
				.nmlzRuleSeq(nmlzRuleSeq)
				.agentVendorId(agentVendorId)
				.serviceItemCode(serviceItemCode)
				.agentModelId(agentModelId)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}