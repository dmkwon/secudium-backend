package com.secudium.api.rulemgmt.nmlzrule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleAgentHistDto {

	private Long nmlzRuleAgentHistSeq;
	private Long nmlzRuleHistSeq;
	private Long nmlzRuleSeq;
	private Long agentVendorId;
	private String agentVendorNm;
	private String serviceItemCode;
	private String serviceItemCodeNm;
	private Long agentModelId;
	private String agentModelNm;
}