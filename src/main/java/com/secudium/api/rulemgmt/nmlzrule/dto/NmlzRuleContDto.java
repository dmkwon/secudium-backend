package com.secudium.api.rulemgmt.nmlzrule.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleCont;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleContDto {

	private Long nmlzRuleContSeq;
	private Long nmlzRuleHdrSeq;
	private String defaultUseYn;
	private List<String> eventTypeCode;
	private String contRegexp;
	private String contSampLog;
	private String contPriority;
	private String contDesc;
	private String status;
	private String useYn;
	
	private List<NmlzRuleFieldDto> nmlzRuleFields;
	private List<NmlzRuleFieldFuncDto> nmlzRuleFieldFuncs;
	private List<NmlzRuleFieldMergeDto> nmlzRuleFieldMerges;
	private List<NmlzRuleFieldReplaceDto> nmlzRuleFieldReplaces;

	public NmlzRuleCont toEntityWithCurrentUser(Long nmlzRuleHdrSeq, Long nmlzRuleContSeq, UserPrincipal currentUser) {	
		return NmlzRuleCont.builder()
				.nmlzRuleContSeq(nmlzRuleContSeq)
				.nmlzRuleHdrSeq(nmlzRuleHdrSeq)
				.defaultUseYn(defaultUseYn)
				.eventTypeCode(eventTypeCode == null ? "": eventTypeCode.stream().map(Object::toString)
                        .collect(Collectors.joining(",")))
				.contRegexp(contRegexp)
				.contSampLog(contSampLog)
				.contPriority(contPriority)
				.contDesc(contDesc)
				.useYn(useYn)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}