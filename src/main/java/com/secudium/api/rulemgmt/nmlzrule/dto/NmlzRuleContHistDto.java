package com.secudium.api.rulemgmt.nmlzrule.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleContHistDto {

	private Long nmlzRuleContHistSeq;
	private Long nmlzRuleHdrHistSeq;
	private String defaultUseYn;
	private List<String> eventTypeCode;
	private String contRegexp;
	private String contSampLog;
	private String contPriority;
	private String contDesc;
	private String useYn;

	private List<NmlzRuleFieldHistDto> nmlzRuleFieldHists;
	private List<NmlzRuleFieldFuncHistDto> nmlzRuleFieldFuncHists;
	private List<NmlzRuleFieldMergeHistDto> nmlzRuleFieldMergeHists;
	private List<NmlzRuleFieldReplaceHistDto> nmlzRuleFieldReplaceHists;
}