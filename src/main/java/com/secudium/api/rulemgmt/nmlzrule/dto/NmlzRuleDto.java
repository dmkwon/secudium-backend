package com.secudium.api.rulemgmt.nmlzrule.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleDto {

	private Long nmlzRuleSeq;
	private String nmlzRuleNm;
	private String logFormatCode;
	private String nmlzRuleDesc;
	private String nmlzRuleChgReason;
	private String status;
	private String useYn;
	private String regUsrNm;
	private String regDateFormatted;
	private String modUsrNm;
	private String modDateFormatted;

	private List<NmlzRuleAgentDto> nmlzRuleAgents;
	private List<NmlzRuleHdrDto> nmlzRuleHdrs;

}
