package com.secudium.api.rulemgmt.nmlzrule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleField;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleFieldDto {

	private Long nmlzRuleFieldSeq;
	private Long nmlzRuleContSeq;
	private Long matchNo;
	private String jsonKey;
	private Long fieldId;
	private String field;
	private Long subFieldId;
	private String subField;
	private String fieldDefault;

	public NmlzRuleField toEntityWithCurrentUser(Long nmlzRuleContSeq, UserPrincipal currentUser) {
		return NmlzRuleField.builder()
				.nmlzRuleFieldSeq(nmlzRuleFieldSeq)
				.nmlzRuleContSeq(nmlzRuleContSeq)
				.matchNo(matchNo)
				.jsonKey(jsonKey)
				.fieldId(fieldId)
				.field(field)
				.subFieldId(subFieldId)
				.subField(subField)
				.fieldDefault(fieldDefault)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}