package com.secudium.api.rulemgmt.nmlzrule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldFunc;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleFieldFuncDto {

	private Long nmlzRuleFieldFuncSeq;
	private Long nmlzRuleContSeq;
	private Long fieldId;
	private String field;
	private String fieldFuncCode;
	private String fieldFuncCodeNm;
	private String fieldFuncArg;
	private String fieldFuncArgCode;
	private Integer printFieldId;
	private String printField;

	public NmlzRuleFieldFunc toEntityWithCurrentUser(Long nmlzRuleContSeq, UserPrincipal currentUser) {
		return NmlzRuleFieldFunc.builder()
				.nmlzRuleContSeq(nmlzRuleContSeq)
				.fieldId(fieldId)
				.field(field)
				.fieldFuncCode(fieldFuncCode)
				.fieldFuncArg(fieldFuncArg)
				.fieldFuncArgCode(fieldFuncArgCode)
				.printFieldId(printFieldId)
        .printField(printField)
        .regUsrNo(currentUser.getUsrNo())
				.build();
	}
}