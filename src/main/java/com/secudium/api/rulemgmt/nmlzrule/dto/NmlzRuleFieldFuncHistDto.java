package com.secudium.api.rulemgmt.nmlzrule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleFieldFuncHistDto {

	private Long nmlzRuleFieldFuncHistSeq;
	private Long nmlzRuleContHistSeq;
	private Long fieldId;
	private String field;
	private String fieldFuncCode;
	private String fieldFuncCodeNm;
	private String fieldFuncArg;
	private String fieldFuncArgCode;
	private Integer printFieldId;
	private String printField;
}