package com.secudium.api.rulemgmt.nmlzrule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleFieldHistDto {

	private Long nmlzRuleFieldHistSeq;
	private Long nmlzRuleContHistSeq;
	private Long matchNo;
	private String jsonKey;
	private Long fieldId;
	private String field;
	private Long subFieldId;
	private String subField;
	private String fieldDefault;
}