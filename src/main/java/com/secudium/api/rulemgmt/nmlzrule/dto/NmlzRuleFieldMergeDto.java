package com.secudium.api.rulemgmt.nmlzrule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldMerge;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleFieldMergeDto {

	private Long nmlzRuleFieldMergeSeq;
	private Long nmlzRuleContSeq;
	private Long fieldId1;
	private String field1;
	private Long fieldId2;
	private String field2;
	private Integer printFieldId;
	private String printField;

	public NmlzRuleFieldMerge toEntityWithCurrentUser(Long nmlzRuleContSeq, UserPrincipal currentUser) {
		return NmlzRuleFieldMerge.builder()
				.nmlzRuleContSeq(nmlzRuleContSeq)
				.fieldId1(fieldId1)
				.field1(field1)
				.fieldId2(fieldId2)
				.field2(field2)
				.printFieldId(printFieldId)
        .printField(printField)
        .regUsrNo(currentUser.getUsrNo())
				.build();
	}
}