package com.secudium.api.rulemgmt.nmlzrule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleFieldMergeHistDto {

	private Long nmlzRuleFieldMergeHistSeq;
	private Long nmlzRuleContHistSeq;
	private Long fieldId1;
	private String field1;
	private Long fieldId2;
	private String field2;
	private Integer printFieldId;
	private String printField;
}