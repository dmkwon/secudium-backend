package com.secudium.api.rulemgmt.nmlzrule.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldReplace;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleFieldReplaceDto {

	private Long nmlzRuleFieldReplaceSeq;
	private Long nmlzRuleContSeq;
	private Long fieldId;
	private String field;
	private String inValue;
	private String printValue;

	public NmlzRuleFieldReplace toEntityWithCurrentUser(Long nmlzRuleContSeq, UserPrincipal currentUser) {
		return NmlzRuleFieldReplace.builder()
				.nmlzRuleContSeq(nmlzRuleContSeq)
				.fieldId(fieldId)
				.field(field)
				.inValue(inValue)
				.printValue(printValue)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}