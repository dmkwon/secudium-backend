package com.secudium.api.rulemgmt.nmlzrule.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleHdr;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleHdrDto {

	private Long nmlzRuleHdrSeq;
	private Long nmlzRuleSeq;
	private String nmlzRuleHdrRegexp;
	private String nmlzRuleHdrSampLog;
	private String nmlzRuleHdrPrefix;
	private String nmlzRuleHdrKeyField;
	private String hdrPriority;
	private String status;
	private String useYn;

	private List<NmlzRuleContDto> nmlzRuleConts;

	public NmlzRuleHdr toEntityWithCurrentUser(Long nmlzRuleSeq, Long nmlzRuleHdrSeq, UserPrincipal currentUser) {
		return NmlzRuleHdr.builder()
				.nmlzRuleHdrSeq(nmlzRuleHdrSeq)
				.nmlzRuleSeq(nmlzRuleSeq)
				.nmlzRuleHdrRegexp(nmlzRuleHdrRegexp)
				.nmlzRuleHdrSampLog(nmlzRuleHdrSampLog)
				.nmlzRuleHdrPrefix(nmlzRuleHdrPrefix)
				.nmlzRuleHdrKeyField(nmlzRuleHdrKeyField)
				.hdrPriority(hdrPriority)
				.useYn(useYn)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}