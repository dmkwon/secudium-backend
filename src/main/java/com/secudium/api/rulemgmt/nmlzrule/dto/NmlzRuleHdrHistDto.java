package com.secudium.api.rulemgmt.nmlzrule.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleHdrHistDto {

	private Long nmlzRuleHdrHistSeq;
	private Long nmlzRuleHistSeq;
	private Long nmlzRuleSeq;
	private String nmlzRuleHdrRegexp;
	private String nmlzRuleHdrSampLog;
	private String nmlzRuleHdrPrefix;
	private String nmlzRuleHdrKeyField;
	private String hdrPriority;
	private String useYn;

	private List<NmlzRuleContHistDto> nmlzRuleContHists;
}