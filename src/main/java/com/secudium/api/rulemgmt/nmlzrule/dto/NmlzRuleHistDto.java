package com.secudium.api.rulemgmt.nmlzrule.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleHistDto {

	private Long nmlzRuleHistSeq;
	private Long nmlzRuleSeq;
	private String nmlzRuleNm;
	private String logFormatCode;
	private String nmlzRuleDesc;
	private String hdrRegexp;
	private String hdrSampLog;
	private String hdrPrefix;
	private String hdrKeyField;
	private String hdrRelayFlag;
	private String nmlzRuleChgReason;
	private boolean isLastVersion;
	private String useYn;
	private String regUsrNm;
	private String regDateFormatted;

	private List<NmlzRuleAgentHistDto> nmlzRuleAgentHists;
	private List<NmlzRuleHdrHistDto> nmlzRuleHdrHists;

}
