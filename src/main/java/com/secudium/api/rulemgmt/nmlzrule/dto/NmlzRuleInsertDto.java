package com.secudium.api.rulemgmt.nmlzrule.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRule;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleInsertDto {

	private String nmlzRuleNm;
	private String logFormatCode;
	private String nmlzRuleDesc;
	private String hdrRegexp;
	private String hdrSampLog;
	private String hdrPrefix;
	private String hdrKeyField;

	@NotBlank
	private String nmlzRuleChgReason;
	private String useYn;
	
	private List<NmlzRuleAgentDto> nmlzRuleAgents;
	private List<NmlzRuleHdrDto> nmlzRuleHdrs;

	public NmlzRule toEntityWithCurrentUser(UserPrincipal currentUSer)  {
		return NmlzRule.builder()
				.nmlzRuleNm(nmlzRuleNm)
				.logFormatCode(logFormatCode)
				.nmlzRuleDesc(nmlzRuleDesc)
        		.useYn(useYn)
				.regUsrNo(currentUSer.getUsrNo())
				.build();
	}
}
