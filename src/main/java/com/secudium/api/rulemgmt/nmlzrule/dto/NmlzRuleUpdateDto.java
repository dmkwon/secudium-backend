package com.secudium.api.rulemgmt.nmlzrule.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRule;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class NmlzRuleUpdateDto {

	private Long nmlzRuleSeq;
	private String nmlzRuleNm;
	private String logFormatCode;
	private String nmlzRuleDesc;
	private String hdrRegexp;
	private String hdrSampLog;
	private String hdrPrefix;
	private String hdrKeyField;
	private String useYn;
	
	@NotBlank
	private String nmlzRuleChgReason;

	private List<NmlzRuleAgentDto> nmlzRuleAgents;
	private List<NmlzRuleHdrDto> nmlzRuleHdrs;

	public NmlzRule toEntityWithCurrentUser(Long nmlzRuleSeq, UserPrincipal currentUser)  {
		return NmlzRule.builder()
				.nmlzRuleSeq(nmlzRuleSeq)
				.nmlzRuleNm(nmlzRuleNm)
				.logFormatCode(logFormatCode)
				.nmlzRuleDesc(nmlzRuleDesc)
				.useYn(useYn)
				.regUsrNo(currentUser.getUsrNo())
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}
}
