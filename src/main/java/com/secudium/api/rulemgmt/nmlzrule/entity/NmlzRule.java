package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRule extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 5045020442046310361L;
	
	private Long nmlzRuleSeq;
	private String nmlzRuleNm;
	private String logFormatCode;
	private String nmlzRuleDesc;
	private String nmlzRule;
	private String useYn;
	private String agentVenderNm;

	public NmlzRuleHist toHistEntityWithCurrentUser(String chgReason, UserPrincipal currentUser) {
		return NmlzRuleHist.builder()
		.nmlzRuleSeq(nmlzRuleSeq)
		.nmlzRuleNm(nmlzRuleNm)
		.logFormatCode(logFormatCode)
		.nmlzRuleDesc(nmlzRuleDesc)
		.nmlzRuleChgReason(chgReason)
		.useYn(useYn)		
		.regUsrNo(currentUser.getUsrNo())
		.build();
	}
}