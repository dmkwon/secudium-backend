package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleAgentDto;
import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleAgent extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = -2515834507455805962L;

	private Long nmlzRuleAgentSeq;
	private Long nmlzRuleSeq;
	private Long agentVendorId;
	private String agentVendorNm;
	private String serviceItemCode;
	private String serviceItemCodeNm;
	private Long agentModelId;
	private String agentModelNm;

	public NmlzRuleAgentDto toDto() {
		return NmlzRuleAgentDto.builder()
		.nmlzRuleAgentSeq(nmlzRuleAgentSeq)
		.nmlzRuleSeq(nmlzRuleSeq)
		.agentVendorId(agentVendorId)
		.agentVendorNm(agentVendorNm)
		.serviceItemCode(serviceItemCode)
		.serviceItemCodeNm(serviceItemCodeNm)
		.agentModelId(agentModelId)
		.agentModelNm(agentModelNm)
		.build();
	}

	public NmlzRuleAgentHist toHistEntityWithCurrentUser(Long nmlzRuleHistSeq, Long nmlzRuleSeq, UserPrincipal currentUser) {
		return NmlzRuleAgentHist.builder()
		.nmlzRuleHistSeq(nmlzRuleHistSeq)
		.nmlzRuleSeq(nmlzRuleSeq)
		.agentVendorId(agentVendorId)
		.serviceItemCode(serviceItemCode)
		.agentModelId(agentModelId)
		.regUsrNo(currentUser.getUsrNo())
		.build();
	}
}