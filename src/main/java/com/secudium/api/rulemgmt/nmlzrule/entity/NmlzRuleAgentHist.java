package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleAgentHistDto;
import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleAgentHist extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 918755808796401892L;

	private Long nmlzRuleAgentHistSeq;
	private Long nmlzRuleHistSeq;
	private Long nmlzRuleSeq;
	private Long agentVendorId;
	private String agentVendorNm;
	private String serviceItemCode;
	private String serviceItemCodeNm;
	private Long agentModelId;
	private String agentModelNm;

	public NmlzRuleAgentHistDto toDto() {
		return NmlzRuleAgentHistDto.builder()
				.nmlzRuleAgentHistSeq(nmlzRuleAgentHistSeq)
				.nmlzRuleHistSeq(nmlzRuleHistSeq)
				.nmlzRuleSeq(nmlzRuleSeq)
				.agentVendorId(agentVendorId)
				.agentVendorNm(agentVendorNm)
				.serviceItemCode(serviceItemCode)
				.serviceItemCodeNm(serviceItemCodeNm)
				.agentModelId(agentModelId)
				.agentModelNm(agentModelNm)
				.build();
	}

	public NmlzRuleAgent toNmlzRuleAgentEntity(Long nmlzRuleSeq, UserPrincipal currentUser) {
		return NmlzRuleAgent.builder()
				.nmlzRuleSeq(nmlzRuleSeq)
				.agentVendorId(agentVendorId)
				.serviceItemCode(serviceItemCode)
				.agentModelId(agentModelId)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}