package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleCont extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -3883120711448638763L;

	private Long nmlzRuleContSeq;
	private Long nmlzRuleHdrSeq;
	private String defaultUseYn;
	private String eventTypeCode;
	private String contRegexp;
	private String contSampLog;
	private String contPriority;
	private String contDesc;
	private String useYn;
	
	public NmlzRuleContHist toHistEntityWithCurrentUser(Long nmlzRuleHdrHistSeq, Long nmlzRuleHdrSeq, UserPrincipal currentUser) {
		return NmlzRuleContHist.builder()
				.nmlzRuleHdrHistSeq(nmlzRuleHdrHistSeq)
				.defaultUseYn(defaultUseYn)
				.eventTypeCode(eventTypeCode)
				.contRegexp(contRegexp)
				.contSampLog(contSampLog)
				.contPriority(contPriority)
				.contDesc(contDesc)
				.useYn(useYn)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}