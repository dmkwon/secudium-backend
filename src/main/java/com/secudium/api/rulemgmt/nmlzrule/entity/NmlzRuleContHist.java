package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleContHist extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 7538975988281988613L;

	private Long nmlzRuleContHistSeq;
	private Long nmlzRuleHdrSeq;
	private Long nmlzRuleHdrHistSeq;
	private String defaultUseYn;
	private String eventTypeCode;
	private String contRegexp;
	private String contSampLog;
	private String contPriority;
	private String contDesc;
	private String useYn;
}