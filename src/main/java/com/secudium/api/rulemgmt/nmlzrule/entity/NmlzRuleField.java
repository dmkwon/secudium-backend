package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldDto;
import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleField extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -7762582104544287165L;

	private Long nmlzRuleFieldSeq;
	private Long nmlzRuleContSeq;
	private Long matchNo;
	private String jsonKey;
	private Long fieldId;
	private String field;
	private Long subFieldId;
	private String subField;
	private String fieldDefault;

	public NmlzRuleFieldDto toDto() {
		return NmlzRuleFieldDto.builder()
				.nmlzRuleFieldSeq(nmlzRuleFieldSeq)
				.nmlzRuleContSeq(nmlzRuleContSeq)
				.matchNo(matchNo)
				.jsonKey(jsonKey)
				.fieldId(fieldId)
				.field(field)
				.subFieldId(subFieldId)
				.subField(subField)
				.fieldDefault(fieldDefault)
				.build();
	}

	public NmlzRuleFieldHist toHistEntityWithCurrentUser(Long nmlzRuleContHistSeq, UserPrincipal currentUser) {
		return NmlzRuleFieldHist.builder()
				.nmlzRuleContHistSeq(nmlzRuleContHistSeq)
				.matchNo(matchNo)
				.jsonKey(jsonKey)
				.fieldId(fieldId)
				.field(field)
				.subFieldId(subFieldId)
				.subField(subField)
				.fieldDefault(fieldDefault)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}