package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldFuncDto;
import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleFieldFunc extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 869934636158361330L;

	private Long nmlzRuleFieldFuncSeq;
	private Long nmlzRuleContSeq;
	private Long fieldId;
	private String field;
	private String fieldFuncCode;
	private String fieldFuncCodeNm;
	private String fieldFuncArg;
	private String fieldFuncArgCode;
	private Integer printFieldId;
	private String printField;

	public NmlzRuleFieldFuncDto toDto() {
    return NmlzRuleFieldFuncDto.builder()
        .nmlzRuleFieldFuncSeq(nmlzRuleFieldFuncSeq)
				.nmlzRuleContSeq(nmlzRuleContSeq)
				.fieldId(fieldId)
				.field(field)
				.fieldFuncCode(fieldFuncCode)
				.fieldFuncCodeNm(fieldFuncCodeNm)
				.fieldFuncArg(fieldFuncArg)
				.fieldFuncArgCode(fieldFuncArgCode)
				.printFieldId(printFieldId)
				.printField(printField)
				.build();
	}

	public NmlzRuleFieldFuncHist toHistEntityWithCurrentUser(Long nmlzRuleContHistSeq, UserPrincipal currentUser) {
		return NmlzRuleFieldFuncHist.builder()
				.nmlzRuleContHistSeq(nmlzRuleContHistSeq)
				.fieldId(fieldId)
				.field(field)
				.fieldFuncCode(fieldFuncCode)
				.fieldFuncArg(fieldFuncArg)
				.fieldFuncArgCode(fieldFuncArgCode)
				.printFieldId(printFieldId)
				.printField(printField)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}