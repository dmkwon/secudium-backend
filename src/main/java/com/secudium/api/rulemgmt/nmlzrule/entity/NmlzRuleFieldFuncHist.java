package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldFuncDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldFuncHistDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleFieldFuncHist extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -7908453955479629693L;

	private Long nmlzRuleFieldFuncHistSeq;
	private Long nmlzRuleContHistSeq;
	private Long fieldId;
	private String field;
	private String fieldFuncCode;
	private String fieldFuncCodeNm;
	private String fieldFuncArg;
	private String fieldFuncArgCode;
	private Integer printFieldId;
	private String printField;

	public NmlzRuleFieldFuncHistDto toDto() {
		return NmlzRuleFieldFuncHistDto.builder()
				.nmlzRuleFieldFuncHistSeq(nmlzRuleFieldFuncHistSeq)
				.nmlzRuleContHistSeq(nmlzRuleContHistSeq)
				.fieldId(fieldId)
				.field(field)
				.fieldFuncCode(fieldFuncCode)
				.fieldFuncCodeNm(fieldFuncCodeNm)
				.fieldFuncArg(fieldFuncArg)
				.fieldFuncArgCode(fieldFuncArgCode)
				.printFieldId(printFieldId)
				.printField(printField)
				.build();
	}

	public NmlzRuleFieldFuncDto toNmlzRuleFieldFuncDto() {
		return NmlzRuleFieldFuncDto.builder()
				.fieldId(fieldId)
				.field(field)
				.fieldFuncCode(fieldFuncCode)
				.fieldFuncCodeNm(fieldFuncCodeNm)
				.fieldFuncArg(fieldFuncArg)
				.fieldFuncArgCode(fieldFuncArgCode)
				.printFieldId(printFieldId)
				.printField(printField)
				.build();
	}
}