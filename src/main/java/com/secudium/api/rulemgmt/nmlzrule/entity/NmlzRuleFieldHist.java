package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldHistDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleFieldHist extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -5972754303178340657L;

	private Long nmlzRuleFieldHistSeq;
	private Long nmlzRuleContHistSeq;
	private Long matchNo;
	private String jsonKey;
	private Long fieldId;
	private String field;
	private Long subFieldId;
	private String subField;
	private String fieldDefault;


	public NmlzRuleFieldHistDto toDto() {
		return NmlzRuleFieldHistDto.builder() 
				.nmlzRuleFieldHistSeq(nmlzRuleFieldHistSeq)
				.nmlzRuleContHistSeq(nmlzRuleContHistSeq)
				.matchNo(matchNo)
				.jsonKey(jsonKey)
				.fieldId(fieldId)
				.field(field)
				.subFieldId(subFieldId)
				.subField(subField)
				.fieldDefault(fieldDefault)
				.build();
	}


	public NmlzRuleFieldDto toNmlzRuleFieldDto() {
		return NmlzRuleFieldDto.builder() 
				.matchNo(matchNo)
				.jsonKey(jsonKey)
				.fieldId(fieldId)
				.field(field)
				.fieldDefault(fieldDefault)
				.build();
	}
}