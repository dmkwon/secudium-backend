package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldMergeDto;
import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleFieldMerge extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -3510762123811747330L;

	private Long nmlzRuleFieldMergeSeq;
	private Long nmlzRuleContSeq;
	private Long fieldId1;
	private String field1;
	private Long fieldId2;
	private String field2;
	private Integer printFieldId;
	private String printField;

	public NmlzRuleFieldMergeDto toDto() {
    return NmlzRuleFieldMergeDto.builder()
        .nmlzRuleFieldMergeSeq(nmlzRuleFieldMergeSeq)
				.nmlzRuleContSeq(nmlzRuleContSeq)
				.fieldId1(fieldId1)
				.field1(field1)
				.fieldId2(fieldId2)
				.field2(field2)
				.printFieldId(printFieldId)
				.printField(printField)
				.build();
	}

	public NmlzRuleFieldMergeHist toHistEntityWithCurrentUser(Long nmlzRuleContHistSeq, UserPrincipal currentUser) {
		return NmlzRuleFieldMergeHist.builder()
				.nmlzRuleContHistSeq(nmlzRuleContHistSeq)
				.fieldId1(fieldId1)
				.field1(field1)
				.fieldId2(fieldId2)
				.field2(field2)
				.printFieldId(printFieldId)
				.printField(printField)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}