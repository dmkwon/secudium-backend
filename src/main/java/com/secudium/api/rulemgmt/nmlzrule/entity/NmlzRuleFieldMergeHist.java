package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldMergeDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldMergeHistDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleFieldMergeHist extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -3615680858156615749L;

	private Long nmlzRuleFieldMergeHistSeq;
	private Long nmlzRuleContHistSeq;
	private Long fieldId1;
	private String field1;
	private Long fieldId2;
	private String field2;
	private Integer printFieldId;
	private String printField;

	public NmlzRuleFieldMergeHistDto toDto() {
		return NmlzRuleFieldMergeHistDto.builder()
				.nmlzRuleFieldMergeHistSeq(nmlzRuleFieldMergeHistSeq)
				.nmlzRuleContHistSeq(nmlzRuleContHistSeq)
				.fieldId1(fieldId1)
				.field1(field1)
				.fieldId2(fieldId2)
				.field2(field2)
				.printFieldId(printFieldId)
				.printField(printField)
				.build();
	}

	public NmlzRuleFieldMergeDto toNmlzRuleFieldMergeDto() {
		return NmlzRuleFieldMergeDto.builder()
				.fieldId1(fieldId1)
				.field1(field1)
				.fieldId2(fieldId2)
				.field2(field2)
				.printFieldId(printFieldId)
				.printField(printField)
				.build();
	}
}