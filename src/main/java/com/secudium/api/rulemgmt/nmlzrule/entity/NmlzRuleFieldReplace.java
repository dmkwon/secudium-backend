package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldReplaceDto;
import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleFieldReplace extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 2351755085623181911L;

	private Long nmlzRuleFieldReplaceSeq;
	private Long nmlzRuleContSeq;
	private Long fieldId;
	private String field;
	private String inValue;
	private String printValue;

	public NmlzRuleFieldReplaceDto toDto() {
    return NmlzRuleFieldReplaceDto.builder()
        .nmlzRuleFieldReplaceSeq(nmlzRuleFieldReplaceSeq)
				.nmlzRuleContSeq(nmlzRuleContSeq)
				.fieldId(fieldId)
				.field(field)
				.inValue(inValue)
				.printValue(printValue)
				.build();
	}

	public NmlzRuleFieldReplaceHist toHistEntityWithCurrentUser(Long nmlzRuleContHistSeq, UserPrincipal currentUser) {
		return NmlzRuleFieldReplaceHist.builder()
				.nmlzRuleContHistSeq(nmlzRuleContHistSeq)
				.fieldId(fieldId)
				.field(field)
				.inValue(inValue)
				.printValue(printValue)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}