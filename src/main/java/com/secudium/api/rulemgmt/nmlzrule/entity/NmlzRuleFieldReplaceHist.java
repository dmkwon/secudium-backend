package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldReplaceDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldReplaceHistDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleFieldReplaceHist extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -8456020864874425340L;

	private Long nmlzRuleFieldReplaceHistSeq;
	private Long nmlzRuleContHistSeq;
	private Long fieldId;
	private String field;
	private String inValue;
	private String printValue;

	public NmlzRuleFieldReplaceHistDto toDto(){
		return NmlzRuleFieldReplaceHistDto.builder()
				.nmlzRuleFieldReplaceHistSeq(nmlzRuleFieldReplaceHistSeq)
				.nmlzRuleFieldReplaceHistSeq(nmlzRuleFieldReplaceHistSeq)
				.fieldId(fieldId)
				.field(field)
				.inValue(inValue)
				.printValue(printValue)
				.build();
	}

	public NmlzRuleFieldReplaceDto toNmlzRuleFieldReplaceDto(){
		return NmlzRuleFieldReplaceDto.builder()
				.fieldId(fieldId)
				.field(field)
				.inValue(inValue)
				.printValue(printValue)
				.build();
	}
}