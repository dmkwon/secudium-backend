package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleHdr extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -3883120711448638763L;

	private Long nmlzRuleHdrSeq;
	private Long nmlzRuleSeq;
	private String nmlzRuleHdrRegexp;
	private String nmlzRuleHdrSampLog;
	private String nmlzRuleHdrPrefix;
	private String nmlzRuleHdrKeyField;
	private String hdrPriority;
	private String useYn;
	
	public NmlzRuleHdrHist toHistEntityWithCurrentUser(Long nmlzRuleHistSeq, Long nmlzRuleSeq, UserPrincipal currentUser) {
		return NmlzRuleHdrHist.builder()
				.nmlzRuleHistSeq(nmlzRuleHistSeq)
				.nmlzRuleSeq(nmlzRuleSeq)
				.nmlzRuleHdrRegexp(nmlzRuleHdrRegexp)
				.nmlzRuleHdrSampLog(nmlzRuleHdrSampLog)
				.nmlzRuleHdrPrefix(nmlzRuleHdrPrefix)
				.nmlzRuleHdrKeyField(nmlzRuleHdrKeyField)
				.hdrPriority(hdrPriority)
				.useYn(useYn)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}