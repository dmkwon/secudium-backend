package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;
import java.util.List;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleContDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleHdrDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleHdrHistDto;
import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleHdrHist extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 7538975988281988613L;

	private Long nmlzRuleHdrHistSeq;
	private Long nmlzRuleHistSeq;
	private Long nmlzRuleSeq;
	private String nmlzRuleHdrRegexp;
	private String nmlzRuleHdrSampLog;
	private String nmlzRuleHdrPrefix;
	private String nmlzRuleHdrKeyField;
	private String hdrPriority;
	private String useYn;


	public NmlzRuleHdrHistDto toDto() {
		return NmlzRuleHdrHistDto.builder()
				.nmlzRuleHdrHistSeq(nmlzRuleHdrHistSeq)
				.nmlzRuleHistSeq(nmlzRuleHistSeq)
				.nmlzRuleSeq(nmlzRuleSeq)
				.nmlzRuleHdrRegexp(nmlzRuleHdrRegexp)
				.nmlzRuleHdrSampLog(nmlzRuleHdrSampLog)
				.nmlzRuleHdrPrefix(nmlzRuleHdrPrefix)
				.nmlzRuleHdrKeyField(nmlzRuleHdrKeyField)
				.hdrPriority(hdrPriority)
				.useYn(useYn)
				.build();
	}

	public NmlzRuleHdrDto toNmlzRuleHdrDto(Long nmlzRuleSeq, List<NmlzRuleContDto> nmlzRuleConts) {
		return NmlzRuleHdrDto.builder()
				.nmlzRuleSeq(nmlzRuleSeq)
				.nmlzRuleHdrRegexp(nmlzRuleHdrRegexp)
				.nmlzRuleHdrSampLog(nmlzRuleHdrSampLog)
				.nmlzRuleHdrPrefix(nmlzRuleHdrPrefix)
				.nmlzRuleHdrKeyField(nmlzRuleHdrKeyField)
				.hdrPriority(hdrPriority)
				.status("U")
				.useYn(useYn)
				.nmlzRuleConts(nmlzRuleConts)
				.build();
	}


	public NmlzRuleHdr toNmlzRuleHdrEntity(Long nmlzRuleSeq, UserPrincipal currentUser) {
		return NmlzRuleHdr.builder()
				.nmlzRuleSeq(nmlzRuleSeq)
				.nmlzRuleHdrRegexp(nmlzRuleHdrRegexp)
				.nmlzRuleHdrSampLog(nmlzRuleHdrSampLog)
				.nmlzRuleHdrPrefix(nmlzRuleHdrPrefix)
				.nmlzRuleHdrKeyField(nmlzRuleHdrKeyField)
				.hdrPriority(hdrPriority)
				.useYn(useYn)
				.regUsrNo(currentUser.getUsrNo())
				.build();
	}
}