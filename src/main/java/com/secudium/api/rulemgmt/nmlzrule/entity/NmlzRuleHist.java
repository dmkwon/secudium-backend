package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleHistDto;
import com.secudium.mybatis.entity.BaseEntity;
import com.secudium.security.UserPrincipal;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
public class NmlzRuleHist extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -6076737262116806268L;

	private Long nmlzRuleHistSeq;
	private Long nmlzRuleSeq;
	private String nmlzRuleNm;
	private String logFormatCode;
	private String nmlzRuleDesc;
	private String nmlzRuleChgReason;
	private String useYn;

	public NmlzRuleHistDto toDto(boolean isLastVersion) {
		return NmlzRuleHistDto.builder()
		.nmlzRuleHistSeq(nmlzRuleHistSeq)
		.nmlzRuleSeq(nmlzRuleSeq)
		.nmlzRuleNm(nmlzRuleNm)
		.logFormatCode(logFormatCode)
		.nmlzRuleDesc(nmlzRuleDesc)
		.nmlzRuleChgReason(nmlzRuleChgReason)
		.useYn(useYn)
		.regUsrNm(getRegUsrNm())
		.regDateFormatted(getRegDateFormatted())
		.isLastVersion(isLastVersion)
		.build();
	}

	public NmlzRule toNmlzRuleEntity(UserPrincipal currentUser) {
		return NmlzRule.builder()
		.nmlzRuleSeq(nmlzRuleSeq)
		.nmlzRuleNm(nmlzRuleNm)
		.logFormatCode(logFormatCode)
		.nmlzRuleDesc(nmlzRuleDesc)
		.useYn("Y")
		.regUsrNo(currentUser.getUsrNo())
		.build();
	}
}