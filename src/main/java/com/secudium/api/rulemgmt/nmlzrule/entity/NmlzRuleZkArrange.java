package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class NmlzRuleZkArrange implements Serializable {

	private static final long serialVersionUID = -9201710354963999322L;

	private Long nmlzRuleContSeq;
	private String fieldId;
	private String subFieldId;
	private String captureOrder;
}