package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * zk 에 쓸 ㄷ
 */
@Getter
@NoArgsConstructor
public class NmlzRuleZkCont implements Serializable {

	private static final long serialVersionUID = -7963515605720679765L;

	private Long nmlzRuleSeq;
	private Long nmlzRuleContSeq;
	private String logFormatCode;
	private String hdrPrefix;
	private String hdrPresent;
	private String contRegexp;
	private String contPriority;
	private String agentModelId;
	private String useYn;
	private String contUseYn;
}