package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class NmlzRuleZkField implements Serializable {

	private static final long serialVersionUID = 4072536082670799898L;

	private Long nmlzRuleSeq;
	private Long nmlzRuleContSeq;
  	private String tokenize;
	private Long fieldId;
	private String field;
	private String fieldDefault;
	
}