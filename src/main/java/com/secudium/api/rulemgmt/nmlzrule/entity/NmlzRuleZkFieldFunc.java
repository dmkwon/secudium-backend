package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class NmlzRuleZkFieldFunc implements Serializable {

	private static final long serialVersionUID = -53499549660053130L;

	private Long nmlzRuleSeq;
	private Long nmlzRuleContSeq;
	private Long fieldId;
	private String fieldFuncCode;
	private String fieldFuncCodeNm;
	private String fieldFuncArg;
	private Integer printFieldId;
}