package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class NmlzRuleZkFieldMerge implements Serializable {

	private static final long serialVersionUID = -584612519438833468L;

	private Long nmlzRuleSeq;
	private Long nmlzRuleContSeq;
	private Long fieldId1;
	private Long fieldId2;
	private Long printFieldId;
}