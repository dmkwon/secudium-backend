package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class NmlzRuleZkFieldReplace implements Serializable {

	private static final long serialVersionUID = -5470766776686603702L;
	
	private Long nmlzRuleSeq;
	private Long nmlzRuleContSeq;
	private Long fieldId;
	private String inValue;
	private String printValue;
}