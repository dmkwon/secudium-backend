package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * 
 */
@Getter
@NoArgsConstructor
public class NmlzRuleZkHdr implements Serializable {

	private static final long serialVersionUID = 9179022000863914041L;
	
	private Long nmlzRuleHdrSeq;
	private String nmlzRuleHdrPrefix;
	private String nmlzRuleHdrRegexp;
	private String hdrPriority;
	//private Long nmlzRuleContSeq;
}