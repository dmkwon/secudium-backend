package com.secudium.api.rulemgmt.nmlzrule.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * 
 */
@Getter
@NoArgsConstructor
public class NmlzRuleZkTokenize implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5835367667651101472L;

	private Long nmlzRuleContSeq;
	private String agentVendorNm;
	private String agentModelId;
	private String agentModelType;
	private String contRegexp;
	private String eventTypeCode;
	private String contPriority;
	private String nmlzRuleHdrSeq;
}