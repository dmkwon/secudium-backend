package com.secudium.api.rulemgmt.nmlzrule.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.enginemgmt.engineenv.dao.EngineCommandDao;
import com.secudium.api.enginemgmt.engineenv.entity.EngineCommand;
import com.secudium.api.enginemgmt.engineenv.type.EngineMenuType;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleAgentDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleAgentHistDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleContDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleContHistDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleFieldDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleFieldFuncDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleFieldFuncHistDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleFieldHistDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleFieldMergeDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleFieldMergeHistDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleFieldReplaceDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleFieldReplaceHistDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleHdrDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleHdrHistDao;
import com.secudium.api.rulemgmt.nmlzrule.dao.NmlzRuleHistDao;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleAgentDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleAgentHistDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleContDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleContHistDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleDeleteDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldFuncDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldFuncHistDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldHistDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldMergeDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldMergeHistDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldReplaceDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleFieldReplaceHistDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleHdrDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleHdrHistDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleHistDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleInsertDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleRollBackDto;
import com.secudium.api.rulemgmt.nmlzrule.dto.NmlzRuleUpdateDto;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRule;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleAgent;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleAgentHist;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleCont;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleContHist;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleField;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldFunc;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldFuncHist;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldHist;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldMerge;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldMergeHist;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldReplace;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleFieldReplaceHist;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleHdr;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleHdrHist;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleHist;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkArrange;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkCont;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkField;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkFieldFunc;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkFieldMerge;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkFieldReplace;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkHdr;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkTokenize;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.error.ErrorCode;
import com.secudium.exception.BusinessException;
import com.secudium.security.UserPrincipal;
import com.secudium.storage.zk.ZKService;
import com.secudium.util.ZkUtils;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class NmlzRuleService {

	private final NmlzRuleDao nmlzRuleDao;
	private final NmlzRuleAgentDao nmlzRuleAgentDao;
	private final NmlzRuleHdrDao nmlzRuleHdrDao;
	private final NmlzRuleContDao nmlzRuleContDao;
	private final NmlzRuleFieldDao nmlzRuleFieldDao;
	private final NmlzRuleFieldFuncDao nmlzRuleFieldFuncDao;
	private final NmlzRuleFieldMergeDao nmlzRuleFieldMergeDao;
	private final NmlzRuleFieldReplaceDao nmlzRuleFieldReplaceDao;

	private final NmlzRuleHistDao nmlzRuleHistDao;
	private final NmlzRuleAgentHistDao nmlzRuleAgentHistDao;
	private final NmlzRuleHdrHistDao nmlzRuleHdrHistDao;

	private final NmlzRuleContHistDao nmlzRuleContHistDao;
	private final NmlzRuleFieldHistDao nmlzRuleFieldHistDao;
	private final NmlzRuleFieldFuncHistDao nmlzRuleFieldFuncHistDao;
	private final NmlzRuleFieldMergeHistDao nmlzRuleFieldMergeHistDao;
	private final NmlzRuleFieldReplaceHistDao nmlzRuleFieldReplaceHistDao;

	private final EngineCommandDao engineCommandDao;

	private final ZKService zkService;

	/**
	 * 룰 목록 가져오는 API 
	 * 조건 키워드, 트리에서 선택
	 */
	public PagedResponse<NmlzRule> getNmlzRules(Pageable pageable, SearchMap search, String locale) {
		Page<NmlzRule> page = nmlzRuleDao.selectNmlzRules(pageable, search, locale);
		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(),
					page.getTotalElements(), page.getTotalPages(), page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(), page.getSize(), page.getTotalElements(),
					page.getTotalPages(), page.isLast());
		}
	}

	public NmlzRuleDto getNmlzRuleBySeq(Long nmlzRuleSeq, String locale) {

		NmlzRule nmlzRule = nmlzRuleDao.selectNmlzRuleBySeq(nmlzRuleSeq);

		List<NmlzRuleAgent> nmlzRuleAgents = nmlzRuleAgentDao.selectNmlzRuleAgentsByNmlzRuleSeq(nmlzRule.getNmlzRuleSeq(), locale);
		List<NmlzRuleAgentDto> nmlzRuleAgentDtos = new ArrayList<>();
		for(int i = 0; i < nmlzRuleAgents.size(); i++) {
			nmlzRuleAgentDtos.add(nmlzRuleAgents.get(i).toDto());
		}

		List<NmlzRuleHdr> nmlzRuleHdrs = nmlzRuleHdrDao.selectNmlzRuleHdrsByNmlzRuleSeq(nmlzRule.getNmlzRuleSeq());
		List<NmlzRuleHdrDto> nmlzRuleHdrDtos = new ArrayList<>();
		for(int i = 0; i < nmlzRuleHdrs.size(); i++) {

			List<NmlzRuleCont> nmlzRuleConts = nmlzRuleContDao.selectNmlzRuleContsByNmlzRuleHdrSeq(nmlzRuleHdrs.get(i).getNmlzRuleHdrSeq());
			List<NmlzRuleContDto> nmlzRuleContDtos = new ArrayList<>();
			for(int j = 0; j < nmlzRuleConts.size(); j++) {

				List<NmlzRuleField> nmlzRuleFields = nmlzRuleFieldDao.selectNmlzRuleFieldsByNmlzRuleContSeq(nmlzRuleConts.get(j).getNmlzRuleContSeq());
				List<NmlzRuleFieldFunc> nmlzRuleFieldFuncs = nmlzRuleFieldFuncDao.selectNmlzRuleFieldFuncsByNmlzRuleContSeq(nmlzRuleConts.get(j).getNmlzRuleContSeq(), locale);
				List<NmlzRuleFieldMerge> nmlzRuleFieldMerges = nmlzRuleFieldMergeDao.selectNmlzRuleFieldMergesByNmlzRuleContSeq(nmlzRuleConts.get(j).getNmlzRuleContSeq());
				List<NmlzRuleFieldReplace> nmlzRuleFieldReplaces = nmlzRuleFieldReplaceDao.selectNmlzRuleFieldReplacesByNmlzRuleContSeq(nmlzRuleConts.get(j).getNmlzRuleContSeq());


				List<NmlzRuleFieldDto> nmlzRuleFieldDtos = new ArrayList<>();
				for(int h = 0; h < nmlzRuleFields.size(); h++) {
					nmlzRuleFieldDtos.add(nmlzRuleFields.get(h).toDto());
				}
				List<NmlzRuleFieldFuncDto> nmlzRuleFieldFuncDtos = new ArrayList<>();
				for(int h = 0; h < nmlzRuleFieldFuncs.size(); h++) {
					nmlzRuleFieldFuncDtos.add(nmlzRuleFieldFuncs.get(h).toDto());
				}
				List<NmlzRuleFieldMergeDto> nmlzRuleFieldMergeDtos = new ArrayList<>();
				for(int h = 0; h < nmlzRuleFieldMerges.size(); h++) {
					nmlzRuleFieldMergeDtos.add(nmlzRuleFieldMerges.get(h).toDto());
				}
				List<NmlzRuleFieldReplaceDto> nmlzRuleFieldReplaceDtos = new ArrayList<>();
				for(int h = 0; h < nmlzRuleFieldReplaces.size(); h++) {
					nmlzRuleFieldReplaceDtos.add(nmlzRuleFieldReplaces.get(h).toDto());
				}

				NmlzRuleContDto dto = NmlzRuleContDto.builder()
						.nmlzRuleContSeq(nmlzRuleConts.get(j).getNmlzRuleContSeq())
						.nmlzRuleHdrSeq(nmlzRuleConts.get(j).getNmlzRuleHdrSeq())
						.defaultUseYn(nmlzRuleConts.get(j).getDefaultUseYn())
						.eventTypeCode(nmlzRuleConts.get(j).getEventTypeCode().equals("") ? null : new ArrayList<String>(Arrays.asList(nmlzRuleConts.get(j).getEventTypeCode().split(","))))
						.contRegexp(nmlzRuleConts.get(j).getContRegexp())
						.contSampLog(nmlzRuleConts.get(j).getContSampLog())
						.contPriority(nmlzRuleConts.get(j).getContPriority())
						.contDesc(nmlzRuleConts.get(j).getContDesc())
						.status("U")
						.useYn(nmlzRuleConts.get(j).getUseYn())
						.nmlzRuleFields(nmlzRuleFieldDtos)
						.nmlzRuleFieldFuncs(nmlzRuleFieldFuncDtos)
						.nmlzRuleFieldMerges(nmlzRuleFieldMergeDtos)
						.nmlzRuleFieldReplaces(nmlzRuleFieldReplaceDtos)
						.build();
				nmlzRuleContDtos.add(dto);
			}


			NmlzRuleHdrDto dto = NmlzRuleHdrDto.builder()
					.nmlzRuleSeq(nmlzRuleHdrs.get(i).getNmlzRuleSeq())
					.nmlzRuleHdrSeq(nmlzRuleHdrs.get(i).getNmlzRuleHdrSeq())
					.nmlzRuleHdrRegexp(nmlzRuleHdrs.get(i).getNmlzRuleHdrRegexp())
					.nmlzRuleHdrSampLog(nmlzRuleHdrs.get(i).getNmlzRuleHdrSampLog())
					.nmlzRuleHdrPrefix(nmlzRuleHdrs.get(i).getNmlzRuleHdrPrefix())
					.nmlzRuleHdrKeyField(nmlzRuleHdrs.get(i).getNmlzRuleHdrKeyField())
					.hdrPriority(nmlzRuleHdrs.get(i).getHdrPriority())
					.status("U")
					.useYn(nmlzRuleHdrs.get(i).getUseYn())
					.nmlzRuleConts(nmlzRuleContDtos)
					.build();
			nmlzRuleHdrDtos.add(dto);
		}

		NmlzRuleDto nmlzRuleDto = NmlzRuleDto.builder()
				.nmlzRuleSeq(nmlzRule.getNmlzRuleSeq())
				.nmlzRuleNm(nmlzRule.getNmlzRuleNm())
				.logFormatCode(nmlzRule.getLogFormatCode())
				.nmlzRuleDesc(nmlzRule.getNmlzRuleDesc())
				.status("U")
				.useYn(nmlzRule.getUseYn())
				.nmlzRuleAgents(nmlzRuleAgentDtos)
				.nmlzRuleHdrs(nmlzRuleHdrDtos)
				.build();

		return nmlzRuleDto;
	}

	public void createNmlzRule(NmlzRuleInsertDto dto, UserPrincipal currentUser, String locale) {

		/**
		 * 정규화룰 이름 중복 체크 로직 추가
		 */
		if(!isExistNmlzRuleByName(dto.getNmlzRuleNm(), null)){
			/**
			 * 현재 rule 데이터 추가
			 */
			NmlzRule nmlzRule = addToNmlzRuleDb(dto.toEntityWithCurrentUser(currentUser));

			for(NmlzRuleAgentDto item : dto.getNmlzRuleAgents()) {
				addToNmlzRuleAgentDb(item.toEntityWithCurrentUser(nmlzRule.getNmlzRuleSeq(), currentUser));
			}

			for(NmlzRuleHdrDto item : dto.getNmlzRuleHdrs()) {
				addToNmlzRuleHdrDb(nmlzRule.getNmlzRuleSeq(), item, currentUser);
			}
			/**
			 * 이력 테이블에 추가 
			 */
			moveToHistoricalDb(nmlzRule.getNmlzRuleSeq(), dto.getNmlzRuleChgReason(), currentUser, locale);
		} else {
			throw new BusinessException(ErrorCode.DUPLICATE_DATA_EXIST);
		}
	}

	public void updateNmlzRule(NmlzRuleUpdateDto dto, UserPrincipal currentUser, String locale) {
		/**
		 * 정규화룰 이름 중복 체크 로직 추가
		 */
		if(!isExistNmlzRuleByName(dto.getNmlzRuleNm(), dto.getNmlzRuleSeq())){
			/**
			 * 현재 rule 데이터 수정
			 */
			removeAtNmlzRuleDb(dto.getNmlzRuleSeq());

			addToNmlzRuleDb(dto.toEntityWithCurrentUser(dto.getNmlzRuleSeq(), currentUser));

			for(NmlzRuleAgentDto item : dto.getNmlzRuleAgents()) {
				addToNmlzRuleAgentDb(item.toEntityWithCurrentUser(dto.getNmlzRuleSeq(), currentUser));
			}

			for(NmlzRuleHdrDto item : dto.getNmlzRuleHdrs()) {
				addToNmlzRuleHdrDb(dto.getNmlzRuleSeq(), item, currentUser);
			}

			/**
			 * 이력 테이블로 move 
			 */
			moveToHistoricalDb(dto.getNmlzRuleSeq(), dto.getNmlzRuleChgReason(), currentUser, locale);
		} else {
			throw new BusinessException(ErrorCode.DUPLICATE_DATA_EXIST);
		}
	}

	public void removeNmlzRule(NmlzRuleDeleteDto dto, UserPrincipal currentUser, String locale) {
		/**
		 * 각 데이터 각 이력 테이블로 move
		 */
		for(Long nmlzRuleSeq: dto.getNmlzRuleSeqs()) {
			moveToHistoricalDb(nmlzRuleSeq, dto.getNmlzRuleChgReason(), currentUser, locale);
			/**
			 * 현재 rule 데이터 삭제
			 */
			removeAtNmlzRuleDb(nmlzRuleSeq);
		}
	}

	/**
	 * 이력 남기는 function
	 * @param nmlzRuleSeq
	 * @param chgReason
	 * @param currentUser
	 */
	private void moveToHistoricalDb(Long nmlzRuleSeq, String chgReason, UserPrincipal currentUser, String locale) {

		NmlzRule nmlzRule = nmlzRuleDao.selectNmlzRuleBySeq(nmlzRuleSeq);
		NmlzRuleHist nmlzRuleHist = nmlzRule.toHistEntityWithCurrentUser(chgReason, currentUser);
		nmlzRuleHistDao.insertNmlzRuleHist(nmlzRuleHist);		

		List<NmlzRuleAgent> nmlzRuleAgents = nmlzRuleAgentDao.selectNmlzRuleAgentsByNmlzRuleSeq(nmlzRuleSeq, locale);
		List<NmlzRuleAgentHist> nmlzRuleAgentHists = new ArrayList<>();
		for(NmlzRuleAgent nmlzRuleAgent: nmlzRuleAgents) {
			nmlzRuleAgentHists.add(nmlzRuleAgent.toHistEntityWithCurrentUser(nmlzRuleHist.getNmlzRuleHistSeq(), nmlzRuleSeq, currentUser));
		}
		if(nmlzRuleAgents.size() > 0) {
			nmlzRuleAgentHistDao.insertBulkNmlzRuleAgentHist(nmlzRuleAgentHists);	
		}

		List<NmlzRuleHdr> nmlzRuleHdrs = nmlzRuleHdrDao.selectNmlzRuleHdrsByNmlzRuleSeq(nmlzRuleSeq);
		for(NmlzRuleHdr nmlzRuleHdr: nmlzRuleHdrs) {
			NmlzRuleHdrHist nmlzRuleHdrHist = nmlzRuleHdr.toHistEntityWithCurrentUser(nmlzRuleHist.getNmlzRuleHistSeq(), nmlzRuleSeq, currentUser);
			nmlzRuleHdrHistDao.insertNmlzRuleHdrHist(nmlzRuleHdrHist);

			List<NmlzRuleCont> nmlzRuleConts = nmlzRuleContDao.selectNmlzRuleContsByNmlzRuleHdrSeq(nmlzRuleHdr.getNmlzRuleHdrSeq());
			for(NmlzRuleCont nmlzRuleCont: nmlzRuleConts) {
				NmlzRuleContHist nmlzRuleContHist = nmlzRuleCont.toHistEntityWithCurrentUser(nmlzRuleHdrHist.getNmlzRuleHdrHistSeq(), nmlzRuleHdr.getNmlzRuleHdrSeq(), currentUser);
				nmlzRuleContHistDao.insertNmlzRuleContHist(nmlzRuleContHist);

				List<NmlzRuleField> nmlzRuleFields = nmlzRuleFieldDao.selectNmlzRuleFieldsByNmlzRuleContSeq(nmlzRuleCont.getNmlzRuleContSeq());
				for(NmlzRuleField nmlzRuleField: nmlzRuleFields) {
					nmlzRuleFieldHistDao.insertNmlzRuleFieldHist(nmlzRuleField.toHistEntityWithCurrentUser(nmlzRuleContHist.getNmlzRuleContHistSeq(), currentUser));
				}

				List<NmlzRuleFieldFunc> nmlzRuleFieldFuncs = nmlzRuleFieldFuncDao.selectNmlzRuleFieldFuncsByNmlzRuleContSeq(nmlzRuleCont.getNmlzRuleContSeq(), locale);
				for(NmlzRuleFieldFunc nmlzRuleFieldFunc: nmlzRuleFieldFuncs) {
					nmlzRuleFieldFuncHistDao.insertNmlzRuleFieldFuncHist(nmlzRuleFieldFunc.toHistEntityWithCurrentUser(nmlzRuleContHist.getNmlzRuleContHistSeq(), currentUser));
				}

				List<NmlzRuleFieldMerge> nmlzRuleFieldMerges = nmlzRuleFieldMergeDao.selectNmlzRuleFieldMergesByNmlzRuleContSeq(nmlzRuleCont.getNmlzRuleContSeq());
				for(NmlzRuleFieldMerge nmlzRuleFieldMerge: nmlzRuleFieldMerges) {
					nmlzRuleFieldMergeHistDao.insertNmlzRuleFieldMergeHist(nmlzRuleFieldMerge.toHistEntityWithCurrentUser(nmlzRuleContHist.getNmlzRuleContHistSeq(), currentUser));
				}

				List<NmlzRuleFieldReplace> nmlzRuleFieldReplaces = nmlzRuleFieldReplaceDao.selectNmlzRuleFieldReplacesByNmlzRuleContSeq(nmlzRuleCont.getNmlzRuleContSeq());
				for(NmlzRuleFieldReplace nmlzRuleFieldReplace: nmlzRuleFieldReplaces) {
					nmlzRuleFieldReplaceHistDao.insertNmlzRuleFieldReplaceHist(nmlzRuleFieldReplace.toHistEntityWithCurrentUser(nmlzRuleContHist.getNmlzRuleContHistSeq(), currentUser));
				}
			}
		}
	}

	private boolean isExistNmlzRuleByName(String nmlzRuleNm, Long nmlzRuleSeq) {
		int count = nmlzRuleDao.selectCountNmlzRuleByName(nmlzRuleNm, nmlzRuleSeq);
		if(count > 0) return true;
		else return false;
	}
	private NmlzRule addToNmlzRuleDb(NmlzRule nmlzRule) {
		nmlzRuleDao.insertNmlzRule(nmlzRule);
		return nmlzRule;
	}

	private void addToNmlzRuleAgentDb(NmlzRuleAgent nmlzRuleAgent) {
		nmlzRuleAgentDao.insertNmlzRuleAgent(nmlzRuleAgent);
	}

	private void addToNmlzRuleHdrDb(Long nmlzRuleSeq, NmlzRuleHdrDto dto, UserPrincipal currentUser) {
		NmlzRuleHdr nmlzRuleHdr;
		if(dto.getStatus().equals("U")) {
			nmlzRuleHdr = dto.toEntityWithCurrentUser(nmlzRuleSeq, dto.getNmlzRuleHdrSeq(), currentUser);
		} else {
			nmlzRuleHdr = dto.toEntityWithCurrentUser(nmlzRuleSeq, null, currentUser);
		}

		nmlzRuleHdrDao.insertNmlzRuleHdr(nmlzRuleHdr);

		Long nmlzRuleHdrSeq = nmlzRuleHdr.getNmlzRuleHdrSeq() == -1 ? dto.getNmlzRuleHdrSeq(): nmlzRuleHdr.getNmlzRuleHdrSeq();
		for(NmlzRuleContDto item: dto.getNmlzRuleConts()) {
			addToNmlzRuleContDb(nmlzRuleHdrSeq, item, currentUser);
		}
	}

	private void addToNmlzRuleContDb(Long nmlzRuleHdrSeq, NmlzRuleContDto dto, UserPrincipal currentUser) {
		NmlzRuleCont nmlzRuleCont;
		if(dto.getStatus().equals("U")) {
			nmlzRuleCont = dto.toEntityWithCurrentUser(nmlzRuleHdrSeq, dto.getNmlzRuleContSeq(), currentUser);
		} else {
			nmlzRuleCont = dto.toEntityWithCurrentUser(nmlzRuleHdrSeq, null, currentUser);
		}
		nmlzRuleContDao.insertNmlzRuleCont(nmlzRuleCont);

		Long nmlzRuleContSeq = nmlzRuleCont.getNmlzRuleContSeq() == -1 ? dto.getNmlzRuleContSeq(): nmlzRuleCont.getNmlzRuleContSeq();
		for(NmlzRuleFieldDto field: dto.getNmlzRuleFields()) {
			nmlzRuleFieldDao.insertNmlzRuleField(field.toEntityWithCurrentUser(nmlzRuleContSeq, currentUser));
		}	

		for(NmlzRuleFieldFuncDto fieldFunc: dto.getNmlzRuleFieldFuncs()) {
			nmlzRuleFieldFuncDao.insertNmlzRuleFieldFunc(fieldFunc.toEntityWithCurrentUser(nmlzRuleContSeq, currentUser));
		}	

		for(NmlzRuleFieldMergeDto fieldMerge: dto.getNmlzRuleFieldMerges()) {
			nmlzRuleFieldMergeDao.insertNmlzRuleFieldMerge(fieldMerge.toEntityWithCurrentUser(nmlzRuleContSeq, currentUser));
		}	

		for(NmlzRuleFieldReplaceDto fieldReplace: dto.getNmlzRuleFieldReplaces()) {
			nmlzRuleFieldReplaceDao.insertNmlzRuleFieldReplace(fieldReplace.toEntityWithCurrentUser(nmlzRuleContSeq, currentUser));
		}
	}

	private void removeAtNmlzRuleDb(Long nmlzRuleSeq) { // DB 삭제

		List<NmlzRuleHdr> nmlzRuleHdrs = nmlzRuleHdrDao.selectNmlzRuleHdrsByNmlzRuleSeq(nmlzRuleSeq);
		for(NmlzRuleHdr nmlzRuleHdr: nmlzRuleHdrs) {
			List<NmlzRuleCont> nmlzRuleConts = nmlzRuleContDao.selectNmlzRuleContsByNmlzRuleSeq(nmlzRuleHdr.getNmlzRuleHdrSeq());
			for(NmlzRuleCont nmlzRuleCont: nmlzRuleConts) {
				nmlzRuleFieldDao.deleteNmlzRuleFieldByNmlzRuleContSeq(nmlzRuleCont.getNmlzRuleContSeq());
				nmlzRuleFieldFuncDao.deleteNmlzRuleFieldFuncByNmlzRuleContSeq(nmlzRuleCont.getNmlzRuleContSeq());
				nmlzRuleFieldMergeDao.deleteNmlzRuleFieldMergeByNmlzRuleContSeq(nmlzRuleCont.getNmlzRuleContSeq());
				nmlzRuleFieldReplaceDao.deleteNmlzRuleFieldReplaceByNmlzRuleContSeq(nmlzRuleCont.getNmlzRuleContSeq());	
			}
			nmlzRuleContDao.deleteNmlzRuleContByNmlzRuleSeq(nmlzRuleHdr.getNmlzRuleHdrSeq());
		}
		nmlzRuleHdrDao.deleteNmlzRuleHdrByNmlzRuleSeq(nmlzRuleSeq);
		nmlzRuleAgentDao.deleteNmlzRuleAgentByNmlzRuleSeq(nmlzRuleSeq);
		nmlzRuleDao.deleteNmlzRuleBySeq(nmlzRuleSeq);
	}

	//이력 목록
	public PagedResponse<NmlzRuleHistDto> getNmlzRuleHists(Long nmlzRuleSeq, Pageable pageable) {
		Page<NmlzRuleHist> page = nmlzRuleHistDao.selectNmlzRuleHists(nmlzRuleSeq, pageable);

		List<NmlzRuleHistDto> dtos = new ArrayList<>();
		List<NmlzRuleHist> hists = page.getContent();

		for(int i = 0; i < page.getContent().size(); i++) {
			if(page.getNumber() == 0 && i == 0) {
				dtos.add(hists.get(i).toDto(true));
			} else {
				dtos.add(hists.get(i).toDto(false));
			}	
		}

		if (page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(), page.getSize(),
					page.getTotalElements(), page.getTotalPages(), page.isLast());
		} else {
			return new PagedResponse<>(dtos, page.getNumber(), page.getSize(), page.getTotalElements(),
					page.getTotalPages(), page.isLast());
		}
	}

	public NmlzRuleHistDto getNmlzRuleHistsByNmlzRuleHistSeq(Long nmlzRuleHistSeq, String locale) {
		NmlzRuleHist nmlzRuleHist = nmlzRuleHistDao.selectNmlzRuleHistsBySeq(nmlzRuleHistSeq);

		List<NmlzRuleAgentHist> nmlzRuleAgentHists = nmlzRuleAgentHistDao.selectNmlzRuleAgentHistsByNmlzRuleHistSeq(nmlzRuleHist.getNmlzRuleHistSeq(), locale);
		List<NmlzRuleAgentHistDto> nmlzRuleAgentHistDtos = new ArrayList<>();
		for(int i = 0; i < nmlzRuleAgentHists.size(); i++) {
			nmlzRuleAgentHistDtos.add(nmlzRuleAgentHists.get(i).toDto());
		}

		List<NmlzRuleHdrHist> nmlzRuleHdrHists = nmlzRuleHdrHistDao.selectNmlzRuleHdrHistsByNmlzRuleHistSeq(nmlzRuleHist.getNmlzRuleHistSeq());
		List<NmlzRuleHdrHistDto> nmlzRuleHdrHistDtos = new ArrayList<>();
		for(int i = 0; i < nmlzRuleHdrHists.size(); i++) {

			List<NmlzRuleContHist> nmlzRuleContHists = nmlzRuleContHistDao.selectNmlzRuleContHistsByNmlzRuleHdrHistSeq(nmlzRuleHdrHists.get(i).getNmlzRuleHdrHistSeq());
			List<NmlzRuleContHistDto> nmlzRuleContHistDtos = new ArrayList<>();
			for(int j = 0; j < nmlzRuleContHists.size(); j++) {

				List<NmlzRuleFieldHist> nmlzRuleFieldHists = nmlzRuleFieldHistDao.selectNmlzRuleFieldHistsByNmlzRuleContHistSeq(nmlzRuleContHists.get(j).getNmlzRuleContHistSeq());
				List<NmlzRuleFieldFuncHist> nmlzRuleFieldFuncHists = nmlzRuleFieldFuncHistDao.selectNmlzRuleFieldFuncHistsByNmlzRuleContHistSeq(nmlzRuleContHists.get(j).getNmlzRuleContHistSeq(), locale);
				List<NmlzRuleFieldMergeHist> nmlzRuleFieldMergeHists = nmlzRuleFieldMergeHistDao.selectNmlzRuleFieldMergeHistsByNmlzRuleContHistSeq(nmlzRuleContHists.get(j).getNmlzRuleContHistSeq());
				List<NmlzRuleFieldReplaceHist> nmlzRuleFieldReplaceHists = nmlzRuleFieldReplaceHistDao.selectNmlzRuleFieldReplaceHistsByNmlzRuleContHistSeq(nmlzRuleContHists.get(j).getNmlzRuleContHistSeq());

				List<NmlzRuleFieldHistDto> nmlzRuleFieldHistDtos = new ArrayList<>();
				for(int h = 0; h < nmlzRuleFieldHists.size(); h++) {
					nmlzRuleFieldHistDtos.add(nmlzRuleFieldHists.get(h).toDto());
				}
				List<NmlzRuleFieldFuncHistDto> nmlzRuleFieldFuncHistDtos = new ArrayList<>();
				for(int h = 0; h < nmlzRuleFieldFuncHists.size(); h++) {
					nmlzRuleFieldFuncHistDtos.add(nmlzRuleFieldFuncHists.get(h).toDto());
				}
				List<NmlzRuleFieldMergeHistDto> nmlzRuleFieldMergeHistDtos = new ArrayList<>();
				for(int h = 0; h < nmlzRuleFieldMergeHists.size(); h++) {
					nmlzRuleFieldMergeHistDtos.add(nmlzRuleFieldMergeHists.get(h).toDto());
				}
				List<NmlzRuleFieldReplaceHistDto> nmlzRuleFieldReplaceHistDtos = new ArrayList<>();
				for(int h = 0; h < nmlzRuleFieldReplaceHists.size(); h++) {
					nmlzRuleFieldReplaceHistDtos.add(nmlzRuleFieldReplaceHists.get(h).toDto());
				}
				NmlzRuleContHistDto dto = NmlzRuleContHistDto.builder()
						.nmlzRuleContHistSeq(nmlzRuleContHists.get(j).getNmlzRuleContHistSeq())
						.nmlzRuleHdrHistSeq(nmlzRuleContHists.get(j).getNmlzRuleHdrHistSeq())
						.defaultUseYn(nmlzRuleContHists.get(j).getDefaultUseYn())
						.eventTypeCode(nmlzRuleContHists.get(j).getEventTypeCode().equals("") ? null : new ArrayList<String>(Arrays.asList(nmlzRuleContHists.get(j).getEventTypeCode().split(","))))
						//.eventTypeCode(new ArrayList<String>(Arrays.asList(nmlzRuleContHists.get(j).getEventTypeCode().split(","))))
						.contRegexp(nmlzRuleContHists.get(j).getContRegexp())
						.contSampLog(nmlzRuleContHists.get(j).getContSampLog())
						.contPriority(nmlzRuleContHists.get(j).getContPriority())
						.contDesc(nmlzRuleContHists.get(j).getContDesc())
						.useYn(nmlzRuleContHists.get(j).getUseYn())
						.nmlzRuleFieldHists(nmlzRuleFieldHistDtos)
						.nmlzRuleFieldFuncHists(nmlzRuleFieldFuncHistDtos)
						.nmlzRuleFieldMergeHists(nmlzRuleFieldMergeHistDtos)
						.nmlzRuleFieldReplaceHists(nmlzRuleFieldReplaceHistDtos)
						.build();

				nmlzRuleContHistDtos.add(dto);
			}

			NmlzRuleHdrHistDto dto = NmlzRuleHdrHistDto.builder()
					.nmlzRuleSeq(nmlzRuleHdrHists.get(i).getNmlzRuleSeq())
					.nmlzRuleHistSeq(nmlzRuleHdrHists.get(i).getNmlzRuleHistSeq())
					.nmlzRuleHdrRegexp(nmlzRuleHdrHists.get(i).getNmlzRuleHdrRegexp())
					.nmlzRuleHdrSampLog(nmlzRuleHdrHists.get(i).getNmlzRuleHdrSampLog())
					.nmlzRuleHdrPrefix(nmlzRuleHdrHists.get(i).getNmlzRuleHdrPrefix())
					.nmlzRuleHdrKeyField(nmlzRuleHdrHists.get(i).getNmlzRuleHdrKeyField())
					.hdrPriority(nmlzRuleHdrHists.get(i).getHdrPriority())
					.useYn(nmlzRuleHdrHists.get(i).getUseYn())
					.nmlzRuleContHists(nmlzRuleContHistDtos)
					.build();
			nmlzRuleHdrHistDtos.add(dto);
		}

		NmlzRuleHistDto nmlzRuleHistDto = NmlzRuleHistDto.builder()
				.nmlzRuleHistSeq(nmlzRuleHist.getNmlzRuleSeq())
				.nmlzRuleSeq(nmlzRuleHist.getNmlzRuleSeq())
				.nmlzRuleNm(nmlzRuleHist.getNmlzRuleNm())
				.logFormatCode(nmlzRuleHist.getLogFormatCode())
				.nmlzRuleDesc(nmlzRuleHist.getNmlzRuleDesc())
				.nmlzRuleChgReason(nmlzRuleHist.getNmlzRuleChgReason())
				.useYn(nmlzRuleHist.getUseYn())
				.nmlzRuleAgentHists(nmlzRuleAgentHistDtos)
				.nmlzRuleHdrHists(nmlzRuleHdrHistDtos)
				.build();

		return nmlzRuleHistDto;
	}

	public void rollbackNmlzRule(NmlzRuleRollBackDto dto, UserPrincipal currentUser, String locale) {

		/**
		 * 선택한 history seq 로 선택한 history 정보를 가져온 다음 
		 */
		NmlzRuleHist nmlzRuleHist = nmlzRuleHistDao.selectNmlzRuleHistsBySeq(dto.getNmlzRuleHistSeq());
		if(!isExistNmlzRuleByName(nmlzRuleHist.getNmlzRuleNm(), nmlzRuleHist.getNmlzRuleSeq())){
			/**
			 * 선택한 history의 rule 정보를 제거한 후 
			 */
			removeAtNmlzRuleDb(nmlzRuleHist.getNmlzRuleSeq());

			/**
			 * 현재 rule 데이터 추가
			 */
			addToNmlzRuleDb(nmlzRuleHist.toNmlzRuleEntity(currentUser));

			List<NmlzRuleAgentHist> nmlzRuleAgentHists = nmlzRuleAgentHistDao.selectNmlzRuleAgentHistsByNmlzRuleHistSeq(dto.getNmlzRuleHistSeq(), locale);
			for(NmlzRuleAgentHist nmlzRuleAgentHist : nmlzRuleAgentHists) {
				addToNmlzRuleAgentDb(nmlzRuleAgentHist.toNmlzRuleAgentEntity(nmlzRuleHist.getNmlzRuleSeq(), currentUser));
			}

			List<NmlzRuleHdrHist> nmlzRuleHdrHists = nmlzRuleHdrHistDao.selectNmlzRuleHdrHistsByNmlzRuleHistSeq(dto.getNmlzRuleHistSeq());
			for(NmlzRuleHdrHist nmlzRuleHdrHist : nmlzRuleHdrHists) {

				List<NmlzRuleContHist> nmlzRuleContHists = nmlzRuleContHistDao.selectNmlzRuleContHistsByNmlzRuleHdrHistSeq(nmlzRuleHdrHist.getNmlzRuleHdrHistSeq());
				List<NmlzRuleContDto> nmlzRuleContDtos = new ArrayList<>();
				for(NmlzRuleContHist nmlzRuleContHist : nmlzRuleContHists) {

					List<NmlzRuleFieldHist> nmlzRuleFieldHists = nmlzRuleFieldHistDao
							.selectNmlzRuleFieldHistsByNmlzRuleContHistSeq(nmlzRuleContHist.getNmlzRuleContHistSeq());

					List<NmlzRuleFieldDto> nmlzRuleFieldDtos = new ArrayList<>();
					for(NmlzRuleFieldHist nmlzRuleFieldHist: nmlzRuleFieldHists) {
						nmlzRuleFieldDtos.add(nmlzRuleFieldHist.toNmlzRuleFieldDto());
					}

					List<NmlzRuleFieldFuncHist> nmlzRuleFieldFuncHists = nmlzRuleFieldFuncHistDao
							.selectNmlzRuleFieldFuncHistsByNmlzRuleContHistSeq(nmlzRuleContHist.getNmlzRuleContHistSeq(), locale);

					List<NmlzRuleFieldFuncDto> nmlzRuleFieldFuncDtos = new ArrayList<>();
					for(NmlzRuleFieldFuncHist nmlzRuleFieldFuncHist: nmlzRuleFieldFuncHists) {
						nmlzRuleFieldFuncDtos.add(nmlzRuleFieldFuncHist.toNmlzRuleFieldFuncDto());
					}

					List<NmlzRuleFieldMergeHist> nmlzRuleFieldMergeHists = nmlzRuleFieldMergeHistDao
							.selectNmlzRuleFieldMergeHistsByNmlzRuleContHistSeq(nmlzRuleContHist.getNmlzRuleContHistSeq());

					List<NmlzRuleFieldMergeDto> nmlzRuleFieldMergeDtos = new ArrayList<>();
					for(NmlzRuleFieldMergeHist nmlzRuleFieldMergeHist: nmlzRuleFieldMergeHists) {
						nmlzRuleFieldMergeDtos.add(nmlzRuleFieldMergeHist.toNmlzRuleFieldMergeDto());
					}

					List<NmlzRuleFieldReplaceHist> nmlzRuleFieldReplaceHists = nmlzRuleFieldReplaceHistDao
							.selectNmlzRuleFieldReplaceHistsByNmlzRuleContHistSeq(nmlzRuleContHist.getNmlzRuleContHistSeq());

					List<NmlzRuleFieldReplaceDto> nmlzRuleFieldReplaceDtos = new ArrayList<>();
					for(NmlzRuleFieldReplaceHist nmlzRuleFieldReplaceHist: nmlzRuleFieldReplaceHists) {
						nmlzRuleFieldReplaceDtos.add(nmlzRuleFieldReplaceHist.toNmlzRuleFieldReplaceDto());
					}

					NmlzRuleContDto nmlzRuleContDto = NmlzRuleContDto.builder()
							.nmlzRuleHdrSeq(nmlzRuleContHist.getNmlzRuleHdrSeq())
							.defaultUseYn(nmlzRuleContHist.getDefaultUseYn())
							.eventTypeCode(nmlzRuleContHist.getEventTypeCode().equals("") ? null : new ArrayList<String>(Arrays.asList(nmlzRuleContHist.getEventTypeCode().split(","))))
							//.eventTypeCode(new ArrayList<String>(Arrays.asList(nmlzRuleContHist.getEventTypeCode().split(","))))
							.contRegexp(nmlzRuleContHist.getContRegexp())
							.contSampLog(nmlzRuleContHist.getContSampLog())
							.contPriority(nmlzRuleContHist.getContPriority())
							.contDesc(nmlzRuleContHist.getContDesc())
							.status("U")
							.useYn(nmlzRuleContHist.getUseYn())
							.nmlzRuleFields(nmlzRuleFieldDtos)
							.nmlzRuleFieldFuncs(nmlzRuleFieldFuncDtos)
							.nmlzRuleFieldMerges(nmlzRuleFieldMergeDtos)
							.nmlzRuleFieldReplaces(nmlzRuleFieldReplaceDtos)
							.build();

					nmlzRuleContDtos.add(nmlzRuleContDto);
				}
				addToNmlzRuleHdrDb(nmlzRuleHist.getNmlzRuleSeq(), nmlzRuleHdrHist.toNmlzRuleHdrDto(nmlzRuleHist.getNmlzRuleSeq(), nmlzRuleContDtos), currentUser);
			}

			/**
			 * 현재 룰 정보를 이력 테이블에 추가 
			 */
			moveToHistoricalDb(nmlzRuleHist.getNmlzRuleSeq(), dto.getChgReason(), currentUser, locale);
		} else {
			throw new BusinessException(ErrorCode.DUPLICATE_DATA_EXIST);
		}
	}

	public void deployNmlzRule(UserPrincipal currentUser) {
		insertZkData(currentUser);
	}

	void insertZkForNmlzRuleCont(EngineMenuType menuType, UserPrincipal currentUser) {
		EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());


		String separator = "";
		if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
			separator = ZkUtils.getHexToText(ec.getDataSprtn());
		} else {
			separator = ZkUtils.TAB;
		}

		List<NmlzRuleZkCont> nmlzRuleConts = nmlzRuleContDao.selectAllNmlzRuleZkConts();
		List<Integer> lengthList = new ArrayList<>();

		StringBuilder sb = new StringBuilder();
		// 헤더정보가 존재하는 경우 첫 라인에 추가
		if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
			String hdr = ec.getHdrDtl();
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
			}
			sb.append(hdr);
			if (nmlzRuleConts != null && nmlzRuleConts.size() > 0) {
				sb.append(ZkUtils.NL);
			}
		}

		if (nmlzRuleConts != null && nmlzRuleConts.size() > 0) {
			for(int i = 0; i < nmlzRuleConts.size(); i++) {
				if(i < nmlzRuleConts.size()-1) {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleConts.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);
				} else {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleConts.get(i), separator, false);
					lengthList.add(data.length());
					sb.append(data);
				}
			}

			zkService.setDataForPath(
					lengthList, sb.toString(), currentUser.getUsername(), menuType);
		}
	}

	void insertZkForNmlzRuleField(EngineMenuType menuType, UserPrincipal currentUser) {

		EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());

		String separator = "";
		if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
			separator = ZkUtils.getHexToText(ec.getDataSprtn());
		} else {
			separator = ZkUtils.TAB;
		}

		List<NmlzRuleZkField> nmlzRuleFields = nmlzRuleFieldDao.selectAllNmlzRuleZkFields();
		List<Integer> lengthList = new ArrayList<>();

		StringBuilder sb = new StringBuilder();
		// 헤더정보가 존재하는 경우 첫 라인에 추가
		if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
			String hdr = ec.getHdrDtl();
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
			}
			sb.append(hdr);
			if (nmlzRuleFields != null && nmlzRuleFields.size() > 0) {
				sb.append(ZkUtils.NL);
			}
		}

		if (nmlzRuleFields != null && nmlzRuleFields.size() > 0) {
			for(int i = 0; i < nmlzRuleFields.size(); i++) {
				if(i < nmlzRuleFields.size()-1) {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleFields.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);
				} else {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleFields.get(i), separator, false);
					lengthList.add(data.length());
					sb.append(data);
				}
			}

			zkService.setDataForPath(
					lengthList, sb.toString(), currentUser.getUsername(), menuType);
		}
	}

	void insertZkForNmlzRuleFieldFunc(EngineMenuType menuType, UserPrincipal currentUser, String locale) {

		EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());

		String separator = "";
		if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
			separator = ZkUtils.getHexToText(ec.getDataSprtn());
		} else {
			separator = ZkUtils.TAB;
		}

		List<NmlzRuleZkFieldFunc> nmlzRuleFieldFuncs = nmlzRuleFieldFuncDao.selectAllNmlzRuleZkFieldFuncs(locale);
		List<Integer> lengthList = new ArrayList<>();

		StringBuilder sb = new StringBuilder();
		// 헤더정보가 존재하는 경우 첫 라인에 추가
		if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
			String hdr = ec.getHdrDtl();
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
			}
			sb.append(hdr);
			if (nmlzRuleFieldFuncs != null && nmlzRuleFieldFuncs.size() > 0) {
				sb.append(ZkUtils.NL);
			}
		}

		if (nmlzRuleFieldFuncs != null && nmlzRuleFieldFuncs.size() > 0) {
			for(int i = 0; i < nmlzRuleFieldFuncs.size(); i++) {
				if(i < nmlzRuleFieldFuncs.size()-1) {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleFieldFuncs.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);
				} else {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleFieldFuncs.get(i), separator, false);
					lengthList.add(data.length());
					sb.append(data);
				}
			}

			zkService.setDataForPath(
					lengthList, sb.toString(), currentUser.getUsername(), menuType);
		}
	}

	void insertZkForNmlzRuleFieldMerge(EngineMenuType menuType, UserPrincipal currentUser) {
		EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());

		String separator = "";
		if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
			separator = ZkUtils.getHexToText(ec.getDataSprtn());
		} else {
			separator = ZkUtils.TAB;
		}

		List<NmlzRuleZkFieldMerge> nmlzRuleFieldMerges = nmlzRuleFieldMergeDao.selectAllNmlzRuleZkFieldMerges();
		List<Integer> lengthList = new ArrayList<>();

		StringBuilder sb = new StringBuilder();
		// 헤더정보가 존재하는 경우 첫 라인에 추가
		if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
			String hdr = ec.getHdrDtl();
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
			}
			sb.append(hdr);
			if (nmlzRuleFieldMerges != null && nmlzRuleFieldMerges.size() > 0) {
				sb.append(ZkUtils.NL);
			}
		}

		if (nmlzRuleFieldMerges != null && nmlzRuleFieldMerges.size() > 0) {
			for(int i = 0; i < nmlzRuleFieldMerges.size(); i++) {
				if(i < nmlzRuleFieldMerges.size()-1) {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleFieldMerges.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);
				} else {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleFieldMerges.get(i), separator, false);
					lengthList.add(data.length());
					sb.append(data);
				}
			}

			zkService.setDataForPath(
					lengthList, sb.toString(), currentUser.getUsername(), menuType);
		}
	}

	void insertZkForNmlzRuleFieldReplace(EngineMenuType menuType, UserPrincipal currentUser) {
		EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());

		String separator = "";
		if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
			separator = ZkUtils.getHexToText(ec.getDataSprtn());
		} else {
			separator = ZkUtils.TAB;
		}

		List<NmlzRuleZkFieldReplace> nmlzRuleFieldReplaces = nmlzRuleFieldReplaceDao.selectAllNmlzRuleZkFieldReplaces();
		List<Integer> lengthList = new ArrayList<>();

		StringBuilder sb = new StringBuilder();
		// 헤더정보가 존재하는 경우 첫 라인에 추가
		if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
			String hdr = ec.getHdrDtl();
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
			}
			sb.append(hdr);
			if (nmlzRuleFieldReplaces != null && nmlzRuleFieldReplaces.size() > 0) {
				sb.append(ZkUtils.NL);
			}
		}

		if (nmlzRuleFieldReplaces != null && nmlzRuleFieldReplaces.size() > 0) {
			for(int i = 0; i < nmlzRuleFieldReplaces.size(); i++) {
				if(i < nmlzRuleFieldReplaces.size()-1) {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleFieldReplaces.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);
				} else {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleFieldReplaces.get(i), separator, false);
					lengthList.add(data.length());
					sb.append(data);
				}
			}

			zkService.setDataForPathWithWatchMsg(
					lengthList, sb.toString(), currentUser.getUsername(), menuType);
		}
	}

	private void insertZkForNmlzRuleHdr(EngineMenuType menuType, UserPrincipal currentUser) {
		EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());

		String separator = "";
		if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
			separator = ZkUtils.getHexToText(ec.getDataSprtn());
		} else {
			separator = ZkUtils.TAB;
		}

		List<NmlzRuleZkHdr> nmlzRuleHdrs = nmlzRuleHdrDao.selectAllNmlzRuleZkHdrs();
		List<Integer> lengthList = new ArrayList<>();

		StringBuilder sb = new StringBuilder();
		// 헤더정보가 존재하는 경우 첫 라인에 추가
		if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
			String hdr = ec.getHdrDtl();
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
			}
			sb.append(hdr);
			if (nmlzRuleHdrs != null && nmlzRuleHdrs.size() > 0) {
				sb.append(ZkUtils.NL);
			}
		}

		if (nmlzRuleHdrs != null && nmlzRuleHdrs.size() > 0) {
			for(int i = 0; i < nmlzRuleHdrs.size(); i++) {
				if(i < nmlzRuleHdrs.size()-1) {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleHdrs.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);
				} else {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleHdrs.get(i), separator, false);
					lengthList.add(data.length());
					sb.append(data);
				}
			}

			zkService.setDataForPathWithWatchMsg(
					lengthList, sb.toString(), currentUser.getUsername(), menuType);
		}
	}

	private void insertZkForNmlzRuleTokenize(EngineMenuType menuType, UserPrincipal currentUser) {
		EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());

		String separator = "";
		if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
			separator = ZkUtils.getHexToText(ec.getDataSprtn());
		} else {
			separator = ZkUtils.TAB;
		}

		List<NmlzRuleZkTokenize> nmlzRuleZkTokenizes = nmlzRuleContDao.selectAllNmlzRuleZkTokenize();
		List<Integer> lengthList = new ArrayList<>();

		StringBuilder sb = new StringBuilder();
		// 헤더정보가 존재하는 경우 첫 라인에 추가
		if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
			String hdr = ec.getHdrDtl();
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
			}
			sb.append(hdr);
			if (nmlzRuleZkTokenizes != null && nmlzRuleZkTokenizes.size() > 0) {
				sb.append(ZkUtils.NL);
			}
		}

		if (nmlzRuleZkTokenizes != null && nmlzRuleZkTokenizes.size() > 0) {
			for(int i = 0; i < nmlzRuleZkTokenizes.size(); i++) {
				if(i < nmlzRuleZkTokenizes.size()-1) {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleZkTokenizes.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);
				} else {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleZkTokenizes.get(i), separator, false);
					lengthList.add(data.length());
					sb.append(data);
				}
			}

			zkService.setDataForPathWithWatchMsg(
					lengthList, sb.toString(), currentUser.getUsername(), menuType);
		}
	}

	@SuppressWarnings("unused")
	private void insertZkForNmlzRuleArrage(EngineMenuType menuType, UserPrincipal currentUser) {
		EngineCommand ec = engineCommandDao.selectEngineCommandByCmd(menuType.name());

		String separator = "";
		if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
			separator = ZkUtils.getHexToText(ec.getDataSprtn());
		} else {
			separator = ZkUtils.TAB;
		}

		List<NmlzRuleZkArrange> nmlzRuleZkArranges = nmlzRuleFieldDao.selectAllNmlzRuleZkArrange();
		List<Integer> lengthList = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		// 헤더정보가 존재하는 경우 첫 라인에 추가
		if (StringUtils.isNotEmpty(ec.getHdrDtl())) {
			String hdr = ec.getHdrDtl();
			if (StringUtils.isNotEmpty(ec.getDataSprtn())) {
				hdr = ec.getHdrDtl().replaceAll(ec.getDataSprtn(), separator);
			}
			sb.append(hdr);
			if (nmlzRuleZkArranges != null && nmlzRuleZkArranges.size() > 0) {
				sb.append(ZkUtils.NL);
			}
		}

		if (nmlzRuleZkArranges != null && nmlzRuleZkArranges.size() > 0) {
			for(int i = 0; i < nmlzRuleZkArranges.size(); i++) {
				if(i < nmlzRuleZkArranges.size()-1) {
					String data = ZkUtils.convertZkDataFormat(nmlzRuleZkArranges.get(i), separator, true);
					lengthList.add(data.length());
					sb.append(data);

					if(nmlzRuleZkArranges.get(i).getSubFieldId() != null && !nmlzRuleZkArranges.get(i).getSubFieldId().equals("") ) {
						String data2 = ZkUtils.convertZkDataFormat2(nmlzRuleZkArranges.get(i), separator, true);
						lengthList.add(data2.length());
						sb.append(data2);	
					}					
				} else {
					if(nmlzRuleZkArranges.get(i).getSubFieldId() != null && !nmlzRuleZkArranges.get(i).getSubFieldId().equals("") ) {						
						String data = ZkUtils.convertZkDataFormat(nmlzRuleZkArranges.get(i), separator, true);
						lengthList.add(data.length());
						sb.append(data);

						String data2 = ZkUtils.convertZkDataFormat2(nmlzRuleZkArranges.get(i), separator, false);
						lengthList.add(data2.length());
						sb.append(data2);
					} else {					
						String data = ZkUtils.convertZkDataFormat(nmlzRuleZkArranges.get(i), separator, false);
						lengthList.add(data.length());
						sb.append(data);
					}
				}
			}

			zkService.setDataForPathWithWatchMsg(
					lengthList, sb.toString(), currentUser.getUsername(), menuType);
		}
	}
	void insertZkData(UserPrincipal currentUser) {
		//dmkwon /수정필요
		insertZkForNmlzRuleHdr(EngineMenuType.UPD_NORM_HEADER_RULES, currentUser);
		insertZkForNmlzRuleTokenize(EngineMenuType.UPD_NORM_TOKENIZE_RULES, currentUser);
		insertZkForNmlzRuleArrage(EngineMenuType.UPD_NORM_ARRANGE_RULES, currentUser);

		//		insertZkForNmlzRuleField(EngineMenuType.UPD_NORM_FIELD, currentUser);
		//		insertZkForNmlzRuleFieldFunc(EngineMenuType.UPD_NORM_FIELD_FUNC, currentUser);
		//		insertZkForNmlzRuleFieldMerge(EngineMenuType.UPD_NORM_FIELD_MERGE, currentUser);
		//		insertZkForNmlzRuleFieldReplace(EngineMenuType.UPD_NORM_FIELD_REPLACE, currentUser);
	}

}