package com.secudium.api.rulemgmt.rule.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.rulemgmt.dstrbt.dto.RuleDistHistoryDto;
import com.secudium.api.rulemgmt.rule.dto.RuleDeleteDto;
import com.secudium.api.rulemgmt.rule.dto.RuleInsertDto;
import com.secudium.api.rulemgmt.rule.dto.RuleSelectDto;
import com.secudium.api.rulemgmt.rule.dto.RuleThreatTaxoDto;
import com.secudium.api.rulemgmt.rule.dto.RuleUpdateDto;
import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.api.rulemgmt.rule.service.RuleService;
import com.secudium.common.dto.OkResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/*
 * git commit test
 * @author intellicode
 *
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/rulemgmt")
public class RuleController {

	private final RuleService ruleService;

	@GetMapping("/rules/all")
	public ResponseEntity<List<Rule>> getRules(
			HttpServletRequest request, 
			Locale locale) {
		SearchMap searchMap = SearchMap.buildFrom(request);		
		return ResponseEntity.ok(ruleService.getRules(searchMap, locale.getLanguage()));
	}

	@PostMapping("/rules")
	public ResponseEntity<List<Rule>> getRulesByThreatClasSeq(
			HttpServletRequest request,
			@RequestBody @Valid final RuleThreatTaxoDto dto, 
			Locale locale) {
		SearchMap searchMap = SearchMap.buildFrom(request);		
		return ResponseEntity.ok(ruleService.getRulesByThreatClasSeq(searchMap, dto.getThreatClasSeq(), locale.getLanguage()));
	}

	@PostMapping("/rulebyseq")
	public ResponseEntity<RuleSelectDto> getRuleByRuleSeq(
			HttpServletRequest request,
			@RequestBody @Valid final Rule dto, 
			Locale locale) {
		return ResponseEntity.ok(ruleService.getRuleByRuleSeq(dto.getRuleSeq(), locale.getLanguage()));
	}

	@PostMapping("/rule")
	public ResponseEntity<OkResponse> insertRule(
			@RequestBody @Valid final RuleInsertDto dto,
			@CurrentUser UserPrincipal currentUser) {
		ruleService.insertRule(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/rule")
	public ResponseEntity<OkResponse> updateRule(
			@RequestBody @Valid final RuleUpdateDto dto,
			@CurrentUser UserPrincipal currentUser) {
		ruleService.updateRule(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/rule/delete/bin")
	public ResponseEntity<OkResponse> deleteRuleFromBin(
			@RequestBody @Valid final RuleDeleteDto dto,
			@CurrentUser UserPrincipal currentUser) {
		ruleService.deleteRuleFromBin(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/rule/delete")
	public ResponseEntity<OkResponse> deleteRule(
			@RequestBody @Valid final RuleDeleteDto dto,
			@CurrentUser UserPrincipal currentUser) {
		ruleService.deleteRule(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/rule/threattaxo")
	public ResponseEntity<OkResponse> setThreatTaxo(
			@RequestBody @Valid final RuleThreatTaxoDto dto,
			@CurrentUser UserPrincipal currentUser) {
		ruleService.setThreatTaxo(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/rule/revert")
	public ResponseEntity<OkResponse> revertRule(
			@RequestBody @Valid final RuleUpdateDto dto,
			@CurrentUser UserPrincipal currentUser) {
		ruleService.revertRule(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/rule/zkdist")
	public ResponseEntity<OkResponse> distributeRules(@RequestBody RuleDistHistoryDto dto, @CurrentUser UserPrincipal currentUser) {
		ruleService.distributeRules(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}
}
