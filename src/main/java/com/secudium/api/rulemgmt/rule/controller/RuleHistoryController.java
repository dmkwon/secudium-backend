package com.secudium.api.rulemgmt.rule.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.rulemgmt.rule.dto.RuleSelectDto;
import com.secudium.api.rulemgmt.rule.dto.RuleUpdateDto;
import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.api.rulemgmt.rule.service.RuleHistoryService;
import com.secudium.common.dto.OkResponse;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/rulemgmt")
public class RuleHistoryController {

	private final RuleHistoryService ruleHistoryService;

	/** 룰 변경이력 목록 조회(룰 일련번호) */
	@PostMapping("/rules/histories")
	public ResponseEntity<List<Rule>> getRuleHistsByRuleSeq(
			HttpServletRequest request,
			@RequestBody @Valid final Rule dto, 
			Locale locale) {
		return ResponseEntity.ok(ruleHistoryService.getRuleHistsByRuleSeq(dto.getRuleSeq(), locale.getLanguage()));
	}

	/** 룰 변경이력 조회(룰이력 일련번호) */
	@PostMapping("/rule/history")
	public ResponseEntity<RuleSelectDto> getRuleHistByRuleHistSeq(
			HttpServletRequest request,
			@RequestBody @Valid final Rule dto, 
			Locale locale) {
		return ResponseEntity.ok(ruleHistoryService.getRuleHistByRuleHistSeq(dto.getRuleHistSeq(), locale.getLanguage()));
	}

	/** 룰 변경이력 조회(룰이력 일련번호) */
	@PostMapping("/rule/history/latest")
	public ResponseEntity<RuleSelectDto> getLatestRuleHistory(
			HttpServletRequest request,
			@RequestBody @Valid final RuleSelectDto dto, 
			Locale locale) {
		return ResponseEntity.ok(ruleHistoryService.getLatestRuleHistory(dto.getRuleSeq(), locale.getLanguage()));
	}

	/** 룰 롤백 */
	@PostMapping("/rule/history/rollback")
	public ResponseEntity<OkResponse> rollbackHistoryByRuleHistSeq(
			@RequestBody @Valid final RuleUpdateDto dto,
			@CurrentUser UserPrincipal currentUser) {
		ruleHistoryService.rollbackHistoryByRuleHistSeq(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}
}
