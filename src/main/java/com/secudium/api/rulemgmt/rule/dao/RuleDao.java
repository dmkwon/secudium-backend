package com.secudium.api.rulemgmt.rule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.rule.dto.RuleSelectDto;
import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.common.entity.SearchMap;
import com.secudium.common.entity.Select;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class RuleDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public List<Rule> getRules(SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<Rule> getRules() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public List<Rule> getDistTargetRules() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public List<Rule> getRulesByThreatClasSeq(SearchMap search, Long threatClasSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		params.put("threatClasSeq", threatClasSeq);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public RuleSelectDto getRuleByRuleSeq(long ruleSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("ruleSeq", ruleSeq);
		params.put("lang", locale);
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public void insertRule(Rule rule) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), rule);
	}

	public void updateRule(Rule rule) {
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), rule);
	}

	public void deleteRule(Rule rule) {
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), rule);
	}

	public long getNewRuleSeq() {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public void updateDetectCndtn(Long ruleSeq, String detectCndtn) {
		Map<String, Object> params = new HashMap<>();
		params.put("ruleSeq", ruleSeq);
		params.put("detectCndtn", detectCndtn);
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<Select> getDistTargetFields() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}


}
