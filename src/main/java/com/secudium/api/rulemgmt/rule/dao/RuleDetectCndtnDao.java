package com.secudium.api.rulemgmt.rule.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.rule.dto.RuleDetectCndtnDto;
import com.secudium.api.rulemgmt.rule.entity.RuleDetectCndtn;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class RuleDetectCndtnDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public void deleteAllRuleDetectCndtn(Long ruleSeq) {
		sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), ruleSeq);
	}

	public void insertRuleDetectCndtn(RuleDetectCndtnDto vo) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), vo);
	}

	public List<RuleDetectCndtnDto> getRuleDetectCndtns(long ruleSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), ruleSeq);
	}
	
	public List<RuleDetectCndtn> getRuleDetectCndtnsForJson(long ruleSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), ruleSeq);
	}
	
}
