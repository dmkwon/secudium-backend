package com.secudium.api.rulemgmt.rule.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.rule.dto.RuleDetectCndtnDto;
import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class RuleDetectCndtnHistDao extends RepositorySupport {

	private final SqlSession sqlSession;


	public void insertRuleDetectCndtnHist(RuleDetectCndtnDto vo) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), vo);
	}

	public void insertRuleDetectCndtnHistByRuleSeq(Rule rule) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), rule);
	}

	public List<RuleDetectCndtnDto> getRuleDetectCndtnHist(long ruleHistSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), ruleHistSeq);
	}

	public void rollbackHistoryByRuleHistSeq(Rule rule) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), rule);
		
	}


}
