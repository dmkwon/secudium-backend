package com.secudium.api.rulemgmt.rule.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.rule.dto.RuleSelectDto;
import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class RuleHistoryDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public List<Rule> getRuleHistsByRuleSeq(long ruleSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("ruleSeq", ruleSeq);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public RuleSelectDto getRuleHistByRuleHistSeq(long ruleHistSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("ruleHistSeq", ruleHistSeq);
		params.put("lang", locale);
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public void rollbackHistoryByRuleHistSeq(Rule rule) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), rule);
	}

	public void insertRuleHistory(Rule rule) {
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), rule);
	}

	public void insertRuleHistoryByRuleSeq(Rule rule) {
		sqlSession.insert(mapperNs +  MethodUtils.getCurrentMethodName(), rule);
	}

	public RuleSelectDto getLatestRuleHistory(long ruleSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("ruleSeq", ruleSeq);
		params.put("lang", locale);
		return sqlSession.selectOne(mapperNs +  MethodUtils.getCurrentMethodName(), params);
	}

	public void updateDetectCndtn(Long ruleChgHistSeq, String detectCndtn) {
		Map<String, Object> params = new HashMap<>();
		params.put("ruleSeq", ruleChgHistSeq);
		params.put("detectCndtn", detectCndtn);
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

}
