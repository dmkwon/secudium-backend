package com.secudium.api.rulemgmt.rule.dto;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class RuleDetectCndtnDto extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = -7853024521042221774L;
	
	private String nodeSeq;			//트리구성을 위한 키
	private String parentNodeSeq;	//트리구성을 위한 키

	private Long ruleDetectCndtnSeq;				// NN
	private Long topRuleDetectCndtnSeq;		//parent node - NN
	private Long ruleSeq;								// NN
	private String cndtnGroupOprtr;
	private String filterNm;
	private Integer detectCndtnLevel;				//depth - NN
	private Integer detectCndtnSortOrder;		//order - NN
	private Long filterSeq;
	private Long filterHistSeq;
	
	private Long ruleHistSeq;							//이력생성을 위한 룰 이력 키
}
