package com.secudium.api.rulemgmt.rule.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class RuleSelectDto {
	private Long ruleSeq;
	private String ruleNm;
	private String ruleType;
	private String ruleTypeString;
	private String ruleCommitStrDt;
	private String ruleCommitEndDt;
	private String ruleCommitStrTime;
	private String ruleCommitEndTime;
	private String ruleCommitWeek;
	private String thrsldGroupField;
	private Integer thrsldGroupPeriod;
	private Integer thrsldGroupCnt;
	private String thrsldDstnctField;
	private Integer thrsldDstnctCnt;
	private Integer thrsldOprtrPeriod;
	private String thrsldOprtr;
	private String thrsldOprtrString;
	private String detectCndtn;
	private String droolsCode;
	private String relshpRule;
	private String cntrlTargt;
	private String custmNm;
	private String ruleDesc;
	private String commitYn;
	
	private String attackCode;
	private String attackCodeString;
	private String severityCode;
	private String severityCodeString;
	private String ruleGroupCode;
	private String ruleGroupCodeString;
	private String autoReportYn;
	private String comment;
	private String takeActionDescription;
	private String takeBActionDescription;
	
	private Long ruleHistSeq;
	private String ruleChgReason;

	private List<RuleDetectCndtnDto> filters;
}
