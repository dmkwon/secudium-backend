package com.secudium.api.rulemgmt.rule.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.rule.entity.DetectCndtn;
import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class RuleUpdateDto {
	private Long ruleSeq;
	private String ruleNm;
	private String ruleType;
	private String ruleCommitStrDt;
	private String ruleCommitEndDt;
	private String ruleCommitStrTime;
	private String ruleCommitEndTime;
	private String ruleCommitWeek;
	private String thrsldGroupField;
	private Integer thrsldGroupPeriod;
	private Integer thrsldGroupCnt;
	private String thrsldDstnctField;
	private Integer thrsldDstnctCnt;
	private Integer thrsldOprtrPeriod;
	private String thrsldOprtr;
	private String droolsCode;
	private String relshpRule;
	private String cntrlTargt;
	private String custmNm;
	private String ruleDesc;
	private String commitYn;
	
	private String attackCode;
	private String severityCode;
	private String ruleGroupCode;
	private String autoReportYn;
	private String comment;
	private String takeActionDescription;
	private String takeBActionDescription;
	
	private Long ruleHistSeq;
	private String ruleChgReason;
	
	private List<RuleDetectCndtnDto> filters;
	
	public Rule toWithCurrentUser(UserPrincipal currentUser) {
		return Rule.builder()
				.ruleHistSeq(ruleHistSeq)
				.ruleChgReason(ruleChgReason)
				.ruleSeq(ruleSeq)
				.ruleNm(ruleNm)
				.ruleType(ruleType)
				.ruleCommitStrDt(ruleCommitStrDt)
				.ruleCommitEndDt(ruleCommitEndDt)
				.ruleCommitStrTime(ruleCommitStrTime)
				.ruleCommitEndTime(ruleCommitEndTime)
				.ruleCommitWeek(ruleCommitWeek)
				.thrsldGroupField(thrsldGroupField)
				.thrsldGroupPeriod(thrsldGroupPeriod)
				.thrsldGroupCnt(thrsldGroupCnt)
				.thrsldDstnctField(thrsldDstnctField)
				.thrsldDstnctCnt(thrsldDstnctCnt)
				.thrsldOprtrPeriod(thrsldOprtrPeriod)
				.thrsldOprtr(thrsldOprtr)
				.droolsCode(droolsCode)
				.relshpRule(relshpRule)
				.cntrlTargt(cntrlTargt)
				.custmNm(custmNm)
				.ruleDesc(ruleDesc)
				.commitYn(commitYn)
				.attackCode(attackCode)
				.severityCode(severityCode)
				.ruleGroupCode(ruleGroupCode)
				.autoReportYn(autoReportYn)
				.comment(comment)
				.takeActionDescription(takeActionDescription)
				.takeBActionDescription(takeBActionDescription)
				.regUsrNo(currentUser.getUsrNo())
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}
	
	public DetectCndtn getDetectCndtn() {
		return DetectCndtn.builder()
				.ruleNm(ruleNm)
				.ruleType(ruleType)
				.ruleCommitStrDt(ruleCommitStrDt)
				.ruleCommitEndDt(ruleCommitEndDt)
				.ruleCommitStrTime(ruleCommitStrTime)
				.ruleCommitEndTime(ruleCommitEndTime)
				.ruleCommitWeek(ruleCommitWeek)
				.thrsldGroupField(thrsldGroupField)
				.thrsldGroupPeriod(thrsldGroupPeriod)
				.thrsldGroupCnt(thrsldGroupCnt)
				.thrsldDstnctField(thrsldDstnctField)
				.thrsldDstnctCnt(thrsldDstnctCnt)
				.thrsldOprtrPeriod(thrsldOprtrPeriod)
				.thrsldOprtr(thrsldOprtr)
				.attackCode(attackCode)
				.severityCode(severityCode)
				.ruleGroupCode(ruleGroupCode)
				.autoReportYn(autoReportYn)
				.comment(comment)
				.takeActionDescription(takeActionDescription)
				.takeBActionDescription(takeBActionDescription)
				.droolsCode(droolsCode)
				.relshpRule(relshpRule)
				.cntrlTargt(cntrlTargt)
				.custmNm(custmNm)
				.ruleDesc(ruleDesc)
				.commitYn(commitYn)
				.build();
	}
}
