package com.secudium.api.rulemgmt.rule.entity;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class DetectCndtn implements Serializable {
	
	private static final long serialVersionUID = -1195601989377357317L;
	
	private Long ruleSeq;
	private String ruleNm;
	private String ruleType;
	private String threatPath;
	private String ruleCommitStrDt;
	private String ruleCommitEndDt;
	private String ruleCommitStrTime;
	private String ruleCommitEndTime;
	private String ruleCommitWeek;
	private String thrsldGroupField;
	private Integer thrsldGroupPeriod;
	private Integer thrsldGroupCnt;
	private String thrsldDstnctField;
	private Integer thrsldDstnctCnt;
	private Integer thrsldOprtrPeriod;
	private String thrsldOprtr;
	private String droolsCode;
	private String relshpRule;
	private String attackCode;
	private String severityCode;
	private String ruleGroupCode;
	private String autoReportYn;
	private String comment;
	private String takeActionDescription;
	private String takeBActionDescription;
	private String cntrlTargt;
	private String custmNm;
	private String ruleDesc;
	private String commitYn;
	private String useYn;
	
	private List<RuleDetectCndtn> ruleDetectCndtn;
}
