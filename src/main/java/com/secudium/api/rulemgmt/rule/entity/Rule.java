package com.secudium.api.rulemgmt.rule.entity;

import java.io.Serializable;

import com.google.gson.Gson;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Rule extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 1749693862311496006L;
	
	private Long ruleSeq;
	private String ruleNm;
	private String ruleType;
	private String threatPath;
	private String ruleCommitStrDt;
	private String ruleCommitEndDt;
	private String ruleCommitStrTime;
	private String ruleCommitEndTime;
	private String ruleCommitWeek;
	private String thrsldGroupField;
	private Integer thrsldGroupPeriod;
	private Integer thrsldGroupCnt;
	private String thrsldDstnctField;
	private Integer thrsldDstnctCnt;
	private Integer thrsldOprtrPeriod;
	private String thrsldOprtr;
	private String detectCndtn;
	private String droolsCode;
	private String relshpRule;
	private String cntrlTargt;
	private String custmNm;
	private String ruleDesc;
	private String commitYn;
	private String useYn;
	
	private String attackCode;
	private String severityCode;
	private String ruleGroupCode;
	private String autoReportYn;
	private String comment;
	private String takeActionDescription;
	private String takeBActionDescription;
	
	private String isLatest;
	
	private String threatClasTypeCode;
	
	/** ruleHistory entity */
	private Long ruleHistSeq;
	private String ruleChgReason;
	

	public RuleDetectCndtn getRuleDetectCondition() {
		if(this.detectCndtn != null && this.detectCndtn.length() > 0) {
			Gson gson = new Gson(); 
			return gson.fromJson(this.detectCndtn, RuleDetectCndtn.class);
		} else {
			return null;
		}
	}	
}
