package com.secudium.api.rulemgmt.rule.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class RuleDetectCndtn extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 4240248759029723718L;
	
	private Long ruleDetectCndtnSeq;				// NN
	private Long topRuleDetectCndtnSeq;		//parent node - NN
	private Long ruleSeq;								// NN
	private String cndtnGroupOprtr;
	private String filterNm;
	private Integer detectCndtnLevel;				//depth - NN
	private Integer detectCndtnSortOrder;		//order - NN
	private Long filterSeq;
	private Long filterHistSeq;
}
