package com.secudium.api.rulemgmt.rule.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.rulemgmt.rule.dao.RuleDetectCndtnDao;
import com.secudium.api.rulemgmt.rule.dao.RuleDetectCndtnHistDao;
import com.secudium.api.rulemgmt.rule.dao.RuleHistoryDao;
import com.secudium.api.rulemgmt.rule.dto.RuleSelectDto;
import com.secudium.api.rulemgmt.rule.dto.RuleUpdateDto;
import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class RuleHistoryService {

	private final RuleHistoryDao ruleHistoryDao;
	private final RuleDetectCndtnDao ruleDetectCndtnDao;
	private final RuleDetectCndtnHistDao ruleDetectCndtnHistDao;
	
	public List<Rule> getRuleHistsByRuleSeq(long ruleSeq, String locale) {
		return ruleHistoryDao.getRuleHistsByRuleSeq(ruleSeq, locale);
	}

	public RuleSelectDto getRuleHistByRuleHistSeq(long ruleHistSeq, String locale) {
		RuleSelectDto rule = ruleHistoryDao.getRuleHistByRuleHistSeq(ruleHistSeq, locale);
		rule.setFilters(ruleDetectCndtnHistDao.getRuleDetectCndtnHist(ruleHistSeq));
		return rule;
	}

	public void rollbackHistoryByRuleHistSeq(RuleUpdateDto dto, UserPrincipal currentUser) {
		Rule rule = dto.toWithCurrentUser(currentUser);
		ruleHistoryDao.rollbackHistoryByRuleHistSeq(rule);			//ruleChgHistSeq로 tb_rule에 select-insert
		
		ruleDetectCndtnDao.deleteAllRuleDetectCndtn(rule.getRuleSeq());
		ruleDetectCndtnHistDao.rollbackHistoryByRuleHistSeq(rule);				//이력에 있는 정보로 롤백
		
		ruleHistoryDao.insertRuleHistoryByRuleSeq(rule);				//ruleSeq로 tb_rule_chg_hist에 select-insert
		ruleDetectCndtnHistDao.insertRuleDetectCndtnHistByRuleSeq(rule);	//현재 필드정보 이력 삽입
	}

	public RuleSelectDto getLatestRuleHistory(long ruleSeq, String locale) {
		RuleSelectDto rule = ruleHistoryDao.getLatestRuleHistory(ruleSeq, locale);
		rule.setFilters(ruleDetectCndtnHistDao.getRuleDetectCndtnHist(rule.getRuleHistSeq()));
		return rule;
	}

}
