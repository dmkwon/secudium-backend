package com.secudium.api.rulemgmt.rule.service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.secudium.api.enginemgmt.engineenv.dao.EngineCommandDao;
import com.secudium.api.enginemgmt.engineenv.entity.EngineCommand;
import com.secudium.api.enginemgmt.engineenv.type.EngineMenuType;
import com.secudium.api.rulemgmt.dstrbt.dao.RuleDistHistoryDao;
import com.secudium.api.rulemgmt.dstrbt.dto.RuleDistHistoryDto;
import com.secudium.api.rulemgmt.dstrbt.entity.RuleDistHistory;
import com.secudium.api.rulemgmt.filtermgmt.dao.FilterMgmtDao;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterCndtn;
import com.secudium.api.rulemgmt.rule.dao.RuleDao;
import com.secudium.api.rulemgmt.rule.dao.RuleDetectCndtnDao;
import com.secudium.api.rulemgmt.rule.dao.RuleDetectCndtnHistDao;
import com.secudium.api.rulemgmt.rule.dao.RuleHistoryDao;
import com.secudium.api.rulemgmt.rule.dto.RuleDeleteDto;
import com.secudium.api.rulemgmt.rule.dto.RuleDetectCndtnDto;
import com.secudium.api.rulemgmt.rule.dto.RuleInsertDto;
import com.secudium.api.rulemgmt.rule.dto.RuleSelectDto;
import com.secudium.api.rulemgmt.rule.dto.RuleThreatTaxoDto;
import com.secudium.api.rulemgmt.rule.dto.RuleUpdateDto;
import com.secudium.api.rulemgmt.rule.entity.DetectCndtn;
import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.api.rulemgmt.rule.entity.RuleDetectCndtn;
import com.secudium.api.rulemgmt.threat.dao.ThreatTaxoDao;
import com.secudium.common.entity.SearchMap;
import com.secudium.common.entity.Select;
import com.secudium.security.UserPrincipal;
import com.secudium.storage.zk.ZKService;
import com.secudium.util.ZkUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class RuleService {

	private final RuleDao ruleDao;
	private final RuleHistoryDao ruleHistoryDao;
	private final ThreatTaxoDao threatTaxoDao;
	private final RuleDetectCndtnDao ruleDetectCndtnDao;
	private final RuleDetectCndtnHistDao ruleDetectCndtnHistDao;
	private final RuleDistHistoryDao ruleDistHistDao;
	private final EngineCommandDao engineCommandDao;

	private final FilterMgmtDao filterMgmtDao;

	private final ZKService zkService;

	public List<Rule> getRules(SearchMap search, String locale) {
		return ruleDao.getRules(search, locale);
	}

	public List<Rule> getRulesByThreatClasSeq(SearchMap search, Long threatClasSeq, String locale) {
		return ruleDao.getRulesByThreatClasSeq(search, threatClasSeq, locale);
	}

	public RuleSelectDto getRuleByRuleSeq(long ruleSeq, String locale) {
		RuleSelectDto rule = ruleDao.getRuleByRuleSeq(ruleSeq, locale);
		rule.setFilters(ruleDetectCndtnDao.getRuleDetectCndtns(ruleSeq));
		return rule;
	}

	public void insertRule(@Valid RuleInsertDto dto, UserPrincipal currentUser) {
		Rule rule = dto.toWithCurrentUser(currentUser);
		ruleDao.insertRule(rule); 	//룰 생성 (seq 생성)
		ruleHistoryDao.insertRuleHistory(rule);	//이력 생성(seq 생성)
		threatTaxoDao.initThreatTaxoClasMppg(rule);	//분류체계 생성(초기 미분류)

		//필터검색조건 삽입
		List<RuleDetectCndtnDto> filters = dto.getFilters().stream().map(filter -> {
			filter.setRuleSeq(rule.getRuleSeq());
			filter.setRuleHistSeq(rule.getRuleHistSeq());
			filter.setRegUsrNo(currentUser.getUsrNo());
			return filter;
		}).collect(Collectors.toList());
		insertRuleTree(filters, "0", 0);
		List<RuleDetectCndtn> ruleDetectCndtn = ruleDetectCndtnDao.getRuleDetectCndtnsForJson(rule.getRuleSeq());

		//탐지조건JSON을 위한 객체
		DetectCndtn detectCndtn = dto.getDetectCndtn();
		detectCndtn.setRuleSeq(rule.getRuleSeq());
		detectCndtn.setRuleDetectCndtn(ruleDetectCndtn);

		//탐지조건 업데이트
		ruleDao.updateDetectCndtn(rule.getRuleSeq(), new Gson().toJson(detectCndtn));
		ruleHistoryDao.updateDetectCndtn(rule.getRuleHistSeq(), new Gson().toJson(detectCndtn));
	}

	/**
	 * 
	 * @param filters 규칙탐지조건들
	 * @param parentNodeSeq	트리상에서 생성한 노드시퀀스
	 * @param topRuleDetectCndtnSeq	실제 DB에 저장될 생성된 시퀀스
	 */
	private void insertRuleTree(List<RuleDetectCndtnDto> filters, String parentNodeSeq, long topRuleDetectCndtnSeq) {
		List<RuleDetectCndtnDto> target = filters.stream()
				.filter(filter -> filter.getParentNodeSeq().equals(parentNodeSeq))
				.collect(Collectors.toList());

		for(RuleDetectCndtnDto vo : target) {
			List<RuleDetectCndtnDto> children = filters.stream()
					.filter(filter -> filter.getParentNodeSeq().equals(vo.getNodeSeq()))
					.collect(Collectors.toList());

			vo.setTopRuleDetectCndtnSeq(topRuleDetectCndtnSeq);
			ruleDetectCndtnDao.insertRuleDetectCndtn(vo);
			ruleDetectCndtnHistDao.insertRuleDetectCndtnHist(vo);
			if(children.size() > 0) {
				insertRuleTree(filters, vo.getNodeSeq(), vo.getRuleDetectCndtnSeq());
			}
		}
	}

	public void updateRule(@Valid RuleUpdateDto dto, UserPrincipal currentUser) {
		//룰 정보 업데이트
		Rule rule = dto.toWithCurrentUser(currentUser);
		ruleDao.updateRule(rule);
		ruleHistoryDao.insertRuleHistory(rule);

		//필터검색조건 삽입
		ruleDetectCndtnDao.deleteAllRuleDetectCndtn(rule.getRuleSeq());
		List<RuleDetectCndtnDto> filters = dto.getFilters().stream().map(filter -> {
			filter.setRuleSeq(rule.getRuleSeq());
			filter.setRuleHistSeq(rule.getRuleHistSeq());
			filter.setRegUsrNo(currentUser.getUsrNo());
			return filter;
		}).collect(Collectors.toList());
		insertRuleTree(filters, "0", 0);
		List<RuleDetectCndtn> ruleDetectCndtn = ruleDetectCndtnDao.getRuleDetectCndtnsForJson(rule.getRuleSeq());

		//탐지조건JSON을 위한 객체
		DetectCndtn detectCndtn = dto.getDetectCndtn();
		detectCndtn.setRuleSeq(rule.getRuleSeq());
		detectCndtn.setRuleDetectCndtn(ruleDetectCndtn);

		//탐지조건 업데이트
		ruleDao.updateDetectCndtn(rule.getRuleSeq(), new Gson().toJson(detectCndtn));
		ruleHistoryDao.updateDetectCndtn(rule.getRuleHistSeq(), new Gson().toJson(detectCndtn));
	}

	//룰 삭제 ( 휴지통으로)
	public void deleteRule(@Valid RuleDeleteDto dto, UserPrincipal currentUser) {
		List<RuleUpdateDto> rules = dto.getRules();
		String ruleChgReason = dto.getRuleChgReason();

		for(RuleUpdateDto vo: rules) {
			Rule rule = vo.toWithCurrentUser(currentUser);
			rule.setRuleChgReason(ruleChgReason);

			ruleHistoryDao.insertRuleHistoryByRuleSeq(rule);
			ruleDetectCndtnHistDao.insertRuleDetectCndtnHistByRuleSeq(rule);
			//분류체계를 휴지통으로 변경
			threatTaxoDao.deleteAllThreatTaxoClasMppg(rule.getRuleSeq());
			threatTaxoDao.deleteThreatTaxoClasMppg(rule);
		} 
	}

	//휴지통 -> 미분류
	public void revertRule(RuleUpdateDto dto, UserPrincipal currentUser) {
		Rule rule = dto.toWithCurrentUser(currentUser);
		ruleHistoryDao.insertRuleHistoryByRuleSeq(rule);
		threatTaxoDao.deleteAllThreatTaxoClasMppg(rule.getRuleSeq());
		threatTaxoDao.initThreatTaxoClasMppg(rule);
	}

	//위협체계분류 등록
	public void setThreatTaxo(@Valid RuleThreatTaxoDto dto, UserPrincipal currentUser) {
		List<RuleUpdateDto> rules = dto.getRules();
		List<Long> threatClasSeqs = dto.getThreatClasSeqs();

		for(RuleUpdateDto vo : rules) {
			Rule rule = vo.toWithCurrentUser(currentUser);
			threatTaxoDao.deleteAllThreatTaxoClasMppg(rule.getRuleSeq());
			for(Long threatClasSeq : threatClasSeqs) {
				threatTaxoDao.setThreatTaxoClasMppg(rule.getRuleSeq(), threatClasSeq, currentUser);
			}
		}
	}

	public void deleteRuleFromBin(@Valid RuleDeleteDto dto, UserPrincipal currentUser) {
		List<RuleUpdateDto> rules = dto.getRules();
		String ruleChgReason = dto.getRuleChgReason();

		for(RuleUpdateDto vo: rules) {
			Rule rule = vo.toWithCurrentUser(currentUser);
			rule.setRuleChgReason(ruleChgReason);

			ruleHistoryDao.insertRuleHistoryByRuleSeq(rule);
			//ruleDao.deleteRule(rule);	//분류체계를 휴지통으로 변경
			ruleDao.deleteRule(rule);		//분류체계를 휴지통으로 변경
		} 
	}

	/**
	 * 룰배포
	 * @param dto 배포내용(이력)
	 * @param currentUser 사용자정보
	 */
	public void distributeRules(RuleDistHistoryDto ruleDistHist, UserPrincipal currentUser) {
		final String NL = "\n";
		StringBuilder liveDetect = new StringBuilder();	//실시간탐지
		StringBuilder threshold = new StringBuilder();		//임계치

		//엔진명령어 정보
		EngineCommand ruleCmd = engineCommandDao.selectEngineCommandByCmd(EngineMenuType.UPD_RULE.name());
		EngineCommand thresholdCmd = engineCommandDao.selectEngineCommandByCmd(EngineMenuType.UPD_THRESHOLD.name());

		//임계치 구분자
		String thresholdSeperator = thresholdCmd.getDataSprtn();
		if (StringUtils.isNotEmpty(thresholdSeperator)) {
			thresholdSeperator = ZkUtils.getHexToText(thresholdSeperator);
		}

		//실시간탐지 구분자 파싱
		String ruleHdr = ruleCmd.getHdrDtl();
		String ruleSeperator = ruleCmd.getDataSprtn();
		if (StringUtils.isNotEmpty(ruleHdr)) {
			String before = ruleCmd.getDataSprtn();
			ruleSeperator = ZkUtils.getHexToText(ruleSeperator);
			ruleHdr = ruleHdr.replace(before, ruleSeperator);
		}
		liveDetect.append(ruleHdr);

		final int fixedHeader = 5;	//고정된 헤더까지 index. type, rulenm .... etc
		String[] ruleHdrSplit = ruleHdr.split(ruleSeperator);
		List<Select> zkHdrs = new ArrayList<>();
		//고정된 필드정보를 별도 list에 추가
		for(int i = fixedHeader, length = ruleHdrSplit.length; i < length; i++) {
			String id = ruleHdrSplit[i];
			String name = id.split(":")[1];
			zkHdrs.add(new Select(null, id, null, name));
		}
		final int fixedField = zkHdrs.size();	//고정된 필드까지 index. signature .... payload

		List<Select> exFields = ruleDao.getDistTargetFields();	 //배포대상 룰이 가진 탐지조건의 필드들(distinct)
		zkHdrs.addAll(exFields);
		//고정필드와 중복되는 필드 제거
		Set<Select> fieldSet = new LinkedHashSet<>(zkHdrs);
		zkHdrs = new ArrayList<>(fieldSet);

		//동적 필드를 헤더에 추가
		for(int i = fixedField, length = zkHdrs.size(); i < length; i++) {
			if(zkHdrs.get(i) != null) {
				liveDetect.append(ruleSeperator).append(zkHdrs.get(i).getId());
			}
		}
		liveDetect.append(NL);

		List<Rule> rules = ruleDao.getDistTargetRules();
		for(Rule rule : rules) {
			switch(rule.getRuleType()) {
			case "1000":	//단순룰
			case "0100":	//임계치
				List<RuleDetectCndtnDto> filterSeqs = ruleDetectCndtnDao.getRuleDetectCndtns(rule.getRuleSeq());
				List<FilterCndtn> filterCndtns = new ArrayList<>();
				for(RuleDetectCndtnDto dto : filterSeqs) {
					if(dto.getFilterSeq() != null) {
						List<FilterCndtn> list = filterMgmtDao.selectFilterCndtnBySeq(dto.getFilterSeq());
						//연산자 제외
						filterCndtns.addAll(list.stream().filter(filterCndtn -> filterCndtn.getFieldId() != 0).collect(Collectors.toList()));
					}
				}
				//주키퍼 헤더 정보
				liveDetect.append(ZkUtils.convertZkDataFormat(rule, filterCndtns, zkHdrs, ruleSeperator, true));

				if(rule.getRuleType().equals("0100")) {	//임계치일 경우
					threshold.append(ZkUtils.convertZkDataFormat(rule, thresholdSeperator, true));
				}
				break;

			default:
				break;
			}
		}

		log.debug(liveDetect.toString());
		log.debug(threshold.toString());

		List<Integer> lengthList = new ArrayList<>();
		String lines[] =  liveDetect.toString().split("\\r?\\n");
		for(int i = 0; i < lines.length; i++) {
			lengthList.add(lines[i].length());
		}

		List<Integer> lengthList2 = new ArrayList<>();
		String lines2[] =  threshold.toString().split("\\r?\\n");
		for(int i = 0; i < lines2.length; i++) {
			lengthList2.add(lines2[i].length());
		}

		zkService.setDataForPathWithWatchMsg(lengthList, liveDetect.toString(), currentUser.getUsername(), EngineMenuType.UPD_RULE);
		zkService.setDataForPathWithWatchMsg(lengthList2, threshold.toString(), currentUser.getUsername(), EngineMenuType.UPD_THRESHOLD);

		//배포이력생성 필요
		RuleDistHistory ruleDist = ruleDistHist.toWithCurrentUser(currentUser);
		ruleDistHistDao.insertRuleDistHistory(ruleDist);
		ruleDistHistDao.insertRuleDistDetail(ruleDist);
	}

}
