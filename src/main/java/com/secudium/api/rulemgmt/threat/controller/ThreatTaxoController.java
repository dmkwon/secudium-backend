package com.secudium.api.rulemgmt.threat.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.rulemgmt.threat.dto.ThreatTaxoInsertDto;
import com.secudium.api.rulemgmt.threat.dto.ThreatTaxoUpdateDto;
import com.secudium.api.rulemgmt.threat.entity.ThreatTaxo;
import com.secudium.api.rulemgmt.threat.service.ThreatTaxoService;
import com.secudium.common.dto.OkResponse;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/rulemgmt")
public class ThreatTaxoController {

	private final  ThreatTaxoService threatTaxoService;

	/*
	 * threats
	 */
	@GetMapping("/threats/top")
	public List<ThreatTaxo> getTopNode(
			HttpServletRequest request) {
		return threatTaxoService.getTopNode();
	}

	@PostMapping("/threats/sub")
	public List<ThreatTaxo> getSubNodes( 
			@RequestBody @Valid ThreatTaxo dto) {
		return threatTaxoService.getSubNodes(dto.getTpThrtSeq());
	}

	@PostMapping("/threats/taxo")
	public List<ThreatTaxo> getTaxo(
			@RequestBody @Valid ThreatTaxo dto) {
		return threatTaxoService.getTaxo(dto.getRuleSeq());
	}

	@PostMapping("/threats/sel")
	public ThreatTaxo getNodeData( 
			@RequestBody @Valid ThreatTaxo dto) {
		return threatTaxoService.getNodeData(dto.getThrtSeq());
	}

	@PostMapping("/threats/rule")
	public List<ThreatTaxo> getRule( 
			@RequestBody @Valid ThreatTaxo dto, 
			Locale locale) {
		return threatTaxoService.getRule(dto.getThrtSeq(), locale.getLanguage());
	}

	@PostMapping("/threats/isrt")
	public ResponseEntity<OkResponse> createTaxonomy(
			@RequestBody @Valid final ThreatTaxoInsertDto dto
			, @CurrentUser UserPrincipal currentUser) {
		threatTaxoService.createTaxonomy(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/threats/updt")
	public ResponseEntity<OkResponse> updateThreatTaxo(
			@RequestBody @Valid final ThreatTaxoUpdateDto dto
			, @CurrentUser UserPrincipal currentUser) {
		threatTaxoService.updateThreatTaxo(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/threats/updt/hier")
	public ResponseEntity<OkResponse> updateThreatTaxoHier(
			@RequestBody @Valid final ThreatTaxoUpdateDto dto
			, @CurrentUser UserPrincipal currentUser) {
		threatTaxoService.updateThreatTaxoHier(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/threats/updt/hier/sub")
	public  List<ThreatTaxo> checkSubNodes(
			@RequestBody @Valid final List<ThreatTaxoUpdateDto> dto
			, @CurrentUser UserPrincipal currentUser) {
		return threatTaxoService.checkSubNodes(dto, currentUser);
	}

	@PutMapping("/threats/updt/hier/subupdt")
	public ResponseEntity<OkResponse> updateSubNodes(
			@RequestBody @Valid final List<ThreatTaxoUpdateDto> dto
			, @CurrentUser UserPrincipal currentUser) {
		threatTaxoService.updateSubNodes(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}


	@PutMapping("/threats/delt")
	public ResponseEntity<OkResponse> deleteThreatTaxo(
			@RequestBody @Valid final ThreatTaxoUpdateDto dto
			, @CurrentUser UserPrincipal currentUser) {
		threatTaxoService.deleteThreatTaxo(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

}