package com.secudium.api.rulemgmt.threat.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.api.rulemgmt.threat.entity.ThreatTaxo;
import com.secudium.security.UserPrincipal;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class ThreatTaxoDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public List<ThreatTaxo> selectTopNode() {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}
	
	public List<ThreatTaxo> selectSubNodes(int tpThrtSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), tpThrtSeq);
	}
	
	public ThreatTaxo selectNode(int thrtSeq) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), thrtSeq);
	}
	
	public List<ThreatTaxo> getRule(int thrtSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("thrtSeq", thrtSeq);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}
	
	public int insertThreatTaxo(ThreatTaxo threatTaxo) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), threatTaxo);
	}
	
	public int updateThreatTaxo(ThreatTaxo threatTaxo) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), threatTaxo);
	}
	
	public int updateThreatTaxoHier(ThreatTaxo threatTaxo) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), threatTaxo);
	}
	
	public List<ThreatTaxo> checkSubNodes(Map<String, Object> map) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}
	
	public int updateSubNodes(Map<String, Object> map) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}
	
	public int updateSubNode(Map<String, Object> map) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}
	
	public int deleteThreatTaxo(ThreatTaxo threatTaxo) {
		return sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), threatTaxo);
	}

	public List<ThreatTaxo> getTaxo(long ruleSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), ruleSeq);
	}
	
	/** 신규 룰 등록 시 미등록 분류체계로 등록 */
	public void initThreatTaxoClasMppg(Rule rule) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), rule);
	}
	
	public void deleteThreatTaxoClasMppg(Rule rule) {
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), rule);
	}
	
	/** 룰 위협체계 등록 */
	public void setThreatTaxoClasMppg(long ruleSeq, long threatClasSeq, UserPrincipal currentUser) {
		Map<String, Object> params = new HashMap<>();
		params.put("ruleSeq", ruleSeq);
		params.put("threatClasSeq", threatClasSeq);
		params.put("regUsrNo", currentUser.getUsrNo());
		
		sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}
	
	/** 위협체계 삭제 */
	public void deleteAllThreatTaxoClasMppg(long ruleSeq) {
		sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), ruleSeq);
	}
	
}
