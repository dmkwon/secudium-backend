package com.secudium.api.rulemgmt.threat.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.rulemgmt.threat.entity.ThreatTaxo;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ThreatTaxoUpdateDto {

	private int thrtSeq;
	private int tpThrtSeq;
	
	private String thrtCd;
	private String thrtNm;
	private int thrtLv;
	private int thrtSort;
	private Long bakThrtSeq;
	private String thrtTpPath;
	private String vuln;
	private String cve;
	private String cpe;
	private String resDtl;
	private String useYn;
	private String hasSubNode;

	public ThreatTaxo toThreatTaxoEntity(UserPrincipal currentUser) {
		return ThreatTaxo.builder()
				.thrtSeq(thrtSeq)
				.tpThrtSeq(tpThrtSeq)
				.thrtCd(thrtCd)
				.thrtNm(thrtNm)
				.thrtLv(thrtLv)
				.thrtSort(thrtSort)
				.bakThrtSeq(bakThrtSeq)
				.thrtTpPath(thrtTpPath)
				.vuln(vuln)
				.cve(cve)
				.cpe(cpe)
				.resDtl(resDtl)
				.useYn(useYn)
				.hasSubNode(hasSubNode)
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}
}