package com.secudium.api.rulemgmt.threat.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ThreatTaxo extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -5676512744350590500L;

	private int thrtSeq;
	private int tpThrtSeq;
	private String thrtCd;
	private String thrtNm;
	private int thrtLv;
	private int thrtSort;
	private Long bakThrtSeq;
	private String thrtTpPath;
	private String vuln;
	private String cve;
	private String cpe;
	private String resDtl;
	private String useYn;
	private String hasSubNode;
	
	private Long ruleSeq;
	private String ruleNm;
	private String ruleType;
	private String ruleTypeStr;
	private String ruleDesc;
	
}