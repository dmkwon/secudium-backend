package com.secudium.api.rulemgmt.threat.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.rulemgmt.threat.dao.ThreatTaxoDao;
import com.secudium.api.rulemgmt.threat.dto.ThreatTaxoInsertDto;
import com.secudium.api.rulemgmt.threat.dto.ThreatTaxoUpdateDto;
import com.secudium.api.rulemgmt.threat.entity.ThreatTaxo;
import com.secudium.exception.BusinessException;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class ThreatTaxoService {

	private final ThreatTaxoDao threatTaxoDao;

	public List<ThreatTaxo> getTopNode() {
		return threatTaxoDao.selectTopNode();
	}
		
	public List<ThreatTaxo> getSubNodes(int tpThrtSeq) {
		return threatTaxoDao.selectSubNodes(tpThrtSeq);
	}
	
	public ThreatTaxo getNodeData(int thrtSeq) {
		return threatTaxoDao.selectNode(thrtSeq);
	}

	public List<ThreatTaxo> getRule(int thrtSeq, String locale) {
		return threatTaxoDao.getRule(thrtSeq, locale);
	}
	
	@Transactional(rollbackFor=BusinessException.class)
	public int createTaxonomy(ThreatTaxoInsertDto dto, UserPrincipal currentUser) {
		int count = 0;
		ThreatTaxo threatTaxo = dto.toThreatTaxoEntity(currentUser);
		count = threatTaxoDao.insertThreatTaxo(threatTaxo);
		return count;
	}
	
	
	private List<ThreatTaxo> getChildren(int thrtSeq, String tpPath ) {
		List<ThreatTaxo> threatTaxos = threatTaxoDao.selectSubNodes(thrtSeq);
		
		List<ThreatTaxo> newThreatTaxos = new ArrayList<>();
		
		for(ThreatTaxo threatTaxo : threatTaxos) {
			newThreatTaxos.add(ThreatTaxo.builder()
					.thrtSeq(threatTaxo.getThrtSeq())
					.tpThrtSeq(threatTaxo.getTpThrtSeq())
					.thrtCd(threatTaxo.getThrtCd())
					.thrtNm(threatTaxo.getThrtNm())
					.thrtLv(threatTaxo.getThrtLv())
					.thrtSort(threatTaxo.getThrtSort())
					.bakThrtSeq(threatTaxo.getBakThrtSeq())
					.thrtTpPath(tpPath)
					.vuln(threatTaxo.getVuln())
					.cve(threatTaxo.getCve())
					.cpe(threatTaxo.getCpe())
					.resDtl(threatTaxo.getResDtl())
					.useYn(threatTaxo.getUseYn())
					.hasSubNode(threatTaxo.getHasSubNode())
					.build());
		}
		
		return newThreatTaxos;
	}
	
	public void updateThreatTaxo(ThreatTaxoUpdateDto dto, UserPrincipal currentUser) {
		ThreatTaxo threatTaxo = dto.toThreatTaxoEntity(currentUser);
		threatTaxoDao.updateThreatTaxo(threatTaxo);
		
		List<ThreatTaxo> children = getChildren(dto.getThrtSeq(), dto.getThrtTpPath() + " > " + dto.getThrtNm());
		while(children != null && children.size() > 0) {
			
			for(ThreatTaxo child : children) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "modUsrNo", currentUser.getUsrNo());
				map.put( "thrtTpPath", child.getThrtTpPath());
				map.put( "thrtSeq",  child.getThrtSeq());
				threatTaxoDao.updateSubNode(map);
				
				if(child.getHasSubNode().equals("Y")) {
					children = getChildren(child.getThrtSeq(), child.getThrtTpPath() + " > " + child.getThrtNm());
				} else {
					children = null;
				}
			}
		}
	}
	
	public void updateThreatTaxoHier(ThreatTaxoUpdateDto dto, UserPrincipal currentUser) {
		ThreatTaxo threatTaxo = dto.toThreatTaxoEntity(currentUser);
		threatTaxoDao.updateThreatTaxoHier(threatTaxo);
	}
	
	public List<ThreatTaxo> checkSubNodes(List<ThreatTaxoUpdateDto> dto, UserPrincipal currentUser) {
		List<ThreatTaxo> list = new ArrayList<ThreatTaxo>();
		for(ThreatTaxoUpdateDto vo : dto) {
			list.add( vo.toThreatTaxoEntity(currentUser) );
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		return threatTaxoDao.checkSubNodes(map);
	}
	
	public void updateSubNodes(List<ThreatTaxoUpdateDto> dto, UserPrincipal currentUser) {
		List<ThreatTaxo> list = new ArrayList<ThreatTaxo>();
		Map<String, Object> map = new HashMap<String, Object>();
		for(ThreatTaxoUpdateDto vo : dto) {
			list.add( vo.toThreatTaxoEntity(currentUser) );
			map.put( "thrtLv", vo.getThrtLv() );
			map.put( "thrtTpPath", vo.getThrtTpPath() );
		}
		map.put("list", list);
		threatTaxoDao.updateSubNodes(map);
	}
	
	public void deleteThreatTaxo(ThreatTaxoUpdateDto dto, UserPrincipal currentUser) {
		ThreatTaxo threatTaxo = dto.toThreatTaxoEntity(currentUser);
		threatTaxoDao.deleteThreatTaxo(threatTaxo);
	}

	public List<ThreatTaxo> getTaxo(long ruleSeq) {
		return threatTaxoDao.getTaxo(ruleSeq);
	}

}