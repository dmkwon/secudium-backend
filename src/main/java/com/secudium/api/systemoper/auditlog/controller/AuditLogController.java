package com.secudium.api.systemoper.auditlog.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.api.systemoper.auditlog.entity.AuditLog;
import com.secudium.api.systemoper.auditlog.service.AuditLogService;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * 
 * audit log
 * @author intellicode
 *
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/systemoper")
public class AuditLogController {

	private final AuditLogService auditLogService;

	@GetMapping("/auditLogs")
	public ResponseEntity<PagedResponse<AuditLog>> getAuditLogs(
			HttpServletRequest request, 
			Pageable pageable, 
			Locale locale) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		return ResponseEntity.ok(auditLogService.getAuditLogs(pageable, searchMap, locale.getLanguage()));
	}

	@PostMapping("/auditLog")
	public ResponseEntity<AuditLog> getAuditLogsBySeq(
			@RequestBody @Valid AuditLog dto, 
			Locale locale) {
		return ResponseEntity.ok(auditLogService.getAuditLogsBySeq(dto.getAuditHistSeq(), locale.getLanguage()));
	}
}