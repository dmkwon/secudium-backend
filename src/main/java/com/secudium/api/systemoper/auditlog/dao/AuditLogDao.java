package com.secudium.api.systemoper.auditlog.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.LocaleResolver;

import com.secudium.api.systemoper.auditlog.entity.AuditLog;
import com.secudium.api.systemoper.program.dao.ProgramDao;
import com.secudium.common.entity.SearchMap;
import com.secudium.error.ErrorCode;
import com.secudium.exception.InvalidValueException;
import com.secudium.security.UserPrincipal;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@RequiredArgsConstructor
public class AuditLogDao extends RepositorySupport implements HttpTraceRepository {

	private final SqlSession sqlSession;
	private final ProgramDao programDao;
	private final Environment env;
	private final LocaleResolver localeResolver;

	public Page<AuditLog> selectAuditLogs(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		params.put("lang", locale); 
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<AuditLog> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public AuditLog selectAuditLogsBySeq(Long auditHistSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("auditHistSeq", auditHistSeq);
		params.put("lang", locale); 
		return this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public int insertAuditLog(AuditLog auditLog) {
		return this.sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), auditLog);
	}

	@Override
	public void add(HttpTrace trace) {
		try {
			if(SecurityContextHolder.getContext().getAuthentication() != null) {
				if(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserPrincipal) {
					// 날짜, 시간
					LocalDateTime datetime = LocalDateTime.ofInstant(trace.getTimestamp(), ZoneOffset.systemDefault());
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
					String formatDateTime = datetime.format(formatter);
					SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date date = transFormat.parse(formatDateTime);

					// 기능 정보 조회 
					String contextPath = env.getProperty("server.servlet.context-path");
					String url = trace.getRequest().getUri().getPath();
					if(!contextPath.equals("/")) {
						url = url.replace(contextPath, "");
					}
					// Principal 정보 조회
					UserPrincipal p = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
					String method = trace.getRequest().getMethod();

					Locale locale = localeResolver.resolveLocale(p.getRequest());
					log.debug("===================== [{}]", locale.getLanguage());
					log.debug("===================== [{}]", locale.getLanguage());
					log.debug("===================== [{}]", locale.getLanguage());
					log.debug("===================== [{}]", locale.getLanguage());
					log.debug("===================== [{}]", locale.getLanguage());
					log.debug("===================== [{}]", locale.getLanguage());
					log.debug("===================== [{}]", locale.getLanguage());

					Map<String, String> function = programDao.selectProgramByUrlMethod(url, method);
					AuditLog auditLog = AuditLog.builder()
							.loginUsrId(trace.getPrincipal() != null ? trace.getPrincipal().getName() : "")
							.funcTypeCode(function != null ? function.get("funcTypeCode") : "")
							.usrno(p != null ? p.getUsrNo() : 0)
							.auditFuncNm(function != null ? function.get("funcNmKo") : "")
							.auditUrl(url)
							.httpTrnsType(method)
							.auditResultCode(Integer.toString(trace.getResponse().getStatus()))
							.acsIp(p != null ? p.getAcsIp() : "")
							.acsTk(p != null ? p.getAcsTk() : "")
							.auditText(p != null ? this.getParameterString(p.getRequest(), url, method) : "")
							.regUsrNo(p != null ? p.getUsrNo() : 0)
							.regDate(date)
							.build();
					insertAuditLog(auditLog);
				}
			}
		} catch (ParseException e) {
			throw new InvalidValueException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Request Parameter 정보 추출
	 * @param request
	 * @param url
	 * @param method
	 * @return
	 */
	private String getParameterString(HttpServletRequest request, String url, String method) {
		Enumeration<?> nms = request.getParameterNames();
		StringBuilder sb = new StringBuilder();
		while(nms.hasMoreElements()) {
			String name = (String)nms.nextElement();
			String[] value = request.getParameterValues(name);

			sb.append(name).append(" = ");
			if(value.length > 1) {
				sb.append("[").append(StringUtils.join(value, ", ")).append("]");
			} else {
				// 로그인인 경우 마스킹처리
				if("/api/auth/signin".equals(url) && "POST".equals(method) && "password".equals(name)) {
					sb.append("**********");
				} else {
					sb.append(value[0]);
				}
			}
			sb.append("\r\n");
		}

		return sb.toString();
	}

	@Override
	public List<HttpTrace> findAll() {
		return Collections.emptyList();
	}
}
