package com.secudium.api.systemoper.auditlog.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class AuditLog extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 4949000473366226628L;

    private Long auditHistSeq;
    private String funcTypeCode;
    private Long usrno;
    private String auditFuncNm;
    private String auditUrl;
    private String httpTrnsType;
    private String auditResultCode;
    private String acsIp;
    private String acsTk;
    private String auditText;

    private String loginUsrId;
    private String funcTypeName;
    private String auditResultName;
}
