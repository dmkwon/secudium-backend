package com.secudium.api.systemoper.auditlog.service;

import java.util.Collections;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.systemoper.auditlog.dao.AuditLogDao;
import com.secudium.api.systemoper.auditlog.entity.AuditLog;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class AuditLogService {

	private final AuditLogDao auditLogDao;

	public PagedResponse<AuditLog> getAuditLogs(Pageable pageable, SearchMap search, String locale) {
		Page<AuditLog> page = auditLogDao.selectAuditLogs(pageable, search, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());	
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public AuditLog getAuditLogsBySeq(Long auditHistSeq, String locale) {
		return auditLogDao.selectAuditLogsBySeq(auditHistSeq, locale);
	}
}
