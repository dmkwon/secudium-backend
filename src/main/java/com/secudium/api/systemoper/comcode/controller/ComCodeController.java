package com.secudium.api.systemoper.comcode.controller;

import java.net.URI;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.secudium.api.systemoper.comcode.dto.ComCodeDto;
import com.secudium.api.systemoper.comcode.dto.ComCodeUpdateDto;
import com.secudium.api.systemoper.comcode.entity.ComCode;
import com.secudium.api.systemoper.comcode.service.ComCodeService;
import com.secudium.common.dto.OkResponse;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.common.entity.Select;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
/**
 * 
 * 
 * git commit test
 * @author intellicode
 *
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/systemoper")
public class ComCodeController {

	private final ComCodeService comCodeService;

	@GetMapping("/comcodes")
	public ResponseEntity<PagedResponse<ComCode>> getComCodes(
			HttpServletRequest request, 
			Pageable pageable,
			Locale locale
			) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		return ResponseEntity.ok(comCodeService.getComCodes(pageable, searchMap, locale.getLanguage()));
	}

	@PostMapping("/comcodebyseq")
	public ResponseEntity<ComCode> getComCodeBySeq(
			@RequestBody @Valid ComCode dto,
			Locale locale
			) {
		return ResponseEntity.ok(comCodeService.getComCodeBySeq(dto.getCommCodeSeq(), locale.getLanguage()));
	}	

	@PostMapping("/comcode/parentCode")
	public ResponseEntity<List<ComCode>> getCodesByParentCode(
			@RequestBody @Valid ComCode dto,
			Locale locale) {
		return ResponseEntity.ok(comCodeService.getComCodesByParentCode(dto.getTopCommCode(), locale.getLanguage()));
	}

	@PostMapping("/comcode/parentCode/codes")
	public ResponseEntity<List<Select>> getCodesByCode(
			@RequestBody @Valid ComCodeDto dto,
			Locale locale) {
		return ResponseEntity.ok(comCodeService.getComCodesByParentCodeNotSelf(dto.getTopCommCode(), dto.getCommCodeSeq(), locale.getLanguage()));
	}

	@PostMapping("/comcode")
	public ResponseEntity<OkResponse> createCode(
			@RequestBody @Valid ComCodeDto dto
			, @CurrentUser UserPrincipal currentUser) {
		comCodeService.createComCodes(dto, currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/comcodes").build().toUri();
		return ResponseEntity.created(location).body(OkResponse.of());
	}

	@PutMapping("/comcode")
	public ResponseEntity<OkResponse> updateCode(
			@RequestBody @Valid ComCodeUpdateDto dto,
			@CurrentUser UserPrincipal currentUser) {
		comCodeService.updateComCodes(dto, currentUser);
		return ResponseEntity.ok().body(OkResponse.of());
	}

	@PostMapping("/comcode/delete")
	public ResponseEntity<OkResponse> removeComCode(
			@RequestBody @Valid ComCodeDto dto
			, @CurrentUser UserPrincipal currentUser) {
		comCodeService.deleteComCode(dto.getCommCodeSeq(), currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/comcodes/delete")
	public ResponseEntity<OkResponse> removeComCodes(
			@RequestBody @Valid List<ComCodeDto> dto
			, @CurrentUser UserPrincipal currentUser) {
		comCodeService.deleteComCodes(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

}
