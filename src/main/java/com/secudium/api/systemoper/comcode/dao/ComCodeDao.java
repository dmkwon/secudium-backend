package com.secudium.api.systemoper.comcode.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.systemoper.comcode.entity.ComCode;
import com.secudium.common.entity.SearchMap;
import com.secudium.common.entity.Select;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class ComCodeDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<ComCode> selectComCodes(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search); 
		params.put("lang", locale); 
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<ComCode> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);	
	}

	public ComCode selectComCodeBySeq(Long commCodeSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("commCodeSeq", commCodeSeq);
		params.put("lang", locale);
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<ComCode> selectComCodesByParentCode(String topCommCode, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("topCommCode", topCommCode);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<Select> selectComCodesByParentCodeNotSelf(String topCommCode, Long commCodeSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("topCommCode", topCommCode);
		params.put("commCodeSeq", commCodeSeq);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public int insertComCode(ComCode comCode) {
		return this.sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), comCode);
	}

	public int updateComCode(ComCode comCode) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), comCode);
	}

	public int updateUseYnByComCode(ComCode comCode) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), comCode);
	}

	/**
	 * 코드 단일 삭제
	 * @param map
	 * @return
	 */
	public int deleteComCode(Map<String, Object> map) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	/**
	 * 코드 복수 삭제
	 * @param map
	 * @return
	 */
	public int deleteComCodes(Map<String, Object> map) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

}
