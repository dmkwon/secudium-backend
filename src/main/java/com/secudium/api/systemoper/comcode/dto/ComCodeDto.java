package com.secudium.api.systemoper.comcode.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.comcode.entity.ComCode;
import com.secudium.security.UserPrincipal;

import lombok.Getter;

@Getter
@JsonInclude(Include.NON_NULL)
public class ComCodeDto {

    private Long commCodeSeq;
    private String commCode;
    private String commCodeNmKo;
    private String commCodeNmEn;
    private String commCodeDesc;
    private int commCodeLevel;
    private int commCodeSortOrder;
    private String topCommCode;
    private String extraCodeNm;
    private Long topCommCodeSeq;
    private String useYn;

    public ComCode toEntityWithCurrentUser(UserPrincipal currentUser) {
        return ComCode.builder()
            .commCodeSeq(commCodeSeq)
            .commCode(commCode)
            .commCodeNmKo(commCodeNmKo)
            .commCodeNmEn(commCodeNmEn)
            .commCodeDesc(commCodeDesc)
            .commCodeLevel(commCodeLevel)
            .commCodeSortOrder(commCodeSortOrder)
            .topCommCode(topCommCode)
            .extraCodeNm(extraCodeNm)
            .regUsrNo(currentUser.getUsrNo())
            .modUsrNo(currentUser.getUsrNo())
            .build();
    }
}
