package com.secudium.api.systemoper.comcode.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.comcode.entity.ComCode;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ComCodeUpdateDto {

    private Long commCodeSeq;
    private String commCode;
    private String commCodeNmKo;
    private String commCodeNmEn;
    private String commCodeDesc;
    private int commCodeLevel;
    private int commCodeSortOrder;
    private String topCommCode;
    private Long topCommCodeSeq;
    private String useYn;

    public ComCode toEntityWithCurrentUser(UserPrincipal currentUser) {
        return ComCode.builder()
            .commCodeSeq(commCodeSeq)
            .commCode(commCode)
            .commCodeNmKo(commCodeNmKo)
            .commCodeNmEn(commCodeNmEn)
            .commCodeDesc(commCodeDesc)
            .commCodeLevel(commCodeLevel)
            .commCodeSortOrder(commCodeSortOrder)
            .topCommCode(topCommCode)
            .topCommCodeSeq(topCommCodeSeq)
            .useYn(useYn)
            .modUsrNo(currentUser.getUsrNo())
            .build();
    }
}
