package com.secudium.api.systemoper.comcode.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ComCode extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 2342146277941685923L;

    private Long commCodeSeq;
    private String commCode;
    private String commCodeNmKo;
    private String commCodeNmEn;
    private String commCodeDesc;
    private int commCodeLevel;
    private int commCodeSortOrder;
    private String topCommCode;
    private String extraCodeNm;
    private Long topCommCodeSeq;
    private String useYn;
    private String lang;

}