package com.secudium.api.systemoper.comcode.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.systemoper.comcode.dao.ComCodeDao;
import com.secudium.api.systemoper.comcode.dto.ComCodeDto;
import com.secudium.api.systemoper.comcode.dto.ComCodeUpdateDto;
import com.secudium.api.systemoper.comcode.entity.ComCode;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.common.entity.Select;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class ComCodeService {

	private final ComCodeDao comCodeDao;

	public PagedResponse<ComCode> getComCodes(Pageable pageable, SearchMap search, String locale) {
		Page<ComCode> page = comCodeDao.selectComCodes(pageable, search, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());	
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}
	public List<ComCode> getComCodesByParentCode(String topCommCode, String locale) {
		return comCodeDao.selectComCodesByParentCode(topCommCode, locale);
	}

	public ComCode getComCodeBySeq(Long commCodeSeq, String locale) {
		return comCodeDao.selectComCodeBySeq(commCodeSeq, locale);
	}
	
	public List<Select> getComCodesByParentCodeNotSelf(String topCommCode, Long commCodeSeq, String locale) {
		return comCodeDao.selectComCodesByParentCodeNotSelf(topCommCode, commCodeSeq, locale);
	}

	public void createComCodes(ComCodeDto dto, UserPrincipal currentUser) {
		comCodeDao.insertComCode(dto.toEntityWithCurrentUser(currentUser));
	}

	public void updateComCodes(ComCodeUpdateDto dto, UserPrincipal currentUser) {
		comCodeDao.updateComCode(dto.toEntityWithCurrentUser(currentUser));
	}

//	public void deleteComCodes(String comCodes, UserPrincipal currentUser) {
//		for(String comCodeCd : comCodes.split(",")) {
//			comCodeDao.updateUseYnByComCode(ComCode.builder()
//					.comCodeCd(comCodeCd)
//					//.currentUser(currentUser)
//					.build());
//		}	
//	}

	public int deleteComCode(Long commCodeSeq, UserPrincipal currentUser) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("commCodeSeq", commCodeSeq);
		map.put("modUsrNo", currentUser.getUsrNo());
		return comCodeDao.deleteComCode(map);
	}

	public void deleteComCodes(List<ComCodeDto> dto, UserPrincipal currentUser) {
		List<ComCode> list = new ArrayList<ComCode>();
		for(ComCodeDto vo : dto) {
			list.add(vo.toEntityWithCurrentUser(currentUser));
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("modUsrNo", currentUser.getUsrNo());
		comCodeDao.deleteComCodes(map);
	}
}
