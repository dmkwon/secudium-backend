package com.secudium.api.systemoper.menu.controller;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.secudium.api.systemoper.menu.dto.MenuInfoDto;
import com.secudium.api.systemoper.menu.entity.MenuInfo;
import com.secudium.api.systemoper.menu.service.MenuService;
import com.secudium.common.dto.OkResponse;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.component.excel.ExcelReader;
import com.secudium.constant.ExcelConstant;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/systemoper")
public class MenuController {

	private final MenuService menuService;
	private final ExcelReader excelReader;

	@GetMapping("/menus")
	public ResponseEntity<PagedResponse<MenuInfo>> getMenus(
			HttpServletRequest request, 
			Pageable pageable
			, @CurrentUser UserPrincipal currentUser
			) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		return ResponseEntity.ok(menuService.getMenus(pageable, searchMap, currentUser));
	}

	@PostMapping("/menubyseq")
	public ResponseEntity<MenuInfoDto> getMenusBySeq(
			@RequestBody @Valid MenuInfoDto dto) {
		return ResponseEntity.ok(menuService.getMenusBySeq(dto.getMenuSeq()));
	}

	@PostMapping("/menus/parentMenu")
	public ResponseEntity<List<MenuInfo>> getMenusByParentSeq(
			@RequestBody @Valid MenuInfoDto dto, 
			Locale locale) {
		return ResponseEntity.ok(menuService.getMenusByParentSeq(dto.getTopMenuSeq(), locale.getLanguage()));
	}

	@PostMapping("/menu")
	public ResponseEntity<OkResponse> createMenu(
			@RequestBody @Valid MenuInfoDto dto
			, @CurrentUser UserPrincipal currentUser) {
		menuService.createMenus(dto, currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/menus").build().toUri();
		return ResponseEntity.created(location).body(OkResponse.of());
	}

	@PutMapping("/menu")
	public ResponseEntity<OkResponse> updateMenu(
			@RequestBody @Valid MenuInfoDto dto
			, @CurrentUser UserPrincipal currentUser) {
		menuService.updateMenus(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/menu/delete")
	public ResponseEntity<OkResponse> removeMenu(
			@RequestBody @Valid MenuInfoDto dto
			, @CurrentUser UserPrincipal currentUser) {
		menuService.deleteMenu(dto.getMenuSeq(), currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PutMapping("/menus/delete")
	public ResponseEntity<OkResponse> removeMenus(
			@RequestBody @Valid List<MenuInfoDto> dto
			, @CurrentUser UserPrincipal currentUser) {
		menuService.deleteMenus(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	/**	
	 * excel download / upload sample
	 * @return
	 */
	@GetMapping("excel-xls")
	public ModelAndView xlsView() {
		return new ModelAndView("excelXlsView", initExcelData());
	}

	@GetMapping("excel-xlsx")
	public ModelAndView xlsxView() {
		return new ModelAndView("excelXlsxView", initExcelData());
	}

	@GetMapping("excel-xlsx-streaming")
	public ModelAndView xlsxStreamingView() {
		return new ModelAndView("excelXlsxStreamingView", initExcelData());
	}

	private Map<String, Object> initExcelData() {
		Map<String, Object> map = new HashMap<>();
		map.put(ExcelConstant.FILE_NAME, "test");
		map.put(ExcelConstant.HEAD, Arrays.asList("ID", "NAME", "COMMENT"));
		map.put(ExcelConstant.BODY,
				Arrays.asList(
						Arrays.asList("A", "a", "가"),
						Arrays.asList("B", "b", "나"),
						Arrays.asList("C", "c", "다")
						));
		return map;
	}

	@PostMapping("excel")
	public List<Product> readExcel(
			@RequestParam("file") MultipartFile multipartFile) throws IOException, InvalidFormatException {
		return excelReader.readFileToList(multipartFile, Product::from);
	}

	public static class Product {

		private String uniqueId;

		private String name;

		private String comment;

		protected Product() { }

		public Product(String uniqueId, String name, String comment) {
			this.uniqueId = uniqueId;
			this.name = name;
			this.comment = comment;
		}

		public String getUniqueId() {
			return uniqueId;
		}

		public void setUniqueId(String uniqueId) {
			this.uniqueId = uniqueId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public static Product from(Row row) {
			return new Product(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue(), row.getCell(2).getStringCellValue());
		}
	}
}