package com.secudium.api.systemoper.menu.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.systemoper.menu.entity.MenuInfo;
import com.secudium.api.systemoper.role.entity.MenuRole;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.UserPrincipal;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class MenuDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public Page<MenuInfo> selectMenus(Pageable pageable, SearchMap search, UserPrincipal currentUser) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search); 
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<MenuInfo> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public MenuInfo selectMenusBySeq(Long seq) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), seq );
	}

	public Integer selectStepByParentSeq(Long parentSeq) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), parentSeq );
	}

	/**
	 * menu 가 속해 있는 role list 가져오는 querying 
	 * @param seq
	 * @return MenuRole List object
	 */
	public List<MenuRole> selectRolesByMenuSeq(Long seq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), seq);
	}

	public int insertMenus(MenuInfo menuInfo) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), menuInfo );
	}

	/**
	 * 
	 * @param menuInfo
	 * @return
	 */
	public int updateMenus(MenuInfo menuInfo) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), menuInfo );
	}

	public int updateMenuSortOrder(MenuInfo menuInfo ) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), menuInfo);
	}

	/**
	 * 부모 메뉴 정보 얻는 querying
	 * @param parentSeq
	 * @return	
	 */
	public List<MenuInfo> selectMenusByParentSeq(Long topMenuSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("topMenuSeq", topMenuSeq);
		params.put("lang", locale); 
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params );
	}	

	/**
	 * 부모에 포함되어 있는 자신 메뉴 제외 메뉴 목록 정보 얻는 querying
	 * @param parentSeq
	 * @return	
	 */
	public List<MenuInfo> selectMenusByParentSeqWithoutMe(Long topMenuSeq, Long menuSeq) {
		Map<String, Object> params = new HashMap<>();
		params.put("topMenuSeq", topMenuSeq);
		params.put("menuSeq", menuSeq);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params );
	}	
	/**
	 * 메뉴 단일 삭제
	 * @param map
	 * @return
	 */
	public int deleteMenu(Map<String, Object> map) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	/**
	 * 메뉴 복수 삭제
	 * @param map
	 * @return
	 */
	public int deleteMenus(Map<String, Object> map) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	public List<MenuInfo> selectMenuOfUser(Long usrNo, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("usrNo", usrNo);
		params.put("lang", locale);
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}	

}