package com.secudium.api.systemoper.menu.dto;

import java.sql.Array;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.menu.entity.MenuInfo;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MenuInfoDto {

	private Long menuSeq;
	private String menuNm;
	private String menuNmKo;
	private String menuNmEn;
	private String menuDesc;
	private String menuUrl;
	private int menuLevel;
	private int menuSortOrder;
	private Long topMenuSeq;
	private String useYn;
	private Array path;
	private String checked;

	private List<MenuRoleDto> menuRoles;

	public MenuInfo toEntityWithCurrentUser (UserPrincipal currentUser) {
		return MenuInfo.builder()
				.menuSeq(menuSeq)
				.menuNm(menuNm)
				.menuNmKo(menuNmKo)
				.menuNmEn(menuNmEn)
				.menuDesc(menuDesc)
				.menuUrl(menuUrl)
				.menuLevel(menuLevel)
				.menuSortOrder(menuSortOrder)
				.topMenuSeq(topMenuSeq)
				.useYn(useYn)
				.path(path)
				.regUsrNo(currentUser.getUsrNo())
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}

	public MenuInfo toEntityWithCurrentUser (int step, UserPrincipal currentUser) {
		return MenuInfo.builder()
				.menuSeq(menuSeq)
				.menuNm(menuNm)
				.menuNmKo(menuNmKo)
				.menuNmEn(menuNmEn)
				.menuDesc(menuDesc)
				.menuUrl(menuUrl)
				.menuLevel(menuLevel)
				.menuSortOrder(menuSortOrder)
				.topMenuSeq(topMenuSeq)
				.useYn(useYn)
				.path(path)
				.regUsrNo(currentUser.getUsrNo())
				.modUsrNo(currentUser.getUsrNo())
				.build();
	}
}
