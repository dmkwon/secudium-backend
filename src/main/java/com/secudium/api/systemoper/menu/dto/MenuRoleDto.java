package com.secudium.api.systemoper.menu.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.role.entity.MenuRole;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MenuRoleDto {

	private Long seq;
    private Long menuSeq;
    private Long roleSeq;
    private String useYn;
    
    public MenuRole toEntityWithCurrentUser(Long menuSeq, Long roleSeq, UserPrincipal currentUser) {
        return MenuRole.builder()
    		.seq(seq)
            .menuSeq(menuSeq)
            .roleSeq(roleSeq)
            .useYn(useYn)
            .regUsrNo(currentUser.getUsrNo())
            .build();
    }
}
