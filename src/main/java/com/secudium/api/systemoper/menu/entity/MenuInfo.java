package com.secudium.api.systemoper.menu.entity;

import java.io.Serializable;
import java.sql.Array;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.menu.dto.MenuInfoDto;
import com.secudium.api.systemoper.menu.dto.MenuRoleDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class MenuInfo extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 3871203663162089750L;

	private Long menuSeq;
	private String menuNm;
	private String menuNmKo;
	private String menuNmEn;
	private String menuDesc;
	private String menuUrl;
	private int menuLevel;
	private int menuSortOrder;
	private Long topMenuSeq;
	private String useYn;

	private Array path;
	private String checked;

	public MenuInfoDto toDto(List<MenuRoleDto> menuRoles) {
		return MenuInfoDto.builder()
				.menuSeq(menuSeq)
				.menuNm(menuNm)
				.menuNmKo(menuNmKo)
				.menuNmEn(menuNmEn)
				.menuDesc(menuDesc)
				.menuUrl(menuUrl)
				.menuLevel(menuLevel)
				.menuSortOrder(menuSortOrder)
				.topMenuSeq(topMenuSeq)
				.useYn(useYn)
				.path(path)
				.checked(checked)
				.menuRoles(menuRoles)
				.build();
	}
}
