package com.secudium.api.systemoper.menu.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.systemoper.menu.dao.MenuDao;
import com.secudium.api.systemoper.menu.dto.MenuInfoDto;
import com.secudium.api.systemoper.menu.dto.MenuRoleDto;
import com.secudium.api.systemoper.menu.entity.MenuInfo;
import com.secudium.api.systemoper.role.dao.RoleDao;
import com.secudium.api.systemoper.role.entity.MenuRole;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class MenuService {

    private final MenuDao menuDao;
    private final RoleDao roleDao;

    public PagedResponse<MenuInfo> getMenus(Pageable pageable, SearchMap search, UserPrincipal currentUser){
        Page<MenuInfo> page = menuDao.selectMenus(pageable, search, currentUser);
        if(page.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
                page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
        } else {
            return new PagedResponse<>(page.getContent(), page.getNumber(),
                page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
        }
    }

    public MenuInfoDto getMenusBySeq(Long seq) {
        MenuInfo menuInfo = menuDao.selectMenusBySeq(seq);
        List<MenuRole> roles = menuDao.selectRolesByMenuSeq(seq);

        List<MenuRoleDto> menuRoles = new ArrayList<>();
        for(MenuRole menuRole : roles) {
            menuRoles.add(menuRole.toDto());
        }
        return menuInfo.toDto(menuRoles);
    }

    public List<MenuInfo> getMenusByParentSeq(Long topMenuSeq, String locale) {
        return menuDao.selectMenusByParentSeq(topMenuSeq, locale);
    }

    public void createMenus(MenuInfoDto dto, UserPrincipal currentUser) {
        menuDao.insertMenus(dto.toEntityWithCurrentUser(currentUser));
    }

    public void updateMenus(MenuInfoDto dto, UserPrincipal currentUser) {
        /**
         * 1. delete menu_role by menuseq
         * 2. insert menu_role
         * 3. update menu_info
         */
    	roleDao.deleteMenuRoleByMenuSeq(dto.getMenuSeq());
        for ( MenuRoleDto menuRoleDto : dto.getMenuRoles() ) {
            roleDao.insertMenuRole(menuRoleDto.toEntityWithCurrentUser(dto.getMenuSeq(), menuRoleDto.getRoleSeq(), currentUser));
        }
        menuDao.updateMenus(dto.toEntityWithCurrentUser(currentUser));   	
    	
    	
//        roleDao.deleteMenuRoleByMenuSeq(dto.getMenuSeq());
//        for ( MenuRoleDto menuRoleDto : dto.getMenuRoles() ) {
//            roleDao.insertMenuRole(menuRoleDto.toEntityWithCurrentUser(dto.getMenuSeq(), menuRoleDto.getRoleSeq(), currentUser));
//        }
//        
//        List<MenuInfo> childrens = menuDao.selectMenusByParentSeqWithoutMe(dto.getTopMenuSeq(), dto.getMenuSeq());
//        childrens.add(dto.getMenuSortOrder()-1, dto.toEntityWithCurrentUser(currentUser));
//        
//        for(int i = 0; i < childrens.size(); i++) {
//    		MenuInfo menuInfo = MenuInfo.builder()
//    				.menuSeq(childrens.get(i).getMenuSeq())
//					.menuNm(childrens.get(i).getMenuNm())
//					.menuDesc(childrens.get(i).getMenuDesc())
//		    		.menuUrl(childrens.get(i).getMenuUrl())
//		    		.menuLevel(childrens.get(i).getMenuLevel())
//		    		.menuSortOrder(i+1)
//		    		.topMenuSeq(childrens.get(i).getTopMenuSeq())
//		    		.useYn(childrens.get(i).getUseYn())
//		    		.modUsrNo(currentUser.getUsrNo())
//		    		.build();
//
//    		menuDao.updateMenus(menuInfo);
//        }        
    }

    public int deleteMenu(Long seq, UserPrincipal currentUser) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("seq", seq);
        map.put("modUsrNo", currentUser.getUsrNo());
        return menuDao.deleteMenu(map);
    }

    public void deleteMenus(List<MenuInfoDto> dto, UserPrincipal currentUser) {
        List<MenuInfo> list = new ArrayList<MenuInfo>();
        for(MenuInfoDto vo : dto) {
            list.add(vo.toEntityWithCurrentUser(currentUser));
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("list", list);
        map.put("modUsrNo", currentUser.getUsrNo());
        menuDao.deleteMenus(map);
    }

    public List<MenuInfo> getMenuOfUser(UserPrincipal currentUser, String locale) {
        return menuDao.selectMenuOfUser(currentUser.getUsrNo(), locale);
    }
}
