package com.secudium.api.systemoper.program.controller;

import java.net.URI;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.secudium.api.systemoper.program.dto.ProgramInsertDto;
import com.secudium.api.systemoper.program.dto.ProgramUpdateDto;
import com.secudium.api.systemoper.program.dto.ProgramsDto;
import com.secudium.api.systemoper.program.entity.Programs;
import com.secudium.api.systemoper.program.service.ProgramService;
import com.secudium.common.dto.OkResponse;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/systemoper")
public class ProgramController {

	private final ProgramService programService;

	@GetMapping("/programs")
	public ResponseEntity<PagedResponse<Programs>> getPrograms(
			HttpServletRequest request, 
			Pageable pageable,
			Locale locale
			) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		return ResponseEntity.ok(programService.getPrograms(pageable, searchMap, locale.getLanguage()));
	}

	@PostMapping("/programbyseq")
	public ResponseEntity<Programs> getProgramBySeq(
			@RequestBody Programs dto, 
			Locale locale
			) {
		return ResponseEntity.ok(programService.getProgramBySeq(dto.getFuncSeq(), locale.getLanguage()));
	}	

	@PostMapping("/program")
	public ResponseEntity<OkResponse> createProgram(
			@RequestBody @Valid ProgramInsertDto dto
			, @CurrentUser UserPrincipal currentUser
			) {
		programService.createProgram(dto, currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/programs").build().toUri();
		return ResponseEntity.created(location).body(OkResponse.of());
	}

	@PutMapping("/program")
	public ResponseEntity<OkResponse> updateProgram(
			@RequestBody @Valid ProgramUpdateDto dto
			, @CurrentUser UserPrincipal currentUser) {
		programService.updateProgram(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/program/delete")
	public ResponseEntity<OkResponse> removeProg(
			@RequestBody @Valid ProgramsDto dto
			, @CurrentUser UserPrincipal currentUser) {
		programService.deleteProg(dto.getFuncSeq(), currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}	

	@PutMapping("/programs/delete")
	public ResponseEntity<OkResponse> removeProgs(
			@RequestBody @Valid List<ProgramsDto> dto
			, @CurrentUser UserPrincipal currentUser) {
		programService.deleteProgs(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}
}
