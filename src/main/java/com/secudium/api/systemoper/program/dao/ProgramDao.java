package com.secudium.api.systemoper.program.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.systemoper.program.entity.Programs;
import com.secudium.api.systemoper.role.entity.ProgramRole;
import com.secudium.common.entity.SearchMap;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class ProgramDao extends RepositorySupport{

	private final SqlSession sqlSession;
	private static final String LANG = "lang";

	public Page<Programs> selectPrograms(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search); 
		params.put(LANG, locale);

		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<Programs> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public Programs selectProgramBySeq(Long funcSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("funcSeq", funcSeq); 
		params.put(LANG, locale);
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	/**
	 * program 이 속해 있는 role list 가져오는 querying 
	 * @param seq
	 * @return ProgramRole List object
	 */
	public List<ProgramRole> selectRolesByProgramSeq(Long programSeq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), programSeq);
	}

	public int insertProgram(Programs programs) {
		return sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), programs);
	}

	public int updateProgram(Programs programs) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), programs);
	}

	/**
	 * 기능 단일 삭제
	 * @param map
	 * @return
	 */
	public int deleteProg(Map<String, Object> map) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	public int selectProgramCountByRoleId(Map<String, Object> map) {
		return this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	/**
	 * 기능 복수 삭제
	 * @param userSeq
	 * @return
	 */
	public int deleteProgs(Map<String, Object> map) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	/**
	 * URL, HTTP Method로 기능정보 조회
	 * @param url
	 * @param method
	 * @return
	 */
	public Map<String, String> selectProgramByUrlMethod(String url, String method) {
		Map<String, Object> params = new HashMap<>();
		params.put("url", url); 
		params.put("method", method);
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

}
