package com.secudium.api.systemoper.program.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.program.entity.Programs;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ProgramInsertDto {

    private String funcNmKo;
    private String funcNmEn;
    private String funcDesc;
    private String funcUrl;
    private String httpTrnsType;
    private String funcTypeCode;
    private String useYn;
    private List<ProgramRoleDto> programRoles;

    public Programs toEntityWithCurrentUser(UserPrincipal currentUser) {
        return Programs.builder()
            .funcNmKo(funcNmKo)
            .funcNmEn(funcNmEn)
            .funcDesc(funcDesc)
            .funcUrl(funcUrl)
            .httpTrnsType(httpTrnsType)
            .funcTypeCode(funcTypeCode)
            .useYn(useYn)
            .regUsrNo(currentUser.getUsrNo())
            .modUsrNo(currentUser.getUsrNo())
            .build();
    }
}
