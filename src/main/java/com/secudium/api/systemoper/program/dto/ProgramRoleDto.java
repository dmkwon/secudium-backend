package com.secudium.api.systemoper.program.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.role.entity.ProgramRole;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ProgramRoleDto {

    private Long roleSeq;
    private Long funcSeq;
    private String useYn;

    public ProgramRole toEntityWithCurrentUser(Long funcSeq, Long roleSeq, UserPrincipal currentUser) {
        return ProgramRole.builder()
            .roleSeq(roleSeq)
            .funcSeq(funcSeq)
            .useYn(useYn)
            .regUsrNo(currentUser.getUsrNo())
            .build();
    }
}