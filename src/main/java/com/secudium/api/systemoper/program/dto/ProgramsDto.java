package com.secudium.api.systemoper.program.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.program.entity.Programs;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ProgramsDto {

    private Long funcSeq;
    private String funcTypeCode;
    private String funcTypeName;
    private String funcNm;
    private String funcNmKo;
    private String funcNmEn;
    private String funcDesc;
    private String funcUrl;
    private String httpTrnsType;
    private String useYn;
    private String checked;

    private List<ProgramRoleDto> programRoles;

    public Programs toEntityWithCurrentUser (UserPrincipal currentUser) {
        return Programs.builder()
            .funcSeq(funcSeq)
            .funcTypeCode(funcTypeCode)
            .funcTypeName(funcTypeName)
            .funcNm(funcNm)
            .funcNmKo(funcNmKo)
            .funcNmEn(funcNmEn)
            .funcDesc(funcDesc)
            .funcUrl(funcUrl)
            .httpTrnsType(httpTrnsType)
            .useYn(useYn)
            .checked(checked)
            .regUsrNo(currentUser.getUsrNo())
            .build();
    }
}
