package com.secudium.api.systemoper.program.entity;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.program.dto.ProgramRoleDto;
import com.secudium.api.systemoper.program.dto.ProgramsDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class Programs extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -7980422512682527378L;

	private Long funcSeq;
	private String funcTypeCode;
	private String funcTypeName;
	private String funcNm;
	private String funcNmKo;
	private String funcNmEn;
	private String funcDesc;
	private String funcUrl;
	private String httpTrnsType;
	private String useYn;
	private String checked;

	public ProgramsDto toDto(List<ProgramRoleDto> programRoles) {
		return ProgramsDto.builder()
				.funcSeq(funcSeq)
				.funcTypeCode(funcTypeCode)
				.funcTypeName(funcTypeName)
				.funcNm(funcNm)
				.funcNmKo(funcNmKo)
				.funcNmEn(funcNmEn)
				.funcDesc(funcDesc)
				.funcUrl(funcUrl)
				.httpTrnsType(httpTrnsType)
				.useYn(useYn)
				.checked(checked)
				.programRoles(programRoles)
				.build();
	}
}
