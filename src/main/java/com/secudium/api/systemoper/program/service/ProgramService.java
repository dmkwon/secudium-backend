package com.secudium.api.systemoper.program.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.systemoper.program.dao.ProgramDao;
import com.secudium.api.systemoper.program.dto.ProgramInsertDto;
import com.secudium.api.systemoper.program.dto.ProgramUpdateDto;
import com.secudium.api.systemoper.program.dto.ProgramsDto;
import com.secudium.api.systemoper.program.entity.Programs;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class ProgramService {

	private final ProgramDao programDao;

	public PagedResponse<Programs> getPrograms(Pageable pageable, SearchMap search, String locale) {
		Page<Programs> page = programDao.selectPrograms(pageable, search, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public Programs getProgramBySeq(Long seq, String locale) {
		return programDao.selectProgramBySeq(seq, locale);
	}

	public int getProgramCountByRoleId(List<String> roles, String url, String method){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("roles", roles);
		map.put("url", url);
		map.put("method", method);
		return programDao.selectProgramCountByRoleId(map);
	}

	public void createProgram(ProgramInsertDto dto, UserPrincipal currentUser) {
		programDao.insertProgram(dto.toEntityWithCurrentUser(currentUser));
	}

	public void updateProgram(ProgramUpdateDto dto, UserPrincipal currentUser) {
		programDao.updateProgram(dto.toEntityWithCurrentUser(currentUser));
	}

	public int deleteProg(Long funcSeq, UserPrincipal currentUser) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("funcSeq", funcSeq);
		map.put("modUsrNo", currentUser.getUsrNo());
		return programDao.deleteProg(map);    
	}

	public void deleteProgs(List<ProgramsDto> dto, UserPrincipal currentUser) {
		List<Programs> list = new ArrayList<Programs>();
		for(ProgramsDto vo : dto) {
			list.add(vo.toEntityWithCurrentUser(currentUser));
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("modUsrNo", currentUser.getUsrNo());
		programDao.deleteProgs(map);
	}

}
