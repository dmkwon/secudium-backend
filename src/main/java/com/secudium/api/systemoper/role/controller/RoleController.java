package com.secudium.api.systemoper.role.controller;

import java.net.URI;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.secudium.api.systemoper.menu.dto.MenuRoleDto;
import com.secudium.api.systemoper.menu.entity.MenuInfo;
import com.secudium.api.systemoper.program.dto.ProgramsDto;
import com.secudium.api.systemoper.program.entity.Programs;
import com.secudium.api.systemoper.role.dto.RemoveUserBySeqDto;
import com.secudium.api.systemoper.role.dto.RoleInsertDto;
import com.secudium.api.systemoper.role.dto.RoleUpdateDto;
import com.secudium.api.systemoper.role.dto.UpdateMenuRoleDto;
import com.secudium.api.systemoper.role.dto.UpdateProgsRoleDto;
import com.secudium.api.systemoper.role.entity.Roles;
import com.secudium.api.systemoper.role.entity.UserRole;
import com.secudium.api.systemoper.role.service.RoleService;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.common.dto.OkResponse;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/systemoper")
public class RoleController {

    /**
     * collection list
     * 
     * roles
     * member_role
     * menu_role
     * program_role
     * 
     * search condition
     * paging 처리
     */
    private final RoleService roleService;

    /**
     *
     * Role 전체 리스트 가져오는 API
     * @param request
     * @param pageable
     * @return
     */
    @GetMapping("/roles")
    public ResponseEntity<PagedResponse<Roles>> getRoles(
            HttpServletRequest request, 
            Pageable pageable,
            Locale locale
            ) {
        SearchMap searchMap = SearchMap.buildFrom(request);
        return ResponseEntity.ok(roleService.getRoles(pageable, searchMap, locale.getLanguage()));
    }

	@PostMapping("/rolebyseq")
	public ResponseEntity<Roles> getRolesBySeq(
			@RequestBody @Valid Roles dto) {
		return ResponseEntity.ok(roleService.getRolesBySeq(dto.getRoleSeq()));
	}

    /**
     * Role 추가하는 API
     * @param vo RoleInsertVO Object
     */
    @PostMapping("/role")
    public ResponseEntity<OkResponse> createRole(
            @RequestBody @Valid RoleInsertDto dto
            , @CurrentUser UserPrincipal currentUser) {
        roleService.createRole(dto, currentUser);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/roles").build().toUri();
        return ResponseEntity.created(location).body(OkResponse.of());
    }

    /**
     * Role 수정하는 API
     * @param vo RoleUpdateRequest Object
     */
    @PutMapping("/role")
    public ResponseEntity<OkResponse> updateRole(
            @RequestBody @Valid RoleUpdateDto dto
            , @CurrentUser UserPrincipal currentUser) {
        roleService.updateRole(dto, currentUser);
        return ResponseEntity.ok(OkResponse.of());
    }

    /**
     * role seq 값에 해당하는 Role 삭제하는 API 
     * @param seq 
     */
	@PostMapping("/role/delete")
	public ResponseEntity<OkResponse> removeRole(
			@RequestBody @Valid Roles dto) {
		roleService.removeRole(dto.getRoleSeq());
		return ResponseEntity.ok(OkResponse.of());
	}

    @PutMapping("/roles/delete")
    public ResponseEntity<OkResponse> removeRoles(
            @RequestBody @Valid List<RoleUpdateDto> dto
            , @CurrentUser UserPrincipal currentUser) {
        roleService.removeRoles(dto, currentUser);
        return ResponseEntity.ok(OkResponse.of());
    }

    /**
     * role seq 에 해당하는  member list 를 가져오는 API
     */
	@PostMapping("/role/users")
	public ResponseEntity<PagedResponse<User>> getUsersBySeq(
			HttpServletRequest request, 
			@RequestBody @Valid UserRole dto,
			Pageable pageable, 
			Locale locale) {
		return ResponseEntity.ok(roleService.getUsersBySeq(pageable, dto.getRoleSeq(), locale.getLanguage()));
	}

    /**
     * ppt 20page 사용자 삭제 
     * 1. seq, memberSeq list 를 받는다.
     * 2. roleService 에 넘긴다.
     * 3. roleService 에서 list를 for loop 로 dao 삭제 함수를 호출해서 member_role 테이블의 데이터 삭제
     * 4. DELETE method 대신에 POST method 사용하여 처리
     */
	@PostMapping("/role/delete/users")
	public ResponseEntity<OkResponse> removeUsersBySeq(
			@RequestBody RemoveUserBySeqDto dto ) {
		roleService.removeMembersBySeq(dto.getSeq(), dto.getMembers());
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/role/update/usersbyseq")
	public ResponseEntity<OkResponse> updateUserRoleBySeq(
			@RequestBody RemoveUserBySeqDto dto
			, @CurrentUser UserPrincipal currentUser) {
		roleService.updateUserRoleBySeq(dto.getSeq(), dto.getMembers(), currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

    /**
     * paging
     * @param seq
     * @return
     */
	@PostMapping("/role/menus")
	public ResponseEntity<PagedResponse<MenuInfo>> getMenusBySeq(
			HttpServletRequest request, 
			@RequestBody @Valid MenuRoleDto dto,
			Pageable pageable,
			Locale locale) {
		return ResponseEntity.ok(roleService.getMenusBySeq(pageable, dto.getMenuSeq(), locale.getLanguage()));
	}

    /**
     * ppt 23page 저장 버튼 
     * 1. seq, menuSeq list 를 받는다.
     * 2. roleService 에 넘긴다.
     * 3. seq menu_role 테이블의 데이터 모두 삭제 
     * 4. roleService 에서 list를 for loop 로 dao 삭제 함수를 호출해서 menu_role 테이블의 데이터 추가
     */

	@PostMapping("/role/update/menus")
	public ResponseEntity<OkResponse> updateMenuRoleBySeq(
			  @RequestBody UpdateMenuRoleDto dto
			, @CurrentUser UserPrincipal currentUser) {
		roleService.updateMenuRoleBySeq(dto.getSeq(), dto.getMenus(), currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

    /**
     * paging
     * @param seq
     * @return
     */
	@PostMapping("/role/programs")
	public ResponseEntity<PagedResponse<Programs>> getProgramsBySeq(
			HttpServletRequest request, 
			@RequestBody @Valid ProgramsDto dto,
			Pageable pageable,
			Locale locale
			) {
		return ResponseEntity.ok(roleService.getProgramsBySeq(pageable, dto.getFuncSeq(), locale.getLanguage()));
	}

    /**
     * ppt 24page 저장 버튼 
     * 1. seq, programSeq list 를 받는다.
     * 2. roleService 에 넘긴다.
     * 3. seq program_role 테이블의 데이터 모두 삭제 
     * 4. roleService 에서 list를 for loop 로 dao 삭제 함수를 호출해서 program_role 테이블의 데이터 추가
     */
	@PostMapping("/role/update/programs")
	public ResponseEntity<OkResponse> updateProgRoleBySeq(
			  @RequestBody UpdateProgsRoleDto dto
			, @CurrentUser UserPrincipal currentUser) {
		roleService.updateProgRoleBySeq(dto.getSeq(), dto.getProgs(), currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

}
