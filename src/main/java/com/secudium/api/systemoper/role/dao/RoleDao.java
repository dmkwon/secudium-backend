package com.secudium.api.systemoper.role.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.systemoper.menu.entity.MenuInfo;
import com.secudium.api.systemoper.program.entity.Programs;
import com.secudium.api.systemoper.role.entity.MenuRole;
import com.secudium.api.systemoper.role.entity.ProgramRole;
import com.secudium.api.systemoper.role.entity.Roles;
import com.secudium.api.systemoper.role.entity.UserRole;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.common.entity.SearchMap;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class RoleDao extends RepositorySupport {

	private static final String ROLE_SEQ = "roleSeq";
	private static final String LANG = "lang";

	private final SqlSession sqlSession;

	public Page<Roles> selectRoles(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		params.put("lang", locale); 
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<Roles> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public Roles selectRolesBySeq(Long seq) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), seq);
	}

	public Roles insertRole(Roles roles) {
		this.sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), roles);
		return roles;
	}

	public int updateRole(Roles roles) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), roles);
	}

	public int deleteRole(Long roleSeq) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), roleSeq);
	}

	public int deleteRoleUser(Long roleSeq) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), roleSeq);
	}

	public int deleteRoleMenu(Long roleSeq) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), roleSeq);
	}

	public int deleteRoleProg(Long roleSeq) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), roleSeq);
	}

	public int deleteRoles(Map<String, Object> map) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	public Page<User> selectUsersByRoleSeq(Pageable pageable, Long seq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put(ROLE_SEQ, seq);
		params.put(LANG, locale);

		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<User> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public int insertUserRole(UserRole userRole) {
		return this.sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), userRole);
	}

	public void deleteUserRole(UserRole userRole) {
		this.sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), userRole);
	}

	public void deleteUserRoleBySeq(Long seq) {
		this.sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), seq);
	}

	public Page<MenuInfo> selectMenusBySeq(Pageable pageable, Long roleSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put(ROLE_SEQ, roleSeq);
		params.put(LANG, locale);

		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<MenuInfo> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public int insertMenuRole(MenuRole menuRole) {
		return this.sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), menuRole);
	}

	public void deleteMenuRoleBySeq(Long roleSeq) {
		this.sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), roleSeq);
	}

	public void deleteMenuRoleByMenuSeq(Long menuSeq) {
		this.sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), menuSeq);
	}

	public Page<Programs> selectProgramsBySeq(Pageable pageable, Long roleSeq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put(ROLE_SEQ, roleSeq);
		params.put(LANG, locale);

		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<Programs> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public int insertProgRoleBySeq(ProgramRole programRole) {
		return this.sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), programRole);
	}

	public void deleteProgRoleBySeq(Long roleSeq) {
		this.sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), roleSeq);
	}

}

