package com.secudium.api.systemoper.role.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.user.entity.User;

import lombok.Getter;

@Getter
@JsonInclude(Include.NON_NULL)
public class RemoveUserBySeqDto {

	private Long seq;
	private List<User> members;

}
