package com.secudium.api.systemoper.role.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.role.entity.Roles;
import com.secudium.security.UserPrincipal;

import lombok.Getter;

@Getter
@JsonInclude(Include.NON_NULL)
public class RoleInsertDto {

    private String roleId;
    private String roleNm;
    private String roleDesc;

    public Roles toEntityWithCurrentUser(UserPrincipal currentUser) {
        return Roles.builder()
            .roleId(roleId)
            .roleNm(roleNm)
            .roleDesc(roleDesc)
            .regUsrNo(currentUser.getUsrNo())
            .modUsrNo(currentUser.getUsrNo())
            .build();
    }
}
