package com.secudium.api.systemoper.role.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.role.entity.Roles;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class RoleUpdateDto {

    private Long roleSeq;
    private String roleId;
    private String roleNm;
    private String roleDesc;

    public Roles toEntityWithCurrentUser(UserPrincipal currentUser) {
        return Roles.builder()
            .roleSeq(roleSeq)
            .roleId(roleId)
            .roleNm(roleNm)
            .roleDesc(roleDesc)
            .modUsrNo(currentUser.getUsrNo())
            .build();
    }
}