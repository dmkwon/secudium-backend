package com.secudium.api.systemoper.role.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.menu.entity.MenuInfo;

import lombok.Getter;

@Getter
@JsonInclude(Include.NON_NULL)
public class UpdateMenuRoleDto {
	private Long seq;
	private List<MenuInfo> menus;

}
