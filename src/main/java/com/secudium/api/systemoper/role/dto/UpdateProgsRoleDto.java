package com.secudium.api.systemoper.role.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.program.entity.Programs;

import lombok.Getter;
@Getter
@JsonInclude(Include.NON_NULL)
public class UpdateProgsRoleDto {
	private Long seq;
	private List<Programs> progs;

}
