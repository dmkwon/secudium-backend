package com.secudium.api.systemoper.role.entity;

import java.io.Serializable;

import com.secudium.api.systemoper.menu.dto.MenuRoleDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class MenuRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -233771613194792183L;

    private Long seq;
    private Long menuSeq;
    private Long roleSeq;
    private String useYn;

    public MenuRoleDto toDto() {
        return MenuRoleDto.builder()
            .menuSeq(menuSeq)
            .roleSeq(roleSeq)
            .useYn(useYn)
            .build();
    }
}
