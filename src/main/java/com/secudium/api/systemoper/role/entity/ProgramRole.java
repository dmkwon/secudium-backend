package com.secudium.api.systemoper.role.entity;

import java.io.Serializable;

import com.secudium.api.systemoper.program.dto.ProgramRoleDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class ProgramRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -5726103426987170584L;

    private Long roleSeq;
    private Long funcSeq;
    private String useYn;

    public ProgramRoleDto toDto() {
        return ProgramRoleDto.builder()
            .funcSeq(funcSeq)
            .roleSeq(roleSeq)
            .useYn(useYn)
            .build();
    }
}
