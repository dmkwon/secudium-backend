package com.secudium.api.systemoper.role.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class Roles extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -1787029883863793898L;

	private Long roleSeq;
	private String roleId;
	private String roleNm;
	private String roleNmKo;
	private String roleNmEn;
	private String roleDesc;

}
