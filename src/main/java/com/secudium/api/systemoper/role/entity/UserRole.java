package com.secudium.api.systemoper.role.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UserRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 4917960992310047779L;

    private Long roleSeq;
    private Long usrNo;

}
