package com.secudium.api.systemoper.role.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.systemoper.menu.entity.MenuInfo;
import com.secudium.api.systemoper.program.entity.Programs;
import com.secudium.api.systemoper.role.dao.RoleDao;
import com.secudium.api.systemoper.role.dto.RoleInsertDto;
import com.secudium.api.systemoper.role.dto.RoleUpdateDto;
import com.secudium.api.systemoper.role.entity.MenuRole;
import com.secudium.api.systemoper.role.entity.ProgramRole;
import com.secudium.api.systemoper.role.entity.Roles;
import com.secudium.api.systemoper.role.entity.UserRole;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class RoleService {

	private final RoleDao roleDao;

	/**
	 * roles
	 * @param roleSeq
	 * @param members
	 */

	public PagedResponse<Roles> getRoles(Pageable pageable, SearchMap search, String locale) {
		Page<Roles> page = roleDao.selectRoles(pageable, search, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public Roles getRolesBySeq(Long seq) {
		return roleDao.selectRolesBySeq(seq);
	}
	public Roles createRole(RoleInsertDto dto, UserPrincipal currentUser) {
		Roles newRole = roleDao.insertRole(dto.toEntityWithCurrentUser(currentUser));
		return roleDao.selectRolesBySeq(newRole.getRoleSeq());
	}

	public int updateRole(RoleUpdateDto dto, UserPrincipal currentUser) {
		return roleDao.updateRole(dto.toEntityWithCurrentUser(currentUser));
	}

	public int removeRole(Long roleSeq) {
		roleDao.deleteRoleUser(roleSeq);
		roleDao.deleteRoleMenu(roleSeq);
		roleDao.deleteRoleProg(roleSeq);
		return roleDao.deleteRole(roleSeq);
	}

	public void removeRoles(List<RoleUpdateDto> dto, UserPrincipal currentUser) {
		List<Roles> list = new ArrayList<Roles>();
		for(RoleUpdateDto vo : dto) {
			list.add(vo.toEntityWithCurrentUser(currentUser));
			roleDao.deleteRoleUser(vo.getRoleSeq());
			roleDao.deleteRoleMenu(vo.getRoleSeq());
			roleDao.deleteRoleProg(vo.getRoleSeq());
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("modUsrNo", currentUser.getUsrNo());
		roleDao.deleteRoles(map);
	}

	/**
	 * members by role
	 * @param roleSeq
	 * @param members
	 */

	public PagedResponse<User> getUsersBySeq(Pageable pageable, Long seq, String locale) {
		Page<User> page = roleDao.selectUsersByRoleSeq(pageable, seq, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public void removeMembersBySeq(Long roleSeq, List<User> users) {
		for(User user : users) {
			UserRole memberRole = UserRole.builder()
					.roleSeq(roleSeq)
					.usrNo(user.getUsrNo())
					.build();
			roleDao.deleteUserRole(memberRole);
		}
	}

	public void updateUserRoleBySeq(Long roleSeq, List<User> users, UserPrincipal currentUser) {
		for(User user : users) {
			UserRole memberRole = UserRole.builder()
					.usrNo(user.getUsrNo())
					.roleSeq(roleSeq)
					.regUsrNo(currentUser.getUsrNo())
					.build();
			roleDao.insertUserRole(memberRole);
		}
	}

	/**
	 * menus by role
	 * @param roleSeq
	 * @param menus
	 */
	public PagedResponse<MenuInfo> getMenusBySeq(Pageable pageable, Long roleSeq, String locale) {
		Page<MenuInfo> page = roleDao.selectMenusBySeq(pageable, roleSeq, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public void updateMenuRoleBySeq(Long roleSeq, List<MenuInfo> menus, UserPrincipal currentUser) {
		roleDao.deleteMenuRoleBySeq(roleSeq);
		for(MenuInfo menu : menus) {
			MenuRole menuRole = MenuRole.builder()
					.menuSeq(menu.getMenuSeq())
					.roleSeq(roleSeq)
					.regUsrNo(currentUser.getUsrNo())
					.build();
			roleDao.insertMenuRole(menuRole);
		}
	}

	/**
	 * program by role
	 * @param roleSeq
	 * @param programs
	 */
	public PagedResponse<Programs> getProgramsBySeq(Pageable pageable, Long roleSeq, String locale) {
		Page<Programs> page = roleDao.selectProgramsBySeq(pageable, roleSeq, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public void updateProgRoleBySeq(Long roleSeq, List<Programs> programs, UserPrincipal currentUser) {
		roleDao.deleteProgRoleBySeq(roleSeq);
		for(Programs program : programs) {
			ProgramRole programRole = ProgramRole.builder()
					.funcSeq(program.getFuncSeq())
					.roleSeq(roleSeq)
					.useYn("Y")
					.regUsrNo(currentUser.getUsrNo())
					.build();
			roleDao.insertProgRoleBySeq(programRole);
		}

	}

}
