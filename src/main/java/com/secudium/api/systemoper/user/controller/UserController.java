package com.secudium.api.systemoper.user.controller;

import java.net.URI;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.secudium.api.systemoper.user.dto.CustomCoListDto;
import com.secudium.api.systemoper.user.dto.UserDto;
import com.secudium.api.systemoper.user.dto.UserUpdateDto;
import com.secudium.api.systemoper.user.entity.CustmCoList;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.api.systemoper.user.service.UserService;
import com.secudium.common.dto.OkResponse;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.CurrentUser;
import com.secudium.security.UserPrincipal;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor	
@RequestMapping(value = "/api/systemoper")
public class UserController {

	private final UserService userService;

	/**
	 * 사용자 리스트 (페이징)
	 * 사용자 정보 (user_seq)
	 * 사용자 등록
	 * 사용자 수정
	 * 사용자 삭제
	 */	

	@GetMapping("/users")
	public ResponseEntity<PagedResponse<User>> getUsers(
			HttpServletRequest request, 
			Pageable pageable,
			Locale locale		
			) {
		SearchMap searchMap = SearchMap.buildFrom(request);
		return ResponseEntity.ok(userService.getUsers(pageable, searchMap, locale.getLanguage()));
	}

	@PostMapping("/userbyseq")
	public ResponseEntity<UserDto> getUserBySeq(
			@RequestBody @Valid UserDto dto, 
			Locale locale) {
		return ResponseEntity.ok(userService.getUserBySeq(dto.getSeq(), locale.getLanguage()));
	}

	@GetMapping("/user/custm/group")
	public ResponseEntity<PagedResponse<CustmCoList>> getCustmGroup(
			HttpServletRequest request, 
			Pageable pageable
			) {	
		SearchMap searchMap = SearchMap.buildFrom(request);
		return ResponseEntity.ok(userService.getCustmGroup(pageable, searchMap));
	}

	@PostMapping("/user/custm/company")
	public ResponseEntity<List<CustomCoListDto>> getCoNmByCustmGroupSeq(
			@RequestBody @Valid CustomCoListDto dto) {
		return ResponseEntity.ok(userService.getCoNmByCustmGroupSeq(dto.getSeq()));
	}

	@PostMapping("/user")
	public ResponseEntity<OkResponse> createUser(
			@RequestBody @Valid UserDto dto
			, @CurrentUser UserPrincipal currentUser) {
		userService.createUser(dto, currentUser);
		URI location = ServletUriComponentsBuilder
				.fromCurrentContextPath().path("/users").build().toUri();
		return ResponseEntity.created(location).body(OkResponse.of());
	}

	@PutMapping("/user")
	public ResponseEntity<OkResponse> updateUser(
			@RequestBody @Valid UserUpdateDto dto
			, @CurrentUser UserPrincipal currentUser) {
		userService.updateUser(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());		
	}	

	@PutMapping("/users/delete")
	public ResponseEntity<OkResponse> removeUsers(
			@RequestBody @Valid List<UserDto> dto
			, @CurrentUser UserPrincipal currentUser) {
		userService.deleteUsers(dto, currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/user/delete")
	public ResponseEntity<OkResponse> removeUser(
			@RequestBody @Valid UserDto dto
			, @CurrentUser UserPrincipal currentUser) {
		userService.deleteUser(dto.getSeq(), currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@PostMapping("/user/changePwd")
	public ResponseEntity<OkResponse> changePwd(
			@RequestBody @Valid UserDto dto
			, @CurrentUser UserPrincipal currentUser) {		
		userService.updateChangePwd(dto.getSeq(), dto.getChangePwd(), currentUser);
		return ResponseEntity.ok(OkResponse.of());
	}

	@GetMapping("/user/isduplicate")
	public ResponseEntity<Boolean> isDuplicate(
			@RequestParam("usrId") String usrId
			) {	
		return ResponseEntity.ok(userService.isDuplicate(usrId));	
	}
}
