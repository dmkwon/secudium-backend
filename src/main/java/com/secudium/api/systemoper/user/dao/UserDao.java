package com.secudium.api.systemoper.user.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.secudium.api.systemoper.user.dto.CustomCoListDto;
import com.secudium.api.systemoper.user.dto.UserDto;
import com.secudium.api.systemoper.user.entity.CustmCoList;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.entity.Role;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class UserDao extends RepositorySupport {

	private final SqlSession sqlSession;
	private static final String LANG = "lang";

	public Page<User> selectUsers(Pageable pageable, SearchMap search, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		params.put(LANG, locale);

		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<User> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public UserDto selectUserBySeq(Long seq, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("seq", seq);
		params.put(LANG, locale);
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public User selectUsersByUsername(String userId) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), userId);
	}

	public List<Role> selectRolesByUsername(String usrId) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), usrId);
	}

	public Page<CustmCoList> selectCustmGroup(Pageable pageable, SearchMap search) {
		Map<String, Object> params = new HashMap<>();
		params.put("search", search);
		int total = sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName() + countSuf, params);
		pageable = ensureValidPageable(pageable, total);
		params.put("page", pageable);
		params.put("sort", prepareSortParameter(pageable.getSort()));
		List<CustmCoList> list = sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName() + pageSuf, params);
		return new PageImpl<>(list, pageable, total);
	}

	public List<CustomCoListDto> selectCoNmByCustmGroupSeq(Long seq) {
		return sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), seq);
	}

	public void insertUser(User user) {
		this.sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), user);
	}

	public int updateUser(User user) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), user);
	}

	public int deleteUsers(Map<String, Object> map) {
		return sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	public int deleteUser(Map<String, Object> map) {
		return this.sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), map);
	}

	public int deleteUserBySeq(Long usrNo) {
		return this.sqlSession.delete(mapperNs + MethodUtils.getCurrentMethodName(), usrNo);
	}

	public void updateChangePwd(User seq) {
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), seq);
	}

	public Boolean isDuplicate(String usrId) {
		return (Integer) this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), usrId) > 0 ? true : false;
	}

	public Boolean isAccLocked(String usrId) {
		return (Integer) this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), usrId) > 0  ? true : false;
	}

	public UserDto loginUsrId(String usrId) {
		return this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), usrId);
	}

	public void loginFail(String usrId) {
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), usrId);
	}

	public void updateLoginCntReset(String usrId) {
		sqlSession.update(mapperNs + MethodUtils.getCurrentMethodName(), usrId);
	}

	public int getLoginFailCount(String usrId) {
		return sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), usrId);
	}

}
