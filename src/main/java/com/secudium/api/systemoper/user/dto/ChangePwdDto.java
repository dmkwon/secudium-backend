package com.secudium.api.systemoper.user.dto;

import javax.validation.constraints.NotBlank;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class ChangePwdDto {

	@NotBlank
	private String usrPwd;
	private String usrId;

	/**
	 * @param currentUser
	 * @return
	 */
	public User toEntityWithCurrentUser(Long seq, UserPrincipal currentUser) {
		return User.builder()
				.seq(seq)
				.usrId(usrId)
				.usrPwd(new BCryptPasswordEncoder().encode(usrPwd))
				.modUsrNo(currentUser.getUsrNo())  
				.build();
	}

}

	