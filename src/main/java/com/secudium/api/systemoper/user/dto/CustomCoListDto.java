package com.secudium.api.systemoper.user.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.user.entity.CustmCoList;
import com.secudium.security.UserPrincipal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class CustomCoListDto {

	private Long seq;
	private Long custmSeq;
	private Long custmGroupSeq;
	private Long custmId;
	private Long custmGroupId;
	private String custmGroupNm;
	private String channelTypeCode;
	private String coNm;
	private String companyTypeCode;
	private String companyGroupTypeCode;
	private Long bizNo;
	private String activeYn;
	private String deActiveReason;
	private String useYn;
	private String checked;


	/**
	 * @param currentUser
	 * @return
	 */
	public CustmCoList toEntityWithCurrentUser(UserPrincipal currentUser) {
		return CustmCoList.builder()
			.custmSeq(custmSeq)
			.custmGroupSeq(custmGroupSeq)
			.custmId(custmId)
			.custmGroupId(custmGroupId)
			.custmGroupNm(custmGroupNm)
			.channelTypeCode(channelTypeCode)
			.coNm(coNm)
			.companyTypeCode(companyTypeCode)
			.companyGroupTypeCode(companyGroupTypeCode)
			.bizNo(bizNo)
			.activeYn(activeYn)
			.deActiveReason(deActiveReason)
			.useYn(useYn)	
			.checked(checked)
			.regUsrNo(currentUser.getUsrNo())
			.modUsrNo(currentUser.getUsrNo())
			.build();
	}
}

