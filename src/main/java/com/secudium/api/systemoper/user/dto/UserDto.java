package com.secudium.api.systemoper.user.dto;

import java.util.Date;
import java.util.List;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.security.UserPrincipal;
import com.secudium.security.entity.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class UserDto {

	private Long seq;
	private Long usrNo;
	private String accTypeCode;
	private String accTypeName;
	private String usrNm;
	private String usrId;
	private String usrPwd;
	private String coNm;
	private String deptNm;
	private String dutyCode;
	private String dutyName;
	private String usrEmail;
	private String usrMobileNo;
	private Long custmSeq;
	private Long loginFailCnt;
	private String accLockYn;
	private String useYn;	
	private String checked;
	private Date loginFailDate;

	private List<Role> roles;
	private ChangePwdDto changePwd;

	/**
	 * @param currentUser
	 * @return
	 */
	public User toEntityWithCurrentUser(UserPrincipal currentUser) {
		return User.builder()
			.seq(seq)
			.usrNo(usrNo)
			.accTypeCode(accTypeCode)
			.accTypeName(accTypeName)
			.usrId(usrId)
			.usrNm(usrNm)
			.usrPwd(new BCryptPasswordEncoder().encode(usrPwd))
			.coNm(coNm)
			.deptNm(deptNm)
			.dutyCode(dutyCode)
			.dutyName(dutyName)
			.usrEmail(usrEmail)
			.usrMobileNo(usrMobileNo)
			.custmSeq(custmSeq)
			.loginFailCnt(loginFailCnt)
			.accLockYn(accLockYn)
			.loginFailDate(loginFailDate)
			.regUsrNo(currentUser.getUsrNo())
			.modUsrNo(currentUser.getUsrNo())
			.build();
	}
}

