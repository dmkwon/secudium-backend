package com.secudium.api.systemoper.user.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.security.UserPrincipal;
import com.secudium.security.entity.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class UserUpdateDto {
	private Long seq;
	private Long usrNo;
	private String accTypeCode;
	private String accTypeName;
	private String usrNm;
	private String usrId;
	private String coNm;
	private String deptNm;
	private String dutyCode;
	private String dutyName;
	private String usrEmail;
	private String usrMobileNo;
	private Long custmSeq;
	private String useYn;	
	private String accLockYn;
	private String checked;

	private List<Role> roles;

	/**
	 * @param currentUser
	 * @return
	 */
	public User toEntityWithCurrentUser(UserPrincipal currentUser) {
		return User.builder()
			.seq(seq)
			.usrNo(usrNo)
			.accTypeCode(accTypeCode)
			.accTypeName(accTypeName)
			.usrId(usrId)
			.usrNm(usrNm)
			.coNm(coNm)
			.deptNm(deptNm)
			.dutyCode(dutyCode)
			.dutyName(dutyName)
			.usrEmail(usrEmail)
			.usrMobileNo(usrMobileNo)
			.custmSeq(custmSeq)
			.accLockYn(accLockYn)
			.regUsrNo(currentUser.getUsrNo())
			.modUsrNo(currentUser.getUsrNo())
			.build();
	}
}

