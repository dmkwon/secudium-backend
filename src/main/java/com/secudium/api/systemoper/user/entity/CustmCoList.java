package com.secudium.api.systemoper.user.entity;

import java.io.Serializable;

import com.secudium.api.systemoper.user.dto.CustomCoListDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class CustmCoList extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -1L;

	private Long seq;
	private Long custmSeq;
	private Long custmGroupSeq;
	private Long custmId;
	private Long custmGroupId;
	private String custmGroupNm;
	private String channelTypeCode;
	private String coNm;
	private String companyTypeCode;
	private String companyGroupTypeCode;
	private Long bizNo;
	private String activeYn;
	private String deActiveReason;
	private String useYn;
	private String checked;
	
	public CustomCoListDto toDto() {
		return CustomCoListDto.builder()
			.seq(seq)
			.custmSeq(custmSeq)
			.custmGroupSeq(custmGroupSeq)
			.custmId(custmId)
			.custmGroupId(custmGroupId)
			.custmGroupNm(custmGroupNm)
			.channelTypeCode(channelTypeCode)
			.coNm(coNm)
			.companyTypeCode(companyTypeCode)
			.companyGroupTypeCode(companyGroupTypeCode)
			.bizNo(bizNo)
			.activeYn(activeYn)
			.deActiveReason(deActiveReason)
			.useYn(useYn)	
			.checked(checked)
			.build();
	}
	
}
