package com.secudium.api.systemoper.user.entity;

import java.io.Serializable;
import java.util.Date;

import com.secudium.api.systemoper.user.dto.UserDto;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 2354252969965092086L;

	private Long seq;
	private Long usrNo;
	private String accTypeCode;
	private String accTypeName;
	private String usrId;
	private String usrNm;
	private String usrPwd;
	private String coNm;
	private String deptNm;
	private String dutyCode;
	private String usrEmail;
	private String usrMobileNo;
	private Long custmSeq;
	private Long loginFailCnt;
	private String accLockYn;
	private String useYn;
	private String dutyName;
	private String checked;
	private Date loginFailDate;

	public UserDto toDto() {
		return UserDto.builder()
			.seq(seq)	
			.usrNo(usrNo)	
			.accTypeCode(accTypeCode)
			.accTypeName(accTypeName)
			.usrId(usrId)
			.usrNm(usrNm)
			.usrPwd(usrPwd)
			.coNm(coNm)
			.deptNm(deptNm)
			.dutyCode(dutyCode)
			.dutyName(dutyName)
			.usrEmail(usrEmail)
			.usrMobileNo(usrMobileNo)
			.custmSeq(custmSeq)
			.loginFailCnt(loginFailCnt)
			.accLockYn(accLockYn)
			.useYn(useYn)
			.checked(checked)
			.loginFailDate(loginFailDate)
			.build();	
	}
}
