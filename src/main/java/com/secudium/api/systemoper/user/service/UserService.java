package com.secudium.api.systemoper.user.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.systemoper.role.dao.RoleDao;
import com.secudium.api.systemoper.user.dao.UserDao;
import com.secudium.api.systemoper.user.dto.ChangePwdDto;
import com.secudium.api.systemoper.user.dto.CustomCoListDto;
import com.secudium.api.systemoper.user.dto.UserDto;
import com.secudium.api.systemoper.user.dto.UserUpdateDto;
import com.secudium.api.systemoper.user.entity.CustmCoList;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.common.dto.PagedResponse;
import com.secudium.common.entity.SearchMap;
import com.secudium.security.UserPrincipal;
import com.secudium.security.entity.Role;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

	private final UserDao userDao;
	private final RoleDao roleDao;

	public PagedResponse<User> getUsers(Pageable pageable, SearchMap search, String locale) {
		Page<User> page = userDao.selectUsers(pageable, search, locale);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());	
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public UserDto getUserBySeq(Long seq, String locale) {
		return userDao.selectUserBySeq(seq, locale);
	}

	public User getUsersByUsername(String username) {
		return userDao.selectUsersByUsername(username);
	}

	public List<Role> getRolesByUsername(String username) {
		return userDao.selectRolesByUsername(username);
	}

	public PagedResponse<CustmCoList> getCustmGroup(Pageable pageable, SearchMap search) {
		Page<CustmCoList> page = userDao.selectCustmGroup(pageable, search);
		if(page.getNumberOfElements() == 0) {
			return new PagedResponse<>(Collections.emptyList(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());	
		} else {
			return new PagedResponse<>(page.getContent(), page.getNumber(),
					page.getSize(), page.getTotalElements(), page.getTotalPages(), page.isLast());
		}
	}

	public List<CustomCoListDto> getCoNmByCustmGroupSeq(Long seq) {
		return userDao.selectCoNmByCustmGroupSeq(seq);
	}

	public void createUser(UserDto dto, UserPrincipal currentUser) {
		userDao.insertUser(dto.toEntityWithCurrentUser(currentUser));
	}

	public int updateUser(UserUpdateDto dto, UserPrincipal currentUser) {
		return userDao.updateUser(dto.toEntityWithCurrentUser(currentUser));
	}

	public void deleteUsers(List<UserDto> dto, UserPrincipal currentUser) {
		List<User> list = new ArrayList<User>();
		for(UserDto vo : dto) {
			roleDao.deleteUserRoleBySeq(vo.getUsrNo());
			list.add(vo.toEntityWithCurrentUser(currentUser));
		}

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("modUsrNo", currentUser.getUsrNo());
		userDao.deleteUsers(map);
	}

	public int deleteUser(Long seq, UserPrincipal currentUser) {
		roleDao.deleteUserRoleBySeq(seq);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("seq", seq);
		map.put("modUsrNo", currentUser.getUsrNo());
		return userDao.deleteUser(map);	
	}

	public void updateChangePwd(Long seq, ChangePwdDto dto, UserPrincipal currentUser) {
		userDao.updateChangePwd(dto.toEntityWithCurrentUser(seq, currentUser));
	}

	public Boolean isDuplicate(String usrId) {
		return userDao.isDuplicate(usrId);
	}

	public Boolean isAccLocked(String usrId) {
		return userDao.isAccLocked(usrId);
	}

	public void loginFail(String usrId) {
		userDao.loginFail(usrId);
	}

	public void updateLoginCntReset(String usrId) {
		userDao.updateLoginCntReset(usrId);
	}

	public int getLoginFailCount(String usrId) {
		return userDao.getLoginFailCount(usrId);
	}


}
