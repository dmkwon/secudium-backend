package com.secudium.common.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.secudium.common.entity.AgentModelInfo;
import com.secudium.common.entity.Select;
import com.secudium.common.service.CommonService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class CommonController {

	@Autowired
	private CommonService commonService;

	/**
	 * Vendor리스트 조회
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/api/vendors")
	public ResponseEntity<List<Select>> getAgentVendors() {
		return ResponseEntity.ok(commonService.getAgentVendorList());
	}

	@GetMapping("/api/satcOfModel")
	public ResponseEntity<List<Select>> getSatcOfModel(
			@RequestParam("vendorId") String vendorId, 
			Locale locale) {
		return ResponseEntity.ok(commonService.getSatcOfModel(Long.parseLong(vendorId), locale.getLanguage()));
	}

	@GetMapping("/api/satcOfModelByVendorIds")
	public ResponseEntity<List<Select>> getSatcOfModelByVendorIds(
			@RequestParam("vendorIds") String vendorIds, 
			Locale locale) {
		return ResponseEntity.ok(commonService.getSatcOfModelByVendorIds(vendorIds, locale.getLanguage()));
	}

	@GetMapping("/api/modelsOfSatc")
	public ResponseEntity<List<Select>> getModelsOfSatc(
			@RequestParam("vendorId") String vendorId, 
			@RequestParam("satc") String satc) {
		return ResponseEntity.ok(commonService.getModelsOfSatc(Long.parseLong(vendorId), satc));
	}

	@GetMapping("/api/modelsOfSatcByVendorIds")
	public ResponseEntity<List<AgentModelInfo>> getModelsOfSatcByVendorIds(
			@RequestParam("vendorIds") String vendorIds, 
			@RequestParam("satcs") String satcs,
			Locale locale) {
		return ResponseEntity.ok(commonService.getModelsOfSatcByVendorIds(vendorIds, satcs, locale.getLanguage()));
	}

	@GetMapping("/api/isduplicate") 	
	public ResponseEntity<Boolean> isDuplicate(
			@RequestParam("tableName") String tableName,
			@RequestParam("colName") String colName,
			@RequestParam("value") String value) {
		return ResponseEntity.ok(commonService.isDuplicate(tableName, colName, value));
	}

	/**
	 * parentCode 에 해당하는 code list 가져오는 api
	 * @param parentCode
	 * @return
	 */
	@GetMapping("/api/comcodes")
	public ResponseEntity<List<Select>> getComcode(
			@RequestParam(value="parentCode", required=false) String parentCode, Locale locale) {
		return ResponseEntity.ok(commonService.getCodeValueNameList(parentCode, locale.getLanguage()));
	}

	/*
	 * 언어 변경 API
	 */
	@GetMapping("/api/change/language")
	public ResponseEntity<String> changeLang(Locale locale) {
		return ResponseEntity.ok(locale.getLanguage());
	}
}
