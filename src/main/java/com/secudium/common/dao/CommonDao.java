package com.secudium.common.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.common.entity.AgentModelInfo;
import com.secudium.common.entity.Select;
import com.secudium.common.entity.TestItem;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class CommonDao extends RepositorySupport {

	private final SqlSession sqlSession;

	public List<Select> selectCodeValueNameList(String parentCode, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("parentCode", parentCode);
		params.put("lang", locale);
		return this.sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<Select> selectAgentVendorInfoValueNameList() {
		return this.sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName());
	}

	public Boolean isDuplicate(Map<String, String> params) {
		return (Integer) this.sqlSession.selectOne(mapperNs + MethodUtils.getCurrentMethodName(), params) > 0 ?
				true : false;
	}

	public List<Select> selectSatcOfModel(Long vendorId, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("vendorId", vendorId);
		params.put("lang", locale);
		return this.sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<Select> selectSatcOfModelByVendorIds(List<Long> vendorIds, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("vendorIds", vendorIds);
		params.put("lang", locale);
		return this.sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<Select> selectModelsOfSatc(Long vendorId, String satc) {
		Map<String, Object> params = new HashMap<>();
		params.put("vendorId", vendorId);
		params.put("satc", satc);
		return this.sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

	public List<AgentModelInfo> selectModelsOfSatcByVendorIds(List<Long> vendorIds, List<String> satcs, String locale) {
		Map<String, Object> params = new HashMap<>();
		params.put("vendorIds", vendorIds);
		params.put("satcs", satcs);
		params.put("lang", locale);
		return this.sqlSession.selectList(mapperNs + MethodUtils.getCurrentMethodName(), params);
	}

}
