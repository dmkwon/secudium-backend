package com.secudium.common.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.secudium.common.entity.LoginHist;
import com.secudium.support.RepositorySupport;
import com.secudium.util.MethodUtils;

import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class LoginHistDao extends RepositorySupport {

	private final SqlSession sqlSession;
	
	public int insertLoginHist(LoginHist loginHist) {
		return this.sqlSession.insert(mapperNs + MethodUtils.getCurrentMethodName(), loginHist);
	}
	
}
