package com.secudium.common.dto;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class FileInfoDto {
	
	private boolean state;
    private String name;
    private String serverName;
    private String size;
    private String fileFullPath;
    private List<Map<String,String>> extra;
	
}
