package com.secudium.common.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OkResponse {

	private String message;
	private int status;
	private String code;

	private OkResponse(final String message 
			, final int status
			, final String code) {
		this.message = message;
		this.status = status;
		this.code = code;
	}
	
	public static OkResponse of() {
		return new OkResponse("OK", 200, "C000");
	}
}