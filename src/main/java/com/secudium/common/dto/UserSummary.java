package com.secudium.common.dto;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserSummary {

	private String usrLoginId;
	private String usrNm;
	private String usrDeptNm;
	private String usrEmail;
	private String lastLoginDate;
	private Collection<? extends GrantedAuthority> authorities;
	private String lang;
}
