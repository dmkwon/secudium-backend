package com.secudium.common.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.mybatis.entity.BaseEntity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class AgentModelInfo extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 3871203663162089750L;

	private Long agentModelSeq;
	private Long agentVendorSeq;
	private Long agentModelId;
	private String agentModelNm;
	private String agentModelSection;
	private String agentModelSectionNm;
	private Long agentVendorId;
	private String agentVendorNm;
	private String delYn;
	private String useYn;


}
