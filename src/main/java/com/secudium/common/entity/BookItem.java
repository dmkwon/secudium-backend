package com.secudium.common.entity;

import java.util.List;

import lombok.Data;

@Data
public class BookItem {

	String title;

	List<String> genres;

	List<String> authors;

	boolean published;
}
