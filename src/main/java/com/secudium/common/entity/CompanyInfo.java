package com.secudium.common.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.secudium.api.systemoper.user.entity.User;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public class CompanyInfo {
	
	private long companySeq;
	private long companyGroupSeq;
	private String companyName;
	private String smsCompanyName;
	private String billCompanyName;
	private String companyCode;
	private String activeYn;
	private String companyCeoName;
	private String companyZipCode;
	private String companyAddr01;
	private String companyAddr02;
	private String agreeYn;
	private long nmsServerSeq;
	private String nmsInFlag;
	private String nrpeServerIp;
	
	// Admin 정보
	private User admin;
	private String salesId;
	private String certId;
	private String projectManagerName;
	private String projectNumberString;
	private int billType;	
}
