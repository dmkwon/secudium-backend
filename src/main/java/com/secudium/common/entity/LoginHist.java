package com.secudium.common.entity;

import java.io.Serializable;

import com.secudium.mybatis.entity.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class LoginHist extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -3494179679968821881L;

	private Long loginHistSeq;  // 로그인이력일련번호
	private String usrId;    // 사용자 ID
	private String acsIp;    // 접속 IP
	private String failMsg;  // 실패 메시지 (실패 시 생성)
	private String acsTk;	 // 접속 토큰 (성공 시 생성)
}
