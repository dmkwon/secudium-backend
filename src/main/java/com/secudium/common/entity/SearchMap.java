package com.secudium.common.entity;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.WordUtils;

/**
 * 검색 조건 map
 * 
 * @author DongMan Kwon
 */
public class SearchMap extends HashMap<String, Object> {

	private static final long serialVersionUID = -7116224054358578695L;

	private static final String PREFIX_STR = "search";

	@Override
	public Object put(String key, Object value) {
		key = StringUtils.removeStart(key, PREFIX_STR);
		key = PREFIX_STR + WordUtils.capitalize(key);
		return super.put(key, value);
	}

	@Override
	public Object get(Object keyObj) {
		String key = (String) keyObj;
		if(!containsKey(key)) {
			key = PREFIX_STR + WordUtils.capitalize(key);
		}
		return super.get(key);
	}

	public SearchMap add(String key, Object value) {
		this.put(key, value);
		return this;
	}

	public SearchMap(Map<? extends String, ? extends Object> map) {
		for (Map.Entry<? extends String, ? extends Object> entry : map.entrySet()) {
		
			if( entry.getValue() instanceof String[] ) {
				String[] values = (String[]) entry.getValue();
				if(values.length == 1) {
					this.put(entry.getKey(), decodeURIComponent(values[0]));
				} else if(values.length > 1) {
					for(String value: values) {
						value = decodeURIComponent(value);
					}
					this.put(entry.getKey(), Arrays.asList(values));
				}
			} else if(entry.getValue() instanceof String) {
				this.put(entry.getKey(), decodeURIComponent((String)entry.getValue()));
			} else {
				this.put(entry.getKey(), entry.getValue());
			}
		}
	}

	private String decodeURIComponent(String value) {
		try {
			return URLDecoder.decode(value, "UTF-8");
		} catch(UnsupportedEncodingException e) {
			return value;
		}
	}

	public SearchMap() {
		super();
	}
	public SearchMap(ServletRequest request) {
		this(request.getParameterMap());
	}
	public static SearchMap buildFrom(Map<? extends String, ? extends Object> map) {
		return new SearchMap(map);
	}
	public static SearchMap buildFrom(ServletRequest request) {
		return new SearchMap(request);
	}
}