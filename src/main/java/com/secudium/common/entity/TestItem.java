package com.secudium.common.entity;

import com.google.gson.Gson;

import lombok.Data;

@Data
public class TestItem {

	String detectCndtnJson;

	public BookItem getBookItem() {
		if(this.detectCndtnJson != null && this.detectCndtnJson.length() > 0) {
			Gson gson = new Gson(); 
			return gson.fromJson(this.detectCndtnJson, BookItem.class);
		} else {
			return null;
		}
	}	
}
