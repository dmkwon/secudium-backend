package com.secudium.common.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.secudium.common.dao.CommonDao;
import com.secudium.common.entity.AgentModelInfo;
import com.secudium.common.entity.Select;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CommonService {

	@Autowired
	private CommonDao commonDao;

	public List<Select> getAgentVendorList() {
		return commonDao.selectAgentVendorInfoValueNameList();
	}

	public List<Select> getSatcOfModel(Long vendorId, String locale) {
		return commonDao.selectSatcOfModel(vendorId, locale);
	}

	public List<Select> getSatcOfModelByVendorIds(String vendorIds, String locale) {
		List<Long> vendors = new ArrayList<>();
		String[] temp = vendorIds.split(",");
		for(int i = 0; i < temp.length; i++) {
			vendors.add(Long.parseLong(temp[i]));
		}
		return commonDao.selectSatcOfModelByVendorIds(vendors, locale);
	}

	public List<Select> getModelsOfSatc(Long vendorId, String satc) {
		return commonDao.selectModelsOfSatc(vendorId, satc);
	}

	public List<AgentModelInfo> getModelsOfSatcByVendorIds(String vendorIds, String satcCodes, String locale) {

		List<Long> vendors = new ArrayList<>();
		String[] tempVendors = vendorIds.split(",");
		for(int i = 0; i < tempVendors.length; i++) {
			vendors.add(Long.parseLong(tempVendors[i]));
		}

		List<String> satcs = new ArrayList<>();
		String[] tempSatcs = satcCodes.split(",");
		for(int i = 0; i < tempSatcs.length; i++) {
			satcs.add(tempSatcs[i]);
		}
		return commonDao.selectModelsOfSatcByVendorIds(vendors, satcs, locale);
	}

	/**
	 * parentCode 에 해당하는 code list 가져오는 함수
	 * @param parentCode
	 * @return
	 */
	public List<Select> getCodeValueNameList(String parentCode, String locale) {
		return commonDao.selectCodeValueNameList(parentCode, locale);
	}

	public Boolean isDuplicate(String tableName, String colName, String value) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("tableName", tableName);
		params.put("colName", colName);
		params.put("value", value);
		return commonDao.isDuplicate(params);
	}
}
