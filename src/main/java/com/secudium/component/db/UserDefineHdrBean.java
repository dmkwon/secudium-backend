package com.secudium.component.db;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.secudium.api.enginemgmt.engineenv.dao.EngineEnvDao;
import com.secudium.api.enginemgmt.engineenv.dao.UserDefineHdrDao;
import com.secudium.api.enginemgmt.engineenv.entity.UserDefineHdr;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserDefineHdrBean implements InitializingBean, DisposableBean {

	private final UserDefineHdrDao userDefineHdrDao;
	private final EngineEnvDao engineEnvDao;
	ConcurrentHashMap<String, String> userDefineHdr = new ConcurrentHashMap<>();
	
	public UserDefineHdrBean(UserDefineHdrDao userDefineHdrDao, EngineEnvDao engineEnvDao) {
		this.userDefineHdrDao = userDefineHdrDao;
		this.engineEnvDao = engineEnvDao;
	}
	
	public void afterPropertiesSet() throws Exception {
		String rootPath = engineEnvDao.selectEngineEnvInfo().getRootPath();
		List<UserDefineHdr> userDefineHdrs = userDefineHdrDao.selectUserDefineHdrs();
		for(UserDefineHdr udh: userDefineHdrs) {
			this.userDefineHdr.put(rootPath+udh.getNodePath(), udh.getHdr());
		}
	}

	public void destroy() throws Exception {
		this.userDefineHdr.clear();
	}

	public ConcurrentHashMap<String, String> getObject() {
		return this.userDefineHdr;
	}
	public String get(String key) {
		return this.userDefineHdr.getOrDefault(key, "");
	}

	public String put(String key, String value) {
		return this.userDefineHdr.putIfAbsent(key, value);
	}
	
	public void putAll(ConcurrentHashMap<String, String> userDefineHdr) {
		userDefineHdr.putAll(userDefineHdr);
	}

	public boolean isSingleton() {
		return true;
	}
	
}
