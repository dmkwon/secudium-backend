package com.secudium.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.secudium.api.enginemgmt.engineenv.dao.EngineEnvDao;
import com.secudium.api.enginemgmt.engineenv.dao.UserDefineHdrDao;
import com.secudium.component.db.UserDefineHdrBean;
import com.secudium.component.zk.CuratorFrameworkFactoryBean;

import lombok.RequiredArgsConstructor;


@Configuration
@RequiredArgsConstructor
public class AppConfig {

	private final EngineEnvDao engineEnvDao;
	private final UserDefineHdrDao userDefineHdrDao;

	@Bean
	public ObjectMapper objectMapper() {
		return Jackson2ObjectMapperBuilder
				.json()
				.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
				.modules(new JavaTimeModule())
				.build();
	}

	@Bean
	public CuratorFrameworkFactoryBean curatorFrameworkFactory() {
		CuratorFrameworkFactoryBean curatorFrameworkFactoryBean = new CuratorFrameworkFactoryBean();
		curatorFrameworkFactoryBean.setConnectString(engineEnvDao.selectEngineEnvInfo().getZkConnInfo());
		curatorFrameworkFactoryBean.setSessionTimeout(5000);
		return curatorFrameworkFactoryBean;
	}

	@Bean 
	public UserDefineHdrBean userDefineHdrBean() {
		return new UserDefineHdrBean(userDefineHdrDao, engineEnvDao);
	}
}