package com.secudium.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.secudium.security.CustomUserDetailsService;
import com.secudium.security.RestAuthenticationEntryPoint;
import com.secudium.security.RestLogoutSuccessHandler;

@Configuration 
@EnableWebSecurity 
@EnableGlobalMethodSecurity(
		securedEnabled=true, 
		prePostEnabled=true, 
		jsr250Enabled=true)
public class SecurityConfig extends WebSecurityConfigurerAdapter { 

	@Autowired
	RestAuthenticationEntryPoint restAuthenticationEntryPoint;

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(
				"/favicon.ico",
				"/**/*.png",
				"/**/*.gif",
				"/**/*.svg",
				"/**/*.jpg",
				"/**/*.html",
				"/**/*.css",
				"/**/*.js");
	}

	/*
	 * AuthenticationManagerBuilder 객체는 스프링 시큐리티가 사용자를 인증하는 방법이다.
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
	}

	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Bean
	public HttpSessionEventPublisher httpSessionEventPublisher() {
		return new HttpSessionEventPublisher();
	}

	@Override 
	protected void configure(HttpSecurity http) throws Exception { 
		http 
		.csrf()
		.disable() 
		.cors()
		.and()
		.exceptionHandling() 
		.authenticationEntryPoint(restAuthenticationEntryPoint) 
		.and() 
		.logout() 
		.logoutUrl("/api/auth/signout")
		.logoutSuccessHandler(logoutSuccessHandler()) //추가2
		.and()
		.authorizeRequests()
		.requestMatchers(CorsUtils::isPreFlightRequest).permitAll() // - (1)
		.antMatchers("/api/auth/signin").permitAll()
		.anyRequest().authenticated()
		.and()
		.headers().frameOptions().sameOrigin().and()
		.sessionManagement()
		.maximumSessions(1)
		.maxSessionsPreventsLogin(true)
		.and()
		.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
	} 

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		// - (3)
		configuration.addAllowedOrigin("*");
		configuration.addAllowedMethod("*");
		configuration.addAllowedHeader("*");
		configuration.setAllowCredentials(true);
		configuration.setMaxAge(3600L);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}

	@Bean 
	public RestLogoutSuccessHandler logoutSuccessHandler() throws Exception { 
		return new RestLogoutSuccessHandler();
	}
}

