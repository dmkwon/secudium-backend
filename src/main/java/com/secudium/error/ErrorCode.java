package com.secudium.error;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ErrorCode {
	// Common
	INVALID_INPUT_VALUE(400, "C001", "Invalid Input Value"),
	ENTITY_NOT_FOUND(400, "C002", "Entity Not Found"),
	INVALID_TYPE_VALUE(400, "C003", "Invalid Type Value"),
	HANDLE_ACCESS_DENIED(403, "C004", "Access is Denied"),
	METHOD_NOT_ALLOWED(405, "C005", "Invalid Input Value"),
	INTERNAL_SERVER_ERROR(500, "C006", "Internal Server Error"),
	NEED_LOGIN_VALUE(401, "C007", "Need Login"),
	UNSUPPORTED_ENCODING(500, "C008", "Unsupported Encoding Error"),
	INVALID_ZK_PATH(500, "C009", "Invalid Zookeeper Path"),
	INVALID_LOGIN_VALUE(401, "C010", "Invalid Login ID or Password"),
	ACCOUNT_LOCK(401, "C011", "User Account Locked"),
  NO_ACCOUNT_INFO(401, "C012", "Account Doesn't Exist"),
  DUPLICATE_DATA_EXIST(200, "C013", "Duplicate Data Exists"),
	;
	
	private final String code;
	private final String message;
	private int status;

	ErrorCode(final int status, final String code, final String message) {
		this.status = status;
		this.message = message;
		this.code = code;
	}

	public String getMessage() {
		return this.message;
	}

	public String getCode() {
		return code;
	}

	public int getStatus() {
		return status;
	}


}
