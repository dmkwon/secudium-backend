package com.secudium.exception;

import com.secudium.error.ErrorCode;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 2546008346723526151L;
	
	private final ErrorCode errorCode;

    public BusinessException(String message, ErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public BusinessException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
