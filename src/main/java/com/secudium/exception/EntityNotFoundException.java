package com.secudium.exception;

import com.secudium.error.ErrorCode;

public class EntityNotFoundException extends BusinessException {

	private static final long serialVersionUID = -8431833407196996454L;

	public EntityNotFoundException(String message) {
        super(message, ErrorCode.ENTITY_NOT_FOUND);
    }
}
