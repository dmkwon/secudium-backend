package com.secudium.exception;

import com.secudium.error.ErrorCode;

public class InvalidValueException extends BusinessException {

	private static final long serialVersionUID = 5518826268221484265L;

	public InvalidValueException(final ErrorCode errorCode) {
        super(errorCode);
    }
}
