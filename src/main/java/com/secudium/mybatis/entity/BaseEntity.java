package com.secudium.mybatis.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class BaseEntity {

	private Long regUsrNo;
	private Date regDate;
	private Long modUsrNo;
	private Date modDate;

	private String regUsrNm;
	private String modUsrNm;

	private String stringModDate;
	private String stringRegDate;

	private int no;

	public String getRegDateFormatted() {
		if(regDate != null) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return df.format(regDate);
		} else {
			return "";
		}
	}
	public String getModDateFormatted() {
		if(modDate != null) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return df.format(modDate);
		} else {
			return "";
		}
	}

	public String getRegDateYmd() {
		if(regDate != null) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(regDate);
		} else {
			return "";
		}
	}
	public String getModDateYmd() {
		if(modDate != null) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(modDate);
		} else {
			return "";
		}
	}
}
