package com.secudium.mybatis.utils;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class Ognl {

	private Ognl() {
		throw new IllegalStateException("Utility class");
	}

	public static boolean isNotEmpty(String value) {
		return !StringUtils.isEmpty(value);
	}

	public static boolean isEmpty(String value) {
		return StringUtils.isEmpty(value);
	}
	
	public static Boolean isEquals(String value1, String value2) {
		return StringUtils.equals(value1, value2);
	}

	public static boolean isNotBlank(String value) {
		return !StringUtils.isBlank(value);
	}

	public static boolean isBlank(String value) {
		return StringUtils.isBlank(value);
	}

	public static boolean isInteger(Integer value) {
		return value == value.intValue();
	}

	public static boolean isNotMinus(String value) {
		return Integer.parseInt(value) >= 0; 
	}

    /**
     * 객체가 Empty가 아닌지 여부
     *
     * @param obj Object
     * @return boolean
     */
    public static boolean isNotEmpty(Object obj){
        return !isEmpty(obj);
    }
    /**
     * 객체가 Empty인지 여부
     *
     * @param obj Object
     * @return boolean
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmpty(Object obj){
        if( obj instanceof String ) return obj==null || "".equals(obj.toString().trim());
        else if( obj instanceof List) return obj==null || ((List)obj).isEmpty();
        else if( obj instanceof Map) return obj==null || ((Map)obj).isEmpty();
        else if( obj instanceof Object[] ) return obj==null || Array.getLength(obj)==0;
        else return obj == null;
    }
}
