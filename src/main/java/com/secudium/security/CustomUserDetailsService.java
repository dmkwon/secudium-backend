package com.secudium.security;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.secudium.api.systemoper.user.dto.UserDto;
import com.secudium.api.systemoper.user.entity.User;
import com.secudium.api.systemoper.user.service.UserService;
import com.secudium.exception.EntityNotFoundException;
import com.secudium.security.entity.Role;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

	private final UserService userService;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) {

		User user = userService.getUsersByUsername(username);
		if(user == null) { 
			throw new EntityNotFoundException("username(" + username + ") is not found");
		}

		List<Role> roles = userService.getRolesByUsername(username);
		return UserPrincipal.create(UserDto.builder()
				.usrNo(user.getUsrNo())
				.usrId(user.getUsrId())
				.usrPwd(user.getUsrPwd())
				.usrNm(user.getUsrNm())
				.coNm(user.getCoNm())
				.deptNm(user.getDeptNm())
				.usrEmail(user.getUsrEmail())
				.roles(roles)
				.build());
	}
}
