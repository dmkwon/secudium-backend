package com.secudium.security;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.secudium.api.systemoper.user.dto.UserDto;
import com.secudium.security.entity.Role;

public class UserPrincipal implements UserDetails {

	/**
	 * serialVersionUID 생성
	 */
	private static final long serialVersionUID = 3330273419668038946L;

	private Long usrNo;

	private String usrId;

	@JsonIgnore
	private String usrPwd;

	private String usrNm;
	private String usrDeptNm;
	private String usrEmail;
	private String acsIp;
	private String acsTk;
	private HttpServletRequest request;
	private Set<GrantedAuthority> authorities;

	public UserPrincipal( 
			Long usrNo,
			String usrId,
			String usrPwd,
			String usrNm,
			String usrDeptNm,
			String usrEmail,
			Collection<? extends GrantedAuthority> authorities) {
		this.usrNo = usrNo;
		this.usrId = usrId; 
		this.usrPwd = usrPwd;
		this.usrNm = usrNm;
		this.usrDeptNm = usrDeptNm;
		this.usrEmail = usrEmail;
		this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
	}

	public static UserPrincipal create(UserDto dto) {
		List<Role> authorities = dto.getRoles();

		return new UserPrincipal(
				dto.getUsrNo(),
				dto.getUsrId(),
				dto.getUsrPwd(),
				dto.getUsrNm(),
				dto.getDeptNm(),
				dto.getUsrEmail(),
				authorities
				);
	}

	public Long getUsrNo() {
		return this.usrNo;
	}

	@Override
	public String getUsername() {
		return this.usrId;
	}

	@Override
	public String getPassword() {
		return this.usrPwd;
	}

	public String getUsrNm() {
		return this.usrNm;
	}

	public String getUsrDeptNm() {
		return this.usrDeptNm;
	}

	public String getUsrEmail() {
		return this.usrEmail;
	}


	public String getAcsIp() {
		return acsIp;
	}
	public void setAcsIp(String acsIp) {
		this.acsIp = acsIp;
	}

	public String getAcsTk() {
		return acsTk;
	}
	public void setAcsTk(String acsTk) {
		this.acsTk = acsTk;
	}

	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Collection<GrantedAuthority> authorities) {
		this.authorities = Collections.unmodifiableSet(sortAuthorities(authorities));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		UserPrincipal that = (UserPrincipal) o;
		return Objects.equals(usrNo, that.usrNo);
	}

	@Override
	public int hashCode() {
		return Objects.hash(usrNo);
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}


	private static SortedSet<GrantedAuthority> sortAuthorities(Collection<? extends GrantedAuthority> authorities) {
		Assert.notNull(authorities, "Can's pass a null GrantedAuthority collection");

		SortedSet<GrantedAuthority> sortedAuthorities = new TreeSet<GrantedAuthority>(new AuthorityComparator());

		for(GrantedAuthority grantedAuthority: authorities) {
			Assert.notNull(grantedAuthority, "GrantedAuthority list cannot contain any null elements");
			sortedAuthorities.add(grantedAuthority);
		}
		
		return sortedAuthorities;
	}

	private static class AuthorityComparator implements Comparator<GrantedAuthority>, Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

		public int compare(GrantedAuthority g1, GrantedAuthority g2) {
			if(g2.getAuthority() == null) {
				return -1;
			}

			if(g1.getAuthority() == null) {
				return 1;
			}

			return g1.getAuthority().compareTo(g2.getAuthority());
		}
	}
}