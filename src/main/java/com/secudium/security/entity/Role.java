package com.secudium.security.entity;

import org.springframework.security.core.GrantedAuthority;

import com.google.gson.Gson;

import lombok.Setter;

public class Role implements GrantedAuthority {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Role(String name){
		this.name = name;
	}

	public Role(){}

	@Setter
	private String name;

	@Override
	public String getAuthority() {
		return this.name;
	}

	@Override
	public int hashCode(){
		return name.hashCode();
	}

	@Override
	public boolean equals(Object r){
		if(r instanceof Role)
			return name.equals(((GrantedAuthority) r).getAuthority());
		return false;
	}

	@Override
	public String toString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}


}
