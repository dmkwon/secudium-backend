package com.secudium.storage.zk;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.x.async.AsyncCuratorFramework;
import org.apache.curator.x.async.AsyncResult;
import org.springframework.stereotype.Service;

import com.secudium.api.enginemgmt.engineenv.dao.EngineCommandDao;
import com.secudium.api.enginemgmt.engineenv.dao.EngineEnvDao;
import com.secudium.api.enginemgmt.engineenv.entity.EngineCommand;
import com.secudium.api.enginemgmt.engineenv.type.EngineMenuType;
import com.secudium.api.enginemgmt.zknode.entity.ZkNode;
import com.secudium.api.enginemgmt.zknode.service.ZkTreeNode;
import com.secudium.api.enginemgmt.zknode.service.ZkTreeRoot;
import com.secudium.component.db.UserDefineHdrBean;
import com.secudium.component.zk.CuratorFrameworkFactoryBean;
import com.secudium.error.ErrorCode;
import com.secudium.exception.BusinessException;
import com.secudium.util.DateUtils;
import com.secudium.util.PatternUtils;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ZKService {

	private static final String EXCEPTIONMSG = "Exception {}";
	private static final String WRITE_SELF_NODE = "WriteSelfNode";
	private final EngineEnvDao engineEnvDao;
	private final EngineCommandDao engineCommandDao;
	private final CuratorFrameworkFactoryBean factoryBean;
	private final UserDefineHdrBean userDefineHdrBean;

	private static final int BACKUP_LIMIT = 3;

	public void create(String path) {
		try {
			CuratorFramework client = factoryBean.getObject();
			client.create().creatingParentsIfNeeded().forPath(getPath(path));
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	public void createWithPayload(String path, byte[] payload) {
		String targetPath = getPath(path);
		try {
			CuratorFramework client = factoryBean.getObject();
			if (client.checkExists().forPath(targetPath) != null) {
				client.setData().forPath(targetPath, payload);
			} else {
				client.create().creatingParentsIfNeeded().forPath(targetPath, payload);
			}
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	public String getData(String path) {
		try {
			CuratorFramework client = factoryBean.getObject();
			AsyncCuratorFramework async = AsyncCuratorFramework.wrap(client);
			CompletionStage<AsyncResult<byte[]>> resultStage = AsyncResult.of(async.getData().forPath(getPath(path)));
			AsyncResult<byte[]> result = null;
			try {
				result = resultStage.toCompletableFuture().get(5, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				log.debug(EXCEPTIONMSG, e);
				Thread.currentThread().interrupt(); // set interrupt flag
			} catch (ExecutionException e) {
				log.debug(EXCEPTIONMSG, e);
				throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
			} catch (TimeoutException e) {
				log.debug(EXCEPTIONMSG, e);
				throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
			}
			if (result != null) {
				return new String(result.getValue().get());
			} else {
				return "";
			}
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	public String getDataForPath(String path) {

		try {
			CuratorFramework client = factoryBean.getObject();
			String generatedPath = getPath(path);
			ZkTreeRoot zkRoot = new ZkTreeRoot(client, generatedPath, userDefineHdrBean.getObject());
			zkRoot.setTabs(false);
			zkRoot.setDisplayData(false);
			zkRoot.setToLeaf(true);
			Map<String, ZkTreeNode> zkNodes = zkRoot.getChildrenIntoHash();
			StringBuilder sb = new StringBuilder();
			if (zkNodes.size() == 0) {
				try {
					sb.append(new String(client.getData().forPath(generatedPath)));
				} catch (Exception e) {
					zkRoot.close();
					throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
				}
			} else {
				if (zkNodes.size() == 1 || !PatternUtils.isNumber(zkNodes.keySet().toArray()[0].toString())) {
					String key = (String) zkNodes.keySet().toArray()[0];
					try {
						sb.append(new String(client.getData().forPath(generatedPath + "/" + key)));
					} catch (Exception e) {
						zkRoot.close();
						throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
					}
				} else {
					ArrayList<Integer> intKeys = new ArrayList<>();
					for (String key : zkNodes.keySet()) {
						intKeys.add(Integer.parseInt(key));
					}
					Collections.sort(intKeys);
					for (Integer intKey : intKeys) {
						try {
							String data = new String(client.getData().forPath(generatedPath + "/" + intKey));
							sb.append(data);
							//							if(intKey.equals(intKeys.get(0))) {
							//								sb.append(data);
							//							} else {
							//								sb.append("\n");
							//								sb.append(data);	
							//							}
						} catch (Exception e) {
							zkRoot.close();
							throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
						}
					}
				}
			}

			zkRoot.close();
			return sb.toString();
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 각 메뉴에 zookeeper write 할 경우 호출 api
	 * 
	 * @param path
	 * @param data
	 * @param usrId
	 * @param menu
	 */
	public void setDataForPathWithWatchMsg(List<Integer> lengthList, String data, String usrId, EngineMenuType menu) {
		EngineCommand engineCommand = engineCommandDao.selectEngineCommandByCmd(menu.name());
		setDataForPath(engineCommand.getZkDataPath(), lengthList, data, usrId);
		String[] paths = engineCommand.getZkWatchPath().split(",");
		for(int i = 0; i < paths.length; i++) {
			createWithPayload(paths[i], engineCommand.getZkWatchMsg().getBytes());	
		}
	}

	/**
	 * 각 메뉴에 zookeeper write 할 경우 호출 api
	 * 
	 * @param path
	 * @param data
	 * @param usrId
	 * @param menu
	 */
	public void setDataForPath(List<Integer> lengthList, String data, String usrId, EngineMenuType menu) {
		EngineCommand engineCommand = engineCommandDao.selectEngineCommandByCmd(menu.name());
		setDataForPath(engineCommand.getZkDataPath(), lengthList, data, usrId);
	}


	public void setDataForPath(String path, List<Integer> lengthList, String data, String usrId) {

		/**
		 * 백업 몇 개 인지 확인 (0) 3개 이하이면 , 추가 (0) 3개 이상이면 , 가장 오래된 것 하나 지우고 추가 (0) 기존 노드 모두
		 * 삭제 (이유: write 하는 데이터가 기존 보다 작은 경우, write 한 후 data 가져오는 경우 기존 데이터가 끝에 붙는다.) 해당
		 * 경로에 data 나눠서 Write (limit 사이즈 확인 from db) (0)
		 * 
		 */
		try {
			CuratorFramework client = factoryBean.getObject();
			String targetPath = getPath(path);
			boolean isExistNode = isExistNode(client, targetPath);
			int countOfBackUp = getCountOfBackUp(client, targetPath, usrId);

			if (isExistNode) {
				if (countOfBackUp == BACKUP_LIMIT) {
					removeFirstBackUp(client, targetPath, usrId);
				}
				doBackUp(client, targetPath, generateBakPath(targetPath, usrId));
			}

			deletePreviousChildNodes(client, targetPath);
			doWriteDateForNode(client, targetPath, lengthList, data);			
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.INVALID_ZK_PATH);
		}
	}

	public void setData(String path, byte[] payload) {
		try {
			CuratorFramework client = factoryBean.getObject();
			client.setData().forPath(getPath(path), payload);
		} catch (Exception e) {
			log.debug(EXCEPTIONMSG, e);
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	public void setDataAsync(String path, byte[] payload) {
		try {
			CuratorFramework client = factoryBean.getObject();
			AsyncCuratorFramework async = AsyncCuratorFramework.wrap(client);
			try {
				async.setData().forPath(getPath(path), payload);
			} catch (Exception e) {
				log.debug(EXCEPTIONMSG, e);
				throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			log.debug(EXCEPTIONMSG, e);
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	public void delete(String path) {
		try {
			CuratorFramework client = factoryBean.getObject();
			client.delete().deletingChildrenIfNeeded().forPath(getPath(path));
		} catch (Exception e) {
			log.debug(EXCEPTIONMSG, e);
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	public void guaranteedDelete(String path) {
		try {
			CuratorFramework client = factoryBean.getObject();
			client.delete().guaranteed().forPath(path);
		} catch (Exception e) {
			log.debug(EXCEPTIONMSG, e);
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	public List<ZkNode> getZkSubTreeNode(String parentNode) {
		try {
			CuratorFramework client = factoryBean.getObject();
			ZkTreeRoot zkRoot = new ZkTreeRoot(client, getPath(parentNode), userDefineHdrBean.getObject());
			zkRoot.setTabs(false);
			zkRoot.setDisplayData(false);
			zkRoot.setToLeaf(false);
			Map<String, ZkTreeNode> zkNodes = zkRoot.getChildrenIntoHash();
			List<ZkNode> treeList = new ArrayList<>();
			for (String mapkey : zkNodes.keySet()) {
				treeList.addAll(zkNodes.get(mapkey).getTreeForRootByMaxLevel(0));
			}
			zkRoot.close();
			return treeList;
		} catch (Exception e) {
			log.debug(EXCEPTIONMSG, e);
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	public List<ZkNode> getZkRootTreeByDepth(int depth) {
		try {
			CuratorFramework client = factoryBean.getObject();
			if(client.getState() == CuratorFrameworkState.STARTED) {
				ZkTreeRoot zkRoot = new ZkTreeRoot(client, getPath(""), userDefineHdrBean.getObject());
				zkRoot.setTabs(false);
				zkRoot.setDisplayData(false);
				zkRoot.setToLeaf(false);
				Map<String, ZkTreeNode> zkNodes = zkRoot.getChildrenIntoHash();
				List<ZkNode> treeList = new ArrayList<>();
				for (String mapkey : zkNodes.keySet()) {
					treeList.addAll(zkNodes.get(mapkey).getTreeForRootByMaxLevel(depth - 1));
				}
				zkRoot.close();
				return treeList;
			} else {
				return Collections.emptyList();
			}
		} catch (Exception e) {
			log.debug(EXCEPTIONMSG, e);
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	private void deletePreviousChildNodes(CuratorFramework client, String targetPath) {
		try {
			if (client.checkExists().forPath(targetPath) != null) {
				client.delete().deletingChildrenIfNeeded().forPath(targetPath);
			}
		} catch (Exception e) {
			log.debug(EXCEPTIONMSG, e);
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	private void writeDataForNode(CuratorFramework client, String targetPath, String data) {
		try {
			byte[] pushData = data.getBytes(Charset.forName("UTF-8"));
			if (client.checkExists().forPath(targetPath) != null) {
				client.setData().forPath(targetPath, pushData);
			} else {
				client.create().creatingParentsIfNeeded().forPath(targetPath, pushData);
			}
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	private boolean isWriteSelfNode(String targetPath) {
		String userDefineHdr = userDefineHdrBean.getObject().get(targetPath);
		boolean ret = false;
		if(userDefineHdr == null) {
			ret = false;
		} else {
			String lines[] = userDefineHdr.split("\\r?\\n");
			for(int i = 0; i < lines.length; i++ ) {
				String option = lines[i].replace("##", "");
				if(option.contains(WRITE_SELF_NODE)) {
					String[] KV = option.split("=");
					if(KV[1].toUpperCase().equals("TRUE")) {
						ret = true;
						break;
					} else {
						ret = false;
						break;
					}
				}
			}
		}
		return ret;
	}


	@Data
	class Index {
		private int start;
		private int end;
	}

	private void doWriteDateForNode(CuratorFramework client, String targetPath, List<Integer> lengthList, String data) {

		if (data.length() == 0 || isWriteSelfNode(targetPath)) {
			writeDataForNode(client, targetPath, data);
		} else if (data.length() > 0) {
			int splitSize = getLimitSize();
			int size = 0;
			int nodeCount = 0;
			String lines[] = data.split("\\r?\\n");
			int start = 0;

			Map<String, Index> idxMap = new HashMap<>();
			Index idx = new Index();
			idx.setStart(start);
			idx.setEnd(start);
			idxMap.put(Integer.toString(nodeCount), idx);

			for (int i = 0; i < lengthList.size(); i++) {
				size += lengthList.get(i);
				if(size > splitSize) {
					idxMap.get(Integer.toString(nodeCount)).setEnd(i-1);
					nodeCount++;
					start = i;
					size = lengthList.get(i);
					Index nextIdx = new Index();
					nextIdx.setStart(start);
					nextIdx.setEnd(start);
					idxMap.put(Integer.toString(nodeCount), nextIdx);
				} else {
					if (i == lines.length - 1) {
						idxMap.get(Integer.toString(nodeCount)).setEnd(i);	
					}
				}
			}

			for(int i = 0; i < idxMap.size(); i++) {
				Index progIdx = idxMap.get(Integer.toString(i));
				StringBuilder sb = new StringBuilder();				
				for(int j = progIdx.getStart(); j <= progIdx.getEnd(); j++ ) {
					if (j == progIdx.getEnd()) {
						sb.append(lines[j]).append("\n");
						String targetIndexPath = targetPath + "/" + Integer.toString(i);
						writeDataForNode(client, targetIndexPath, sb.toString());
					} else {
						sb.append(lines[j]).append("\n");
					}
				}
			}
		}
	}

	private boolean isExistNode(CuratorFramework client, String targetPath) {
		boolean ret = false;
		try {
			if (client.checkExists().forPath(targetPath) != null) {
				ret = true;
			}
		} catch (Exception e) {
			ret = false;
		}
		return ret;
	}

	private int getCountOfBackUp(CuratorFramework client, String targetPath, String usrId) {
		List<String> listOfNode = getBackUpNode(client, targetPath, usrId);
		return listOfNode.size();
	}

	private void removeFirstBackUp(CuratorFramework client, String targetPath, String usrId) {
		List<String> listOfNode = getBackUpNode(client, targetPath, usrId);
		List<Date> dateList = new ArrayList<>();
		for (String child : listOfNode) {
			String strDate = child.substring(0, DateUtils.YYYYMMDDHHMMSS.length());

			DateFormat formatter = new SimpleDateFormat(DateUtils.YYYYMMDDHHMMSS);
			Date date;
			try {
				date = formatter.parse(strDate);
			} catch (ParseException e) {
				throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
			}
			dateList.add(date);
		}

		Date minDate = Collections.min(dateList);
		DateFormat df = new SimpleDateFormat(DateUtils.YYYYMMDDHHMMSS);
		String toDeleteDate = df.format(minDate);
		try {
			log.debug(" path {}", getBakRootPath() + "/" + toDeleteDate + "_" + usrId);
			client.delete().deletingChildrenIfNeeded().forPath(getBakRootPath() + "/" + toDeleteDate + "_" + usrId);
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	private void doBackUp(CuratorFramework client, String orig, String dest) {
		copyZNode(client, orig, dest, false);
	}

	private List<String> getBackUpNode(CuratorFramework client, String path, String usrId) {
		ZkTreeRoot zkRoot = new ZkTreeRoot(client, getBakRootPath(), userDefineHdrBean.getObject());
		zkRoot.setTabs(false);
		zkRoot.setDisplayData(false);
		zkRoot.setToLeaf(false);
		Map<String, ZkTreeNode> zkNodes = zkRoot.getChildrenIntoHash();

		List<String> nodeList = new ArrayList<>();
		for (String child : zkNodes.keySet()) {
			if (child.substring((DateUtils.YYYYMMDDHHMMSS + "_").length()).equals(usrId)) {
				String listing = zkNodes.get(child).getListing(Integer.MAX_VALUE);
				if (listing.substring(("/" + child).length()).equals(path)) {
					nodeList.add(child);
				}
			}
		}
		zkRoot.close();
		return nodeList;
	}

	/**
	 * copy z node
	 * 
	 * @param client
	 * @param orig
	 * @param dest
	 * @param childrenOnly if true is copy only child node
	 */
	private void copyZNode(CuratorFramework client, String orig, String dest, boolean childrenOnly) {

		if (!childrenOnly) {
			byte[] origData;
			try {
				origData = client.getData().forPath(orig);
				if (client.checkExists().forPath(dest) != null) {
					client.setData().forPath(dest, origData);
				} else {
					client.create().creatingParentsIfNeeded().forPath(dest, origData);
				}
			} catch (Exception e) {
				throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
			}
		}

		/**
		 * check children
		 */
		List<String> children;
		try {
			children = client.getChildren().forPath(orig);
			for (String child : children) {
				String origChildPath = orig + "/" + child;
				copyZNode(client, origChildPath, dest + "/" + child, false);
			}
		} catch (Exception e) {
			throw new BusinessException(ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}

	private String getPath(String path) {
		if (path.equals("")) {
			return getRootPath();
		} else {
			if (path.charAt(0) == '/') {
				return getRootPath() + path;
			} else {
				return getRootPath() + "/" + path;
			}
		}
	}

	private String getRootPath() {
		return engineEnvDao.selectEngineEnvInfo().getRootPath();
	}

	private String getBakRootPath() {
		return engineEnvDao.selectEngineEnvInfo().getBakPath();
	}

	private int getLimitSize() {
		return engineEnvDao.selectEngineEnvInfo().getNodeSize() * 1024;
	}

	private String generateBakPath(String curNodePath, String usrId) {
		StringBuilder sb = new StringBuilder();
		sb.append(getBakRootPath());
		sb.append("/");
		sb.append(DateUtils.getNowDate(DateUtils.YYYYMMDDHHMMSS));
		sb.append("_");
		sb.append(usrId);
		sb.append(curNodePath);
		return sb.toString();
	}
}