package com.secudium.support;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.google.common.base.CaseFormat;
import com.secudium.error.ErrorCode;
import com.secudium.exception.BusinessException;

/**
 * Repository Class 의 공통
 */
public class RepositorySupport {

	/** Mapper Namespace */
	protected String mapperNs = this.getClass().getName() + ".";
	
	/** paging querying suffix */
	protected String countSuf = "Count";  
	protected String pageSuf = "Page";  

	/**
	 * Pageable 객체에서 MyBatis RowBounds 획득
	 */
	protected RowBounds getRowBounds(Pageable pageable) {  
		RowBounds bounds = RowBounds.DEFAULT;  
		if (null != pageable) {  
			bounds = new RowBounds((int) pageable.getOffset(), pageable.getPageSize());  
		}  
		return bounds;  
	}

	/**
	 * Sort 객체의 toString값 (Field: Direction,Field: Direction)으로 쿼리의 Order By에 전달할 수 있게 포매팅
	 * @param sort {@link Sort} 객체
	 * @return Order By에 연결할 수 있는 문자열
	 */
	protected List<String> prepareSortParameter(Sort sort) {
		if(sort == null) {
			return null;
		}
		
		List<String> sorts = new ArrayList<>();
		for (Order order : sort) {
			//myName => MY_NAME
			String property = order.getProperty();
			String columnName = CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, property);
			Direction direction = order.getDirection();
			sorts.add(columnName + " " + direction);
		}
		
		if(sorts.size() > 0) {
			return sorts;	
		} else {
			return null;
		}
	}

	/**
	 * Pageable객체의 page와 size를 제한
	 * (page>=0, size>=1, 최대size제한)
	 * @param pageable Pagination 정보 객체
	 * @return 보정된 Pageable 객체
	 */
	protected Pageable ensureValidPageable(Pageable pageable, int total) {
		if (pageable == null) {
			return null;
		}
		if (pageable instanceof PageRequest) {
			int page = pageable.getPageNumber();
			int size = pageable.getPageSize();
			Sort sort = pageable.getSort();
			//size
			if (size > 1000) {
				size = 1000; // 최대 페이지 제한
			} else if (size < 1) {
				size = 10; // 기본 페이지당 표시 갯수
			}
			//page
			int totalPage = (size == 0 || total == 0) ? 1 : (int) Math.ceil((double) total / (double) size);
			if (page <= 0) {
				page = 0; //zero-based index
			} else if (page >= totalPage) {
				page = totalPage - 1; //last page
			}
			return PageRequest.of(page, size, sort);
		} else {
			throw new BusinessException("Unknown Pageable Type. type: " + pageable.getClass().getSimpleName(), ErrorCode.INTERNAL_SERVER_ERROR);
		}
	}
}
