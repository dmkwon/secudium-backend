package com.secudium.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

public class ApplicationUtils {

    /**
     * <p>
     * Local IP 조회
     * </p>
     * Local IP를 조회하여 리턴한다.
     *
     * @return String Local IP
     */
    public static String getLocalIp() {
        String ip = "";

        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            ip = "Unknown IP";
        }

        return ip;
    }

    /**
     * <p>
     * Client IP 조회
     * </p>
     * Client IP를 조회하여 리턴한다.
     *
     * @param request HttpServletRequest
     * @return String Client IP
     */
    public static String getClientIp(HttpServletRequest request) {
        String ip = "";

        if(request == null) {
            return "Unknown IP";
        }

        ip = request.getHeader("X-Forwarded-For");

        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }

        return ip;
    }

}
