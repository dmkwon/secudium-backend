package com.secudium.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateUtils {

	private static final String YYYYMMDDTHHMMSSZ = "yyyy-MM-dd'T'hh:mm:ssZ";
	private static final String YYYYMMDD_HHMMSS = "yyyy-MM-dd hh:mm:ss";
	public static final String YYYYMMDDHHMMSS = "yyyyMMddhhmmss";

	private DateUtils() {
		throw new IllegalStateException("Utility class");
	}

	private static String[] weekName = {"", "일", "월", "화", "수", "목", "금", "토"};
	private SimpleDateFormat reportFormat = new SimpleDateFormat("yyyy-MM-dd");

	public String convertDateToReportFormat(Date inputDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(inputDate);
		return reportFormat.format(calendar.getTime()) + " ("
		+ weekName[calendar.get(Calendar.DAY_OF_WEEK)] + ") ";
	}


	public static final String yymmddDate(){
		SimpleDateFormat yymmddFormat = new SimpleDateFormat("yyMMdd");
		return yymmddFormat.format(new Date());
	}

	public static final String yyyymmddDate(){
		SimpleDateFormat yyyymmddFormat = new SimpleDateFormat("yyyyMMdd");
		return yyyymmddFormat.format(new Date());
	}

	public final String yyyymmddDate(int afterDays){
		SimpleDateFormat yymmddFormat = new SimpleDateFormat("yyMMdd");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, afterDays);
		return yymmddFormat.format(c.getTime());
	}

	public final String reportFormatDate(int afterDays){
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, afterDays);
		return reportFormat.format(c.getTime());
	}
	public final String setAfterDate(String yyyymmdd, int afterDays){
		Calendar c = Calendar.getInstance();
		int year = Integer.parseInt(yyyymmdd.substring(0,4));
		int month = Integer.parseInt(yyyymmdd.substring(4,6));
		int date = Integer.parseInt(yyyymmdd.substring(6,8));
		c.set(year, month-1, date);
		c.add(Calendar.DATE, afterDays);
		return reportFormat.format(c.getTime());
	}

	private SimpleDateFormat nowDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public final String nowDate(){
		return nowDateFormat.format(new Date());
	}
	public final String nowDateAppendTime(String time){
		return nowDateFormat.format(new Date())+" "+time;
	}

	private SimpleDateFormat nowDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public final String nowDateTime(){
		return nowDateTimeFormat.format(new Date());
	}

	private SimpleDateFormat nowDateHourFormat = new SimpleDateFormat("yyyy-MM-dd HH");
	public final String nowDateHour(){
		return nowDateHourFormat.format(new Date())+":00";
	}

	/**
	 * Time Zone(2016-01-01T15:00:00.000Z) convert To Date
	 *
	 * @param inputTime
	 * @return Date
	 */
	public static final Date convertTimeZoneToDate(String inputTime) {
		DateFormat df = new SimpleDateFormat(YYYYMMDDTHHMMSSZ);
		Date result;

		try {
			result = df.parse(inputTime.substring(0, inputTime.indexOf(':')) + ":00:00+0000");
		} catch (Exception e) {
			log.error(e.getMessage());
			result = null;
		}
		return result;
	}


	/**
	 * 날짜의 시간을 00 시로 변환
	 * @param inputDate
	 * @return
	 */
	public static final Date dateTimeSetZeroTime(Date inputDate){
		Calendar cal = Calendar.getInstance();
		cal.setTime(inputDate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	/**
	 * 날짜의 시간을 23:59:59로 변환
	 * @param inputDate
	 * @return
	 */
	public static final Date dateTimeSetEndTime(Date inputDate){
		Calendar cal = Calendar.getInstance();
		cal.setTime(inputDate);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	/**
	 * Time Zone(2016-01-01T15:00:00.000Z) convert To String(yyyy-MM-dd HH:mm:ss)
	 *
	 * @param inputTime
	 * @param formatType
	 * @return
	 */
	public static final String convertTimeZoneToString(String inputTime) {
		return convertTimeZoneToString(inputTime, null);
	}


	/**
	 * Time Zone(2016-01-01T15:00:00.000Z) convert To input formatType
	 *
	 * @param inputTime, formatType
	 * @param formatType
	 * @return
	 */
	public static final String convertTimeZoneToString(String inputTime, String formatType) {
		DateFormat df = new SimpleDateFormat(YYYYMMDDTHHMMSSZ);
		Date result;

		String returnDateStr = "";
		try {
			result = df.parse(inputTime.substring(0, inputTime.indexOf(':')) + ":00:00+0000");

			SimpleDateFormat sdf = new SimpleDateFormat(YYYYMMDD_HHMMSS);
			if (formatType != null && !"".equals(formatType)) {
				sdf = new SimpleDateFormat(formatType);
			}

			returnDateStr = sdf.format(result); // prints date in the format sdf

		} catch (Exception e) {
			log.error(e.getMessage());
			returnDateStr = inputTime;
		}
		return returnDateStr;
	}

	/**
	 * Time Zone(2016-01-01T15:00:00.000Z) convert To input formatType minute
	 *
	 * @param inputTime, formatType
	 * @param formatType
	 * @return
	 */
	public static final String convertDateTimeMinuteZoneToString(String inputTime) {
		return convertDateTimeMinuteZoneToString(inputTime, null);
	}

	public static final String convertDateTimeMinuteZoneToString(String inputTime,
			String formatType) {
		DateFormat df = new SimpleDateFormat(YYYYMMDDTHHMMSSZ);
		Date result;

		String returnDateStr = "";
		try {
			result = df.parse(inputTime.substring(0, inputTime.lastIndexOf(':')) + ":00+0000");

			SimpleDateFormat sdf = new SimpleDateFormat(YYYYMMDD_HHMMSS);
			if (formatType != null && !"".equals(formatType)) {
				sdf = new SimpleDateFormat(formatType);
			}

			returnDateStr = sdf.format(result); // prints date in the format sdf

		} catch (Exception e) {
			log.error(e.getMessage());
			returnDateStr = inputTime;
		}
		return returnDateStr;
	}

	public static final String getNowDate() {
		SimpleDateFormat transFormat = new SimpleDateFormat(YYYYMMDD_HHMMSS);
		return transFormat.format(new Date());
	}
	
	
	public static final String getNowDate(String dateFormat) {
		SimpleDateFormat transFormat = new SimpleDateFormat(dateFormat);
		return transFormat.format(new Date());
	}
	
	public static final Date convertString2Date(String date){
		try {
			SimpleDateFormat transFormat = new SimpleDateFormat(YYYYMMDD_HHMMSS);
			return transFormat.parse(date);
		} catch (ParseException e) {
			return new Date();
		}
	}
	
	/**
	 * data 를 받아 String (yyyy-MM-dd HH:mm:ss) 로 변환
	 */
	public static String setDateString(Date date) {
		SimpleDateFormat transFormat = new SimpleDateFormat(YYYYMMDD_HHMMSS);
		return transFormat.format(date);
	}
	
	public static String setNow() {
		return new SimpleDateFormat(YYYYMMDDHHMMSS).format(new Date());
	}
}
