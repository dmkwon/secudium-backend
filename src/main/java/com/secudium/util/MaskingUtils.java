package com.secudium.util;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MaskingUtils {

	private MaskingUtils() {}
	
	private static final char DEFAULT_REPLACE = '*';
	// 유선 전화 패턴
	private static final String PHONE_NUM_PATTERN = "(\\d{2,3})\\s*-?\\s*(\\d{3,4})\\s*-?\\s*(\\d{4})";
	// 무선 전화 패턴
	private static final String MOBILE_NUM_PATTERN = "(01[016789])\\s*-?\\s*(\\d{3,4})\\s*-?\\s*(\\d{4})";
	// 메일 주소 패턴
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * 문자열 마스킹
	 *
	 * @param src
	 * @return maskedValue
	 */
	public static String masking(String src) {
		if (isEmail(src)) {
			return getMaskedEmail(src);
		} else if (isMobileNum(src)) {
			return getMaskedMobileNum(src);
		} else if (isPhoneNum(src)) {
			return getMaskedPhoneNum(src);
		}
		return src;
	}

	/**
	 * 이메일 포맷 Validator
	 * 
	 * @param str
	 * @return isValidEmailFormat
	 */
	private static boolean isEmail(final String str) {
		return isValid(EMAIL_PATTERN, str);
	}

	/**
	 * 전화 번호 포맷 Validator
	 * 
	 * @param str
	 * @return isValidPhoneNumFormat
	 */
	private static boolean isPhoneNum(final String str) {
		return isValid(PHONE_NUM_PATTERN, str);
	}

	/**
	 * 휴대폰 번호 포맷 Validator
	 * 
	 * @param str
	 * @return isValidCellPhoneNumFormat
	 */
	private static boolean isMobileNum(final String str) {
		return isValid(MOBILE_NUM_PATTERN, str);
	}

	/**
	 * 문자열이 정규식에 맞는 포맷인지 체크
	 * 
	 * @param regex
	 * @param target
	 * @return isValid
	 */
	private static boolean isValid(final String regex, final String target) {
		try {
			Matcher matcher = Pattern.compile(regex).matcher(target);
			return matcher.matches();
		} catch (Exception e) {
			log.debug(e.getMessage());
			return false;
		}
	}

	/**
	 * 이메일 주소 마스킹 처리
	 * 
	 * @param email
	 * @return maskedEmailAddress
	 */
	private static String getMaskedEmail(String email) {
		/*
		 * 요구되는 메일 포맷 {userId}@domain.com
		 */
		String regex = "\\b(\\S+)+@(\\S+.\\S+)";
		Matcher matcher = Pattern.compile(regex).matcher(email);
		if (matcher.find()) {
			String id = matcher.group(1); // 마스킹 처리할 부분인 userId
			/*
			 * userId의 길이를 기준으로 세글자 초과인 경우 뒤 세자리를 마스킹 처리하고, 세글자인 경우 뒤 두글자만 마스킹, 세글자 미만인 경우
			 * 모두 마스킹 처리
			 */
			int length = id.length();
			if (length < 3) {
				char[] c = new char[length];
				Arrays.fill(c, DEFAULT_REPLACE);
				return email.replace(id, String.valueOf(c));
			} else if (length == 3) {
				return email.replaceAll("\\b(\\S+)[^@][^@]+@(\\S+)", "$1**@$2");
			} else {
				return email.replaceAll("\\b(\\S+)[^@][^@][^@]+@(\\S+)", "$1***@$2");
			}
		}
		return email;
	}

	/**
	 * 휴대폰 번호 마스킹 처리
	 * 
	 * @param phoneNum
	 * @return maskedCellPhoneNumber
	 */
	private static String getMaskedMobileNum(String mobileNum) {
		Matcher matcher = Pattern.compile(MOBILE_NUM_PATTERN).matcher(mobileNum);
		if (matcher.find()) {
			String replaceTarget = matcher.group(2);
			char[] c = new char[replaceTarget.length()];
			Arrays.fill(c, DEFAULT_REPLACE);
			return mobileNum.replace(replaceTarget, String.valueOf(c));
		}
		return mobileNum;
	}

	/**
	 * 전화 번호 마스킹 처리
	 * 
	 * @param phoneNum
	 * @return maskedPhoneNumber
	 */
	private static String getMaskedPhoneNum(String phoneNum) {
		Matcher matcher = Pattern.compile(PHONE_NUM_PATTERN).matcher(phoneNum);
		if (matcher.find()) {
			String replaceTarget = matcher.group(2);
			char[] c = new char[replaceTarget.length()];
			Arrays.fill(c, DEFAULT_REPLACE);
			return phoneNum.replace(replaceTarget, String.valueOf(c));
		}
		return phoneNum;
	}

    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param startIdx 시작위치
     * @return 마스킹 적용된 문자열
     */
    public static String masking(String src, int startIdx) {
        return masking(src, DEFAULT_REPLACE, null, startIdx, src.length());
    }

    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param startIdx 시작위치
     * @param length   길이
     * @return 마스킹 적용된 문자열
     */
	public static String masking(String src, int startIdx, int length) {
		return masking(src, DEFAULT_REPLACE, null, startIdx, length);
	}

    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param replace  대치문자
     * @param startIdx 시작위치
     * @return 마스킹 적용된 문자열
     */
	public static String masking(String src, char replace, int startIdx) {
		return masking(src, replace, null, startIdx, src.length());
	}

    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param replace  대치문자
     * @param startIdx 시작위치
     * @param length   길이
     * @return 마스킹 적용된 문자열
     */
	public static String masking(String src, char replace, int startIdx, int length) {
		return masking(src, replace, null, startIdx, length);
	}

    /**
     * 문자열 마스킹
     *
     * @param src      원본
     * @param replace  대치문자
     * @param exclude  제외문자
     * @param startIdx 시작위치
     * @param length   길이
     * @return 마스킹 적용된 문자열
     */
	public static String masking(String src, char replace, char[] exclude, int startIdx, int length) {
		StringBuilder sb = new StringBuilder(src);

		// 종료 인덱스
		int endIdx = startIdx + length;
		if (sb.length() < endIdx)
			endIdx = sb.length();

		// 치환
		for (int i = startIdx; i < endIdx; i++) {
			boolean isExclude = false;
			// 제외 문자처리
			if (exclude != null && 0 < exclude.length) {
				char currentChar = sb.charAt(i);

				for (char excludeChar : exclude) {
					if (currentChar == excludeChar)
						isExclude = true;
				}
			}

			if (!isExclude)
				sb.setCharAt(i, replace);
		}

		return sb.toString();
	}
}
