package com.secudium.util;

public class PatternUtils {

	private static final String NUMBER_ONLY = "^[0-9]*$";
	private static final String ALPHABET_ONLY = "^[a-zA-Z]*$";
	private static final String KOR_ONLY = "^[가-힣]*$";
	private static final String ALPHABET_NUM_ONLY = "^[a-zA-Z0-9]*$";
	private static final String E_MAIL_PATTERN = " ^[a-zA-Z0-9]+@[a-zA-Z0-9]+$";
	private static final String MOBILE_PATTERN = "^01(?:0|1|[6-9]) - (?:\\d{3}|\\d{4}) - \\d{4}$";
	private static final String TELEPHONE_PATTERN = "^\\d{2,3} - \\d{3,4} - \\d{4}$";
	private static final String JUMIN_ID_PATTERN = "\\d{6} \\- [1-4]\\d{6}";
	private static final String IP_PATTERN = "([0-9]{1,3}) \\. ([0-9]{1,3}) \\. ([0-9]{1,3}) \\. ([0-9]{1,3})";

	public static boolean isNumber(String str) {
		return str.matches(NUMBER_ONLY);
	}
	
	public static boolean isAlphabet(String str) {
		return str.matches(ALPHABET_ONLY);
	}
	
	public static boolean isKorean(String str) {
		return str.matches(KOR_ONLY);
	}
	
	public static boolean isAlphabetNumber(String str) {
		return str.matches(ALPHABET_NUM_ONLY);
	}
	
	public static boolean isEmail(String str) {
		return str.matches(E_MAIL_PATTERN);
	}
	
	public static boolean isMobile(String str) {
		return str.matches(MOBILE_PATTERN);
	}
	
	public static boolean isTelephone(String str) {
		return str.matches(TELEPHONE_PATTERN);
	}
	
	public static boolean isJuminId(String str) {
		return str.matches(JUMIN_ID_PATTERN);
	}
	
	public static boolean isIp(String str) {
		return str.matches(IP_PATTERN);
	}
	
}	
