package com.secudium.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.secudium.api.enginemgmt.blackcc.entity.BlackCcIp;
import com.secudium.api.enginemgmt.blackcc.entity.BlackCcUrl;
import com.secudium.api.enginemgmt.field.entity.Field;
import com.secudium.api.rulemgmt.filtermgmt.entity.FilterCndtn;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkArrange;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkCont;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkField;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkFieldFunc;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkFieldMerge;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkFieldReplace;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkHdr;
import com.secudium.api.rulemgmt.nmlzrule.entity.NmlzRuleZkTokenize;
import com.secudium.api.rulemgmt.rule.entity.Rule;
import com.secudium.common.entity.Select;

public class ZkUtils {

	private ZkUtils() {
		throw new IllegalStateException("Utility class");
	}

	public static final String TAB = "\t@\t";
	public static final String NL = "\n";

	private static final String IN = " || ";
	private static final String NOT_EQUAL = "!";

	/**
	 * ASCII(16진수) 코드값을 Text로 변환
	 * @param param
	 * @return String
	 */
	public static String getHexToText(String param) {
		String retValue = "";
		if (StringUtils.isNotEmpty(param)) {
			String[] list = param.trim().split("0x");
			if (list != null && list.length > 0) {
				for (String hex: list) {
					if (StringUtils.isNotEmpty(hex)) {
						retValue += (char) Integer.parseInt(hex, 16);
					}
				}
			}
		}
		return retValue;
	}

	/**
	 * Field entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(Field vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getFieldId()).append(separator);
		//sb.append(vo.getFieldType()).append(separator);
		sb.append(vo.getField());
		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * BlackCcIp entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param BlackCcIp
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */	
	public static final String convertZkDataFormat(BlackCcIp vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();

		if(vo.getIpDstnct().toUpperCase().equals("WHITE")) {
			sb.append(vo.getCompanyId()).append(separator);
			sb.append(ConvertUtils.longToIp(vo.getDeviceIp())).append(separator);
		}

		sb.append(ConvertUtils.longToIp(vo.getStIp())).append(separator);

		if(vo.getEdIp() != null) {
			sb.append(ConvertUtils.longToIp(vo.getEdIp())).append(separator);
		} else {
			sb.append("").append(separator);
		}

		sb.append(vo.getCountry()).append(separator);
		sb.append(vo.getDetectDate()).append(separator);
		sb.append(vo.getExpDate());
		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * BlackCcUrl entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param BlackCcUrl
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(BlackCcUrl vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getUrl()).append(separator);
		sb.append(vo.getAttkTypeString()).append(separator);
		sb.append(vo.getDetectDate()).append(separator);
		sb.append(vo.getExpDate());
		if(useNL) {
			sb.append(NL);
		}		
		return sb.toString();
	}

	//임계치룰
	public static final String convertZkDataFormat(Rule vo, String Seperator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getRuleSeq()).append(Seperator);
		sb.append(vo.getRuleNm()).append(Seperator);
		sb.append(vo.getRuleSeq()).append(Seperator);
		sb.append(vo.getThrsldGroupPeriod() != null ? vo.getThrsldGroupPeriod() : "").append(Seperator);
		sb.append(vo.getThrsldGroupCnt() != null ? vo.getThrsldGroupCnt() : "").append(Seperator);
		//ex) 19:signature,20:companyid ...
		String thrsldGroupField = vo.getThrsldGroupField();
		String [] groupFields = thrsldGroupField.split(",");
		String delimeter = "";
		for(String groupField : groupFields) {
			sb.append(delimeter).append(groupField.split(":")[0]);
			delimeter = ",";
		}
		sb.append(Seperator);
		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * 단순룰 배포 데이터
	 * @param rule
	 * @param filterCndtns 필터정보
	 * @param zkHdrs 주키퍼 헤더정보
	 * @param useNL
	 * @return
	 */
	public static final String convertZkDataFormat(Rule vo, List<FilterCndtn> filterCndtns, List<Select> zkHdrs, String Seperator, boolean useNL) {
		StringBuilder sb = new StringBuilder();

		//룰타입
		sb.append(vo.getRuleType());

		//룰일련번호
		sb.append(Seperator).append(vo.getRuleSeq());

		//룰명
		sb.append(Seperator).append(vo.getRuleNm());

		//키필드
		sb.append(Seperator);
		String delimeter = "";
		for(FilterCndtn filter : filterCndtns) {
			if(filter.getKeyFieldYn() == 'Y') {
				sb.append(delimeter).append(filter.getFieldId());
				delimeter = ",";
			}
		}

		//Order
		sb.append(Seperator);
		delimeter = "";
		for(FilterCndtn filter : filterCndtns) {
			char filterType = filter.getFieldType();
			if(filterType == 'N' || filterType == 'I') {
				sb.append(delimeter).append(filterType + ":" + filter.getFieldId());
			} else {
				sb.append(delimeter).append(filter.getFieldId());
			}
			delimeter = ",";
		}

		//필터정보
		Map<String, StringBuilder> fieldMap = new HashMap<>();
		for(Select header : zkHdrs) {
			if(header != null) {
				fieldMap.put(header.getName(), new StringBuilder());	
			}
		}

		//필터정보 파싱
		for(FilterCndtn filter : filterCndtns) {
			StringBuilder fieldValues = fieldMap.get(filter.getField());

			//필드맵에 없는 필드일경우
			if(fieldValues == null) {
				String id = filter.getFieldId() + ":" + filter.getField();
				String name = filter.getField();

				Select header = new Select(null, id, null, name);
				zkHdrs.add(header);

				fieldMap.put(name, new StringBuilder());
				fieldValues = fieldMap.get(name);
			}

			if(fieldValues.length() > 0) {
				//같은 필드가 존재할 경우 값을 ||로 연결해준다.
				fieldValues.append(IN);
			}

			if(filter.getOprtr().equals("in")) {	//in경우 value가 복수일 수 있다
				String[] cndtnValues = filter.getCndtnValue().split(",");
				StringBuilder values = new StringBuilder();

				if(filter.getFieldType() == 'S') {
					for(String value : cndtnValues) {
						if(values.length() > 0) {
							values.append(IN);
						}
						if(!Pattern.matches("\\*\\\"(.*?)\\\"\\*", value)) {
							values.append("\"").append(value).append("\"");
						} else {
							values.append(value);
						}
					}
				} else {
					for(String value : cndtnValues) {
						if(values.length() > 0) {
							values.append(IN);
						}
						values.append(value);
					}
				}
				fieldValues.append(values.toString());
			} else {
				String value = filter.getCndtnValue();
				//fieldType이 String이면서 *"value"*같은 형태가 아닌 것
				if(filter.getFieldType() == 'S' && !Pattern.matches("\\*\\\"(.*?)\\\"\\*", value)) {
					value = new StringBuilder().append("\"").append(value).append("\"").toString();
				}

				if(filter.getOprtr().equals("!=")) {
					value = NOT_EQUAL + value;
				}

				fieldValues.append(value);
			}
		}

		for(Select header : zkHdrs) {
			if(header != null) {
				sb.append(Seperator).append(fieldMap.get(header.getName()).toString());	
			}
		}

		if(useNL) {
			sb.append(NL);
		}

		return sb.toString();
	}


	/**
	 * NmlzRuleZkCont entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(NmlzRuleZkCont vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getNmlzRuleSeq()).append(separator);
		sb.append(vo.getNmlzRuleContSeq()).append(separator);
		sb.append(vo.getLogFormatCode()).append(separator);
		sb.append(vo.getHdrPrefix()).append(separator);
		sb.append(vo.getHdrPresent()).append(separator);
		sb.append(vo.getContRegexp()).append(separator);
		sb.append(vo.getContPriority()).append(separator);
		sb.append(vo.getAgentModelId()).append(separator);
		sb.append(vo.getUseYn()).append(separator);
		sb.append(vo.getContUseYn());

		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * NmlzRuleZkField entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(NmlzRuleZkField vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getNmlzRuleSeq()).append(separator);
		sb.append(vo.getNmlzRuleContSeq()).append(separator);
		sb.append(vo.getTokenize()).append(separator);
		sb.append(vo.getFieldId()).append(separator);
		sb.append(vo.getField()).append(separator);
		sb.append(vo.getFieldDefault());
		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * NmlzRuleZkFieldFunc entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(NmlzRuleZkFieldFunc vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getNmlzRuleSeq()).append(separator);
		sb.append(vo.getNmlzRuleContSeq()).append(separator);
		sb.append(vo.getFieldId()).append(separator);
		sb.append(vo.getFieldFuncCodeNm()).append(separator);
		sb.append(vo.getFieldFuncArg()).append(separator);
		sb.append(vo.getPrintFieldId());
		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * NmlzRuleZkFieldMerge entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(NmlzRuleZkFieldMerge vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getNmlzRuleSeq()).append(separator);
		sb.append(vo.getNmlzRuleContSeq()).append(separator);
		sb.append(vo.getFieldId1()).append(separator);
		sb.append(vo.getFieldId2()).append(separator);
		sb.append(vo.getPrintFieldId());
		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * NmlzRuleZkFieldReplace entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(NmlzRuleZkFieldReplace vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getNmlzRuleSeq()).append(separator);
		sb.append(vo.getNmlzRuleContSeq()).append(separator);
		sb.append(vo.getFieldId()).append(separator);
		sb.append(vo.getInValue()).append(separator);
		sb.append(vo.getPrintValue());
		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * NmlzRuleZkHdr entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(NmlzRuleZkHdr vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getNmlzRuleHdrSeq()).append(separator);
		sb.append(vo.getNmlzRuleHdrPrefix()).append(separator);
		sb.append(vo.getNmlzRuleHdrRegexp()).append(separator);
		sb.append(vo.getHdrPriority() == null ? "": vo.getHdrPriority());
		//sb.append(vo.getNmlzRuleContSeq());

		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * NmlzRuleZkTokenize entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(NmlzRuleZkTokenize vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getNmlzRuleContSeq()).append(separator);
		sb.append(vo.getContRegexp()).append(separator);
		sb.append(vo.getEventTypeCode()).append(separator);
		sb.append(vo.getAgentVendorNm()).append(separator);
		sb.append(vo.getAgentModelId() == null ? "": vo.getAgentModelId()).append(separator);
		sb.append(vo.getAgentModelType()).append(separator);
		sb.append(vo.getContPriority()).append(separator);
		sb.append(vo.getNmlzRuleHdrSeq());
		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * NmlzRuleZkTokenize entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat(NmlzRuleZkArrange vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getNmlzRuleContSeq()).append(separator);
		sb.append(vo.getFieldId() == null ? "" : vo.getFieldId()).append(separator);
		sb.append(vo.getCaptureOrder());

		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}

	/**
	 * NmlzRuleZkTokenize entity를 받아 주키퍼 데이터포맷으로 리턴
	 * @param Field
	 * @param separator String
	 * @param useNL  NL 을 붙일 것인지 여부 true / false
	 * @return String
	 */
	public static final String convertZkDataFormat2(NmlzRuleZkArrange vo, String separator, boolean useNL) {
		StringBuilder sb = new StringBuilder();
		sb.append(vo.getNmlzRuleContSeq()).append(separator);
		sb.append(vo.getSubFieldId() == null ? "" : vo.getSubFieldId()).append(separator);
		sb.append(vo.getCaptureOrder());

		if(useNL) {
			sb.append(NL);
		}
		return sb.toString();
	}
}
