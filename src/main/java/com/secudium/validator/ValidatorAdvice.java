package com.secudium.validator;

import javax.inject.Inject;

import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

@ControllerAdvice
public class ValidatorAdvice {
	@Inject
	protected LocalValidatorFactoryBean validator;
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new CollectionValidator(validator));
	}
}
