<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.secudium.api.systemoper.role.dao.RoleDao">

    <sql id="selectList_sql">
        SELECT
            ROW_NUMBER() OVER (ORDER BY COALESCE(modDate, regDate) ASC) AS no, T.*
        FROM (
            SELECT ROLE_SEQ    AS roleSeq
                  ,ROLE_ID     AS roleId
                  ,role_nm_${lang} AS roleNm
                  ,ROLE_DESC   AS roleDesc
                  ,USE_YN      AS useYn
                  ,REG_USRNO   AS regUsrNo
                  ,REG_DATE    AS regDate
                  ,MOD_USRNO   AS modUsrNo
                  ,MOD_DATE    AS modDate
                  ,TO_CHAR(MOD_DATE, 'YYYY-MM-DD') AS stringModDate
            FROM TB_ROLE
            WHERE USE_YN = 'Y'
            ORDER BY ROLE_SEQ ASC
            <if test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(search)">
                <if test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(search.roleNm)">
                    AND role_nm_${lang} like concat('%',#{search.roleNm},'%')
                </if>
                <if test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(search.sdate) and @com.secudium.mybatis.utils.Ognl@isNotEmpty(search.edate)">
                    AND REG_DATE <![CDATA[>=]]> #{search.sdate} AND REG_DATE <![CDATA[<=]]> #{search.edate} + INTERVAL 1 DAY
                </if>
            </if>
        ) T
    </sql>

    <select id="selectRolesPage" parameterType="Map" resultType="Roles">
        <include refid="base.pagingStart" />
        <include refid="selectList_sql" />
        <if test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(sort)">
            ORDER BY 
            <foreach collection="sort" item="item" index="index" separator=",">
                #{item}
            </foreach>
        </if>
        <include refid="base.pagingEnd" />
    </select>
    <select id="selectRolesCount" parameterType="Map" resultType="int">
        <include refid="base.countStart" />
        <include refid="selectList_sql" />
        <include refid="base.countEnd" />
    </select>

    <select id="selectRolesBySeq" parameterType="Long" resultType="Roles">
        SELECT ROLE_SEQ    AS roleSeq
              ,ROLE_ID     AS roleId
              ,role_nm_ko     AS roleNmKo
              ,role_nm_en     AS roleNmEn
              ,ROLE_DESC   AS roleDesc
              ,USE_YN      AS useYn
              ,REG_USRNO   AS regUsrNo
              ,REG_DATE    AS regDate
              ,MOD_USRNO   AS modUsrNo
              ,MOD_DATE    AS modDate
        FROM TB_ROLE
        WHERE ROLE_SEQ = #{roleSeq}
    </select>

    <insert id="insertRole" parameterType="Roles">
        INSERT INTO TB_ROLE
        (
             ROLE_ID
            ,role_nm_ko
            ,role_nm_en
            ,ROLE_DESC
            ,USE_YN
            ,REG_USRNO
            ,REG_DATE
            ,MOD_USRNO
            ,MOD_DATE
        )
        VALUES (
             #{roleId}
            ,#{roleNmKo}
            ,#{roleNmEn}
            ,#{roleDesc}
            ,'Y'
            ,#{regUsrNo}
            ,NOW()
            ,#{modUsrNo}
            ,NOW()
        );
        <selectKey keyProperty="roleSeq" resultType="Long" order="AFTER">
            SELECT CURRVAL('SEQ_ROLE')
        </selectKey>
    </insert>

    <update id="updateRole" parameterType="Roles">
        UPDATE TB_ROLE
        SET  ROLE_ID = #{roleId}
            ,role_nm_ko = #{roleNmKo}
            ,role_nm_en = #{roleNmEn}
            ,ROLE_DESC = #{roleDesc}
            ,MOD_USRNO = #{modUsrNo}
            ,MOD_DATE = NOW()
        WHERE ROLE_SEQ = #{roleSeq}
    </update>

    <update id="deleteRole" parameterType="Long">
        UPDATE TB_ROLE
        SET  USE_YN = 'N'
            ,MOD_USRNO = #{modUsrNo}
            ,MOD_DATE = NOW()
        WHERE ROLE_SEQ = #{roleSeq}
    </update>
    
    <delete id="deleteRoleUser" parameterType="Long">
    	DELETE
    	  FROM TB_USER_ROLE
    	 WHERE ROLE_SEQ = #{roleSeq}
    </delete>
    
    <delete id="deleteRoleMenu" parameterType="Long">
    	DELETE
    	  FROM TB_ROLE_MENU
    	 WHERE ROLE_SEQ = #{roleSeq}
    </delete>
    
    <delete id="deleteRoleProg" parameterType="Long">
    	DELETE
    	  FROM TB_ROLE_FUNC
    	 WHERE ROLE_SEQ = #{roleSeq}
    </delete>

     <update id="deleteRoles" parameterType="java.util.Map">
        UPDATE TB_ROLE
        SET  USE_YN = 'N'
            ,MOD_USRNO = #{modUsrNo}
            ,MOD_DATE = NOW()
        WHERE ROLE_SEQ IN 
        <foreach item="rolesSelected" index="index" collection="list" open="(" separator="," close=")">
            #{rolesSelected.roleSeq}
        </foreach>
     </update>

    <delete id="deleteRoleBySeq" parameterType="Long">
        DELETE FROM TB_ROLE
        WHERE  ROLE_SEQ = #{roleSeq}
    </delete>

    <sql id="selectUserList_sql">
        SELECT
            ROW_NUMBER() OVER (ORDER BY regDate DESC) AS no, T.*
        FROM (
            SELECT B.USRNO                           AS usrNo
                  ,B.USR_ID                          AS usrId
                  ,B.USR_NM                          AS usrNm
                  ,COALESCE(F.CO_NM, B.CO_NM, '')    AS coNm
                  ,B.DEPT_NM                         AS deptNm
                  ,B.DUTY_CODE                       AS dutyCode
                  ,COALESCE(D.comm_code_nm_${lang}, '')      AS dutyName
                  ,COALESCE(E.comm_code_nm_${lang}, '')      AS accTypeName
                  ,A.REG_DATE                        AS regDate
                  ,TO_CHAR(A.REG_DATE, 'YYYY-MM-DD') AS stringRegDate
            FROM  TB_USER_ROLE A
                LEFT OUTER JOIN TB_USER_MSTR B
                    ON A.USRNO = B.USRNO
                LEFT OUTER JOIN TB_ROLE C
                    ON A.ROLE_SEQ = C.ROLE_SEQ
                LEFT OUTER JOIN TB_COMM_CODE D
                    ON D.COMM_CODE = B.DUTY_CODE AND D.TOP_COMM_CODE = 'MBPO'
                LEFT OUTER JOIN TB_COMM_CODE E
                    ON E.COMM_CODE = B.ACC_TYPE_CODE AND E.TOP_COMM_CODE = 'ATCD'
                LEFT OUTER JOIN TB_CUSTM_MSTR F
                    ON F.CUSTM_SEQ = B.CUSTM_SEQ
            WHERE B.USRNO IS NOT NULL
                AND A.ROLE_SEQ = #{roleSeq}
            ORDER BY B.USRNO ASC
        ) T
    </sql>

    <select id="selectUsersByRoleSeqPage" parameterType="Map" resultType="User">
        <include refid="base.pagingStart" />
        <include refid="selectUserList_sql" />
        <if test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(sort)">
            ORDER BY 
            <foreach collection="sort" item="item" index="index" separator=",">
                #{item}
            </foreach>
        </if>
        <include refid="base.pagingEnd" />
    </select>
    <select id="selectUsersByRoleSeqCount" parameterType="Map" resultType="int">
        <include refid="base.countStart" />
        <include refid="selectUserList_sql" />
        <include refid="base.countEnd" />
    </select>

    <insert id="insertUserRole" parameterType="UserRole">
        INSERT INTO TB_USER_ROLE
        (
             USRNO
            ,ROLE_SEQ
            ,REG_USRNO
            ,REG_DATE
        )
        VALUES (
             #{usrNo}
            ,#{roleSeq}
            ,#{regUsrNo}
            ,NOW()
        ) ON CONFLICT ON CONSTRAINT PK_USER_ROLE
        DO UPDATE SET REG_USRNO = #{regUsrNo}, REG_DATE = NOW()
    </insert>

    <delete id="deleteUserRole" parameterType="Long">
        DELETE
        FROM TB_USER_ROLE
        WHERE USRNO = #{usrNo}
            AND ROLE_SEQ = #{roleSeq}
    </delete>

    <delete id="deleteUserRoleBySeq" parameterType="Long">
        DELETE
        FROM TB_USER_ROLE
        WHERE USRNO = #{seq}
    </delete>

    <sql id="selectMenuList_sql">
        WITH TB_ROLE_MENU(MENU_SEQ) AS (
            SELECT B.MENU_SEQ
            FROM TB_ROLE A, TB_ROLE_MENU B
            WHERE A.ROLE_SEQ = #{roleSeq}
                AND A.ROLE_SEQ = B.ROLE_SEQ
                AND A.USE_YN = 'Y'
        )
        SELECT
            ROW_NUMBER() OVER (ORDER BY COALESCE(modDate, regDate) DESC) AS no, T.*
        FROM (
            SELECT T.MENU_SEQ         AS menuSeq
                  ,T.MENU_NM_${lang}  AS menuNm
                  ,T.MENU_DESC        AS menuDesc
                  ,T.MENU_URL         AS menuUrl
                  ,T.MENU_LEVEL       AS menuLevel
                  ,T.MENU_SORT_ORDER  AS menuSortOrder
                  ,T.TOP_MENU_SEQ     AS topMenuSeq
                  ,T.USE_YN           AS useYn
                  ,T.REG_USRNO        AS regUsrNo
                  ,T.REG_DATE         AS regDate
                  ,T.MOD_USRNO        AS modUsrNo
                  ,T.MOD_DATE         AS modDate
                  ,CASE WHEN T.exist IS NULL THEN 'N' ELSE 'Y' END AS checked
            FROM (
                SELECT A.*
                      ,(SELECT B.MENU_SEQ FROM TB_ROLE_MENU B WHERE A.MENU_SEQ = B.MENU_SEQ) AS EXIST
                FROM TB_MENU A
                WHERE A.USE_YN = 'Y'
                ORDER BY A.MENU_SEQ ASC
            ) T
        ) T
    </sql>

    <select id="selectMenusBySeqPage" parameterType="Map" resultType="MenuInfo">
        <include refid="base.pagingStart" />
        <include refid="selectMenuList_sql" />
        <if test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(sort)">
            ORDER BY 
            <foreach collection="sort" item="item" index="index" separator=",">
                #{item}
            </foreach>
        </if>
        <include refid="base.pagingEnd" />
    </select>
    <select id="selectMenusBySeqCount" parameterType="Map" resultType="int">
        <include refid="base.countStart" />
        <include refid="selectMenuList_sql" />
        <include refid="base.countEnd" />
    </select>

    <insert id="insertMenuRole" parameterType="MenuRole">
        INSERT INTO TB_ROLE_MENU
        (
             ROLE_SEQ
            ,MENU_SEQ
            ,REG_USRNO
            ,REG_DATE
        ) VALUES (
             #{roleSeq}
            ,#{menuSeq}
            ,#{regUsrNo}
            ,NOW()
        );
    </insert>

    <delete id="deleteMenuRoleBySeq" parameterType="Long">
        DELETE
        FROM TB_ROLE_MENU
        WHERE ROLE_SEQ = #{roleSeq}
    </delete>

    <delete id="deleteMenuRoleByMenuSeq" parameterType="Long">
        DELETE
        FROM TB_ROLE_MENU
        WHERE MENU_SEQ = #{menuSeq}
    </delete>

    <sql id="selectProgramList_sql">
        WITH ROLE_FUNC(FUNC_SEQ) AS (
            SELECT B.FUNC_SEQ
            FROM TB_ROLE A, TB_ROLE_FUNC B
            WHERE A.ROLE_SEQ = #{roleSeq}
                AND A.ROLE_SEQ = B.ROLE_SEQ
                AND A.USE_YN = 'Y'
        )
        SELECT
            ROW_NUMBER() OVER (ORDER BY COALESCE(modDate, regDate) DESC) AS no, T.*
        FROM (
            SELECT T.FUNC_SEQ                    AS funcSeq
                  ,T.FUNC_TYPE_CODE              AS funcTypeCode
                  ,COALESCE(C.comm_code_nm_${lang}, '')  AS funcTypeName
                  ,T.FUNC_NM_${lang}             AS funcNm
                  ,T.FUNC_DESC                   AS funcDesc
                  ,T.FUNC_URL                    AS funcUrl
                  ,T.HTTP_TRNS_TYPE              AS httpTrnsType
                  ,T.USE_YN                      AS useYn
                  ,T.REG_USRNO                   AS regUsrNo
                  ,T.REG_DATE                    AS regDate
                  ,T.MOD_USRNO                   AS modUsrNo
                  ,T.MOD_DATE                    AS modDate
                  ,(CASE WHEN T.exist IS NULL THEN 'N' ELSE 'Y' END) AS checked
            FROM (
                SELECT A.*
                      ,(SELECT B.FUNC_SEQ FROM ROLE_FUNC B WHERE A.FUNC_SEQ = B.FUNC_SEQ) AS exist
                FROM TB_FUNC A
                WHERE A.USE_YN = 'Y'
                ORDER BY A.FUNC_SEQ ASC
            ) T
            LEFT OUTER JOIN TB_COMM_CODE C
                ON C.COMM_CODE = T.FUNC_TYPE_CODE AND C.TOP_COMM_CODE = 'FUNC_TYPE'
        ) T
    </sql>

    <select id="selectProgramsBySeqPage" parameterType="Map" resultType="Programs">
        <include refid="base.pagingStart" />
        <include refid="selectProgramList_sql" />
           <if test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(sort)">
            ORDER BY 
            <foreach collection="sort" item="item" index="index" separator=",">
                #{item}
            </foreach>
        </if>
        <include refid="base.pagingEnd" />
    </select>
    <select id="selectProgramsBySeqCount" parameterType="Map" resultType="int">
        <include refid="base.countStart" />
        <include refid="selectProgramList_sql" />
        <include refid="base.countEnd" />
    </select>


    <insert id="insertProgRoleBySeq" parameterType="ProgramRole">
        INSERT INTO TB_ROLE_FUNC
        (
              ROLE_SEQ
             ,FUNC_SEQ
             ,REG_USRNO
             ,REG_DATE
        ) VALUES (
              #{roleSeq}
             ,#{funcSeq}
             ,#{regUsrNo}
             ,NOW()
        )
    </insert>
    
    <delete id="deleteProgRoleBySeq" parameterType="Long">
        DELETE
        FROM TB_ROLE_FUNC
        WHERE ROLE_SEQ = #{roleSeq}
    </delete>

    <delete id="deleteProgRoleByProgSeq" parameterType="Long">
        DELETE
        FROM TB_ROLE_FUNC
        WHERE FUNC_SEQ = #{funcSeq}
    </delete>


</mapper>