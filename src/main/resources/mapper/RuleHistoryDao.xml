<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper
	namespace="com.secudium.api.rulemgmt.rule.dao.RuleHistoryDao">

	<select id="getRuleHistsByRuleSeq" parameterType="Map" resultType="Rule">
		SELECT rule_hist_seq
		, rule_seq
		, rule_nm
		, get_codenm('RULE_TYPE', rule_type, '${lang}') AS rule_type
		, TO_CHAR(rule_commit_str_dt, 'YYYY-MM-DD') as rule_commit_str_dt
		, TO_CHAR(rule_commit_end_dt, 'YYYY-MM-DD') as rule_commit_end_dt
		, rule_commit_str_time
		, rule_commit_end_time
		, rule_commit_week
		, thrsld_group_field
		, thrsld_group_period
		, thrsld_group_cnt
		, thrsld_dstnct_field
		, thrsld_dstnct_cnt
		, thrsld_oprtr_period
		, thrsld_oprtr
		, detect_cndtn
		, drools_code
		, relshp_rule
		, cntrl_targt
		, custm_nm
		, rule_desc
		, commit_yn
		, rule_chg_reason
		, reg_usrno
		, get_usrnm(reg_usrno) as reg_usrnm
		, reg_date
		, CASE WHEN rule_hist_seq = (SELECT COALESCE(max(rule_hist_seq), 0)
		FROM tb_rule_hist WHERE rule_seq = #{ruleSeq})
		THEN 'Y' ELSE 'N' END AS is_latest
		FROM tb_rule_hist
		WHERE rule_seq =
		#{ruleSeq}
		ORDER BY reg_date DESC
	</select>

	<select id="getRuleHistByRuleHistSeq" parameterType="Map" resultType="RuleSelectDto">
		SELECT rule_hist_seq
		, rule_seq
		, rule_nm
		, get_codenm('RULE_TYPE', rule_type, '${lang}') AS rule_type_string
		, rule_type
		, TO_CHAR(rule_commit_str_dt, 'YYYY-MM-DD') as rule_commit_str_dt
		, TO_CHAR(rule_commit_end_dt, 'YYYY-MM-DD') as rule_commit_end_dt
		, rule_commit_str_time
		, rule_commit_end_time
		, rule_commit_week
		, thrsld_group_field
		, thrsld_group_period
		, thrsld_group_cnt
		, thrsld_dstnct_field
		, thrsld_dstnct_cnt
		, thrsld_oprtr_period
		, get_codenm('THRSLD_OPRTR', thrsld_oprtr, '${lang}') as thrsld_oprtr_string
		, thrsld_oprtr
		, detect_cndtn
		, drools_code
		, relshp_rule
		, cntrl_targt
		, custm_nm
		, rule_desc
		, attack_code
		, get_codenm('ATTK', attack_code, '${lang}') as attack_code_string
		, severity_code
		, get_codenm('SEVE', severity_code, '${lang}') as severity_code_string
		, rule_group_code
		, get_codenm('RRGC', rule_group_code, '${lang}') as rule_group_code_string
		, auto_report_yn
		, comment
		, take_action_description
		, take_b_action_description
		, commit_yn
		, rule_chg_reason
		, reg_usrno
		, get_usrnm(reg_usrno) as reg_usrnm
		, reg_date
		FROM tb_rule_hist
		WHERE rule_hist_seq = #{ruleHistSeq}
	</select>

	<insert id="insertRuleHistory" parameterType="Rule">
		INSERT INTO tb_rule_hist(
		rule_seq
		, rule_nm
		, rule_type
		<if
			test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(ruleCommitStrDt)">
			, rule_commit_str_dt
		</if>
		<if
			test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(ruleCommitEndDt)">
			, rule_commit_end_dt
		</if>
		, rule_commit_str_time
		, rule_commit_end_time
		, rule_commit_week
		, thrsld_group_field
		, thrsld_group_period
		, thrsld_group_cnt
		, thrsld_dstnct_field
		, thrsld_dstnct_cnt
		, thrsld_oprtr_period
		, thrsld_oprtr
		, drools_code
		, relshp_rule
		, cntrl_targt
		, custm_nm
		, rule_desc
		, rule_chg_reason
		, commit_yn
		, reg_usrno
		, reg_date
		, attack_code
		, rule_group_code
		, severity_code
		, auto_report_yn
		, comment
		, take_action_description
		, take_b_action_description
		) VALUES (
		#{ruleSeq}
		, #{ruleNm}
		, #{ruleType}
		<if
			test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(ruleCommitStrDt)">
			, TO_DATE(#{ruleCommitStrDt}, 'YYYY-MM-DD')
		</if>
		<if
			test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(ruleCommitEndDt)">
			, TO_DATE(#{ruleCommitEndDt}, 'YYYY-MM-DD')
		</if>
		, #{ruleCommitStrTime}
		, #{ruleCommitEndTime}
		, #{ruleCommitWeek}
		, #{thrsldGroupField}
		, #{thrsldGroupPeriod}
		, #{thrsldGroupCnt}
		, #{thrsldDstnctField}
		, #{thrsldDstnctCnt}
		, #{thrsldOprtrPeriod}
		, #{thrsldOprtr}
		, #{droolsCode}
		, #{relshpRule}
		, #{cntrlTargt}
		, #{custmNm}
		, #{ruleDesc}
		, #{ruleChgReason}
		, #{commitYn}
		, #{regUsrNo}
		, NOW()
		, #{attackCode}
		, #{ruleGroupCode}
		, #{severityCode}
		, #{autoReportYn}
		, #{comment}
		, #{takeActionDescription}
		, #{takeBActionDescription}
		)
		<selectKey keyProperty="ruleHistSeq" resultType="long"
			order="AFTER">
			select CURRVAL('seq_rule_chg_hist')
		</selectKey>
	</insert>

	<update id="rollbackHistoryByRuleHistSeq" parameterType="Rule">
		UPDATE
		tb_rule
		SET (
		rule_nm
		, rule_type
		, rule_commit_str_dt
		, rule_commit_end_dt
		, rule_commit_str_time
		, rule_commit_end_time
		, rule_commit_week
		, thrsld_group_field
		, thrsld_group_period
		, thrsld_group_cnt
		, thrsld_dstnct_field
		, thrsld_dstnct_cnt
		, thrsld_oprtr_period
		, thrsld_oprtr
		, detect_cndtn
		, drools_code
		, relshp_rule
		, cntrl_targt
		, custm_nm
		, rule_desc
		, commit_yn
		, mod_usrno
		, mod_date
		, attack_code
		, severity_code
		, rule_group_code
		, auto_report_yn
		, comment
		, take_action_description
		, take_b_action_description
		) = (
		SELECT rule_nm
		, rule_type
		, rule_commit_str_dt
		, rule_commit_end_dt
		, rule_commit_str_time
		, rule_commit_end_time
		, rule_commit_week
		, thrsld_group_field
		, thrsld_group_period
		, thrsld_group_cnt
		, thrsld_dstnct_field
		, thrsld_dstnct_cnt
		, thrsld_oprtr_period
		, thrsld_oprtr
		, detect_cndtn
		, drools_code
		, relshp_rule
		, cntrl_targt
		, custm_nm
		, rule_desc
		, commit_yn
		, #{modUsrNo}
		, NOW()
		, attack_code
		, severity_code
		, rule_group_code
		, auto_report_yn
		, comment
		, take_action_description
		, take_b_action_description
		FROM tb_rule_hist
		WHERE rule_hist_seq = #{ruleHistSeq}
		) WHERE rule_seq = #{ruleSeq}
	</update>

	<insert id="insertRuleHistoryByRuleSeq"
		parameterType="java.util.Map">
		INSERT INTO tb_rule_hist(
		rule_seq
		, rule_nm
		, rule_type
		, rule_commit_str_dt
		, rule_commit_end_dt
		, rule_commit_str_time
		, rule_commit_end_time
		, rule_commit_week
		, thrsld_group_field
		, thrsld_group_period
		, thrsld_group_cnt
		, thrsld_dstnct_field
		, thrsld_dstnct_cnt
		, thrsld_oprtr_period
		, thrsld_oprtr
		, detect_cndtn
		, drools_code
		, relshp_rule
		, cntrl_targt
		, custm_nm
		, rule_desc
		, rule_chg_reason
		, commit_yn
		, reg_usrno
		, reg_date
		, attack_code
		, severity_code
		, rule_group_code
		, auto_report_yn
		, comment
		, take_action_description
		, take_b_action_description
		)
		SELECT rule_seq
		, rule_nm
		, rule_type
		, rule_commit_str_dt
		, rule_commit_end_dt
		, rule_commit_str_time
		, rule_commit_end_time
		, rule_commit_week
		, thrsld_group_field
		, thrsld_group_period
		, thrsld_group_cnt
		, thrsld_dstnct_field
		, thrsld_dstnct_cnt
		, thrsld_oprtr_period
		, thrsld_oprtr
		, detect_cndtn
		, drools_code
		, relshp_rule
		, cntrl_targt
		, custm_nm
		, rule_desc
		, #{ruleChgReason}
		, commit_yn
		, #{regUsrNo}
		, NOW()
		, attack_code
		, severity_code
		, rule_group_code
		, auto_report_yn
		, comment
		, take_action_description
		, take_b_action_description
		FROM tb_rule
		WHERE rule_seq = #{ruleSeq}
		<selectKey keyProperty="ruleHistSeq" resultType="long"
			order="AFTER">
			select CURRVAL('seq_rule_chg_hist')
		</selectKey>
	</insert>

	<select id="getLatestRuleHistory" parameterType="Map" resultType="RuleSelectDto">
		SELECT
		rule_hist_seq
		, rule_seq
		, rule_nm
		, get_codenm('RULE_TYPE', rule_type, '${lang}') as rule_type_string
		, rule_type
		, TO_CHAR(rule_commit_str_dt, 'YYYY-MM-DD') as rule_commit_str_dt
		, TO_CHAR(rule_commit_end_dt, 'YYYY-MM-DD') as rule_commit_end_dt
		, rule_commit_str_time
		, rule_commit_end_time
		, rule_commit_week
		, thrsld_group_field
		, thrsld_group_period
		, thrsld_group_cnt
		, thrsld_dstnct_field
		, thrsld_dstnct_cnt
		, thrsld_oprtr_period
		, get_codenm('THRSLD_OPRTR', thrsld_oprtr, '${lang}') as thrsld_oprtr_string
		, thrsld_oprtr
		, detect_cndtn
		, drools_code
		, relshp_rule
		, cntrl_targt
		, custm_nm
		, rule_desc
		, commit_yn
		, rule_chg_reason
		, reg_usrno
		, reg_date
		, attack_code
		, get_codenm('ATTK', attack_code, '${lang}') as attack_code_string
		, severity_code
		, get_codenm('SEVE', severity_code, '${lang}') as severity_code_string
		, rule_group_code
		, get_codenm('RRGC', rule_group_code, '${lang}') as rule_group_code_string
		, auto_report_yn
		, comment
		, take_action_description
		, take_b_action_description
		FROM tb_rule_hist
		WHERE rule_seq = #{ruleSeq}
		AND rule_hist_seq = (SELECT rule_hist_seq FROM tb_rule_hist WHERE
		rule_seq = #{ruleSeq} ORDER BY reg_date DESC LIMIT 1)
		ORDER BY reg_date
		DESC
	</select>

	<update id="updateDetectCndtn" parameterType="java.util.Map">
		update tb_rule
		set
		detect_cndtn = #{detectCndtn}
		where rule_seq = #{ruleSeq}
	</update>

</mapper>