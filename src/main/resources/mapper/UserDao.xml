<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.secudium.api.systemoper.user.dao.UserDao">

    <sql id="selectList_sql">
        SELECT
            ROW_NUMBER() OVER (ORDER BY usrNo DESC) AS no, T.*
        FROM (
            SELECT A.USRNO                            AS usrNo
                  ,A.ACC_TYPE_CODE                    AS accTypeCode
                  ,COALESCE(B.comm_code_nm_${lang}, '')       AS accTypeName
                  ,A.USR_ID                           AS usrId
                  ,A.USR_NM                           AS usrNm
                  ,A.USR_PWD                          AS usrPwd
                  ,COALESCE(E.CO_NM, A.CO_NM, '')     AS coNm
                  ,A.DEPT_NM                          AS deptNm
                  ,A.DUTY_CODE                        AS dutyCode
                  ,COALESCE(C.comm_code_nm_${lang}, '')       AS dutyName
                  ,A.USR_EMAIL                        AS usrEmail
                  ,A.USR_MOBILE_NO                    AS usrMobileNo
                  ,A.CUSTM_SEQ                        AS custmSeq
                  ,A.ACC_LOCK_YN                      AS accLockYn
                  ,A.USE_YN                           AS useYn
                  ,A.REG_USRNO                        AS regUsrNo
                  ,A.REG_DATE                         AS regDate
                  ,A.MOD_USRNO                        AS modUsrNo
                  ,COALESCE(D.USR_NM, '')             AS modUsrNm
                  ,A.MOD_DATE                         AS modDate
                  ,TO_CHAR(A.MOD_DATE, 'YYYY-MM-DD')  AS stringModDate
            FROM TB_USER_MSTR A
                LEFT OUTER JOIN TB_COMM_CODE B
                    ON B.COMM_CODE = A.ACC_TYPE_CODE AND B.TOP_COMM_CODE = 'ATCD'
                LEFT OUTER JOIN TB_COMM_CODE C
                    ON C.COMM_CODE = A.DUTY_CODE AND C.TOP_COMM_CODE = 'MBPO'
                LEFT OUTER JOIN TB_USER_MSTR D
                    ON D.USRNO = A.MOD_USRNO
                LEFT OUTER JOIN TB_CUSTM_MSTR E
                    ON E.CUSTM_SEQ = A.CUSTM_SEQ
            WHERE A.USE_YN = 'Y'
        ) T
    </sql>

    <select id="selectUsersPage" parameterType="Map" resultType="User">
        <include refid="base.pagingStart" />
        <include refid="selectList_sql" />
        <if test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(sort)">
            ORDER BY 
            <foreach collection="sort" item="item" index="index" separator=",">
                #{item}
            </foreach>
        </if>
        <include refid="base.pagingEnd" />
    </select>
    <select id="selectUsersCount" parameterType="Map" resultType="int">
        <include refid="base.countStart" />
        <include refid="selectList_sql" />
        <include refid="base.countEnd" />
    </select>

    <select id="selectUserBySeq" parameterType="Map" resultType="UserDto">
        SELECT A.USRNO                            AS usrNo
              ,A.ACC_TYPE_CODE                    AS accTypeCode
              ,COALESCE(B.comm_code_nm_${lang}, '')       AS accTypeName
              ,A.USR_ID                           AS usrId
              ,A.USR_NM                           AS usrNm
              ,COALESCE(E.CO_NM, A.CO_NM, '')     AS coNm
              ,A.DEPT_NM                          AS deptNm
              ,A.DUTY_CODE                        AS dutyCode
              ,COALESCE(C.comm_code_nm_${lang}, '')       AS dutyName
              ,A.USR_EMAIL                        AS usrEmail
              ,A.USR_MOBILE_NO                    AS usrMobileNo
              ,A.CUSTM_SEQ                        AS custmSeq
              ,A.ACC_LOCK_YN                      AS accLockYn
              ,A.USE_YN                           AS useYn
              ,A.REG_USRNO                        AS regUsrNo
              ,A.REG_DATE                         AS regDate
              ,A.MOD_USRNO                        AS modUsrNo
              ,COALESCE(D.USR_NM, '')             AS modUsrNm
              ,A.MOD_DATE                         AS modDate
        FROM TB_USER_MSTR A
            LEFT OUTER JOIN TB_COMM_CODE B
                ON B.COMM_CODE = A.ACC_TYPE_CODE AND B.TOP_COMM_CODE = 'ATCD'
            LEFT OUTER JOIN TB_COMM_CODE C
                ON C.COMM_CODE = A.DUTY_CODE AND C.TOP_COMM_CODE = 'MBPO'
            LEFT OUTER JOIN TB_USER_MSTR D
                ON D.USRNO = A.MOD_USRNO
            LEFT OUTER JOIN TB_CUSTM_MSTR E
                ON E.CUSTM_SEQ = A.CUSTM_SEQ
        WHERE A.USRNO = #{seq}
        AND A.USE_YN = 'Y'
        ORDER BY A.REG_DATE DESC
    </select>

    <select id="selectUsersByUsername" parameterType="String" resultType="User">
        SELECT A.USRNO                       AS usrNo
              ,A.USR_ID                      AS usrId
              ,A.USR_NM                      AS usrNm
              ,A.USR_PWD                     AS usrPwd
              ,A.CO_NM                       AS coNm
              ,A.DEPT_NM                     AS deptNm
              ,A.USR_EMAIL                   AS usrEmail
              ,A.USR_MOBILE_NO               AS usrMobileNo
              ,A.CUSTM_SEQ                   AS custmSeq
              ,A.USE_YN                      AS useYn
              ,A.REG_USRNO                   AS regUsrNo
              ,A.REG_DATE                    AS regDate
              ,A.MOD_USRNO                   AS modUsrNo
              ,A.MOD_DATE                    AS modDate
        FROM TB_USER_MSTR A
        WHERE A.USR_ID = #{usrId}
        AND A.USE_YN = 'Y'
    </select>

    <select id="selectRolesByUsername" parameterType="String" resultType="Role">
        SELECT tr.ROLE_ID AS name
        FROM TB_USER_MSTR tu
            INNER JOIN TB_USER_ROLE tur
                ON tur.USRNO = tu.USRNO
            INNER JOIN TB_ROLE tr
                ON tur.ROLE_SEQ = tr.ROLE_SEQ
        WHERE tu.USR_ID = #{usrId}
    </select>

    <sql id="selectGroupList_sql">
        SELECT 
            CUSTM_GROUP_SEQ AS custmGroupSeq,
        	CUSTM_GROUP_ID AS custmGroupId,
        	CUSTM_GROUP_NM AS custmGroupNm,
        	CHANNEL_TYPE_CODE AS channelTypeCode,
        	CHANNEL_TYPE_CODE AS channelTypeCode,
        	DEACTIVE_REASON AS deactiveReason,
        	USE_YN AS useYn,
        	REG_USRNO AS regUsrNo,
        	REG_DATE AS regDate,
        	MOD_USRNO AS modUsrNo,
        	MOD_DATE AS modDate
        FROM TB_CUSTM_GROUP
    </sql>

    <select id="selectCustmGroupPage" parameterType="Map" resultType="CustmCoList">
        <include refid="base.pagingStart" />
        <include refid="selectGroupList_sql" />
        <if test="@com.secudium.mybatis.utils.Ognl@isNotEmpty(sort)">
            ORDER BY 
            <foreach collection="sort" item="item" index="index" separator=",">
                #{item}
            </foreach>
        </if>
        <include refid="base.pagingEnd" />
    </select>
    <select id="selectCustmGroupCount" parameterType="Map" resultType="int">
        <include refid="base.countStart" />
        <include refid="selectGroupList_sql" />
        <include refid="base.countEnd" />
    </select>

    <select id="selectCoNmByCustmGroupSeq" parameterType="Long" resultType="CustomCoListDto">
        SELECT 
        	TCM.CUSTM_SEQ AS custmSeq,
        	TCM.CUSTM_ID AS custmId,
        	TCM.CUSTM_GROUP_ID AS custmGroupId,
        	TCG.CUSTM_GROUP_NM AS custmGroupNm,
        	TCG.CHANNEL_TYPE_CODE AS channelTypeCode,
        	TCM.CO_NM AS coNm,
        	TCM.COMPANY_TYPE_CODE AS companyTypeCode,
        	TCM.COMPANY_GROUP_TYPE_CODE AS companyGroupTypeCode,
        	TCM.BIZNO AS bizNo,
        	TCM.ACTIVE_YN AS activeYn,
        	TCM.USE_YN AS useYn,
        	TCM.REG_USRNO AS regUsrNo,
        	TCM.REG_DATE AS regDate,
        	TCM.MOD_USRNO AS modUsrNo,
        	TCM.MOD_DATE AS modDate
        FROM TB_CUSTM_MSTR TCM
            INNER JOIN TB_CUSTM_GROUP TCG
                ON TCG.CUSTM_GROUP_SEQ = TCM.CUSTM_GROUP_SEQ
        WHERE TCM.CUSTM_GROUP_SEQ = #{seq}
    </select>

    <insert id="insertUser" parameterType="User">
         INSERT INTO
         TB_USER_MSTR
         (
              ACC_TYPE_CODE
             ,USR_ID
             ,USR_NM
             ,USR_PWD
             ,CO_NM
             ,DEPT_NM
             ,DUTY_CODE
             ,USR_EMAIL
             ,USR_MOBILE_NO
             ,CUSTM_SEQ
             ,REG_USRNO
             ,REG_DATE
             ,MOD_USRNO
             ,MOD_DATE
         )
         VALUES
         (
              #{accTypeCode}
             ,#{usrId}
             ,#{usrNm}
             ,#{usrPwd}
             ,#{coNm}
             ,#{deptNm}
             ,#{dutyCode}
             ,#{usrEmail}
             ,#{usrMobileNo}
             ,#{custmSeq}
             ,#{regUsrNo}
             ,NOW()
             ,#{modUsrNo}
             ,NOW()
         )
    </insert>

    <update id="updateUser" parameterType="UserUpdateDto">
        UPDATE TB_USER_MSTR
        SET  ACC_TYPE_CODE = #{accTypeCode}
            ,USR_NM = #{usrNm}
            ,CO_NM = #{coNm}
            ,DEPT_NM = #{deptNm}
            ,DUTY_CODE = #{dutyCode}
            ,USR_EMAIL = #{usrEmail}
            ,USR_MOBILE_NO = #{usrMobileNo}
            ,CUSTM_SEQ = #{custmSeq}
            ,MOD_USRNO = #{modUsrNo}
            ,MOD_DATE = NOW()
        WHERE USRNO = #{usrNo}
    </update>

    <update id="deleteUser" parameterType="java.util.Map">
        UPDATE TB_USER_MSTR
        SET  USE_YN ='N'
            ,MOD_USRNO = #{modUsrNo}
            ,MOD_DATE = NOW()
        WHERE USRNO = #{seq}
    </update>

    <update id="deleteUsers" parameterType="java.util.Map">
        UPDATE TB_USER_MSTR
        SET  USE_YN ='N'
            ,MOD_USRNO = #{modUsrNo}
            ,MOD_DATE = NOW()
        WHERE USRNO IN
        <foreach item="usersSelected" index="index" collection="list" open="(" separator="," close=")">
            #{usersSelected.usrNo}
          </foreach>
    </update>

    <delete id="deleteUserBySeq" parameterType="Long">
         DELETE
         FROM TB_USER_MSTR
         WHERE USRNO = #{usrNo}
    </delete>

    <update id="updateChangePwd" parameterType="User">
        UPDATE TB_USER_MSTR
        SET  USR_PWD = #{usrPwd}
        	,ACC_LOCK_YN = 'N'
        	,LOGIN_FAIL_CNT = 0
            ,MOD_USRNO = #{modUsrNo}
            ,MOD_DATE = NOW()
            ,LOGIN_FAIL_DATE = null
        WHERE USRNO = #{seq}
        AND USE_YN = 'Y'
     </update>

    <select id="isDuplicate" parameterType="String" resultType="Int">
        SELECT COUNT(USR_ID) FROM TB_USER_MSTR WHERE USR_ID = #{usrId} AND USE_YN = 'Y'
    </select>

    <select id="isAccLocked" parameterType="String" resultType="Int">
        SELECT COUNT(ACC_LOCK_YN) FROM TB_USER_MSTR WHERE USR_ID = #{usrId} AND ACC_LOCK_YN = 'N' AND USE_YN = 'Y'
    </select>

    <select id="getLoginFailCount" parameterType="String" resultType="Int">
        SELECT LOGIN_FAIL_CNT FROM TB_USER_MSTR WHERE USR_ID = #{usrId} AND USE_YN = 'Y'
    </select>

    <update id="updateLoginCntReset" parameterType="String">
        UPDATE TB_USER_MSTR
        SET  login_fail_cnt = 0
        WHERE USR_ID = #{usrId}
        AND USE_YN = 'Y'
    </update>

    <update id="loginFail" parameterType="String">
        UPDATE TB_USER_MSTR
        SET  LOGIN_FAIL_CNT = (SELECT LOGIN_FAIL_CNT + 1 FROM TB_USER_MSTR WHERE USR_ID = #{usrId})
           , LOGIN_FAIL_DATE = NOW()
           , ACC_LOCK_YN = CASE WHEN (SELECT LOGIN_FAIL_CNT + 1 FROM TB_USER_MSTR WHERE USR_ID = #{usrId}) >= 5 THEN 'Y' ELSE 'N' END
        WHERE USR_ID = #{usrId}
        AND USE_YN = 'Y'
     </update>

</mapper>