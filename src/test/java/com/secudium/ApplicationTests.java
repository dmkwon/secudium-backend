package com.secudium;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.JVM)  // top down order
public class ApplicationTests {

	@Test
	public void testPasswordEncoder() {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		testPasswordMatch(encoder, "Secudium!@#", "ad5747ced1819018552207c6013cb911a50c24fbe1beca64b6d0e0de8d9ad10d0ddda87d953d4d3b");
		testPasswordMatch(encoder, "isap_admin", "ad5747ced1819018552207c6013cb911a50c24fbe1beca64b6d0e0de8d9ad10d0ddda87d953d4d3b");
	}
	private void testPasswordMatch(BCryptPasswordEncoder encoder, CharSequence password, String expected) {
		String hash = encoder.encode(password);
		System.out.println("password: " + password);
		System.out.println("hash	  : " + hash);
		System.out.println("expected: " + expected);
		System.out.println("matched : " + encoder.matches(password, expected));
	}
}
